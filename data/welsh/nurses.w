< ?xml version = "1.0" encoding="UTF-8"?>
< corpus topic="nurses-midwives-health-visitors" language="Welsh">
< file elementid="20032527w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Rheoliadau Asiantaethau Nyrsys ( Cymru ) 2003< /pagetitle>
< doc_class>Statutory Instrument - Wales< /doc_class>
< year>2003< /year>
< number>2527< /number>
< legislation_code>30102< /legislation_code>
< legislation_type>Statutory Instrument - Wales< /legislation_type>
< subject>NYRSYS, BYDWRAGEDD AC YMWELWYR IECHYD, CYMRU< /subject>
< laid_date>_EMPTY< /laid_date>
< made_date>20031001< /made_date>
< coming_into_force>20031002< /coming_into_force>
< isbn>0110907876< /isbn>
< title>Rheoliadau Asiantaethau Nyrsys (Cymru) 2003< /title>
< signatory>D. Elis-Thomas< /signatory>
< signatory_title>Llywydd y Cynulliad Cenedlaethol< /signatory_title>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20032527w-p1">
< sentence type="heading" elementid="20032527w-s1">
Offerynnau Statudol 2003 Rhif 2527
< roundbracket elementid="20032527w-rb1">
(  Cy.242 )
< /roundbracket>
< /sentence>
< /p>
< p elementid="20032527w-p2">
< sentence type="heading" elementid="20032527w-s2">
< strong elementid="20032527w-st1">
Rheoliadau Asiantaethau Nyrsys
< roundbracket elementid="20032527w-rb2">
(  Cymru )
< /roundbracket>
2003
< /strong>
< /sentence>
< /p>
< p elementid="20032527w-p3">
< sentence type="heading" elementid="20032527w-s3">
© Hawlfraint y Goron 2003
< /sentence>
< /p>
< p elementid="20032527w-p4">
< sentence type="normal" elementid="20032527w-s4">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20032527w-p5">
< sentence type="normal" elementid="20032527w-s5">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20032527w-s6">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20032527w-p6">
< sentence type="normal" elementid="20032527w-s7">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20032527w-s8">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20032527w-s9">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20032527w-p7">
< sentence type="normal" elementid="20032527w-s10">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20032527w-s11">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan
< english>
The Stationery Office Limited
< /english>
fel
< strong elementid="20032527w-st2">
Rheoliadau Asiantaethau Nyrsys
< roundbracket elementid="20032527w-rb3">
(  Cymru )
< /roundbracket>
2003
< /strong>
, ISBN 0110907876 .
< /sentence>
< sentence type="normal" elementid="20032527w-s12">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20032527w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20032527w-s13">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20032527w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20032527w-p8">
< sentence type="normal" elementid="20032527w-s14">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20032527w-a3">
< english>
Frequently Asked Questions
< /english>
< /a> .
< /sentence>
< /p>
< p elementid="20032527w-p9">
< sentence type="normal" elementid="20032527w-s15">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20032527w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20032527w-s16">
Pan welwch fotwm
< doublequotation elementid="20032527w-dq2">
"
< english>
continue
< /english>
"
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20032527w-p10">
< sentence type="heading" elementid="20032527w-s17">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="heading" elementid="20032527w-s18">
< strong elementid="20032527w-st3">
2003 Rhif 2527
< roundbracket elementid="20032527w-rb4">
(  Cy.242 )
< /roundbracket>
< /strong>
< /sentence>
< sentence type="heading" elementid="20032527w-s19">
< strong elementid="20032527w-st4">
NYRSYS , BYDWRAGEDD AC YMWELWYR IECHYD , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20032527w-s20">
Rheoliadau Asiantaethau Nyrsys
< roundbracket elementid="20032527w-rb5">
(  Cymru )
< /roundbracket>
2003
< /sentence>
< /p>
< table elementid="20032527w-tb1">
< td elementid="20032527w-td1">
< p elementid="20032527w-p11">
< sentence type="heading" elementid="20032527w-s21">
< em elementid="20032527w-em1">
Wedi 'u gwneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td2">
< p elementid="20032527w-p12">
< sentence type="heading" elementid="20032527w-s22">
< em elementid="20032527w-em2">
1 Hydref 2003
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td3">
< p elementid="20032527w-p13">
< sentence type="heading" elementid="20032527w-s23">
< em elementid="20032527w-em3">
Yn dod i rym
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td4">
< p elementid="20032527w-p14">
< sentence type="heading" elementid="20032527w-s24">
< em elementid="20032527w-em4">
2 Hydref 2003
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20032527w-p15">
< sentence type="heading" elementid="20032527w-s25">
< strong elementid="20032527w-st5">
TREFN Y RHEOLIADAU
< /strong>
< /sentence>
< sentence type="heading" elementid="20032527w-s26">
RHAN I -
< /sentence>
< sentence type="heading" elementid="20032527w-s27">
CYFFREDINOL
< /sentence>
< /p>
< table elementid="20032527w-tb2">
< td elementid="20032527w-td5">
< p elementid="20032527w-p16">
< sentence type="heading" elementid="20032527w-s28">
< a type="href" elementid="20032527w-a4">
1 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td6">
< p elementid="20032527w-p17">
< sentence type="heading" elementid="20032527w-s29">
< a type="href" elementid="20032527w-a5">
Enwi , cychwyn a chymhwyso
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb3">
< td elementid="20032527w-td7">
< p elementid="20032527w-p18">
< sentence type="heading" elementid="20032527w-s30">
< a type="href" elementid="20032527w-a6">
2 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td8">
< p elementid="20032527w-p19">
< sentence type="heading" elementid="20032527w-s31">
< a type="href" elementid="20032527w-a7">
Dehongli
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb4">
< td elementid="20032527w-td9">
< p elementid="20032527w-p20">
< sentence type="heading" elementid="20032527w-s32">
< a type="href" elementid="20032527w-a8">
3 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td10">
< p elementid="20032527w-p21">
< sentence type="heading" elementid="20032527w-s33">
< a type="href" elementid="20032527w-a9">
Asiantaethau sydd wedi 'u heithrio
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb5">
< td elementid="20032527w-td11">
< p elementid="20032527w-p22">
< sentence type="heading" elementid="20032527w-s34">
< a type="href" elementid="20032527w-a10">
4 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td12">
< p elementid="20032527w-p23">
< sentence type="heading" elementid="20032527w-s35">
< a type="href" elementid="20032527w-a11">
Datganiad o ddiben
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb6">
< td elementid="20032527w-td13">
< p elementid="20032527w-p24">
< sentence type="heading" elementid="20032527w-s36">
< a type="href" elementid="20032527w-a12">
5 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td14">
< p elementid="20032527w-p25">
< sentence type="heading" elementid="20032527w-s37">
< a type="href" elementid="20032527w-a13">
Arweiniad defnyddiwr gwasanaeth
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb7">
< td elementid="20032527w-td15">
< p elementid="20032527w-p26">
< sentence type="heading" elementid="20032527w-s38">
< a type="href" elementid="20032527w-a14">
6 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td16">
< p elementid="20032527w-p27">
< sentence type="heading" elementid="20032527w-s39">
< a type="href" elementid="20032527w-a15">
Adolygu 'r datganiad o ddiben a 'r arweiniad defnyddiwr gwasanaeth
< /a>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20032527w-p28">
< sentence type="heading" elementid="20032527w-s40">
RHAN II -
< /sentence>
< sentence type="heading" elementid="20032527w-s41">
PERSONAU COFRESTREDIG
< /sentence>
< /p>
< table elementid="20032527w-tb8">
< td elementid="20032527w-td17">
< p elementid="20032527w-p29">
< sentence type="heading" elementid="20032527w-s42">
< a type="href" elementid="20032527w-a16">
7 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td18">
< p elementid="20032527w-p30">
< sentence type="heading" elementid="20032527w-s43">
< a type="href" elementid="20032527w-a17">
Ffitrwydd y darparydd cofrestredig
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb9">
< td elementid="20032527w-td19">
< p elementid="20032527w-p31">
< sentence type="heading" elementid="20032527w-s44">
< a type="href" elementid="20032527w-a18">
8 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td20">
< p elementid="20032527w-p32">
< sentence type="heading" elementid="20032527w-s45">
< a type="href" elementid="20032527w-a19">
Penodi rheolwr
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb10">
< td elementid="20032527w-td21">
< p elementid="20032527w-p33">
< sentence type="heading" elementid="20032527w-s46">
< a type="href" elementid="20032527w-a20">
9 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td22">
< p elementid="20032527w-p34">
< sentence type="heading" elementid="20032527w-s47">
< a type="href" elementid="20032527w-a21">
Ffitrwydd y rheolwr
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb11">
< td elementid="20032527w-td23">
< p elementid="20032527w-p35">
< sentence type="heading" elementid="20032527w-s48">
< a type="href" elementid="20032527w-a22">
10 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td24">
< p elementid="20032527w-p36">
< sentence type="heading" elementid="20032527w-s49">
< a type="href" elementid="20032527w-a23">
Y person cofrestredig - gofynion cyffredinol a hyfforddiant
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb12">
< td elementid="20032527w-td25">
< p elementid="20032527w-p37">
< sentence type="heading" elementid="20032527w-s50">
< a type="href" elementid="20032527w-a24">
11 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td26">
< p elementid="20032527w-p38">
< sentence type="heading" elementid="20032527w-s51">
< a type="href" elementid="20032527w-a25">
Hysbysu o dramgwyddau
< /a>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20032527w-p39">
< sentence type="heading" elementid="20032527w-s52">
RHAN III -
< /sentence>
< sentence type="heading" elementid="20032527w-s53">
RHEDEG ASIANTAETHAU NYRSYS
< /sentence>
< sentence type="heading" elementid="20032527w-s54">
Pennod 1
< /sentence>
< sentence type="heading" elementid="20032527w-s55">
Ansawdd y gwasanaeth sy 'n cael ei ddarparu
< /sentence>
< /p>
< table elementid="20032527w-tb13">
< td elementid="20032527w-td27">
< p elementid="20032527w-p40">
< sentence type="heading" elementid="20032527w-s56">
< a type="href" elementid="20032527w-a26">
12 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td28">
< p elementid="20032527w-p41">
< sentence type="heading" elementid="20032527w-s57">
< a type="href" elementid="20032527w-a27">
Ffitrwydd y nyrsys sy 'n cael eu cyflenwi gan asiantaeth
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb14">
< td elementid="20032527w-td29">
< p elementid="20032527w-p42">
< sentence type="heading" elementid="20032527w-s58">
< a type="href" elementid="20032527w-a28">
13 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td30">
< p elementid="20032527w-p43">
< sentence type="heading" elementid="20032527w-s59">
< a type="href" elementid="20032527w-a29">
Polisïau a gweithdrefnau
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb15">
< td elementid="20032527w-td31">
< p elementid="20032527w-p44">
< sentence type="heading" elementid="20032527w-s60">
< a type="href" elementid="20032527w-a30">
14 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td32">
< p elementid="20032527w-p45">
< sentence type="heading" elementid="20032527w-s61">
< a type="href" elementid="20032527w-a31">
Staffio
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb16">
< td elementid="20032527w-td33">
< p elementid="20032527w-p46">
< sentence type="heading" elementid="20032527w-s62">
< a type="href" elementid="20032527w-a32">
15 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td34">
< p elementid="20032527w-p47">
< sentence type="heading" elementid="20032527w-s63">
< a type="href" elementid="20032527w-a33">
Y llawlyfr staff
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb17">
< td elementid="20032527w-td35">
< p elementid="20032527w-p48">
< sentence type="heading" elementid="20032527w-s64">
< a type="href" elementid="20032527w-a34">
16 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td36">
< p elementid="20032527w-p49">
< sentence type="heading" elementid="20032527w-s65">
< a type="href" elementid="20032527w-a35">
Darparu gwybodaeth i ddefnyddwyr gwasanaeth
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb18">
< td elementid="20032527w-td37">
< p elementid="20032527w-p50">
< sentence type="heading" elementid="20032527w-s66">
< a type="href" elementid="20032527w-a36">
17 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td38">
< p elementid="20032527w-p51">
< sentence type="heading" elementid="20032527w-s67">
< a type="href" elementid="20032527w-a37">
Cofnodion
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb19">
< td elementid="20032527w-td39">
< p elementid="20032527w-p52">
< sentence type="heading" elementid="20032527w-s68">
< a type="href" elementid="20032527w-a38">
18 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td40">
< p elementid="20032527w-p53">
< sentence type="heading" elementid="20032527w-s69">
< a type="href" elementid="20032527w-a39">
Cwynion
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb20">
< td elementid="20032527w-td41">
< p elementid="20032527w-p54">
< sentence type="heading" elementid="20032527w-s70">
< a type="href" elementid="20032527w-a40">
19 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td42">
< p elementid="20032527w-p55">
< sentence type="heading" elementid="20032527w-s71">
< a type="href" elementid="20032527w-a41">
Adolygu ansawdd y gwasanaeth sy 'n cael ei ddarparu
< /a>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20032527w-p56">
< sentence type="heading" elementid="20032527w-s72">
Pennod 2
< /sentence>
< sentence type="heading" elementid="20032527w-s73">
Y safle
< /sentence>
< /p>
< table elementid="20032527w-tb21">
< td elementid="20032527w-td43">
< p elementid="20032527w-p57">
< sentence type="heading" elementid="20032527w-s74">
< a type="href" elementid="20032527w-a42">
20 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td44">
< p elementid="20032527w-p58">
< sentence type="heading" elementid="20032527w-s75">
< a type="href" elementid="20032527w-a43">
Ffitrwydd y safle
< /a>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20032527w-p59">
< sentence type="heading" elementid="20032527w-s76">
Pennod 3
< /sentence>
< sentence type="heading" elementid="20032527w-s77">
Materion ariannol
< /sentence>
< /p>
< table elementid="20032527w-tb22">
< td elementid="20032527w-td45">
< p elementid="20032527w-p60">
< sentence type="heading" elementid="20032527w-s78">
< a type="href" elementid="20032527w-a44">
21 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td46">
< p elementid="20032527w-p61">
< sentence type="heading" elementid="20032527w-s79">
< a type="href" elementid="20032527w-a45">
Y sefyllfa ariannol
< /a>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20032527w-p62">
< sentence type="heading" elementid="20032527w-s80">
Pennod 4
< /sentence>
< sentence type="heading" elementid="20032527w-s81">
Yr hysbysiadau sydd i 'w rhoi i 'r Cynulliad Cenedlaethol
< /sentence>
< /p>
< table elementid="20032527w-tb23">
< td elementid="20032527w-td47">
< p elementid="20032527w-p63">
< sentence type="heading" elementid="20032527w-s82">
< a type="href" elementid="20032527w-a46">
22 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td48">
< p elementid="20032527w-p64">
< sentence type="heading" elementid="20032527w-s83">
< a type="href" elementid="20032527w-a47">
Hysbysu o absenoldeb
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb24">
< td elementid="20032527w-td49">
< p elementid="20032527w-p65">
< sentence type="heading" elementid="20032527w-s84">
< a type="href" elementid="20032527w-a48">
23 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td50">
< p elementid="20032527w-p66">
< sentence type="heading" elementid="20032527w-s85">
< a type="href" elementid="20032527w-a49">
Hysbysu o newidiadau
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb25">
< td elementid="20032527w-td51">
< p elementid="20032527w-p67">
< sentence type="heading" elementid="20032527w-s86">
< a type="href" elementid="20032527w-a50">
24 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td52">
< p elementid="20032527w-p68">
< sentence type="heading" elementid="20032527w-s87">
< a type="href" elementid="20032527w-a51">
Penodi datodwyr etc .
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb26">
< td elementid="20032527w-td53">
< p elementid="20032527w-p69">
< sentence type="heading" elementid="20032527w-s88">
< a type="href" elementid="20032527w-a52">
25 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td54">
< p elementid="20032527w-p70">
< sentence type="heading" elementid="20032527w-s89">
< a type="href" elementid="20032527w-a53">
Marwolaeth y person cofrestredig
< /a>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20032527w-p71">
< sentence type="heading" elementid="20032527w-s90">
RHAN IV -
< /sentence>
< sentence type="heading" elementid="20032527w-s91">
AMRYWIOL
< /sentence>
< /p>
< table elementid="20032527w-tb27">
< td elementid="20032527w-td55">
< p elementid="20032527w-p72">
< sentence type="heading" elementid="20032527w-s92">
< a type="href" elementid="20032527w-a54">
26 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td56">
< p elementid="20032527w-p73">
< sentence type="heading" elementid="20032527w-s93">
< a type="href" elementid="20032527w-a55">
Cydymffurfio â 'r rheoliadau
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb28">
< td elementid="20032527w-td57">
< p elementid="20032527w-p74">
< sentence type="heading" elementid="20032527w-s94">
< a type="href" elementid="20032527w-a56">
27 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td58">
< p elementid="20032527w-p75">
< sentence type="heading" elementid="20032527w-s95">
< a type="href" elementid="20032527w-a57">
Tramgwyddau
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb29">
< td elementid="20032527w-td59">
< p elementid="20032527w-p76">
< sentence type="heading" elementid="20032527w-s96">
< a type="href" elementid="20032527w-a58">
28 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td60">
< p elementid="20032527w-p77">
< sentence type="heading" elementid="20032527w-s97">
< a type="href" elementid="20032527w-a59">
Ffioedd
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb30">
< td elementid="20032527w-td61">
< p elementid="20032527w-p78">
< sentence type="heading" elementid="20032527w-s98">
< a type="href" elementid="20032527w-a60">
29 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td62">
< p elementid="20032527w-p79">
< sentence type="heading" elementid="20032527w-s99">
< a type="href" elementid="20032527w-a61">
Cofrestru
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb31">
< td elementid="20032527w-td63">
< p elementid="20032527w-p80">
< sentence type="heading" elementid="20032527w-s100">
< a type="href" elementid="20032527w-a62">
30 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td64">
< p elementid="20032527w-p81">
< sentence type="heading" elementid="20032527w-s101">
< a type="href" elementid="20032527w-a63">
Darpariaethau trosiannol
< /a>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20032527w-p82">
< sentence type="heading" elementid="20032527w-s102">
YR ATODLENNI
< /sentence>
< /p>
< table elementid="20032527w-tb32">
< td elementid="20032527w-td65">
< p elementid="20032527w-p83">
< sentence type="heading" elementid="20032527w-s103">
< a type="href" elementid="20032527w-a64">
1 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td66">
< p elementid="20032527w-p84">
< sentence type="heading" elementid="20032527w-s104">
< a type="href" elementid="20032527w-a65">
Yr wybodaeth sydd i 'w chynnwys yn y datganiad o ddiben
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb33">
< td elementid="20032527w-td67">
< p elementid="20032527w-p85">
< sentence type="heading" elementid="20032527w-s105">
< a type="href" elementid="20032527w-a66">
2 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td68">
< p elementid="20032527w-p86">
< sentence type="heading" elementid="20032527w-s106">
< a type="href" elementid="20032527w-a67">
Yr wybodaeth sy 'n ofynnol yngl n â darparwyr cofrestredig a rheolwyr asiantaeth a nyrsys sy 'n gyfrifol am ddethol nyrsys i 'w cyflenwi i ddefnyddwyr gwasanaeth
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb34">
< td elementid="20032527w-td69">
< p elementid="20032527w-p87">
< sentence type="heading" elementid="20032527w-s107">
< a type="href" elementid="20032527w-a68">
3 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td70">
< p elementid="20032527w-p88">
< sentence type="heading" elementid="20032527w-s108">
< a type="href" elementid="20032527w-a69">
Yr wybodaeth sy 'n ofynnol yngl n â nyrsys sydd i 'w cyflenwi gan asiantaeth
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb35">
< td elementid="20032527w-td71">
< p elementid="20032527w-p89">
< sentence type="heading" elementid="20032527w-s109">
< a type="href" elementid="20032527w-a70">
4 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td72">
< p elementid="20032527w-p90">
< sentence type="heading" elementid="20032527w-s110">
< a type="href" elementid="20032527w-a71">
Y cofnodion sydd i 'w cadw ar gyfer archwiliad
< /a>
< /sentence>
< /p>
< /td>
< /table>
< table elementid="20032527w-tb36">
< td elementid="20032527w-td73">
< p elementid="20032527w-p91">
< sentence type="heading" elementid="20032527w-s111">
< a type="href" elementid="20032527w-a72">
5 .
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td74">
< p elementid="20032527w-p92">
< sentence type="heading" elementid="20032527w-s112">
< a type="href" elementid="20032527w-a73">
Darpariaethau trosiannol
< /a>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20032527w-p93">
< sentence type="heading" elementid="20032527w-s113">
Mae Cynulliad Cenedlaethol Cymru , drwy arfer y pwerau a roddwyd iddo gan adrannau 4
< roundbracket elementid="20032527w-rb6">
(  6 )
< /roundbracket>
, 16
< roundbracket elementid="20032527w-rb7">
(  3 )
< /roundbracket>
, 22
< roundbracket elementid="20032527w-rb8">
(  1 )
< /roundbracket>
,
< roundbracket elementid="20032527w-rb9">
(  2 )
< /roundbracket>
< roundbracket elementid="20032527w-rb10">
(  a )
< /roundbracket>
i
< roundbracket elementid="20032527w-rb11">
(  c )
< /roundbracket>
,
< roundbracket elementid="20032527w-rb12">
(  f )
< /roundbracket>
i
< roundbracket elementid="20032527w-rb13">
(  j )
< /roundbracket>
a
< roundbracket elementid="20032527w-rb14">
(  7 )
< /roundbracket>
< roundbracket elementid="20032527w-rb15">
(  a )
< /roundbracket>
i
< roundbracket elementid="20032527w-rb16">
(  h )
< /roundbracket>
a
< roundbracket elementid="20032527w-rb17">
(  j )
< /roundbracket>
, 25 , 34
< roundbracket elementid="20032527w-rb18">
(  1 )
< /roundbracket>
, 35 a 118
< roundbracket elementid="20032527w-rb19">
(  5 )
< /roundbracket>
i
< roundbracket elementid="20032527w-rb20">
(  7 )
< /roundbracket>
o Ddeddf Safonau Gofal 2000
< squarebracket elementid="20032527w-sb1">
[ 
< a type="href" elementid="20032527w-a74">
1
< /a>
]
< /squarebracket>
a phob p er arall sy 'n ei alluogi yn cyswllt hwnnw , ar ôl ymgynghori â 'r personau y mae 'n barnu eu bod yn briodol
< squarebracket elementid="20032527w-sb2">
[ 
< a type="href" elementid="20032527w-a75">
2
< /a>
]
< /squarebracket>
, drwy hyn yn gwneud y Rheoliadau canlynol :  - 
< /sentence>
< sentence type="heading" elementid="20032527w-s114">
RHAN I
< /sentence>
< sentence type="heading" elementid="20032527w-s115">
CYFFREDINOL
< /sentence>
< sentence type="heading" elementid="20032527w-s116">
< strong elementid="20032527w-st6">
Enwi , cychwyn a chymhwyso
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s117">
1 .
< /sentence>
< sentence type="normal" elementid="20032527w-s118">
 - 
< roundbracket elementid="20032527w-rb21">
(  1 )
< /roundbracket>
Enw 'r Rheoliadau hyn yw Rheoliadau Asiantaethau Nyrsys
< roundbracket elementid="20032527w-rb22">
(  Cymru )
< /roundbracket>
2003 a deuant i rym ar 2 Hydref 2003 .
< /sentence>
< sentence type="normal" elementid="20032527w-s119">
< roundbracket elementid="20032527w-rb23">
(  2 )
< /roundbracket>
Mae 'r Rheoliadau hyn yn gymwys i asiantaethau nyrsys yng Nghymru yn unig .
< /sentence>
< sentence type="heading" elementid="20032527w-s120">
< strong elementid="20032527w-st7">
Dehongli
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s121">
2 .
< /sentence>
< sentence type="heading" elementid="20032527w-s122">
 - 
< roundbracket elementid="20032527w-rb24">
(  1 )
< /roundbracket>
Yn y rheoliadau hyn  - 
< /sentence>
< sentence type="heading" elementid="20032527w-s123">
ystyr
< doublequotation elementid="20032527w-dq3">
"  arweiniad defnyddiwr gwasanaeth "
< /doublequotation>
< roundbracket elementid="20032527w-rb25">
(
< doublequotation elementid="20032527w-dq4">
"
< em elementid="20032527w-em5">
< english>
service user 's guide
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw 'r arweiniad ysgrifenedig sy 'n cael ei gynhyrchu yn unol â rheoliad 5 ; ystyr
< doublequotation elementid="20032527w-dq5">
"  asiantaeth "
< /doublequotation>
< roundbracket elementid="20032527w-rb26">
(
< doublequotation elementid="20032527w-dq6">
"
< em elementid="20032527w-em6">
< english>
agency
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw asiantaeth nyrsys ; ystyr
< doublequotation elementid="20032527w-dq7">
"  awdurdod "
< /doublequotation>
< roundbracket elementid="20032527w-rb27">
(
< doublequotation elementid="20032527w-dq8">
"
< em elementid="20032527w-em7">
< english>
authority
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
, mewn perthynas â pherson  - 
< roundbracket elementid="20032527w-rb28">
(  a )
< /roundbracket>
sy 'n rhedeg neu sy 'n dymuno rhedeg , asiantaeth i gyflenwi nyrsys o fewn ystyr
< doublequotation elementid="20032527w-dq9">
"
< english>
agency for the supply of nurses
< /english>
"
< /doublequotation>
yn Neddf 1957 ; a
< /sentence>
< sentence type="heading" elementid="20032527w-s124">
< roundbracket elementid="20032527w-rb29">
(  b )
< /roundbracket>
sy 'n ddeiliad trwydded sydd wedi 'i rhoi i 'r person hwnnw gan awdurdod lleol o dan adran 2 o 'r Ddeddf honno ac sy 'n awdurdodi 'r deiliad i redeg yr asiantaeth honno o 'r safle sydd wedi 'i bennu yn y drwydded , neu sydd wedi gwneud cais am drwydded o 'r fath,
< /sentence>
< /p>
< p elementid="20032527w-p94">
< sentence type="heading" elementid="20032527w-s125">
yw 'r awdurdod lleol , sef , at ddibenion yr adran honno , yr awdurdod trwyddedu y mae 'r safle wedi 'i leoli yn ei ardal ;
< /sentence>
< /p>
< p elementid="20032527w-p95">
< sentence type="heading" elementid="20032527w-s126">
ystyr
< doublequotation elementid="20032527w-dq10">
"  claf "
< /doublequotation>
< roundbracket elementid="20032527w-rb30">
(
< doublequotation elementid="20032527w-dq11">
"
< em elementid="20032527w-em8">
< english>
patient
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw person y mae gwasanaeth nyrsio yn cael ei ddarparu iddo gan nyrs a gyflenwyd gan asiantaeth ; ystyr
< doublequotation elementid="20032527w-dq12">
"  corff "
< /doublequotation>
< roundbracket elementid="20032527w-rb31">
(
< doublequotation elementid="20032527w-dq13">
"
< em elementid="20032527w-em9">
< english>
organisation
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw corff corfforedig ; ystyr
< doublequotation elementid="20032527w-dq14">
"  Cynulliad Cenedlaethol "
< /doublequotation>
< roundbracket elementid="20032527w-rb32">
(
< doublequotation elementid="20032527w-dq15">
"
< em elementid="20032527w-em10">
< english>
National Assembly
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw Cynulliad Cenedlaethol Cymru ; ystyr
< doublequotation elementid="20032527w-dq16">
"  darparydd asiantaeth nyrsys "
< /doublequotation>
< roundbracket elementid="20032527w-rb33">
(
< doublequotation elementid="20032527w-dq17">
"
< em elementid="20032527w-em11">
< english>
nurses agency provider
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw person sydd , yn union cyn 2 Hydref 2003  - 
< roundbracket elementid="20032527w-rb34">
(  a )
< /roundbracket>
yn rhedeg asiantaeth i gyflenwi nyrsys o fewn ystyr
< doublequotation elementid="20032527w-dq18">
"
< english>
agency for the supply of nurses
< /english>
"
< /doublequotation>
yn Neddf 1957 ; a
< /sentence>
< sentence type="heading" elementid="20032527w-s127">
< roundbracket elementid="20032527w-rb35">
(  b )
< /roundbracket>
yn ddeiliad trwydded sydd wedi 'i rhoi i 'r person hwnnw gan awdurdod lleol o dan adran 2 o 'r Ddeddf honno ac sy 'n awdurdodi 'r deiliad i redeg yr asiantaeth honno o 'r safle sydd wedi 'i bennu yn y drwydded ;
< /sentence>
< sentence type="heading" elementid="20032527w-s128">
ystyr
< doublequotation elementid="20032527w-dq19">
"  darparydd cofrestredig "
< /doublequotation>
< roundbracket elementid="20032527w-rb36">
(
< doublequotation elementid="20032527w-dq20">
"
< em elementid="20032527w-em12">
< english>
registered provider
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
, mewn perthynas ag asiantaeth , yw person sydd wedi 'i gofrestru o dan Ran II o 'r Ddeddf fel y person sy 'n rhedeg yr asiantaeth honno ; ystyr
< doublequotation elementid="20032527w-dq21">
"  datganiad o ddiben "
< /doublequotation>
< roundbracket elementid="20032527w-rb37">
(
< doublequotation elementid="20032527w-dq22">
"
< em elementid="20032527w-em13">
< english>
statement of purpose
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw 'r datganiad ysgrifenedig a luniwyd yn unol â rheoliad 4 ; ystyr
< doublequotation elementid="20032527w-dq23">
"  Deddf 1957 "
< /doublequotation>
< roundbracket elementid="20032527w-rb38">
(
< doublequotation elementid="20032527w-dq24">
"
< em elementid="20032527w-em14">
< english>
1957 Act
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw Deddf Asiantaethau Nyrsys 1957
< squarebracket elementid="20032527w-sb3">
[ 
< a type="href" elementid="20032527w-a76">
3
< /a>
]
< /squarebracket>
; ystyr
< doublequotation elementid="20032527w-dq25">
"  defnyddiwr gwasanaeth "
< /doublequotation>
< roundbracket elementid="20032527w-rb39">
(
< doublequotation elementid="20032527w-dq26">
"
< em elementid="20032527w-em15">
< english>
service user
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw person y mae asiantaeth  - 
< roundbracket elementid="20032527w-rb40">
(  a )
< /roundbracket>
yn cyflenwi nyrs , sy 'n cael ei chyflogi gan yr asiantaeth , iddo ; neu
< /sentence>
< sentence type="heading" elementid="20032527w-s129">
< roundbracket elementid="20032527w-rb41">
(  b )
< /roundbracket>
yn darparu gwasanaethau at ddibenion cyflenwi nyrs i 'r defnyddiwr gwasanaeth er mwyn iddi gael ei chyflogi gan y defnyddiwr gwasanaeth hwnnw ;
< /sentence>
< sentence type="heading" elementid="20032527w-s130">
ystyr
< doublequotation elementid="20032527w-dq27">
"  dyddiad effeithiol "
< /doublequotation>
< roundbracket elementid="20032527w-rb42">
(
< doublequotation elementid="20032527w-dq28">
"
< em elementid="20032527w-em16">
< english>
effective date
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw 'r dyddiad o bryd y mae darparydd presennol i 'w drin at ddibenion Rhan II o 'r Ddeddf , yn unol â pharagraff 1
< roundbracket elementid="20032527w-rb43">
(  6 )
< /roundbracket>
o Atodlen 5 i 'r Rheoliadau hyn , fel petai wedi gwneud cais am gofrestriad ar gyfer yr ymgymeriad presennol a bod y cofrestriad hwnnw wedi 'i ganiatáu ; ystyr
< doublequotation elementid="20032527w-dq29">
"  y Ddeddf "
< /doublequotation>
< roundbracket elementid="20032527w-rb44">
(
< doublequotation elementid="20032527w-dq30">
"
< em elementid="20032527w-em17">
< english>
the Act
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw Deddf Safonau Gofal 2000 ; ystyr
< doublequotation elementid="20032527w-dq31">
"  nyrs "
< /doublequotation>
< roundbracket elementid="20032527w-rb45">
(
< doublequotation elementid="20032527w-dq32">
"
< em elementid="20032527w-em18">
< english>
nurse
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw nyrs gofrestredig , bydwraig gofrestredig neu ymwelydd iechyd cofrestredig
< squarebracket elementid="20032527w-sb4">
[ 
< a type="href" elementid="20032527w-a77">
4
< /a>
]
< /squarebracket>
; ystyr
< doublequotation elementid="20032527w-dq33">
"  person cofrestredig "
< /doublequotation>
< roundbracket elementid="20032527w-rb46">
(
< doublequotation elementid="20032527w-dq34">
"
< em elementid="20032527w-em19">
< english>
registered person
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
, mewn perthynas ag asiantaeth , yw unrhyw berson sy 'n ddarparydd cofrestredig yr asiantaeth honno neu 'n rheolwr cofrestredig arni ; ystyr
< doublequotation elementid="20032527w-dq35">
"  rheolwr cofrestredig "
< /doublequotation>
< roundbracket elementid="20032527w-rb47">
(
< doublequotation elementid="20032527w-dq36">
"
< em elementid="20032527w-em20">
< english>
registered manager
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
, mewn perthynas ag asiantaeth , yw unrhyw berson sydd wedi 'i gofrestru o dan Ran II o 'r Ddeddf fel rheolwr yr asiantaeth honno ; ystyr
< doublequotation elementid="20032527w-dq37">
"  swyddfa briodol "
< /doublequotation>
< roundbracket elementid="20032527w-rb48">
(
< doublequotation elementid="20032527w-dq38">
"
< em elementid="20032527w-em21">
< english>
appropriate office
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
mewn perthynas ag asiantaeth nyrsys  - 
< roundbracket elementid="20032527w-rb49">
(  a )
< /roundbracket>
os oes swyddfa wedi 'i phennu o dan baragraff
< roundbracket elementid="20032527w-rb50">
(  4 )
< /roundbracket>
isod ar gyfer yr ardal y mae 'r asiantaeth nyrsys yn gweithredu ynddi , yw 'r swyddfa honno ;
< /sentence>
< sentence type="heading" elementid="20032527w-s131">
< roundbracket elementid="20032527w-rb51">
(  b )
< /roundbracket>
mewn unrhyw achos arall , yw unrhyw un o swyddfeydd y Cynulliad Cenedlaethol ;
< /sentence>
< sentence type="heading" elementid="20032527w-s132">
mae
< doublequotation elementid="20032527w-dq39">
"  unigolyn cyfrifol "
< /doublequotation>
< roundbracket elementid="20032527w-rb52">
(
< doublequotation elementid="20032527w-dq40">
"
< em elementid="20032527w-em22">
< english>
responsible individual
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
i 'w ddehongli yn unol â rheoliad 7 ; mae i
< doublequotation elementid="20032527w-dq41">
"  ymddiriedolaeth GIG "
< /doublequotation>
yr un ystyr ag
< doublequotation elementid="20032527w-dq42">
"
< em elementid="20032527w-em23">
< english>
NHS trust
< /english>
< /em>
"
< /doublequotation>
yn Neddf y Gwasanaeth Iechyd Gwladol a Gofal Cymunedol 1990
< squarebracket elementid="20032527w-sb5">
[ 
< a type="href" elementid="20032527w-a78">
5
< /a>
]
< /squarebracket>
; ystyr
< doublequotation elementid="20032527w-dq43">
"  ymgymeriad presennol "
< /doublequotation>
< roundbracket elementid="20032527w-rb53">
(
< doublequotation elementid="20032527w-dq44">
"
< em elementid="20032527w-em24">
< english>
existing undertaking
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw person sydd , yn union cyn 2 Hydref 2003  - 
< roundbracket elementid="20032527w-rb54">
(  a )
< /roundbracket>
yn rhedeg asiantaeth i gyflenwi nyrsys o fewn ystyr
< doublequotation elementid="20032527w-dq45">
"
< english>
agency for the supply of nurses
< /english>
"
< /doublequotation>
yn Neddf 1957 ; a
< /sentence>
< sentence type="normal" elementid="20032527w-s133">
< roundbracket elementid="20032527w-rb55">
(  b )
< /roundbracket>
yn ddeiliad trwydded sydd wedi 'i rhoi i 'r person hwnnw gan awdurdod lleol o dan adran 2 o 'r Ddeddf honno ac sy 'n awdurdodi 'r deiliad i redeg yr asiantaeth honno o 'r safle sydd wedi 'i bennu yn y drwydded .
< /sentence>
< /p>
< p elementid="20032527w-p96">
< sentence type="heading" elementid="20032527w-s134">
< roundbracket elementid="20032527w-rb56">
(  2 )
< /roundbracket>
Yn y Rheoliadau hyn , mae cyfeiriadau at gyflenwi nyrs yn golygu  - 
< /sentence>
< /p>
< p elementid="20032527w-p97">
< sentence type="heading" elementid="20032527w-s135">
< roundbracket elementid="20032527w-rb57">
(  a )
< /roundbracket>
cyflenwi nyrs sy 'n cael ei chyflogi at ddibenion asiantaeth i weithredu i berson arall neu o dan ei reolaeth ; a
< /sentence>
< sentence type="normal" elementid="20032527w-s136">
< roundbracket elementid="20032527w-rb58">
(  b )
< /roundbracket>
cyflwyno nyrs gan asiantaeth i ddefnyddiwr gwasanaeth er mwyn iddi gael ei chyflogi gan y defnyddiwr gwasanaeth hwnnw .
< /sentence>
< /p>
< p elementid="20032527w-p98">
< sentence type="normal" elementid="20032527w-s137">
< roundbracket elementid="20032527w-rb59">
(  3 )
< /roundbracket>
Yn y diffiniad o
< doublequotation elementid="20032527w-dq46">
"  defnyddiwr gwasanaeth "
< /doublequotation>
ym mharagraffau
< roundbracket elementid="20032527w-rb60">
(  1 )
< /roundbracket>
a
< roundbracket elementid="20032527w-rb61">
(  2 )
< /roundbracket>
, mae 'r term
< doublequotation elementid="20032527w-dq47">
"  ei chyflogi "
< /doublequotation>
yn cynnwys cyflogaeth o dan gontract ar gyfer gwasanaethau .
< /sentence>
< sentence type="normal" elementid="20032527w-s138">
< roundbracket elementid="20032527w-rb62">
(  4 )
< /roundbracket>
Caiff y Cynulliad Cenedlaethol bennu swyddfa y mae 'n ei rheoli fel y swyddfa briodol mewn perthynas ag asiantaeth nyrsys sydd wedi 'i lleoli mewn rhan benodol o Gymru .
< /sentence>
< sentence type="heading" elementid="20032527w-s139">
< strong elementid="20032527w-st8">
Asiantaethau sydd wedi 'u heithrio
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s140">
3 .
< /sentence>
< sentence type="normal" elementid="20032527w-s141">
At ddibenion y Ddeddf , mae ymddiriedolaeth GIG sy 'n cyflenwi nyrsys i weithio ar gyfer ymddiriedolaethau GIG eraill yn unig wedi 'i heithrio rhag bod yn asiantaeth nyrsys .
< /sentence>
< sentence type="heading" elementid="20032527w-s142">
< strong elementid="20032527w-st9">
Datganiad o ddiben
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s143">
4 .
< /sentence>
< sentence type="normal" elementid="20032527w-s144">
 - 
< roundbracket elementid="20032527w-rb63">
(  1 )
< /roundbracket>
Rhaid i 'r person cofrestredig lunio mewn perthynas â 'r asiantaeth ddatganiad ysgrifenedig
< roundbracket elementid="20032527w-rb64">
(  y cyfeirir ato yn y Rheoliadau hyn fel
< doublequotation elementid="20032527w-dq48">
"  y datganiad o ddiben "
< /doublequotation>
)
< /roundbracket>
y mae 'n rhaid iddo gynnwys datganiad yngl n â 'r materion a restrir yn Atodlen 1 .
< /sentence>
< sentence type="normal" elementid="20032527w-s145">
< roundbracket elementid="20032527w-rb65">
(  2 )
< /roundbracket>
Rhaid i 'r person cofrestredig ddarparu copi o 'r datganiad o ddiben i swyddfa briodol y Cynulliad Cenedlaethol a rhaid iddo drefnu bod copi ohono ar gael ar gais i 'w archwilio gan bob defnyddiwr gwasanaeth ac unrhyw berson sy 'n gweithredu ar ran defnyddiwr gwasanaeth .
< /sentence>
< sentence type="heading" elementid="20032527w-s146">
< roundbracket elementid="20032527w-rb66">
(  3 )
< /roundbracket>
Ni fydd dim yn rheoliad 20 yn ei gwneud yn ofynnol i 'r person cofrestredig dorri neu beidio â chydymffurfio â 'r canlynol , nac yn ei awdurdodi i wneud hynny  - 
< /sentence>
< /p>
< p elementid="20032527w-p99">
< sentence type="heading" elementid="20032527w-s147">
< roundbracket elementid="20032527w-rb67">
(  a )
< /roundbracket>
unrhyw ddarpariaeth arall yn y Rheoliadau hyn ; neu
< /sentence>
< sentence type="normal" elementid="20032527w-s148">
< roundbracket elementid="20032527w-rb68">
(  b )
< /roundbracket>
yr amodau sydd mewn grym am y tro mewn perthynas â chofrestru 'r person cofrestredig o dan Ran II o 'r Ddeddf .
< /sentence>
< /p>
< p elementid="20032527w-p100">
< sentence type="heading" elementid="20032527w-s149">
< strong elementid="20032527w-st10">
Arweiniad defnyddiwr gwasanaeth
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s150">
5 .
< /sentence>
< sentence type="heading" elementid="20032527w-s151">
 - 
< roundbracket elementid="20032527w-rb69">
(  1 )
< /roundbracket>
Rhaid i 'r person cofrestredig baratoi arweiniad defnyddiwr gwasanaeth y mae 'n rhaid iddo gynnwys  - 
< /sentence>
< /p>
< p elementid="20032527w-p101">
< sentence type="heading" elementid="20032527w-s152">
< roundbracket elementid="20032527w-rb70">
(  a )
< /roundbracket>
crynodeb o 'r datganiad o ddiben ;
< /sentence>
< sentence type="heading" elementid="20032527w-s153">
< roundbracket elementid="20032527w-rb71">
(  b )
< /roundbracket>
y telerau a 'r amodau ar gyfer y gwasanaethau sydd i 'w darparu i 'r defnyddwyr gwasanaeth , gan gynnwys y telerau a 'r amodau yngl n â swm y ffioedd a dull eu talu ;
< /sentence>
< sentence type="heading" elementid="20032527w-s154">
< roundbracket elementid="20032527w-rb72">
(  c )
< /roundbracket>
crynodeb o 'r weithdrefn gwyno a sefydlwyd yn unol â rheoliad 18 ; ac
< /sentence>
< sentence type="normal" elementid="20032527w-s155">
< roundbracket elementid="20032527w-rb73">
(  ch )
< /roundbracket>
cyfeiriad a Rhif ffôn unrhyw swyddfa briodol benodedig y Cynulliad Cenedlaethol .
< /sentence>
< /p>
< p elementid="20032527w-p102">
< sentence type="normal" elementid="20032527w-s156">
< roundbracket elementid="20032527w-rb74">
(  2 )
< /roundbracket>
Rhaid i 'r person cofrestredig drefnu bod copi o 'r arweiniad defnyddiwr gwasanaeth ar gael ar gais i 'w archwilio ar safle 'r asiantaeth gan bob defnyddiwr gwasanaeth ac unrhyw berson sy 'n gweithredu ar ran defnyddiwr gwasanaeth .
< /sentence>
< sentence type="heading" elementid="20032527w-s157">
< strong elementid="20032527w-st11">
Adolygu 'r datganiad o ddiben a 'r arweiniad defnyddiwr gwasanaeth
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s158">
6 .
< /sentence>
< sentence type="heading" elementid="20032527w-s159">
Rhaid i 'r person cofrestredig  - 
< /sentence>
< /p>
< p elementid="20032527w-p103">
< sentence type="heading" elementid="20032527w-s160">
< roundbracket elementid="20032527w-rb75">
(  a )
< /roundbracket>
cadw 'r ddatganiad o ddiben a 'r arweiniad defnyddiwr gwasanaeth o dan sylw ac , os yw 'n briodol , eu diwygio ; a
< /sentence>
< sentence type="normal" elementid="20032527w-s161">
< roundbracket elementid="20032527w-rb76">
(  b )
< /roundbracket>
hysbysu swyddfa briodol y Cynulliad Cenedlaethol o fewn 28 diwrnod o unrhyw ddiwygiad o bwys .
< /sentence>
< /p>
< p elementid="20032527w-p104">
< sentence type="heading" elementid="20032527w-s162">
RHAN II
< /sentence>
< sentence type="heading" elementid="20032527w-s163">
PERSONAU COFRESTREDIG
< /sentence>
< sentence type="heading" elementid="20032527w-s164">
< strong elementid="20032527w-st12">
Ffitrwydd y darparydd cofrestredig
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s165">
7 .
< /sentence>
< sentence type="normal" elementid="20032527w-s166">
 - 
< roundbracket elementid="20032527w-rb77">
(  1 )
< /roundbracket>
Rhaid i berson beidio â rhedeg asiantaeth oni bai bod y person hwnnw yn ffit i wneud hynny .
< /sentence>
< sentence type="heading" elementid="20032527w-s167">
< roundbracket elementid="20032527w-rb78">
(  2 )
< /roundbracket>
Nid yw berson yn ffit i redeg asiantaeth oni bai bod y person  - 
< /sentence>
< sentence type="heading" elementid="20032527w-s168">
< roundbracket elementid="20032527w-rb79">
(  a )
< /roundbracket>
yn unigolyn sy 'n bodloni 'r gofynion a nodir ym mharagraff
< roundbracket elementid="20032527w-rb80">
(  3 )
< /roundbracket>
; neu
< /sentence>
< sentence type="heading" elementid="20032527w-s169">
< roundbracket elementid="20032527w-rb81">
(  b )
< /roundbracket>
yn gorff a  - 
< /sentence>
< sentence type="heading" elementid="20032527w-s170">
< roundbracket elementid="20032527w-rb82">
(  i )
< /roundbracket>
bod y corff wedi hysbysu swyddfa briodol y Cynulliad Cenedlaethol o enw , cyfeiriad a safle unigolyn yn y corff
< roundbracket elementid="20032527w-rb83">
(  cyfeirir at yr unigolyn yn y rheoliadau hyn fel
< doublequotation elementid="20032527w-dq49">
"  yr unigolyn cyfrifol "
< /doublequotation>
)
< /roundbracket>
sy 'n gyfarwyddwr , rheolwr , ysgrifennydd neu swyddog arall i 'r corff ac sy 'n gyfrifol am oruchwylio 'r rheolaeth ar yr asiantaeth ; a
< /sentence>
< sentence type="normal" elementid="20032527w-s171">
< roundbracket elementid="20032527w-rb84">
(  ii )
< /roundbracket>
bod yr unigolyn hwnnw yn bodloni 'r gofynion a nodir ym mharagraff
< roundbracket elementid="20032527w-rb85">
(  3 )
< /roundbracket> .
< /sentence>
< /p>
< p elementid="20032527w-p105">
< sentence type="heading" elementid="20032527w-s172">
< roundbracket elementid="20032527w-rb86">
(  3 )
< /roundbracket>
Dyma 'r gofynion  - 
< /sentence>
< /p>
< p elementid="20032527w-p106">
< sentence type="heading" elementid="20032527w-s173">
< roundbracket elementid="20032527w-rb87">
(  a )
< /roundbracket>
bod yr unigolyn yn addas o ran ei onestrwydd a 'i gymeriad da ;
< /sentence>
< sentence type="heading" elementid="20032527w-s174">
< roundbracket elementid="20032527w-rb88">
(  b )
< /roundbracket>
bod yr unigolyn yn ffit yn gorfforol ac yn feddyliol i redeg yr asiantaeth ; ac
< /sentence>
< sentence type="heading" elementid="20032527w-s175">
< roundbracket elementid="20032527w-rb89">
(  c )
< /roundbracket>
bod gwybodaeth lawn a boddhaol ar gael mewn perthynas â 'r unigolyn  - 
< /sentence>
< sentence type="heading" elementid="20032527w-s176">
< roundbracket elementid="20032527w-rb90">
(  i )
< /roundbracket>
ac eithrio os yw paragraff
< roundbracket elementid="20032527w-rb91">
(  4 )
< /roundbracket>
yn gymwys , am bob mater a bennir ym mharagraffau 1 i 8 o Atodlen 2 ;
< /sentence>
< sentence type="normal" elementid="20032527w-s177">
< roundbracket elementid="20032527w-rb92">
(  ii )
< /roundbracket>
os yw paragraff
< roundbracket elementid="20032527w-rb93">
(  4 )
< /roundbracket>
yn gymwys , am bob mater a bennir ym mharagraffau 1 a 3 i 9 o Atodlen 2 .
< /sentence>
< /p>
< p elementid="20032527w-p107">
< sentence type="normal" elementid="20032527w-s178">
< roundbracket elementid="20032527w-rb94">
(  4 )
< /roundbracket>
Mae 'r paragraff hwn yn gymwys os yw unigolyn wedi gwneud cais am dystysgrif y cyfeirir ati ym mharagraff 2 o Atodlen 2 ond nad yw 'r dystysgrif wedi 'i dyroddi .
< /sentence>
< sentence type="heading" elementid="20032527w-s179">
< roundbracket elementid="20032527w-rb95">
(  5 )
< /roundbracket>
Rhaid i berson beidio â rhedeg asiantaeth  - 
< /sentence>
< /p>
< p elementid="20032527w-p108">
< sentence type="heading" elementid="20032527w-s180">
< roundbracket elementid="20032527w-rb96">
(  a )
< /roundbracket>
os yw wedi 'i ddyfarnu 'n fethdalwr neu os dyfarnwyd atafaeliad ar ei ystad ac ,
< roundbracket elementid="20032527w-rb97">
(  yn y naill achos neu 'r llall )
< /roundbracket>
nad yw wedi 'i ryddhau ac nad yw 'r gorchymyn methdaliad wedi 'i ddiddymu neu wedi 'i ddileu ; neu
< /sentence>
< sentence type="normal" elementid="20032527w-s181">
< roundbracket elementid="20032527w-rb98">
(  b )
< /roundbracket>
os yw wedi gwneud cyfansoddiad neu drefniant gyda 'i gredydwyr ac nad yw wedi 'i ryddhau mewn perthynas ag ef .
< /sentence>
< /p>
< p elementid="20032527w-p109">
< sentence type="heading" elementid="20032527w-s182">
< strong elementid="20032527w-st13">
Penodi rheolwr
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s183">
8 .
< /sentence>
< sentence type="heading" elementid="20032527w-s184">
 - 
< roundbracket elementid="20032527w-rb99">
(  1 )
< /roundbracket>
Rhaid i 'r darparydd cofrestredig benodi unigolyn i reoli 'r asiantaeth  - 
< /sentence>
< /p>
< p elementid="20032527w-p110">
< sentence type="heading" elementid="20032527w-s185">
< roundbracket elementid="20032527w-rb100">
(  a )
< /roundbracket>
pan nad oes unrhyw reolwr cofrestredig ar gyfer yr asiantaeth ; a
< /sentence>
< sentence type="heading" elementid="20032527w-s186">
< roundbracket elementid="20032527w-rb101">
(  b )
< /roundbracket>
pan fo 'r darparydd cofrestredig  - 
< /sentence>
< sentence type="heading" elementid="20032527w-s187">
< roundbracket elementid="20032527w-rb102">
(  i )
< /roundbracket>
yn gorff ; neu
< /sentence>
< sentence type="heading" elementid="20032527w-s188">
< roundbracket elementid="20032527w-rb103">
(  ii )
< /roundbracket>
yn berson anffit i reoli asiantaeth ; neu
< /sentence>
< sentence type="normal" elementid="20032527w-s189">
< roundbracket elementid="20032527w-rb104">
(  iii )
< /roundbracket>
yn berson nad oes ganddo ofal amser llawn dros yr asiantaeth o ddydd i ddydd , neu nad yw 'n bwriadu bod â gofal o 'r fath drosti .
< /sentence>
< /p>
< p elementid="20032527w-p111">
< sentence type="heading" elementid="20032527w-s190">
< roundbracket elementid="20032527w-rb105">
(  2 )
< /roundbracket>
Pan fydd y darparydd cofrestredig yn penodi person i reoli 'r asiantaeth , rhaid iddo hysbysu swyddfa briodol y Cynulliad Cenedlaethol ar unwaith  - 
< /sentence>
< /p>
< p elementid="20032527w-p112">
< sentence type="heading" elementid="20032527w-s191">
< roundbracket elementid="20032527w-rb106">
(  a )
< /roundbracket>
o enw 'r person a benodwyd felly ; a
< /sentence>
< sentence type="normal" elementid="20032527w-s192">
< roundbracket elementid="20032527w-rb107">
(  b )
< /roundbracket>
o 'r dyddiad y mae 'r penodiad i fod i ddod i rym .
< /sentence>
< /p>
< p elementid="20032527w-p113">
< sentence type="heading" elementid="20032527w-s193">
< strong elementid="20032527w-st14">
Ffitrwydd y rheolwr
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s194">
9 .
< /sentence>
< sentence type="normal" elementid="20032527w-s195">
 - 
< roundbracket elementid="20032527w-rb108">
(  1 )
< /roundbracket>
Rhaid i berson beidio â rheoli asiantaeth oni bai ei fod yn ffit i wneud hynny .
< /sentence>
< sentence type="heading" elementid="20032527w-s196">
< roundbracket elementid="20032527w-rb109">
(  2 )
< /roundbracket>
Nid yw person yn ffit i reoli asiantaeth oni bai  - 
< /sentence>
< /p>
< p elementid="20032527w-p114">
< sentence type="heading" elementid="20032527w-s197">
< roundbracket elementid="20032527w-rb110">
(  a )
< /roundbracket>
ei fod yn addas o ran ei onestrwydd a 'i gymeriad da ;
< /sentence>
< sentence type="heading" elementid="20032527w-s198">
< roundbracket elementid="20032527w-rb111">
(  b )
< /roundbracket>
o roi sylw i faint yr asiantaeth , ei datganiad o ddiben a nifer ac anghenion y defnyddwyr gwasanaeth  - 
< /sentence>
< sentence type="heading" elementid="20032527w-s199">
< roundbracket elementid="20032527w-rb112">
(  i )
< /roundbracket>
bod gan y person y cymwysterau , y medrau a 'r profiad sy 'n angenrheidiol i reoli 'r asiantaeth ; a
< /sentence>
< sentence type="heading" elementid="20032527w-s200">
< roundbracket elementid="20032527w-rb113">
(  ii )
< /roundbracket>
bod y person yn ffit yn gorfforol ac yn feddyliol i wneud hynny ;
< /sentence>
< /p>
< p elementid="20032527w-p115">
< sentence type="heading" elementid="20032527w-s201">
< roundbracket elementid="20032527w-rb114">
(  c )
< /roundbracket>
bod gwybodaeth lawn a boddhaol ar gael mewn perthynas ag ef  - 
< /sentence>
< /p>
< p elementid="20032527w-p116">
< sentence type="heading" elementid="20032527w-s202">
< roundbracket elementid="20032527w-rb115">
(  i )
< /roundbracket>
ac eithrio os yw paragraff
< roundbracket elementid="20032527w-rb116">
(  3 )
< /roundbracket>
yn gymwys , am bob mater a bennir ym mharagraffau 1 i 8 o Atodlen 2,
< /sentence>
< sentence type="normal" elementid="20032527w-s203">
< roundbracket elementid="20032527w-rb117">
(  ii )
< /roundbracket>
os yw paragraff
< roundbracket elementid="20032527w-rb118">
(  3 )
< /roundbracket>
yn gymwys , am bob mater a bennir ym mharagraffau 1 a 3 i 9 o Atodlen 2 .
< /sentence>
< /p>
< p elementid="20032527w-p117">
< sentence type="normal" elementid="20032527w-s204">
< roundbracket elementid="20032527w-rb119">
(  3 )
< /roundbracket>
Mae 'r paragraff hwn yn gymwys os yw unigolyn wedi gwneud cais am dystysgrif y cyfeirir ati ym mharagraff 2 o Atodlen 2 ond nad yw 'r dystysgrif wedi 'i dyroddi .
< /sentence>
< sentence type="heading" elementid="20032527w-s205">
< strong elementid="20032527w-st15">
Y person cofrestredig - gofynion cyffredinol a hyfforddiant
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s206">
10 .
< /sentence>
< sentence type="normal" elementid="20032527w-s207">
 - 
< roundbracket elementid="20032527w-rb120">
(  1 )
< /roundbracket>
Rhaid i 'r darparydd cofrestredig a 'r rheolwr cofrestredig , o ystyried maint yr asiantaeth , ei datganiad o ddiben a nifer ac anghenion y defnyddwyr gwasanaeth , redeg yr asiantaeth , neu
< roundbracket elementid="20032527w-rb121">
(  yn ôl fel y digwydd )
< /roundbracket>
ei rheoli , â gofal , medrusrwydd a medr digonol .
< /sentence>
< sentence type="heading" elementid="20032527w-s208">
< roundbracket elementid="20032527w-rb122">
(  2 )
< /roundbracket>
Os yw 'r darparydd cofrestredig  - 
< /sentence>
< /p>
< p elementid="20032527w-p118">
< sentence type="heading" elementid="20032527w-s209">
< roundbracket elementid="20032527w-rb123">
(  a )
< /roundbracket>
yn unigolyn , rhaid iddo ymgymryd ; neu
< /sentence>
< sentence type="heading" elementid="20032527w-s210">
< roundbracket elementid="20032527w-rb124">
(  b )
< /roundbracket>
yn gorff , rhaid iddo sicrhau bod yr unigolyn cyfrifol yn ymgymryd,
< /sentence>
< /p>
< p elementid="20032527w-p119">
< sentence type="normal" elementid="20032527w-s211">
o dro i dro â 'r hyfforddiant sy 'n briodol i sicrhau bod ganddo 'r profiad a 'r medrau sy 'n angenrheidiol ar gyfer rhedeg yr asiantaeth .
< /sentence>
< sentence type="normal" elementid="20032527w-s212">
< roundbracket elementid="20032527w-rb125">
(  3 )
< /roundbracket>
Rhaid i 'r rheolwr cofrestredig ymgymryd o dro i dro â 'r hyfforddiant sy 'n briodol i sicrhau bod ganddo 'r profiad a 'r medrau sy 'n angenrheidiol ar gyfer rheoli 'r asiantaeth .
< /sentence>
< sentence type="heading" elementid="20032527w-s213">
< strong elementid="20032527w-st16">
Hysbysu o dramgwyddau
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s214">
11 .
< /sentence>
< sentence type="heading" elementid="20032527w-s215">
Os yw 'r person cofrestredig neu 'r unigolyn cyfrifol wedi 'i gollfarnu o unrhyw dramgwydd troseddol , boed hwnnw wedi 'i gyflawni yng Nghymru neu mewn man arall , rhaid iddo roi hysbysiad ysgrifenedig ar unwaith i swyddfa briodol y Cynulliad Cenedlaethol  - 
< /sentence>
< /p>
< p elementid="20032527w-p120">
< sentence type="heading" elementid="20032527w-s216">
< roundbracket elementid="20032527w-rb126">
(  i )
< /roundbracket>
o ddyddiad a man y collfarniad ;
< /sentence>
< sentence type="heading" elementid="20032527w-s217">
< roundbracket elementid="20032527w-rb127">
(  ii )
< /roundbracket>
o 'r tramgwydd y mae wedi 'i gollfarnu ohono ; a
< /sentence>
< sentence type="normal" elementid="20032527w-s218">
< roundbracket elementid="20032527w-rb128">
(  iii )
< /roundbracket>
o 'r gosb a osodwyd mewn perthynas â 'r tramgwydd .
< /sentence>
< /p>
< p elementid="20032527w-p121">
< sentence type="heading" elementid="20032527w-s219">
RHAN III
< /sentence>
< sentence type="heading" elementid="20032527w-s220">
RHEDEG ASIANTAETHAU NYRSYS
< /sentence>
< sentence type="heading" elementid="20032527w-s221">
PENNOD 1
< /sentence>
< sentence type="heading" elementid="20032527w-s222">
ANSAWDD Y GWASANAETH SY 'N CAEL EI DDARPARU
< /sentence>
< sentence type="heading" elementid="20032527w-s223">
< strong elementid="20032527w-st17">
Ffitrwydd y nyrsys sy 'n cael eu cyflenwi gan asiantaeth
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s224">
12 .
< /sentence>
< sentence type="heading" elementid="20032527w-s225">
 - 
< roundbracket elementid="20032527w-rb129">
(  1 )
< /roundbracket>
Rhaid i 'r person cofrestredig sicrhau na fydd unrhyw nyrs yn cael ei chyflenwi gan yr asiantaeth oni bai  - 
< /sentence>
< sentence type="heading" elementid="20032527w-s226">
< roundbracket elementid="20032527w-rb130">
(  a )
< /roundbracket>
ei bod yn berson addas o ran ei gonestrwydd a 'i chymeriad da ;
< /sentence>
< sentence type="heading" elementid="20032527w-s227">
< roundbracket elementid="20032527w-rb131">
(  b )
< /roundbracket>
bod ganddi 'r cymwysterau , y medrau a 'r profiad sy 'n angenrheidiol ar gyfer y gwaith y mae i 'w gyflawni ;
< /sentence>
< sentence type="heading" elementid="20032527w-s228">
< roundbracket elementid="20032527w-rb132">
(  c )
< /roundbracket>
ei bod yn ffit yn gorfforol ac yn feddyliol ar gyfer y gwaith hwnnw ; ac
< /sentence>
< sentence type="normal" elementid="20032527w-s229">
< roundbracket elementid="20032527w-rb133">
(  ch )
< /roundbracket>
bod gwybodaeth lawn a boddhaol ar gael amdani ar gyfer pob un o 'r materion a bennir yn Atodlen 3 .
< /sentence>
< /p>
< p elementid="20032527w-p122">
< sentence type="heading" elementid="20032527w-s230">
< roundbracket elementid="20032527w-rb134">
(  2 )
< /roundbracket>
Rhaid i 'r person cofrestredig sicrhau bod gwaith dethol nyrs i 'w chyflenwi yn cael ei wneud gan nyrs neu o dan ei goruchwyliaeth a bod gwybodaeth lawn a boddhaol ar gael am y nyrs sy 'n gwneud y gwaith dethol  - 
< /sentence>
< /p>
< p elementid="20032527w-p123">
< sentence type="heading" elementid="20032527w-s231">
< roundbracket elementid="20032527w-rb135">
(  i )
< /roundbracket>
ac eithrio os yw paragraff
< roundbracket elementid="20032527w-rb136">
(  3 )
< /roundbracket>
yn gymwys , am bob mater a bennir ym mharagraffau 1 i 8 o Atodlen 2 ;
< /sentence>
< sentence type="normal" elementid="20032527w-s232">
< roundbracket elementid="20032527w-rb137">
(  ii )
< /roundbracket>
os yw paragraff
< roundbracket elementid="20032527w-rb138">
(  3 )
< /roundbracket>
yn gymwys , am bob mater a bennir ym mharagraffau 1 a 3 i 9 o Atodlen 2 .
< /sentence>
< /p>
< p elementid="20032527w-p124">
< sentence type="normal" elementid="20032527w-s233">
< roundbracket elementid="20032527w-rb139">
(  3 )
< /roundbracket>
Mae 'r paragraff hwn yn gymwys os yw unigolyn wedi gwneud cais am dystysgrif y cyfeirir ati ym mharagraff 2 o Atodlen 2 ond nad yw 'r dystysgrif wedi 'i dyroddi .
< /sentence>
< sentence type="normal" elementid="20032527w-s234">
< roundbracket elementid="20032527w-rb140">
(  4 )
< /roundbracket>
Rhaid i 'r person cofrestredig sicrhau bod pob nyrs sy 'n cael ei chyflenwi gan yr asiantaeth pan fydd yn gweithredu fel busnes cyflogi
< squarebracket elementid="20032527w-sb6">
[ 
< a type="href" elementid="20032527w-a79">
6
< /a>
]
< /squarebracket>
yn cael ei chyfarwyddo bod rhaid iddi wisgo bob amser , pan fydd yn gweithio i ddefnyddiwr gwasanaeth , gerdyn adnabod sy 'n dangos ei henw , enw 'r asiantaeth a ffotograff diweddar .
< /sentence>
< sentence type="heading" elementid="20032527w-s235">
< strong elementid="20032527w-st18">
Polisïau a gweithdrefnau
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s236">
13 .
< /sentence>
< sentence type="normal" elementid="20032527w-s237">
 - 
< roundbracket elementid="20032527w-rb141">
(  1 )
< /roundbracket>
Mae paragraffau
< roundbracket elementid="20032527w-rb142">
(  2 )
< /roundbracket>
a
< roundbracket elementid="20032527w-rb143">
(  3 )
< /roundbracket>
yn gymwys pan fydd asiantaeth sy 'n gweithredu fel busnes cyflogi yn cyflenwi nyrs i ddarparu gofal nyrsio ym mhreswylfa breifat defnyddiwr gwasanaeth neu glaf .
< /sentence>
< sentence type="heading" elementid="20032527w-s238">
< roundbracket elementid="20032527w-rb144">
(  2 )
< /roundbracket>
Rhaid i 'r person cofrestredig baratoi polisïau ysgrifenedig a 'u rhoi ar waith mewn perthynas â 'r canlynol  - 
< /sentence>
< /p>
< p elementid="20032527w-p125">
< sentence type="heading" elementid="20032527w-s239">
< roundbracket elementid="20032527w-rb145">
(  a )
< /roundbracket>
sicrhau bod y gwasanaethau sy 'n cael eu darparu i bob claf yn cyd-fynd â 'r datganiad o ddiben ac yn diwallu anghenion unigol y claf hwnnw ;
< /sentence>
< sentence type="heading" elementid="20032527w-s240">
< roundbracket elementid="20032527w-rb146">
(  b )
< /roundbracket>
o dan ba amgylchiadau y caiff nyrsys roi meddyginiaeth i 'r claf neu helpu i 'w rhoi ;
< /sentence>
< sentence type="heading" elementid="20032527w-s241">
< roundbracket elementid="20032527w-rb147">
(  c )
< /roundbracket>
y tasgau eraill y caiff neu na chaiff nyrsys eu cyflawni mewn cysylltiad â gofal claf , a 'r tasgau na chaniateir eu cyflawni os nad yw 'r nyrs wedi cael hyfforddiant arbenigol ;
< /sentence>
< sentence type="heading" elementid="20032527w-s242">
< roundbracket elementid="20032527w-rb148">
(  ch )
< /roundbracket>
trefniadau i helpu cleifion yngl n â materion symudedd yn eu cartrefi , yn ôl yr angen ;
< /sentence>
< sentence type="heading" elementid="20032527w-s243">
< roundbracket elementid="20032527w-rb149">
(  d )
< /roundbracket>
mesurau i sicrhau diogelwch y claf a diogelu ei eiddo ;
< /sentence>
< sentence type="heading" elementid="20032527w-s244">
< roundbracket elementid="20032527w-rb150">
(  dd )
< /roundbracket>
trefniadau i sicrhau bod preifatrwydd , urddas a dymuniadau 'r claf yn cael eu parchu ;
< /sentence>
< sentence type="heading" elementid="20032527w-s245">
< roundbracket elementid="20032527w-rb151">
(  e )
< /roundbracket>
mesurau i ddiogelu 'r claf rhag camdriniaeth neu esgeulustod ;
< /sentence>
< sentence type="heading" elementid="20032527w-s246">
< roundbracket elementid="20032527w-rb152">
(  f )
< /roundbracket>
mesurau i ddiogelu nyrsys rhag camdriniaeth neu niwed arall ;
< /sentence>
< sentence type="normal" elementid="20032527w-s247">
< roundbracket elementid="20032527w-rb153">
(  ff )
< /roundbracket>
y weithdrefn sydd i 'w dilyn ar ôl i honiad o gamdriniaeth , esgeulustod neu niwed arall gael ei wneud .
< /sentence>
< /p>
< p elementid="20032527w-p126">
< sentence type="heading" elementid="20032527w-s248">
< roundbracket elementid="20032527w-rb154">
(  3 )
< /roundbracket>
Rhaid i 'r weithdrefn y cyfeiriwyd ati ym mharagraff
< roundbracket elementid="20032527w-rb155">
(  2 )
< /roundbracket>
< roundbracket elementid="20032527w-rb156">
(  ff )
< /roundbracket>
ddarparu yn benodol ar gyfer y canlynol  - 
< /sentence>
< /p>
< p elementid="20032527w-p127">
< sentence type="heading" elementid="20032527w-s249">
< roundbracket elementid="20032527w-rb157">
(  a )
< /roundbracket>
bod cofnodion ysgrifenedig yn cael eu cadw o unrhyw honiad o gamdriniaeth , esgeulustod neu niwed arall ac o 'r camau a gymerwyd mewn ymateb i hynny ; a
< /sentence>
< sentence type="heading" elementid="20032527w-s250">
< roundbracket elementid="20032527w-rb158">
(  b )
< /roundbracket>
bod swyddfa briodol y Cynulliad Cenedlaethol yn cael gwybod o unrhyw ddigwyddiad yr hysbyswyd yr heddlu ohono , a hynny heb fod yn hwyrach na 24 awr ar ôl i 'r person cofrestredig  - 
< /sentence>
< sentence type="heading" elementid="20032527w-s251">
< roundbracket elementid="20032527w-rb159">
(  i )
< /roundbracket>
hysbysu 'r heddlu o 'r mater ; neu
< /sentence>
< sentence type="normal" elementid="20032527w-s252">
< roundbracket elementid="20032527w-rb160">
(  ii )
< /roundbracket>
cael gwybod yr hysbyswyd yr heddlu o 'r mater .
< /sentence>
< /p>
< p elementid="20032527w-p128">
< sentence type="normal" elementid="20032527w-s253">
< roundbracket elementid="20032527w-rb161">
(  4 )
< /roundbracket>
Rhaid i 'r person cofrestredig sicrhau na fydd unrhyw wybodaeth bersonol am glaf y mae nyrs wedi 'i chyflenwi gan yr asiantaeth ar ei gyfer yn cael ei datgelu i unrhyw aelod o staff yr asiantaeth oni bai ei bod yn angenrheidiol gwneud hynny er mwyn darparu gwasanaeth effeithiol i 'r claf .
< /sentence>
< sentence type="heading" elementid="20032527w-s254">
< strong elementid="20032527w-st19">
Staffio
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s255">
14 .
< /sentence>
< sentence type="normal" elementid="20032527w-s256">
 - 
< roundbracket elementid="20032527w-rb162">
(  1 )
< /roundbracket>
Os yw asiantaeth yn gweithredu fel busnes cyflogi , rhaid i 'r person cofrestredig , o ystyried maint yr asiantaeth , ei datganiad o ddiben a nifer ac anghenion y defnyddwyr gwasanaeth , gymryd pob mesur rhesymol i sicrhau bod nifer priodol o bersonau gyda chymwysterau , medrau a phrofiad addas yn cael eu cyflogi at ddibenion yr asiantaeth .
< /sentence>
< sentence type="heading" elementid="20032527w-s257">
< roundbracket elementid="20032527w-rb163">
(  2 )
< /roundbracket>
Rhaid i 'r person cofrestredig sicrhau bod pob un o gyflogeion yr asiantaeth  - 
< /sentence>
< /p>
< p elementid="20032527w-p129">
< sentence type="heading" elementid="20032527w-s258">
< roundbracket elementid="20032527w-rb164">
(  a )
< /roundbracket>
yn cael ei oruchwylio 'n briodol ; a
< /sentence>
< sentence type="normal" elementid="20032527w-s259">
< roundbracket elementid="20032527w-rb165">
(  b )
< /roundbracket>
yn cael disgrifiad swydd sy 'n amlinellu ei gyfrifoldebau .
< /sentence>
< /p>
< p elementid="20032527w-p130">
< sentence type="normal" elementid="20032527w-s260">
< roundbracket elementid="20032527w-rb166">
(  3 )
< /roundbracket>
Rhaid i 'r person cofrestredig sefydlu gweithdrefn ar gyfer casglu gwybodaeth oddi wrth ddefnyddwyr gwasanaeth am berfformiad y nyrsys sy 'n cael eu cyflogi at ddibenion yr asiantaeth , a rhaid iddo gymryd unrhyw gamau sy 'n angenrheidiol i fynd i 'r afael ag unrhyw agwedd ar arferion clinigol nyrs .
< /sentence>
< sentence type="normal" elementid="20032527w-s261">
< roundbracket elementid="20032527w-rb167">
(  4 )
< /roundbracket>
Rhaid i 'r person cofrestredig ddarparu i bob nyrs sy 'n cael ei chyflogi at ddibenion yr asiantaeth ddatganiad ysgrifenedig ar y telerau a 'r amodau y bydd yn cael ei chyflenwi i weithio odanynt i ddefnyddiwr gwasanaeth , ac o dan ei reolaeth .
< /sentence>
< sentence type="normal" elementid="20032527w-s262">
< roundbracket elementid="20032527w-rb168">
(  5 )
< /roundbracket>
Rhaid i 'r datganiad o delerau ac amodau a ddarperir o dan baragraff
< roundbracket elementid="20032527w-rb169">
(  4 )
< /roundbracket>
bennu , yn benodol , statws cyflogaeth y nyrs .
< /sentence>
< sentence type="heading" elementid="20032527w-s263">
< strong elementid="20032527w-st20">
Y llawlyfr staff
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s264">
15 .
< /sentence>
< sentence type="normal" elementid="20032527w-s265">
 - 
< roundbracket elementid="20032527w-rb170">
(  1 )
< /roundbracket>
Pan fydd yr asiantaeth yn gweithredu fel busnes cyflogi , rhaid i 'r person cofrestredig baratoi llawlyfr staff a darparu copi i bob aelod o 'r staff .
< /sentence>
< sentence type="heading" elementid="20032527w-s266">
< roundbracket elementid="20032527w-rb171">
(  2 )
< /roundbracket>
Rhaid i 'r llawlyfr gael ei baratoi yn unol â pharagraff
< roundbracket elementid="20032527w-rb172">
(  1 )
< /roundbracket>
a rhaid iddo gynnwys datganiad ynghylch  - 
< /sentence>
< /p>
< p elementid="20032527w-p131">
< sentence type="heading" elementid="20032527w-s267">
< roundbracket elementid="20032527w-rb173">
(  a )
< /roundbracket>
yr ymddygiad a ddisgwylir oddi wrth y staff , a 'r camau disgyblu y gellir eu cymryd yn eu herbyn ;
< /sentence>
< sentence type="heading" elementid="20032527w-s268">
< roundbracket elementid="20032527w-rb174">
(  b )
< /roundbracket>
rôl a chyfrifoldebau 'r nyrsys a 'r staff eraill ;
< /sentence>
< sentence type="heading" elementid="20032527w-s269">
< roundbracket elementid="20032527w-rb175">
(  c )
< /roundbracket>
y gofynion yngl n â chadw cofnodion ;
< /sentence>
< sentence type="heading" elementid="20032527w-s270">
< roundbracket elementid="20032527w-rb176">
(  ch )
< /roundbracket>
gweithdrefnau recriwtio ; a
< /sentence>
< sentence type="normal" elementid="20032527w-s271">
< roundbracket elementid="20032527w-rb177">
(  d )
< /roundbracket>
gofynion a chyfleoedd hyfforddi a datblygu .
< /sentence>
< /p>
< p elementid="20032527w-p132">
< sentence type="heading" elementid="20032527w-s272">
< strong elementid="20032527w-st21">
Darparu gwybodaeth i ddefnyddwyr gwasanaeth
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s273">
16 .
< /sentence>
< sentence type="heading" elementid="20032527w-s274">
 - 
< roundbracket elementid="20032527w-rb178">
(  1 )
< /roundbracket>
Rhaid i 'r person cofrestredig sicrhau bod y defnyddiwr gwasanaeth yn cael ei hysbysu , cyn bod nyrs yn cael ei chyflenwi  - 
< /sentence>
< /p>
< p elementid="20032527w-p133">
< sentence type="heading" elementid="20032527w-s275">
< roundbracket elementid="20032527w-rb179">
(  a )
< /roundbracket>
o enw 'r nyrs sydd i 'w chyflenwi a 'r dull o gysylltu â 'r nyrs honno ;
< /sentence>
< sentence type="heading" elementid="20032527w-s276">
< roundbracket elementid="20032527w-rb180">
(  b )
< /roundbracket>
o enw 'r aelod o staff yr asiantaeth sy 'n gyfrifol am gyflenwi 'r nyrs honno ; ac
< /sentence>
< sentence type="normal" elementid="20032527w-s277">
< roundbracket elementid="20032527w-rb181">
(  c )
< /roundbracket>
os yw 'r asiantaeth yn gweithredu fel busnes cyflogi , o 'r manylion yngl n â 'r ffordd y gall y defnyddiwr gwasanaeth gysylltu â 'r person cofrestredig , neu berson sydd wedi 'i enwi i weithredu ar ran y person cofrestredig .
< /sentence>
< /p>
< p elementid="20032527w-p134">
< sentence type="normal" elementid="20032527w-s278">
< roundbracket elementid="20032527w-rb182">
(  2 )
< /roundbracket>
Os y claf yw 'r defnyddiwr gwasanaeth hefyd , rhaid i 'r person cofrestredig sicrhau bod yr wybodaeth a bennwyd ym mharagraff
< roundbracket elementid="20032527w-rb183">
(  1 )
< /roundbracket>
yn cael ei darparu , os yw 'n briodol , i 'r person sy 'n gweithredu ar ran y claf .
< /sentence>
< sentence type="heading" elementid="20032527w-s279">
< strong elementid="20032527w-st22">
Cofnodion
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s280">
17 .
< /sentence>
< sentence type="heading" elementid="20032527w-s281">
Rhaid i 'r person cofrestredig sicrhau bod y cofnodion a bennir yn Atodlen 4 yn cael eu cadw a 'u bod  - 
< /sentence>
< /p>
< p elementid="20032527w-p135">
< sentence type="heading" elementid="20032527w-s282">
< roundbracket elementid="20032527w-rb184">
(  a )
< /roundbracket>
yn cael eu cadw 'n gyfoes , mewn cyflwr da ac mewn modd diogel ; a
< /sentence>
< sentence type="normal" elementid="20032527w-s283">
< roundbracket elementid="20032527w-rb185">
(  b )
< /roundbracket>
yn cael eu cadw am gyfnod heb fod yn llai na thair blynedd gan ddechrau ar ddyddiad yr eitem ddiwethaf .
< /sentence>
< /p>
< p elementid="20032527w-p136">
< sentence type="heading" elementid="20032527w-s284">
< strong elementid="20032527w-st23">
Cwynion
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s285">
18 .
< /sentence>
< sentence type="normal" elementid="20032527w-s286">
 - 
< roundbracket elementid="20032527w-rb186">
(  1 )
< /roundbracket>
Rhaid i 'r person cofrestredig sefydlu gweithdrefn
< roundbracket elementid="20032527w-rb187">
(
< doublequotation elementid="20032527w-dq50">
"  y weithdrefn gwyno "
< /doublequotation>
)
< /roundbracket>
ar gyfer ystyried cwynion sy 'n cael eu gwneud i 'r person cofrestredig gan ddefnyddiwr gwasanaeth neu berson sy 'n gweithredu ar ran y defnyddiwr gwasanaeth .
< /sentence>
< sentence type="normal" elementid="20032527w-s287">
< roundbracket elementid="20032527w-rb188">
(  2 )
< /roundbracket>
Rhaid i 'r person cofrestredig ddarparu copi ysgrifenedig o 'r weithdrefn gwyno i bob defnyddiwr gwasanaeth ac , os gofynnir iddo wneud hynny , i unrhyw berson sy 'n gweithredu ar ran defnyddiwr gwasanaeth .
< /sentence>
< sentence type="heading" elementid="20032527w-s288">
< roundbracket elementid="20032527w-rb189">
(  3 )
< /roundbracket>
Rhaid i 'r copi ysgrifenedig o 'r weithdrefn gwyno gynnwys  - 
< /sentence>
< /p>
< p elementid="20032527w-p137">
< sentence type="heading" elementid="20032527w-s289">
< roundbracket elementid="20032527w-rb190">
(  a )
< /roundbracket>
enw a Rhif ffôn unrhyw swyddfa briodol benodedig y Cynulliad Cenedlaethol ; a
< /sentence>
< sentence type="normal" elementid="20032527w-s290">
< roundbracket elementid="20032527w-rb191">
(  b )
< /roundbracket>
y weithdrefn
< roundbracket elementid="20032527w-rb192">
(  os oes un )
< /roundbracket>
y mae swyddfa briodol y Cynulliad Cenedlaethol wedi hysbysu 'r person cofrestredig ohoni ar gyfer gwneud cwynion i swyddfa briodol y Cynulliad Cenedlaethol yngl n â 'r asiantaeth .
< /sentence>
< /p>
< p elementid="20032527w-p138">
< sentence type="normal" elementid="20032527w-s291">
< roundbracket elementid="20032527w-rb193">
(  4 )
< /roundbracket>
Rhaid i 'r person cofrestredig sicrhau bod ymchwiliad llawn yn cael ei gynnal i bob cwyn sy 'n cael ei gwneud o dan y weithdrefn gwyno .
< /sentence>
< sentence type="normal" elementid="20032527w-s292">
< roundbracket elementid="20032527w-rb194">
(  5 )
< /roundbracket>
Rhaid i 'r person cofrestredig , o fewn y cyfnod o 28 diwrnod gan ddechrau ar y dyddiad y cafodd y g yn ei gwneud , roi gwybod i 'r person a wnaeth y g yn am y camau sydd i 'w cymryd mewn ymateb i hynny , neu unrhyw gyfnod byrrach a fydd yn rhesymol o dan yr amgylchiadau .
< /sentence>
< sentence type="normal" elementid="20032527w-s293">
< roundbracket elementid="20032527w-rb195">
(  6 )
< /roundbracket>
Rhaid i 'r person cofrestredig gadw cofnod o bob cwyn , gan gynnwys manylion yr ymchwiliad a wnaed , y canlyniad ac unrhyw gamau a gymerwyd o ganlyniad i hynny a bydd gofynion rheoliad 17 yn gymwys i 'r cofnod hwnnw .
< /sentence>
< sentence type="normal" elementid="20032527w-s294">
< roundbracket elementid="20032527w-rb196">
(  7 )
< /roundbracket>
Rhaid i 'r person cofrestredig roi i swyddfa briodol y Cynulliad Cenedlaethol bob blwyddyn ddatganiad sy 'n cynnwys crynodeb o 'r cwynion a wnaed yn ystod y 12 mis blaenorol a 'r camau a gymerwyd mewn ymateb i hynny .
< /sentence>
< sentence type="normal" elementid="20032527w-s295">
< roundbracket elementid="20032527w-rb197">
(  8 )
< /roundbracket>
Rhaid i 'r person cofrestredig sicrhau bod adroddiad yn cael ei gyflwyno yn brydlon ac yn ysgrifenedig i 'r Cyngor Nyrsio a Bydwreigiaeth ar unrhyw dystiolaeth am gamymddygiad gan nyrs
< squarebracket elementid="20032527w-sb7">
[ 
< a type="href" elementid="20032527w-a80">
7
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="heading" elementid="20032527w-s296">
< strong elementid="20032527w-st24">
Adolygu ansawdd y gwasanaeth sy 'n cael ei ddarparu
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s297">
19 .
< /sentence>
< sentence type="normal" elementid="20032527w-s298">
 - 
< roundbracket elementid="20032527w-rb198">
(  1 )
< /roundbracket>
Rhaid i 'r person cofrestredig gyflwyno a chynnal system ar gyfer adolygu bob hyn a hyn fel y bo 'n briodol ansawdd y gwasanaethau sy 'n cael eu darparu gan yr asiantaeth .
< /sentence>
< sentence type="normal" elementid="20032527w-s299">
< roundbracket elementid="20032527w-rb199">
(  2 )
< /roundbracket>
Rhaid i 'r person cofrestredig gyflwyno adroddiad i swyddfa briodol y Cynulliad Cenedlaethol mewn perthynas ag unrhyw adolygiad y mae 'r person cofrestredig wedi 'i gynnal at ddibenion paragraff
< roundbracket elementid="20032527w-rb200">
(  1 )
< /roundbracket>
, a threfnu bod copi o 'r adroddiad ar gael i 'r defnyddwyr gwasanaeth a 'r personau sy 'n gweithredu ar ran y defnyddwyr gwasanaeth ei archwilio , os byddant yn gofyn amdano .
< /sentence>
< sentence type="normal" elementid="20032527w-s300">
< roundbracket elementid="20032527w-rb201">
(  3 )
< /roundbracket>
Rhaid i 'r system y cyfeirir ati ym mharagraff
< roundbracket elementid="20032527w-rb202">
(  1 )
< /roundbracket>
ddarparu ar gyfer ymgynghori â 'r defnyddwyr gwasanaeth a 'r personau sy 'n gweithredu ar ran y defnyddwyr gwasanaeth .
< /sentence>
< /p>
< p elementid="20032527w-p139">
< sentence type="heading" elementid="20032527w-s301">
PENNOD 2
< /sentence>
< sentence type="heading" elementid="20032527w-s302">
Y SAFLE
< /sentence>
< sentence type="heading" elementid="20032527w-s303">
< strong elementid="20032527w-st25">
Ffitrwydd y safle
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s304">
20 .
< /sentence>
< sentence type="normal" elementid="20032527w-s305">
Rhaid i 'r person cofrestredig beidio â defnyddio safle at ddibenion asiantaeth oni bai bod y safle yn addas ar gyfer cyflawni nodau ac amcanion yr asiantaeth sydd wedi 'u nodi yn y datganiad o ddiben .
< /sentence>
< sentence type="heading" elementid="20032527w-s306">
PENNOD 3
< /sentence>
< sentence type="heading" elementid="20032527w-s307">
MATERION ARIANNOL
< /sentence>
< sentence type="heading" elementid="20032527w-s308">
< strong elementid="20032527w-st26">
Y sefyllfa ariannol
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s309">
21 .
< /sentence>
< sentence type="normal" elementid="20032527w-s310">
 - 
< roundbracket elementid="20032527w-rb203">
(  1 )
< /roundbracket>
Rhaid i 'r darparydd cofrestredig redeg yr asiantaeth mewn modd sy 'n debyg o sicrhau y bydd yr asiantaeth yn hyfyw yn ariannol er mwyn cyflawni 'r nodau a 'r amcanion sydd wedi 'u nodi yn y datganiad o ddiben .
< /sentence>
< sentence type="heading" elementid="20032527w-s311">
< roundbracket elementid="20032527w-rb204">
(  2 )
< /roundbracket>
Os bydd swyddfa briodol y Cynulliad Cenedlaethol yn gofyn amdanynt , rhaid i 'r person cofrestredig ddarparu i 'r swyddfa honno unrhyw wybodaeth a dogfennau y mae arni eu hangen er mwyn ystyried hyfywedd ariannol yr asiantaeth , gan gynnwys  - 
< /sentence>
< sentence type="heading" elementid="20032527w-s312">
< roundbracket elementid="20032527w-rb205">
(  a )
< /roundbracket>
cyfrifon blynyddol yr asiantaeth wedi 'u hardystio gan gyfrifydd ; a
< /sentence>
< sentence type="normal" elementid="20032527w-s313">
< roundbracket elementid="20032527w-rb206">
(  b )
< /roundbracket>
tystysgrif yswiriant i 'r darparydd cofrestredig ar gyfer atebolrwydd a allai ddod i 'w ran mewn perthynas â 'r asiantaeth yngl n â marwolaeth , niwed , atebolrwydd cyhoeddus , difrod neu golled arall .
< /sentence>
< /p>
< p elementid="20032527w-p140">
< sentence type="heading" elementid="20032527w-s314">
PENNOD 4
< /sentence>
< sentence type="heading" elementid="20032527w-s315">
YR HYSBYSIADAU SYDD I 'W RHOI I 'R CYNULLIAD CENEDLAETHOL
< /sentence>
< sentence type="heading" elementid="20032527w-s316">
< strong elementid="20032527w-st27">
Hysbysu o absenoldeb
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s317">
22 .
< /sentence>
< sentence type="heading" elementid="20032527w-s318">
 - 
< roundbracket elementid="20032527w-rb207">
(  1 )
< /roundbracket>
Os yw  - 
< /sentence>
< sentence type="heading" elementid="20032527w-s319">
< roundbracket elementid="20032527w-rb208">
(  a )
< /roundbracket>
y darparydd cofrestredig , a hwnnw 'n unigolyn â gofal amser-llawn o ddydd i ddydd dros yr asiantaeth ; neu
< /sentence>
< sentence type="heading" elementid="20032527w-s320">
< roundbracket elementid="20032527w-rb209">
(  b )
< /roundbracket>
y rheolwr cofrestredig,
< /sentence>
< /p>
< p elementid="20032527w-p141">
< sentence type="normal" elementid="20032527w-s321">
yn bwriadu bod yn absennol o 'r asiantaeth am gyfnod parhaus o 28 diwrnod neu fwy , rhaid i 'r person cofrestredig roi hysbysiad ysgrifenedig i swyddfa briodol y Cynulliad Cenedlaethol o 'r absenoldeb arfaethedig .
< /sentence>
< sentence type="heading" elementid="20032527w-s322">
< roundbracket elementid="20032527w-rb210">
(  2 )
< /roundbracket>
Ac eithrio mewn argyfwng , rhaid i 'r hysbysiad y cyfeirwiyd ato ym mharagraff
< roundbracket elementid="20032527w-rb211">
(  1 )
< /roundbracket>
gael ei roi heb fod yn hwyrach nag un mis cyn i 'r absenoldeb arfaethedig ddechrau neu o fewn unrhyw gyfnod byrrach y cytunir arno gyda swyddfa briodol y Cynulliad Cenedlaethol a rhaid i 'r hysbysiad bennu  - 
< /sentence>
< /p>
< p elementid="20032527w-p142">
< sentence type="heading" elementid="20032527w-s323">
< roundbracket elementid="20032527w-rb212">
(  a )
< /roundbracket>
pa mor hir y bydd yr absenoldeb arfaethedig neu pa mor hir y disgwylir iddo fod ;
< /sentence>
< sentence type="heading" elementid="20032527w-s324">
< roundbracket elementid="20032527w-rb213">
(  b )
< /roundbracket>
y rheswm dros yr absenoldeb ;
< /sentence>
< sentence type="heading" elementid="20032527w-s325">
< roundbracket elementid="20032527w-rb214">
(  c )
< /roundbracket>
y trefniadau sydd wedi 'u gwneud ar gyfer rhedeg yr asiantaeth yn ystod yr absenoldeb hwnnw ;
< /sentence>
< sentence type="heading" elementid="20032527w-s326">
< roundbracket elementid="20032527w-rb215">
(  ch )
< /roundbracket>
enw , cyfeiriad a chymwysterau 'r person a fydd yn gyfrifol am yr asiantaeth yn ystod yr absenoldeb hwnnw ; a
< /sentence>
< sentence type="normal" elementid="20032527w-s327">
< roundbracket elementid="20032527w-rb216">
(  d )
< /roundbracket>
yn achos absenoldeb y rheolwr cofrestredig , y trefniadau sydd wedi 'u gwneud , neu y bwriedir eu gwneud , ar gyfer penodi person arall i reoli 'r asiantaeth yn ystod yr absenoldeb hwnnw , gan gynnwys y dyddiad arfaethedig erbyn pryd y mae 'r penodiad i 'w wneud .
< /sentence>
< /p>
< p elementid="20032527w-p143">
< sentence type="normal" elementid="20032527w-s328">
< roundbracket elementid="20032527w-rb217">
(  3 )
< /roundbracket>
Os yw 'r absenoldeb yn codi yn sgil argyfwng , rhaid i 'r person cofrestredig roi hysbysiad o 'r absenoldeb o fewn un wythnos ar ôl i 'r argyfwng ddigwydd gan bennu 'r materion a nodwyd ym mharagraff
< roundbracket elementid="20032527w-rb218">
(  2 )
< /roundbracket>
< roundbracket elementid="20032527w-rb219">
(  a )
< /roundbracket>
i
< roundbracket elementid="20032527w-rb220">
(  d )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20032527w-s329">
< roundbracket elementid="20032527w-rb221">
(  4 )
< /roundbracket>
Os yw  - 
< /sentence>
< /p>
< p elementid="20032527w-p144">
< sentence type="heading" elementid="20032527w-s330">
< roundbracket elementid="20032527w-rb222">
(  a )
< /roundbracket>
y darparydd cofrestredig , a hwnnw 'n unigolyn â gofal amser-llawn o ddydd i ddydd dros yr asiantaeth ; neu
< /sentence>
< sentence type="heading" elementid="20032527w-s331">
< roundbracket elementid="20032527w-rb223">
(  b )
< /roundbracket>
y rheolwr cofrestredig,
< /sentence>
< /p>
< p elementid="20032527w-p145">
< sentence type="normal" elementid="20032527w-s332">
wedi bod yn absennol o 'r asiantaeth am gyfnod parhaus o 28 diwrnod neu fwy , ac na roddwyd hysbysiad o 'r absenoldeb i swyddfa briodol y Cynulliad Cenedlaethol , rhaid i 'r person cofrestredig roi hysbysiad ysgrifenedig yn ddi-oed i swyddfa briodol y Cynulliad Cenedlaethol o 'r absenoldeb , gan bennu 'r materion a nodwyd ym mharagraff
< roundbracket elementid="20032527w-rb224">
(  2 )
< /roundbracket>
< roundbracket elementid="20032527w-rb225">
(  a )
< /roundbracket>
i
< roundbracket elementid="20032527w-rb226">
(  d )
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20032527w-s333">
< roundbracket elementid="20032527w-rb227">
(  5 )
< /roundbracket>
Rhaid i 'r person cofrestredig hysbysu swyddfa briodol y Cynulliad Cenedlaethol fod y darparydd cofrestredig neu
< roundbracket elementid="20032527w-rb228">
(  yn ôl fel y digwydd )
< /roundbracket>
y rheolwr cofrestredig wedi dychwelyd i 'r gwaith a rhaid iddo hysbysu hyn heb fod yn hwyrach na saith diwrnod ar ôl dyddiad y dychweliad .
< /sentence>
< sentence type="heading" elementid="20032527w-s334">
< strong elementid="20032527w-st28">
Hysbysu o newidiadau
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s335">
23 .
< /sentence>
< sentence type="heading" elementid="20032527w-s336">
Rhaid i 'r person cofrestredig roi hysbysiad ysgrifenedig i swyddfa briodol y Cynulliad Cenedlaethol cyn gynted ag y bo 'n ymarferol gwneud hynny os yw unrhyw un o 'r pethau canlynol yn digwydd neu os bwriedir iddynt ddigwydd  - 
< /sentence>
< /p>
< p elementid="20032527w-p146">
< sentence type="heading" elementid="20032527w-s337">
< roundbracket elementid="20032527w-rb229">
(  a )
< /roundbracket>
bod person ac eithrio 'r person cofrestredig yn rhedeg neu 'n rheoli 'r asiantaeth ;
< /sentence>
< sentence type="heading" elementid="20032527w-s338">
< roundbracket elementid="20032527w-rb230">
(  b )
< /roundbracket>
bod person yn rhoi 'r gorau i redeg neu i reoli 'r asiantaeth ;
< /sentence>
< sentence type="heading" elementid="20032527w-s339">
< roundbracket elementid="20032527w-rb231">
(  c )
< /roundbracket>
os unigolyn yw 'r person cofrestredig , ei fod yn newid ei enw ;
< /sentence>
< sentence type="heading" elementid="20032527w-s340">
< roundbracket elementid="20032527w-rb232">
(  ch )
< /roundbracket>
os corff yw 'r darparydd cofrestredig  - 
< /sentence>
< sentence type="heading" elementid="20032527w-s341">
< roundbracket elementid="20032527w-rb233">
(  i )
< /roundbracket>
bod enw neu gyfeiriad y corff yn newid ;
< /sentence>
< sentence type="heading" elementid="20032527w-s342">
< roundbracket elementid="20032527w-rb234">
(  ii )
< /roundbracket>
bod unrhyw newid cyfarwyddwr , rheolwr , ysgrifennydd neu swyddog cyffelyb arall i 'r corff yn digwydd ;
< /sentence>
< sentence type="heading" elementid="20032527w-s343">
< roundbracket elementid="20032527w-rb235">
(  iii )
< /roundbracket>
bod unrhyw newid o ran pwy yw 'r unigolyn cyfrifol ;
< /sentence>
< /p>
< p elementid="20032527w-p147">
< sentence type="heading" elementid="20032527w-s344">
< roundbracket elementid="20032527w-rb236">
(  d )
< /roundbracket>
os unigolyn yw 'r darparydd cofrestredig , bod ymddiriedolwr mewn methdaliad yn cael ei benodi ;
< /sentence>
< sentence type="heading" elementid="20032527w-s345">
< roundbracket elementid="20032527w-rb237">
(  dd )
< /roundbracket>
os cwmni yw 'r darparydd cofrestredig , bod derbynnydd , rheolwr , datodwr neu ddatodwr dros dro yn cael ei benodi ;
< /sentence>
< sentence type="heading" elementid="20032527w-s346">
< roundbracket elementid="20032527w-rb238">
(  e )
< /roundbracket>
os yw darparydd cofrestredig mewn partneriaeth y mae ei busnes yn cynnwys rhedeg asiantaeth nyrsys , bod derbynnydd neu reolwr yn cael , neu 'n debyg o gael , ei benodi ar gyfer y bartneriaeth ; neu
< /sentence>
< sentence type="normal" elementid="20032527w-s347">
< roundbracket elementid="20032527w-rb239">
(  f )
< /roundbracket>
bod y darparydd cofrestredig yn caffael safle ychwanegol at ddibenion yr asiantaeth .
< /sentence>
< /p>
< p elementid="20032527w-p148">
< sentence type="normal" elementid="20032527w-s348">
Penodi datodwyr etc .
< /sentence>
< sentence type="normal" elementid="20032527w-s349">
24 .
< /sentence>
< sentence type="heading" elementid="20032527w-s350">
 - 
< roundbracket elementid="20032527w-rb240">
(  1 )
< /roundbracket>
Rhaid i unrhyw berson y mae paragraff
< roundbracket elementid="20032527w-rb241">
(  2 )
< /roundbracket>
yn gymwys iddo  - 
< /sentence>
< /p>
< p elementid="20032527w-p149">
< sentence type="heading" elementid="20032527w-s351">
< roundbracket elementid="20032527w-rb242">
(  a )
< /roundbracket>
hysbysu swyddfa briodol y Cynulliad Cenedlaethol ar unwaith o 'i benodiad gan nodi 'r rhesymau drosto ;
< /sentence>
< sentence type="heading" elementid="20032527w-s352">
< roundbracket elementid="20032527w-rb243">
(  b )
< /roundbracket>
penodi rheolwr i gymryd gofal amser-llawn o ddydd i ddydd dros yr asiantaeth mewn unrhyw achos lle nad oes rheolwr cofrestredig ; ac
< /sentence>
< sentence type="normal" elementid="20032527w-s353">
< roundbracket elementid="20032527w-rb244">
(  c )
< /roundbracket>
heb fod yn fwy na 28 diwrnod ar ôl y penodiad , hysbysu swyddfa briodol y Cynulliad Cenedlaethol o 'r dull arfaethedig o weithredu 'r asiantaeth yn y dyfodol .
< /sentence>
< /p>
< p elementid="20032527w-p150">
< sentence type="heading" elementid="20032527w-s354">
< roundbracket elementid="20032527w-rb245">
(  2 )
< /roundbracket>
Mae 'r paragraff hwn yn gymwys i unrhyw berson a benodir  - 
< /sentence>
< /p>
< p elementid="20032527w-p151">
< sentence type="heading" elementid="20032527w-s355">
< roundbracket elementid="20032527w-rb246">
(  a )
< /roundbracket>
yn dderbynydd neu 'n rheolwr eiddo cwmni sy 'n ddarparydd cofrestredig mewn perthynas ag asiantaeth ;
< /sentence>
< sentence type="heading" elementid="20032527w-s356">
< roundbracket elementid="20032527w-rb247">
(  b )
< /roundbracket>
yn ddatodwr neu 'n ddatodwr dros dro ar gwmni sy 'n ddarparydd cofrestredig mewn perthynas ag asiantaeth ;
< /sentence>
< sentence type="heading" elementid="20032527w-s357">
< roundbracket elementid="20032527w-rb248">
(  c )
< /roundbracket>
yn dderbynydd neu 'n rheolwr eiddo partneriaeth y mae ei busnes yn cynnwys rhedeg asiantaeth ;
< /sentence>
< sentence type="normal" elementid="20032527w-s358">
< roundbracket elementid="20032527w-rb249">
(  ch )
< /roundbracket>
yn ymddiriedolwr mewn methdaliad i ddarparydd cofrestredig mewn perthynas ag asiantaeth .
< /sentence>
< /p>
< p elementid="20032527w-p152">
< sentence type="heading" elementid="20032527w-s359">
< strong elementid="20032527w-st29">
Marwolaeth y person cofrestredig
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s360">
25 .
< /sentence>
< sentence type="normal" elementid="20032527w-s361">
 - 
< roundbracket elementid="20032527w-rb250">
(  1 )
< /roundbracket>
Os oes mwy nag un person wedi 'i gofrestru ar gyfer asiantaeth , a bod person cofrestredig yn marw , rhaid i 'r person cofrestredig sy 'n goroesi hysbysu yn ysgrifenedig swyddfa briodol y Cynulliad Cenedlaethol yn ddi-oed o 'r farwolaeth .
< /sentence>
< sentence type="heading" elementid="20032527w-s362">
< roundbracket elementid="20032527w-rb251">
(  2 )
< /roundbracket>
Os un person yn unig sydd wedi 'i gofrestru ar gyfer asiantaeth , a bod y person hwnnw yn marw , rhaid i 'w gynrychiolwyr personol hysbysu swyddfa briodol y Cynulliad Cenedlaethol yn ysgrifenedig  - 
< /sentence>
< /p>
< p elementid="20032527w-p153">
< sentence type="heading" elementid="20032527w-s363">
< roundbracket elementid="20032527w-rb252">
(  a )
< /roundbracket>
o 'r farwolaeth yn ddi-oed ; a
< /sentence>
< sentence type="normal" elementid="20032527w-s364">
< roundbracket elementid="20032527w-rb253">
(  b )
< /roundbracket>
o fewn 28 diwrnod o 'u bwriadau yngl n â rhedeg yr asiantaeth yn y dyfodol .
< /sentence>
< /p>
< p elementid="20032527w-p154">
< sentence type="heading" elementid="20032527w-s365">
< roundbracket elementid="20032527w-rb254">
(  3 )
< /roundbracket>
Caiff cynrychiolwyr personol y darparydd cofrestredig ymadawedig redeg yr asiantaeth heb fod wedi 'u cofrestru ar ei chyfer  - 
< /sentence>
< /p>
< p elementid="20032527w-p155">
< sentence type="heading" elementid="20032527w-s366">
< roundbracket elementid="20032527w-rb255">
(  a )
< /roundbracket>
am gyfnod heb fod yn hwy nag 28 diwrnod ; a
< /sentence>
< sentence type="normal" elementid="20032527w-s367">
< roundbracket elementid="20032527w-rb256">
(  b )
< /roundbracket>
am unrhyw gyfnod pellach a benderfynnir yn unol â pharagraff
< roundbracket elementid="20032527w-rb257">
(  4 )
< /roundbracket> .
< /sentence>
< /p>
< p elementid="20032527w-p156">
< sentence type="normal" elementid="20032527w-s368">
< roundbracket elementid="20032527w-rb258">
(  4 )
< /roundbracket>
Caiff y Cynulliad Cenedlaethol estyn y cyfnod a bennwyd ym mharagraff
< roundbracket elementid="20032527w-rb259">
(  3 )
< /roundbracket>
< roundbracket elementid="20032527w-rb260">
(  a )
< /roundbracket>
am unrhyw gyfnod pellach , heb fod yn hwy na blwyddyn , y bydd y Cynulliad Cenedlaethol yn penderfynu arno , a rhaid iddo hysbysu 'r cynrychiolwyr personol yn ysgrifenedig o unrhyw benderfyniad o 'r fath .
< /sentence>
< sentence type="normal" elementid="20032527w-s369">
< roundbracket elementid="20032527w-rb261">
(  5 )
< /roundbracket>
Rhaid i 'r cynrychiolwyr personol benodi person i gymryd gofal amser-llawn o ddydd i ddydd dros yr asiantaeth yn ystod unrhyw gyfnod pryd y byddant , yn unol â pharagraff
< roundbracket elementid="20032527w-rb262">
(  3 )
< /roundbracket>
, yn rhedeg yr asiantaeth , heb fod wedi 'u cofrestru ar ei chyfer .
< /sentence>
< /p>
< p elementid="20032527w-p157">
< sentence type="heading" elementid="20032527w-s370">
RHAN IV
< /sentence>
< sentence type="heading" elementid="20032527w-s371">
AMRYWIOL
< /sentence>
< sentence type="heading" elementid="20032527w-s372">
< strong elementid="20032527w-st30">
Cydymffurfio â 'r rheoliadau
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s373">
26 .
< /sentence>
< sentence type="normal" elementid="20032527w-s374">
Os oes mwy nag un person cofrestredig mewn perthynas ag asiantaeth , ni fydd yn ofynnol i unrhyw un o 'r personau cofrestredig wneud unrhyw beth y mae 'n ofynnol o dan y rheoliadau hyn iddo gael ei wneud gan y person cofrestredig os yw wedi 'i wneud gan un o 'r personau cofrestredig eraill .
< /sentence>
< sentence type="heading" elementid="20032527w-s375">
< strong elementid="20032527w-st31">
Tramgwyddau
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s376">
27 .
< /sentence>
< sentence type="normal" elementid="20032527w-s377">
 - 
< roundbracket elementid="20032527w-rb263">
(  1 )
< /roundbracket>
Bydd torri neu fethu â chydymffurfio ag unrhyw un o darpariaethau rheoliadau 4 i 23 yn dramgwydd .
< /sentence>
< sentence type="normal" elementid="20032527w-s378">
< roundbracket elementid="20032527w-rb264">
(  2 )
< /roundbracket>
Caiff y Cynulliad Cenedlaethol ddwyn achos yn erbyn y person a oedd ar un adeg , ond nad yw bellach , yn berson cofrestredig , mewn perthynas â methiant i gydymffurfio â rheoliad 17 .
< /sentence>
< sentence type="heading" elementid="20032527w-s379">
< strong elementid="20032527w-st32">
Ffioedd
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s380">
28 .
< /sentence>
< sentence type="normal" elementid="20032527w-s381">
 - 
< roundbracket elementid="20032527w-rb265">
(  1 )
< /roundbracket>
Mae Rheoliadau Cofrestru Gofal Cymdeithasol a Gofal Iechyd Annibynnol
< roundbracket elementid="20032527w-rb266">
(  Ffioedd )
< /roundbracket>
< roundbracket elementid="20032527w-rb267">
(  Cymru )
< /roundbracket>
2002
< squarebracket elementid="20032527w-sb8">
[ 
< a type="href" elementid="20032527w-a81">
8
< /a>
]
< /squarebracket>
yn cael eu diwygio yn unol â darpariaethau canlynol y rheoliad hwn .
< /sentence>
< sentence type="normal" elementid="20032527w-s382">
< roundbracket elementid="20032527w-rb268">
(  2 )
< /roundbracket>
Yn y paragraff sy 'n dwyn y pennawd
< doublequotation elementid="20032527w-dq51">
"
< english>
Arrangements of Regulations
< /english>
"
< /doublequotation>
, ychwanegir y llinell ganlynol at y diwedd  13 .
< /sentence>
< sentence type="normal" elementid="20032527w-s383">
Annual fee - nurses agencies  .
< /sentence>
< sentence type="heading" elementid="20032527w-s384">
< roundbracket elementid="20032527w-rb269">
(  3 )
< /roundbracket>
Yn rheoliad 2
< roundbracket elementid="20032527w-rb270">
(  1 )
< /roundbracket>
 - 
< /sentence>
< sentence type="heading" elementid="20032527w-s385">
< doublequotation elementid="20032527w-dq52">
"  "
< /doublequotation>
< english>
the 1957 Act
< /english>
< doublequotation elementid="20032527w-dq53">
"
< english>
means the Nurses Agencies Act 1957
< /english>
"
< /doublequotation>
;
< roundbracket elementid="20032527w-rb271">
(  b )
< /roundbracket>
ar ôl y geiriau
< /sentence>
< sentence type="heading" elementid="20032527w-s386">
< doublequotation elementid="20032527w-dq54">
"  "
< /doublequotation>
< english>
agency
< /english>
< doublequotation elementid="20032527w-dq55">
"
< english>
means a fostering agency
< /english>
"
< /doublequotation>
ychwanegwch y geiriau
< doublequotation elementid="20032527w-dq56">
"
< english>
or nurses agency
< /english>
"
< /doublequotation>
;
< roundbracket elementid="20032527w-rb272">
(  c )
< /roundbracket>
yn y diffiniad o
< doublequotation elementid="20032527w-dq57">
"
< english>
existing undertaking
< /english>
"
< /doublequotation>
ychwanegwch yn y man priodol  - 
< /sentence>
< sentence type="normal" elementid="20032527w-s387">
< english>
< doublequotation elementid="20032527w-dq58">
"
< english>
< roundbracket elementid="20032527w-rb273">
(  e )
< /roundbracket>
a nurses agency that is licensed immediately before 2 Hydref 2003 under the 1957 Act ; "
< /english>
< /doublequotation> .
< /english>
< /sentence>
< /p>
< p elementid="20032527w-p158">
< sentence type="heading" elementid="20032527w-s388">
< roundbracket elementid="20032527w-rb274">
(  4 )
< /roundbracket>
Ar ôl rheoliad 12
< roundbracket elementid="20032527w-rb275">
(  
< english>
Annual fee - fostering agencies and local authority fostering services
< /english>
)
< /roundbracket>
, mewnosodwch y rheoliad canlynol  - 
< /sentence>
< /p>
< p elementid="20032527w-p159">
< sentence type="heading" elementid="20032527w-s389">
< strong elementid="20032527w-st33">
< english>
Annual fee - nurses agencies
< /english>
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s390">
13 .
< /sentence>
< sentence type="normal" elementid="20032527w-s391">
< english>
 - 
< roundbracket elementid="20032527w-rb276">
(  1 )
< /roundbracket>
The annual fee in respect of a nurses agency is £ 500 .
< /english>
< /sentence>
< sentence type="normal" elementid="20032527w-s392">
< english>
< roundbracket elementid="20032527w-rb277">
(  2 )
< /roundbracket>
The annual fee in respect of a nurses agency shall be first payable by the registered provider on the date specified in respect of him or her in paragraph
< roundbracket elementid="20032527w-rb278">
(  3 )
< /roundbracket>
< roundbracket elementid="20032527w-rb279">
(
< doublequotation elementid="20032527w-dq59">
"  the first date "
< /doublequotation>
)
< /roundbracket>
and thereafter on the anniversary of the first date .
< /english>
< /sentence>
< sentence type="heading" elementid="20032527w-s393">
< english>
< roundbracket elementid="20032527w-rb280">
(  3 )
< /roundbracket>
The specified date is  - 
< /english>
< /sentence>
< sentence type="heading" elementid="20032527w-s394">
< english>
< roundbracket elementid="20032527w-rb281">
(  a )
< /roundbracket>
in the case of the registered provider of a nurses agency that is an existing undertaking  - 
< /english>
< /sentence>
< sentence type="heading" elementid="20032527w-s395">
< english>
< roundbracket elementid="20032527w-rb282">
(  i )
< /roundbracket>
in a case where a licence fee was payable under the 1957 Act in respect of the undertaking , the anniversary of the date on which the last fee was payable ;
< /english>
< /sentence>
< sentence type="heading" elementid="20032527w-s396">
< english>
< roundbracket elementid="20032527w-rb283">
(  ii )
< /roundbracket>
in any other case , 31st December 2003 ;
< /english>
< /sentence>
< /p>
< p elementid="20032527w-p160">
< sentence type="normal" elementid="20032527w-s397">
< english>
< roundbracket elementid="20032527w-rb284">
(  b )
< /roundbracket>
in the case of a registered provider who is a new provider , the date on which a certificate of registration is first issued under Part II of the Act in respect of that registered provider of the nurses agency .
< /english>
< /sentence>
< /p>
< p elementid="20032527w-p161">
< sentence type="heading" elementid="20032527w-s398">
< strong elementid="20032527w-st34">
Cofrestru
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s399">
29 .
< /sentence>
< sentence type="heading" elementid="20032527w-s400">
Mae rheoliad 2
< roundbracket elementid="20032527w-rb285">
(  1 )
< /roundbracket>
o Reoliadau Cofrestru Gofal Cymdeithasol a Gofal Iechyd Annibynnol
< roundbracket elementid="20032527w-rb286">
(  Cymru )
< /roundbracket>
2002
< squarebracket elementid="20032527w-sb9">
[ 
< a type="href" elementid="20032527w-a82">
9
< /a>
]
< /squarebracket>
yn cael ei ddiwygio fel a ganlyn  - 
< /sentence>
< /p>
< p elementid="20032527w-p162">
< sentence type="heading" elementid="20032527w-s401">
< english>
< a type="href" elementid="20032527w-a83">
10
< /a>
] , that office ;
< /english>
< /sentence>
< sentence type="normal" elementid="20032527w-s402">
< english>
< roundbracket elementid="20032527w-rb287">
(  ii )
< /roundbracket>
in any other case , any office of the National Assembly .
< /english>
< /sentence>
< /p>
< p elementid="20032527w-p163">
< sentence type="heading" elementid="20032527w-s403">
< roundbracket elementid="20032527w-rb288">
(  b )
< /roundbracket>
yn y diffiniad o
< doublequotation elementid="20032527w-dq60">
"
< english>
statement of purpose
< /english>
"
< /doublequotation>
, mewnosodwch ar ôl is-baragraff
< roundbracket elementid="20032527w-rb289">
(  e )
< /roundbracket>
 - 
< /sentence>
< /p>
< p elementid="20032527w-p164">
< sentence type="normal" elementid="20032527w-s404">
< english>
< roundbracket elementid="20032527w-rb290">
(  f )
< /roundbracket>
in relation to a nurses agency , the written statement required to be compiled in relation to the nurses agency in accordance with regulation 4
< roundbracket elementid="20032527w-rb291">
(  1 )
< /roundbracket>
of the Nurses Agency
< roundbracket elementid="20032527w-rb292">
(  Wales )
< /roundbracket>
Regulations 2003 .
< /english>
< /sentence>
< /p>
< p elementid="20032527w-p165">
< sentence type="heading" elementid="20032527w-s405">
< strong elementid="20032527w-st35">
Darpariaethau trosiannol
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s406">
30 .
< /sentence>
< sentence type="normal" elementid="20032527w-s407">
Bydd Atodlen 5 i 'r rheoliadau hyn , sy 'n gwneud darpariaethau trosiannol , yn effeithiol .
< /sentence>
< sentence type="heading" elementid="20032527w-s408">
Llofnodwyd ar rhan Cynulliad Cenedlaethol Cymru o dan adran 66
< roundbracket elementid="20032527w-rb293">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20032527w-sb10">
[ 
< a type="href" elementid="20032527w-a84">
11
< /a>
]
< /squarebracket>
< /sentence>
< sentence type="heading" elementid="20032527w-s409">
< em elementid="20032527w-em25">
D. Elis-Thomas
< /em>
< /sentence>
< sentence type="heading" elementid="20032527w-s410">
Llywydd y Cynulliad Cenedlaethol
< /sentence>
< sentence type="heading" elementid="20032527w-s411">
1 Hydref 2003
< /sentence>
< /p>
< p elementid="20032527w-p166">
< sentence type="heading" elementid="20032527w-s412">
< strong elementid="20032527w-st37">
< strong elementid="20032527w-st36">
ATODLEN 1
< /strong>
< /strong>
Rheoliad 4
< /sentence>
< sentence type="heading" elementid="20032527w-s413">
YR WYBODAETH SYDD I 'W CHYNNWYS YN Y DATGANIAD O DDIBEN
< /sentence>
< sentence type="normal" elementid="20032527w-s414">
1 .
< /sentence>
< sentence type="normal" elementid="20032527w-s415">
Nodau ac amcanion yr asiantaeth .
< /sentence>
< sentence type="normal" elementid="20032527w-s416">
2 .
< /sentence>
< sentence type="normal" elementid="20032527w-s417">
Natur y gwasanaethau y mae 'r asiantaeth yn eu darparu .
< /sentence>
< sentence type="normal" elementid="20032527w-s418">
3 .
< /sentence>
< sentence type="normal" elementid="20032527w-s419">
Enw a chyfeiriad y darparydd cofrestredig ac unrhyw reolwr cofrestredig .
< /sentence>
< sentence type="normal" elementid="20032527w-s420">
4 .
< /sentence>
< sentence type="normal" elementid="20032527w-s421">
Cymwysterau a phrofiad perthnasol y darparydd cofrestredig ac unrhyw reolwr cofrestredig .
< /sentence>
< sentence type="normal" elementid="20032527w-s422">
5 .
< /sentence>
< sentence type="normal" elementid="20032527w-s423">
Ystod cymwysterau 'r nyrsys sy 'n cael eu cyflenwi gan yr asiantaeth , a 'r mathau o sefydliadau y maent yn cael eu cyflenwi i weithio ynddynt .
< /sentence>
< sentence type="normal" elementid="20032527w-s424">
6 .
< /sentence>
< sentence type="normal" elementid="20032527w-s425">
Y weithdrefn gwyno a sefydlwyd yn unol â rheoliad 18 .
< /sentence>
< sentence type="heading" elementid="20032527w-s426">
< strong elementid="20032527w-st38">
ATODLEN 2
< /strong>
Rheoliadau 7
< roundbracket elementid="20032527w-rb294">
(  3 )
< /roundbracket>
, 9
< roundbracket elementid="20032527w-rb295">
(  2 )
< /roundbracket>
a 12
< roundbracket elementid="20032527w-rb296">
(  2 )
< /roundbracket>
< /sentence>
< sentence type="heading" elementid="20032527w-s427">
YR WYBODAETH SY 'N OFYNNOL MEWN PERTHYNAS Â DARPARWYR A RHEOLWYR COFRESTREDIG ASIANTAETH A NYRSYS SY 'N GYFRIFOL AM DDEWIS NYRSYS I 'W CYFLENWI I DDEFNYDDWYR GWASANAETH
< /sentence>
< sentence type="normal" elementid="20032527w-s428">
1 .
< /sentence>
< sentence type="normal" elementid="20032527w-s429">
Prawf o bwy yw 'r person , gan gynnwys ffotograff diweddar .
< /sentence>
< sentence type="normal" elementid="20032527w-s430">
2 .
< /sentence>
< sentence type="heading" elementid="20032527w-s431">
Naill ai  - 
< /sentence>
< sentence type="heading" elementid="20032527w-s432">
< roundbracket elementid="20032527w-rb297">
(  a )
< /roundbracket>
os oes angen y dystysgrif at ddiben sy 'n gysylltiedig ag adran 115
< roundbracket elementid="20032527w-rb298">
(  5 )
< /roundbracket>
< roundbracket elementid="20032527w-rb299">
(  ea )
< /roundbracket>
o Ddeddf yr Heddlu 1997
< roundbracket elementid="20032527w-rb300">
(  cofrestru o dan Ran II o Ddeddf Safonau Gofal 2000 )
< /roundbracket>
< squarebracket elementid="20032527w-sb11">
[ 
< a type="href" elementid="20032527w-a85">
12
< /a>
]
< /squarebracket>
, neu os yw 'r swydd yn dod o dan adran 115
< roundbracket elementid="20032527w-rb301">
(  3 )
< /roundbracket>
neu
< roundbracket elementid="20032527w-rb302">
(  4 )
< /roundbracket>
o 'r Ddeddf honno
< squarebracket elementid="20032527w-sb12">
[ 
< a type="href" elementid="20032527w-a86">
13
< /a>
]
< /squarebracket>
, tystysgrif cofnod troseddol fanwl a ddyroddwyd o dan adran 115 o 'r Ddeddf honno ; neu
< /sentence>
< sentence type="heading" elementid="20032527w-s433">
< roundbracket elementid="20032527w-rb303">
(  b )
< /roundbracket>
mewn unrhyw achos arall , tystysgrif cofnod troseddol a ddyroddwyd o dan adran 113 o 'r Ddeddf honno,
< /sentence>
< /p>
< p elementid="20032527w-p167">
< sentence type="normal" elementid="20032527w-s434">
gan gynnwys , os yw 'n gymwys , y materion a bennir yn adrannau 133
< roundbracket elementid="20032527w-rb304">
(  3A )
< /roundbracket>
a 115
< roundbracket elementid="20032527w-rb305">
(  6A )
< /roundbracket>
o 'r Ddeddf honno a 'r darpariaethau canlynol pan fyddant mewn grym , sef adran 113
< roundbracket elementid="20032527w-rb306">
(  3C )
< /roundbracket>
< roundbracket elementid="20032527w-rb307">
(  a )
< /roundbracket>
a
< roundbracket elementid="20032527w-rb308">
(  b )
< /roundbracket>
ac adran 115
< roundbracket elementid="20032527w-rb309">
(  6B )
< /roundbracket>
< roundbracket elementid="20032527w-rb310">
(  a )
< /roundbracket>
a
< roundbracket elementid="20032527w-rb311">
(  b )
< /roundbracket>
o 'r Ddeddf honno
< squarebracket elementid="20032527w-sb13">
[ 
< a type="href" elementid="20032527w-a87">
14
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="normal" elementid="20032527w-s435">
3 .
< /sentence>
< sentence type="normal" elementid="20032527w-s436">
Dau dystlythyr , gan gynnwys tystlythyr sy 'n ymwneud â 'r cyfnod cyflogaeth diwethaf nad oedd wedi para 'n llai na thri mis .
< /sentence>
< sentence type="normal" elementid="20032527w-s437">
4 .
< /sentence>
< sentence type="normal" elementid="20032527w-s438">
Os yw person wedi gweithio o 'r blaen mewn swydd a oedd yn golygu gweithio gyda phlant neu oedolion hawdd eu niweidio , cadarnhad o 'r rheswm pam y daeth y gyflogaeth neu 'r swydd honno i ben ac eithrio os yw 'r Cynulliad Cenedlaethol wedi penderfynu bod pob cam rhesymol wedi 'i gymryd i sicrhau cadarnhad o 'r fath ond nad yw ar gael .
< /sentence>
< sentence type="normal" elementid="20032527w-s439">
5 .
< /sentence>
< sentence type="normal" elementid="20032527w-s440">
Tystiolaeth ddogfennol am unrhyw gymwysterau a hyfforddiant perthnasol .
< /sentence>
< sentence type="normal" elementid="20032527w-s441">
6 .
< /sentence>
< sentence type="normal" elementid="20032527w-s442">
Hanes cyflogaeth llawn , ynghyd ag esboniad ysgrifenedig boddhaol am unrhyw fylchau yn y gyflogaeth .
< /sentence>
< sentence type="normal" elementid="20032527w-s443">
7 .
< /sentence>
< sentence type="normal" elementid="20032527w-s444">
Mewn perthynas â nyrs y mae rheoliad 12
< roundbracket elementid="20032527w-rb312">
(  2 )
< /roundbracket>
yn gymwys iddi , cadarnhad o 'i chofrestriad cyfredol â 'r Cyngor Nyrsio a Bydwreigiaeth
< squarebracket elementid="20032527w-sb14">
[ 
< a type="href" elementid="20032527w-a88">
15
< /a>
]
< /squarebracket>
, gan gynnwys manylion am y Rhan o 'r gofrestr y mae enw 'r nyrs wedi 'i gofrestru ynddi .
< /sentence>
< sentence type="normal" elementid="20032527w-s445">
8 .
< /sentence>
< sentence type="normal" elementid="20032527w-s446">
Manylion unrhyw yswiriant indemnio proffesiynol .
< /sentence>
< sentence type="normal" elementid="20032527w-s447">
9 .
< /sentence>
< sentence type="heading" elementid="20032527w-s448">
Gwiriad heddlu , sef adroddiad a luniwyd gan neu ar ran prif swyddog heddlu o fewn ystyr
< doublequotation elementid="20032527w-dq61">
"
< english>
chief officer
< /english>
"
< /doublequotation>
yn Neddf yr Heddlu 1997 ac sy 'n cofnodi , adeg llunio 'r adroddiad , bob tramgwydd troseddol  - 
< /sentence>
< /p>
< p elementid="20032527w-p168">
< sentence type="heading" elementid="20032527w-s449">
< roundbracket elementid="20032527w-rb313">
(  a )
< /roundbracket>
yr oedd y person wedi 'i gollfarnu o 'i herwydd gan gynnwys collfarnau sydd wedi 'u disbyddu o fewn ystyr
< doublequotation elementid="20032527w-dq62">
"
< english>
spent
< /english>
"
< /doublequotation>
yn Neddf Adsefydlu Tramgwyddwyr 1974
< squarebracket elementid="20032527w-sb15">
[ 
< a type="href" elementid="20032527w-a89">
16
< /a>
]
< /squarebracket>
ac y caniateir eu datgelu yn rhinwedd Gorchymyn Deddf Adsefydlu Tramgwyddwyr 1974
< roundbracket elementid="20032527w-rb314">
(  Eithriadau )
< /roundbracket>
1975
< squarebracket elementid="20032527w-sb16">
[ 
< a type="href" elementid="20032527w-a90">
17
< /a>
]
< /squarebracket>
) ; neu
< /sentence>
< sentence type="normal" elementid="20032527w-s450">
< roundbracket elementid="20032527w-rb315">
(  b )
< /roundbracket>
yr oedd y person wedi 'i rybuddio amdanynt ac wedi 'u cyfaddef adeg cael y rhybudd .
< /sentence>
< /p>
< p elementid="20032527w-p169">
< sentence type="heading" elementid="20032527w-s451">
< strong elementid="20032527w-st39">
ATODLEN 3
< /strong>
Rheoliad 12
< roundbracket elementid="20032527w-rb316">
(  1 )
< /roundbracket>
< /sentence>
< sentence type="heading" elementid="20032527w-s452">
YR WYBODAETH SY 'N OFYNNOL MEWN PERTHYNAS Â NYRSYS SYDD I 'W CYFLENWI GAN ASIANTAETH
< /sentence>
< sentence type="normal" elementid="20032527w-s453">
1 .
< /sentence>
< sentence type="normal" elementid="20032527w-s454">
Enw , cyfeiriad , dyddiad geni a Rhif ffôn .
< /sentence>
< sentence type="normal" elementid="20032527w-s455">
2 .
< /sentence>
< sentence type="normal" elementid="20032527w-s456">
Enw , cyfeiriad a Rhif ffôn y perthynas agosaf .
< /sentence>
< sentence type="normal" elementid="20032527w-s457">
3 .
< /sentence>
< sentence type="normal" elementid="20032527w-s458">
Prawf o bwy yw 'r person , gan gynnwys ffotograff diweddar .
< /sentence>
< sentence type="normal" elementid="20032527w-s459">
4 .
< /sentence>
< sentence type="heading" elementid="20032527w-s460">
Naill ai  - 
< /sentence>
< sentence type="heading" elementid="20032527w-s461">
< roundbracket elementid="20032527w-rb317">
(  a )
< /roundbracket>
os yw 'r swydd y mae 'r nyrs yn cael ei chyflenwi i 'w chyflawni yn dod o dan adran 115
< roundbracket elementid="20032527w-rb318">
(  3 )
< /roundbracket>
neu
< roundbracket elementid="20032527w-rb319">
(  4 )
< /roundbracket>
o Ddeddf yr Heddlu 1997 , tystysgrif cofnod troseddol fanwl a ddyroddwyd o dan adran 115 o 'r Ddeddf honno ; neu
< /sentence>
< sentence type="heading" elementid="20032527w-s462">
< roundbracket elementid="20032527w-rb320">
(  b )
< /roundbracket>
mewn unrhyw achos arall , tystysgrif cofnod troseddol a ddyroddwyd o dan adran 113 o 'r Ddeddf honno,
< /sentence>
< /p>
< p elementid="20032527w-p170">
< sentence type="normal" elementid="20032527w-s463">
gan gynnwys , os yw 'n gymwys , y materion a bennir yn adrannau 113
< roundbracket elementid="20032527w-rb321">
(  3A )
< /roundbracket>
a 115
< roundbracket elementid="20032527w-rb322">
(  6A )
< /roundbracket>
o 'r Ddeddf honno a 'r darpariaethau canlynol pan fyddant mewn grym , sef adran 113
< roundbracket elementid="20032527w-rb323">
(  3C )
< /roundbracket>
< roundbracket elementid="20032527w-rb324">
(  a )
< /roundbracket>
a
< roundbracket elementid="20032527w-rb325">
(  b )
< /roundbracket>
ac adran 115
< roundbracket elementid="20032527w-rb326">
(  6B )
< /roundbracket>
< roundbracket elementid="20032527w-rb327">
(  a )
< /roundbracket>
a
< roundbracket elementid="20032527w-rb328">
(  b )
< /roundbracket>
o 'r Ddeddf hono .
< /sentence>
< sentence type="normal" elementid="20032527w-s464">
5 .
< /sentence>
< sentence type="heading" elementid="20032527w-s465">
Gwiriad heddlu , sef adroddiad a luniwyd gan neu ar ran prif swyddog heddlu o fewn ystyr
< doublequotation elementid="20032527w-dq63">
"
< english>
chief officer
< /english>
"
< /doublequotation>
yn Neddf yr Heddlu 1997 ac sy 'n cofnodi , adeg llunio 'r adroddiad , bob tramgwydd troseddol  - 
< /sentence>
< /p>
< p elementid="20032527w-p171">
< sentence type="heading" elementid="20032527w-s466">
< roundbracket elementid="20032527w-rb329">
(  a )
< /roundbracket>
y caniateir eu ddatgelu yn rhinwedd Gorchymyn Deddf Adsefydlu Tramgwyddwyr 1974
< roundbracket elementid="20032527w-rb330">
(  Eithriadau )
< /roundbracket>
1975
< squarebracket elementid="20032527w-sb17">
[ 
< a type="href" elementid="20032527w-a91">
18
< /a>
]
< /squarebracket>
; neu
< /sentence>
< sentence type="normal" elementid="20032527w-s467">
< roundbracket elementid="20032527w-rb331">
(  b )
< /roundbracket>
yr oedd y person wedi 'i rybuddio amdanynt ac wedi 'u cyfaddef adeg cael y rhybudd .
< /sentence>
< /p>
< p elementid="20032527w-p172">
< sentence type="normal" elementid="20032527w-s468">
6 .
< /sentence>
< sentence type="normal" elementid="20032527w-s469">
Dau dystlythyr oddi wrth nyrsys neu broffesiynolion iechyd eraill , gan gynnwys tystlythyr sy 'n ymwneud â 'r cyfnod cyflogaeth diwethaf fel nyrs nad oedd wedi para 'n llai na thri mis .
< /sentence>
< sentence type="normal" elementid="20032527w-s470">
7 .
< /sentence>
< sentence type="normal" elementid="20032527w-s471">
Os yw 'r nyrs wedi gweithio o 'r blaen mewn swydd a oedd yn golygu gweithio gyda phlant neu oedolion hawdd eu niweidio , cadarnhad o 'r rheswm pam y rhoes y gorau i weithio yn y swydd honno , ac eithrio os nad yw 'r Cynulliad Cenedlaethol wedi penderfynu bod pob cam rhesymol wedi 'i gymryd i sicrhau cadarnhad o 'r fath ond nad yw ar gael .
< /sentence>
< sentence type="normal" elementid="20032527w-s472">
8 .
< /sentence>
< sentence type="normal" elementid="20032527w-s473">
Tystiolaeth bod y nyrs yn medru Saesneg i raddau boddhaol , os oedd cymwysterau nyrsio 'r nyrs wedi 'u sicrhau y tu allan i 'r Deyrnas Unedig .
< /sentence>
< sentence type="normal" elementid="20032527w-s474">
9 .
< /sentence>
< sentence type="normal" elementid="20032527w-s475">
Tystiolaeth ddogfennol am unrhyw gymwysterau a hyfforddiant perthnasol .
< /sentence>
< sentence type="normal" elementid="20032527w-s476">
10 .
< /sentence>
< sentence type="normal" elementid="20032527w-s477">
Hanes cyflogaeth llawn , ynghyd ag esboniad ysgrifenedig boddhaol am unrhyw fylchau yn y gyflogaeth a manylion unrhyw gyflogaeth gyfredol ac eithrio at ddibenion yr asiantaeth .
< /sentence>
< sentence type="normal" elementid="20032527w-s478">
11 .
< /sentence>
< sentence type="normal" elementid="20032527w-s479">
Cofnod o statws imwneiddio .
< /sentence>
< sentence type="normal" elementid="20032527w-s480">
12 .
< /sentence>
< sentence type="normal" elementid="20032527w-s481">
Cadarnhad o 'i chofrestriad cyfredol â 'r Cyngor Nyrsio a Bydwreigiaeth , gan gynnwys manylion am y Rhan o 'r gofrestr y mae 'r nyrs wedi 'i chofrestru ynddi .
< /sentence>
< sentence type="normal" elementid="20032527w-s482">
13 .
< /sentence>
< sentence type="normal" elementid="20032527w-s483">
Manylion unrhyw yswiriant indemnio proffesiynol .
< /sentence>
< /p>
< p elementid="20032527w-p173">
< sentence type="heading" elementid="20032527w-s484">
< strong elementid="20032527w-st40">
ATODLEN 4
< /strong>
Rheoliad 17
< /sentence>
< sentence type="heading" elementid="20032527w-s485">
Y COFNODION SYDD I 'W CADW AR GYFER ARCHWILIAD
< /sentence>
< sentence type="heading" elementid="20032527w-s486">
< strong elementid="20032527w-st41">
Cofnodion sy 'n ymwneud â chyflenwi nyrsys
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s487">
1 .
< /sentence>
< sentence type="normal" elementid="20032527w-s488">
Copïau o 'r holl gytundebau rhwng yr asiantaeth a 'r nyrsys a gyflenwyd neu sydd i 'w cyflenwi gan yr asiantaeth a thystiolaeth bod copi o unrhyw delerau ac amodau safonol wedi 'i ddarparu gan yr asiantaeth i bob nyrs .
< /sentence>
< sentence type="normal" elementid="20032527w-s489">
2 .
< /sentence>
< sentence type="normal" elementid="20032527w-s490">
Manylion y tâl sy 'n daladwy i bob nyrs sy 'n cael ei chyflogi gan yr asiantaeth a 'i hamodau gwaith .
< /sentence>
< sentence type="normal" elementid="20032527w-s491">
3 .
< /sentence>
< sentence type="normal" elementid="20032527w-s492">
Copïau o unrhyw ddatganiad a roddwyd i ddefnyddiwr gwasanaeth ac sy 'n nodi cymwysterau a phrofiad perthnasol nyrs a gyflenwyd i 'r defnyddiwr gwasanaeth hwnnw .
< /sentence>
< sentence type="normal" elementid="20032527w-s493">
4 .
< /sentence>
< sentence type="normal" elementid="20032527w-s494">
Mynegai defnyddwyr gwasanaeth yn ôl trefn yr wyddor , gan gynnwys enw llawn , cyfeiriad a Rhif ffôn pob un ohonynt ac unrhyw rifau cyfresol a bennwyd ar eu cyfer .
< /sentence>
< sentence type="normal" elementid="20032527w-s495">
5 .
< /sentence>
< sentence type="normal" elementid="20032527w-s496">
Mynegai yn ôl trefn yr wyddor o 'r nyrsys a gyflenwyd neu sydd ar gael i 'w cyflenwi gan yr asiantaeth , gan gynnwys unrhyw Rhif au cyfresol a bennwyd ar eu cyfer .
< /sentence>
< sentence type="normal" elementid="20032527w-s497">
6 .
< /sentence>
< sentence type="normal" elementid="20032527w-s498">
Manylion pob cyflenwad nyrs i ddefnyddiwr gwasanaeth .
< /sentence>
< sentence type="normal" elementid="20032527w-s499">
7 .
< /sentence>
< sentence type="heading" elementid="20032527w-s500">
Os yw 'r asiantaeth yn gweithredu fel busnes cyflogi a bod nyrs yn cael ei chyflenwi i ddarparu gofal nyrsio ym mhreswylfa breifat defnyddiwr gwasanaeth neu glaf , manylion  - 
< /sentence>
< sentence type="heading" elementid="20032527w-s501">
< roundbracket elementid="20032527w-rb332">
(  a )
< /roundbracket>
yr afiechyd neu 'r anabledd y mae 'r claf yn dioddef ganddo ;
< /sentence>
< sentence type="heading" elementid="20032527w-s502">
< roundbracket elementid="20032527w-rb333">
(  b )
< /roundbracket>
enw a chyfeiriad ymarferydd meddygol cyffredinol y claf ;
< /sentence>
< sentence type="heading" elementid="20032527w-s503">
< roundbracket elementid="20032527w-rb334">
(  c )
< /roundbracket>
y proffesiynolion iechyd eraill y mae 'r claf yn cael triniaeth ganddynt ;
< /sentence>
< sentence type="heading" elementid="20032527w-s504">
< roundbracket elementid="20032527w-rb335">
(  ch )
< /roundbracket>
perthynas agosaf y claf ;
< /sentence>
< sentence type="heading" elementid="20032527w-s505">
< roundbracket elementid="20032527w-rb336">
(  d )
< /roundbracket>
crefydd y claf ;
< /sentence>
< sentence type="heading" elementid="20032527w-s506">
< roundbracket elementid="20032527w-rb337">
(  dd )
< /roundbracket>
meddianwyr eraill yn y safle lle darperir y gwasanaeth nyrsio ; ac
< /sentence>
< sentence type="normal" elementid="20032527w-s507">
< roundbracket elementid="20032527w-rb338">
(  e )
< /roundbracket>
y cynllun nyrsio a luniwyd ar gyfer y claf a chofnod manwl o 'r gofal nyrsio sy 'n cael ei ddarparu .
< /sentence>
< /p>
< p elementid="20032527w-p174">
< sentence type="heading" elementid="20032527w-s508">
< strong elementid="20032527w-st42">
Cofnodion eraill
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s509">
1 .
< /sentence>
< sentence type="normal" elementid="20032527w-s510">
Yr holl wybodaeth sy 'n cael ei darparu i 'r Cynulliad Cenedlaethol at ddibenion cofrestru mewn perthynas â 'r asiantaeth .
< /sentence>
< sentence type="normal" elementid="20032527w-s511">
2 .
< /sentence>
< sentence type="heading" elementid="20032527w-s512">
Manylion pob honiad o gam-drin  - 
< /sentence>
< /p>
< p elementid="20032527w-p175">
< sentence type="heading" elementid="20032527w-s513">
< roundbracket elementid="20032527w-rb339">
(  a )
< /roundbracket>
yn erbyn nyrs ; neu
< /sentence>
< sentence type="heading" elementid="20032527w-s514">
< roundbracket elementid="20032527w-rb340">
(  b )
< /roundbracket>
gan nyrs  nad yw 'n destun cwyn sydd wedi 'i gwneud o dan reoliad 18 ),
< /sentence>
< /p>
< p elementid="20032527w-p176">
< sentence type="normal" elementid="20032527w-s515">
ac sy 'n cael ei chyflogi gan yr asiantaeth , gan gynnwys manylion yr ymchwiliadau a wnaed , y canlyniad ac unrhyw gamau a gymerwyd o ganlyniad i hynny .
< /sentence>
< /p>
< p elementid="20032527w-p177">
< sentence type="heading" elementid="20032527w-s516">
< strong elementid="20032527w-st43">
ATODLEN 5
< /strong>
Rheoliad 30
< /sentence>
< sentence type="heading" elementid="20032527w-s517">
< strong elementid="20032527w-st44">
Trosglwyddo o drwyddedu o dan Ddeddf 1957 i gofrestru o dan Ddeddf 2000
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s518">
1 .
< /sentence>
< sentence type="normal" elementid="20032527w-s519">
 - 
< roundbracket elementid="20032527w-rb341">
(  1 )
< /roundbracket>
Mae 'r is-baragraff hwn yn gymwys i ddarparydd asiantaeth nyrsys
< roundbracket elementid="20032527w-rb342">
(  y cyfeirir ato fel
< doublequotation elementid="20032527w-dq64">
"  y darparydd "
< /doublequotation>
< roundbracket elementid="20032527w-rb343">
(
< doublequotation elementid="20032527w-dq65">
"
< english>
the provider
< /english>
"
< /doublequotation>
)
< /roundbracket>
yn narpariaethau canlynol yr Atodlen hon )
< /roundbracket>
pan fydd yn cael ei drin yn unol ag is-baragraff
< roundbracket elementid="20032527w-rb344">
(  6 )
< /roundbracket>
fel petai wedi gwneud cais am gofrestriad o dan Ran II o Ddeddf 2000 a bod y cofrestriad hwnnw wedi 'i ganiatáu o dan y Ddeddf honno mewn perthynas â 'r asiantaeth yr oedd wedi 'i drwyddedu i 'w rhedeg o dan Ddeddf 1957 .
< /sentence>
< sentence type="normal" elementid="20032527w-s520">
< roundbracket elementid="20032527w-rb345">
(  2 )
< /roundbracket>
Bydd Deddf 1957 yn parhau mewn grym er mywn rhoi effaith i ddarpariaethau canlynol y paragraff hwn .
< /sentence>
< sentence type="normal" elementid="20032527w-s521">
< roundbracket elementid="20032527w-rb346">
(  3 )
< /roundbracket>
Nes y bydd is-baragraff
< roundbracket elementid="20032527w-rb347">
(  1 )
< /roundbracket>
yn gymwys i ddarparydd asiantaeth nyrsys , bydd darpariaethau Deddf 1957 yn parhau mewn grym mewn perthynas â 'r darparydd , ac ar ei gyfer , fel petai unrhyw gyfeiriad yn Neddf 1957 at yr awdurdod trwyddedu yn gyfeiriad at y Cynulliad Cenedlaethol .
< /sentence>
< sentence type="normal" elementid="20032527w-s522">
< roundbracket elementid="20032527w-rb348">
(  4 )
< /roundbracket>
Rhaid i 'r Cynulliad Cenedlaethol , gan ystyried unrhyw sylwadau a gyflwynwyd gan y darparydd o dan is-baragraff
< roundbracket elementid="20032527w-rb349">
(  9 )
< /roundbracket>
, benderfynu pan fydd yn gweld yn dda , ar y materion a ddisgrifir yn is-baragraff
< roundbracket elementid="20032527w-rb350">
(  5 )
< /roundbracket>
, a chyflwyno hysbysiad o 'i benderfyniad i 'r darparydd .
< /sentence>
< sentence type="heading" elementid="20032527w-s523">
< roundbracket elementid="20032527w-rb351">
(  5 )
< /roundbracket>
Y materion yw  - 
< /sentence>
< sentence type="heading" elementid="20032527w-s524">
< roundbracket elementid="20032527w-rb352">
(  a )
< /roundbracket>
yr amodau
< roundbracket elementid="20032527w-rb353">
(  os o gwbl )
< /roundbracket>
yr oedd cofrestru 'r darparydd o dan Ddeddf 1957 yn ddarostyngedig iddynt ;
< /sentence>
< sentence type="heading" elementid="20032527w-s525">
< roundbracket elementid="20032527w-rb354">
(  b )
< /roundbracket>
unrhyw fater arall i 'r graddau y mae penderfynu ar y mater hwnnw yn angenrheidiol i alluogi 'r darparydd , yn unol ag is-baragraff
< roundbracket elementid="20032527w-rb355">
(  6 )
< /roundbracket>
, i gael ei drin at ddibenion Rhan II o Ddeddf 2000 fel petai wedi gwneud cais am gofrestriad ar gyfer yr asiantaeth a bod y cofrestriad hwnnw wedi 'i ganiatáu ;
< /sentence>
< /p>
< p elementid="20032527w-p178">
< sentence type="normal" elementid="20032527w-s526">
a rhaid ymdrin ag unrhyw benderfyniad gan y Cynulliad Cenedlaethol o dan is-baragraff
< roundbracket elementid="20032527w-rb356">
(  4 )
< /roundbracket>
at ddibenion adran 21 o Ddeddf 2000
< roundbracket elementid="20032527w-rb357">
(  apelau i 'r Tribiwnlys )
< /roundbracket>
fel petai 'n benderfyniad gan y Cynulliad o dan Ran II o 'r Ddeddf honno .
< /sentence>
< sentence type="heading" elementid="20032527w-s527">
< roundbracket elementid="20032527w-rb358">
(  6 )
< /roundbracket>
Os yw 'r Cynulliad Cenedlaethol wedi gwneud penderfyniad yn unol ag is-baragraff
< roundbracket elementid="20032527w-rb359">
(  4 )
< /roundbracket>
, yna o ddyddiad a bennir gan y Cynulliad Cenedlaethol ymlaen  'y dyddiad effeithiol ')  - 
< /sentence>
< /p>
< p elementid="20032527w-p179">
< sentence type="heading" elementid="20032527w-s528">
< roundbracket elementid="20032527w-rb360">
(  a )
< /roundbracket>
rhaid ymdrin â 'r darparydd , at ddibenion Rhan II o Ddeddf 2000 , fel petai wedi gwneud cais am gofrestriad ar gyfer yr ymgymeriad presennol a bod y cofrestriad hwnnw wedi 'i ganiatáu ;
< /sentence>
< sentence type="heading" elementid="20032527w-s529">
< roundbracket elementid="20032527w-rb361">
(  b )
< /roundbracket>
bydd yr amodau
< roundbracket elementid="20032527w-rb362">
(  os o gwbl )
< /roundbracket>
y penderfynwyd arnynt yn unol ag is-baragraff
< roundbracket elementid="20032527w-rb363">
(  5 )
< /roundbracket>
< roundbracket elementid="20032527w-rb364">
(  a )
< /roundbracket>
, i 'r graddau y maent yn gallu bod yn amodau y mae cofrestru at ddibenion Rhan II o Ddeddf 2000 yn ddarostyngedig iddynt , yn effeithiol  - 
< /sentence>
< sentence type="heading" elementid="20032527w-s530">
< roundbracket elementid="20032527w-rb365">
(  i )
< /roundbracket>
fel petaent yn amodau y mae 'r cofrestriad at y dibenion hynny yn ddarostyngedig iddynt ; a
< /sentence>
< sentence type="normal" elementid="20032527w-s531">
< roundbracket elementid="20032527w-rb366">
(  ii )
< /roundbracket>
at ddibenion adran 19
< roundbracket elementid="20032527w-rb367">
(  1 )
< /roundbracket>
o Ddeddf 2000 , fel pe baent wedi 'u cytuno yn ysgrifenedig rhwng y darparydd a 'r Cynulliad Cenedlaethol .
< /sentence>
< /p>
< p elementid="20032527w-p180">
< sentence type="heading" elementid="20032527w-s532">
< roundbracket elementid="20032527w-rb368">
(  7 )
< /roundbracket>
Ar , neu cyn , y dyddiad effeithiol ar gyfer penderfyniad o dan y paragraff hwn rhaid i 'r Cynulliad Cenedlaethol ddyroddi tystysgrif i 'r darparydd  - 
< /sentence>
< /p>
< p elementid="20032527w-p181">
< sentence type="heading" elementid="20032527w-s533">
< roundbracket elementid="20032527w-rb369">
(  a )
< /roundbracket>
y mae 'n rhaid i 'w chynnwys fod yn unol ag unrhyw reoliadau a wnaed o dan adran 16
< roundbracket elementid="20032527w-rb370">
(  1 )
< /roundbracket>
< roundbracket elementid="20032527w-rb371">
(  b )
< /roundbracket>
o Ddeddf 2000 am gynnwys tystysgrifau sy 'n cael eu dyroddi o dan Ran II o 'r Ddeddf honno ; a
< /sentence>
< sentence type="normal" elementid="20032527w-s534">
< roundbracket elementid="20032527w-rb372">
(  b )
< /roundbracket>
y mae 'n rhaid ei thrin fel petai 'n dystysgrif ar gyfer yr ymgymeriad presennol sydd wedi 'i dyroddi gan y Cynulliad Cenedlaethol o dan Ran II o Ddeddf 2000 .
< /sentence>
< /p>
< p elementid="20032527w-p182">
< sentence type="normal" elementid="20032527w-s535">
< roundbracket elementid="20032527w-rb373">
(  8 )
< /roundbracket>
Bydd darpariaethau is-baragraffau
< roundbracket elementid="20032527w-rb374">
(  5 )
< /roundbracket>
a
< roundbracket elementid="20032527w-rb375">
(  6 )
< /roundbracket>
heb ragfarn i bwerau 'r Cynulliad Cenedlaethol i amrywio , dileu neu osod unrhyw amod yn unol â Rhan II o Ddeddf 2000 .
< /sentence>
< sentence type="normal" elementid="20032527w-s536">
< roundbracket elementid="20032527w-rb376">
(  9 )
< /roundbracket>
Cyn gwneud penderfyniad yngl n â 'r materion a ddisgrifiwyd yn is-baragraff
< roundbracket elementid="20032527w-rb377">
(  5 )
< /roundbracket>
mewn perthynas ag ymgymeriad presennol rhaid i 'r Cynulliad Cenedlaethol roi hysbysiad ysgrifenedig i 'r darparydd yn rhoi gwybod i 'r darparydd y caiff gyflwyno , cyn pen 28 diwrnod ar ôl i 'r hysbysiad hwnnw ddod i law , sylwadau ysgrifenedig am y penderfyniad , ac na chaiff unrhyw benderfyniad ei wneud cyn i 'r 28 diwrnod hynny ddod i ben .
< /sentence>
< sentence type="heading" elementid="20032527w-s537">
< strong elementid="20032527w-st45">
Trosglwyddo ceisiadau am drwyddedu o dan Ddeddf 1957 sydd heb eu penderfynu
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s538">
2 .
< /sentence>
< sentence type="heading" elementid="20032527w-s539">
 - 
< roundbracket elementid="20032527w-rb378">
(  1 )
< /roundbracket>
Mae is-baragraff
< roundbracket elementid="20032527w-rb379">
(  3 )
< /roundbracket>
yn gymwys i gais am drwydded o dan Ddeddf 1957 i redeg asiantaeth er mwyn cyflenwi nyrsys  - 
< /sentence>
< /p>
< p elementid="20032527w-p183">
< sentence type="heading" elementid="20032527w-s540">
< roundbracket elementid="20032527w-rb380">
(  a )
< /roundbracket>
sydd wedi 'i wneud i awdurdod cyn 2 Hydref 2003 ac nad yw wedi 'i ganiatáu ar y dyddiad hwnnw ; a
< /sentence>
< sentence type="normal" elementid="20032527w-s541">
< roundbracket elementid="20032527w-rb381">
(  b )
< /roundbracket>
nad yw is-baragraff
< roundbracket elementid="20032527w-rb382">
(  2 )
< /roundbracket>
yn gymwys iddo .
< /sentence>
< /p>
< p elementid="20032527w-p184">
< sentence type="heading" elementid="20032527w-s542">
< roundbracket elementid="20032527w-rb383">
(  2 )
< /roundbracket>
Mae 'r is-baragraff hwn yn gymwys i gais y mae 'r awdurdod wedi rhoi 'r canlynol mewn perthynas ag ef i 'r person a wnaeth y cais  - 
< /sentence>
< /p>
< p elementid="20032527w-p185">
< sentence type="heading" elementid="20032527w-s543">
< roundbracket elementid="20032527w-rb384">
(  a )
< /roundbracket>
hysbysiad o dan adran 2
< roundbracket elementid="20032527w-rb385">
(  4 )
< /roundbracket>
o Ddeddf 1957 o wrthod trwydded , neu roi trwydded yn ddarostyngedig i amodau a naill ai  - 
< /sentence>
< sentence type="heading" elementid="20032527w-s544">
< roundbracket elementid="20032527w-rb386">
(  i )
< /roundbracket>
bod y cyfnod ar gyfer apelio mewn perthynas â 'r hysbysiad hwnnw heb ddod i ben ; neu
< /sentence>
< sentence type="heading" elementid="20032527w-s545">
< roundbracket elementid="20032527w-rb387">
(  ii )
< /roundbracket>
bod y person a wnaeth y cais wedi apelio ond nad yw 'r apêl wedi 'i phenderfynu neu wedi 'i gollwng ; neu
< /sentence>
< /p>
< p elementid="20032527w-p186">
< sentence type="heading" elementid="20032527w-s546">
< roundbracket elementid="20032527w-rb388">
(  b )
< /roundbracket>
y cyfle i gael gwrandawiad o dan adran 2
< roundbracket elementid="20032527w-rb389">
(  5 )
< /roundbracket>
o 'r Ddeddf honno mewn perthynas ag unrhyw gynnig i wrthod rhoi trwydded , oni bai  - 
< /sentence>
< /p>
< p elementid="20032527w-p187">
< sentence type="heading" elementid="20032527w-s547">
< roundbracket elementid="20032527w-rb390">
(  i )
< /roundbracket>
bod y person , o fewn yr amser a ganiatawyd gan yr awdurdod yn yr hysbysiad i roi cyfle iddo gael gwrandawiad , heb fanteisio ar y cyfle a gynigiwyd felly nac wedi nodi ei fod yn dymuno gwneud hynny ; neu
< /sentence>
< sentence type="normal" elementid="20032527w-s548">
< roundbracket elementid="20032527w-rb391">
(  ii )
< /roundbracket>
bod yr awdurod wedi rhoi hysbysiad o wrthod trwydded .
< /sentence>
< /p>
< p elementid="20032527w-p188">
< sentence type="normal" elementid="20032527w-s549">
< roundbracket elementid="20032527w-rb392">
(  3 )
< /roundbracket>
Os yw 'r is-baragraff hwn yn gymwys i gais rhaid i 'r cais hwnnw gael ei drin fel cais am gofrestriad o dan Ran II o Ddeddf 2000 .
< /sentence>
< sentence type="heading" elementid="20032527w-s550">
< roundbracket elementid="20032527w-rb393">
(  4 )
< /roundbracket>
Os yw is-baragraff
< roundbracket elementid="20032527w-rb394">
(  2 )
< /roundbracket>
yn gymwys  - 
< /sentence>
< /p>
< p elementid="20032527w-p189">
< sentence type="heading" elementid="20032527w-s551">
< roundbracket elementid="20032527w-rb395">
(  a )
< /roundbracket>
bydd Ddeddf 1957 , yn ddarostyngedig i baragraff canlynol nesaf yr is-baragraff hwn , yn parhau mewn grym mewn perthynas â 'r materion canlynol  - 
< /sentence>
< sentence type="heading" elementid="20032527w-s552">
< roundbracket elementid="20032527w-rb396">
(  i )
< /roundbracket>
y penderfyniad i roi neu wrthod trwydded o dan adran 2 o 'r Ddeddf honno ;
< /sentence>
< sentence type="heading" elementid="20032527w-s553">
< roundbracket elementid="20032527w-rb397">
(  ii )
< /roundbracket>
apêl yn erbyn penderfyniad o 'r fath ;
< /sentence>
< /p>
< p elementid="20032527w-p190">
< sentence type="heading" elementid="20032527w-s554">
< roundbracket elementid="20032527w-rb398">
(  b )
< /roundbracket>
bydd y swyddogaethau , y pwerau a 'r dyletswyddau a oedd gan yr awdurdod , yn union cyn 2 Hydref 2003 , o dan y Ddeddf honno mewn perthynas â 'r materion a grybwyllwyd ym mharagraff blaenorol yr is-baragraff hwn yn gymwys i 'r Cynulliad Cenedlaethol yn hytrach na 'r awdurdod , a byddant yn arferadwy gan y Cynulliad Cenedlaethol ;
< /sentence>
< sentence type="heading" elementid="20032527w-s555">
< roundbracket elementid="20032527w-rb399">
(  c )
< /roundbracket>
rhaid ymdrin â 'r penderfyniad i fabwysiadu cynnig i ganiatáu cais o 'r dyddiad y mae 'n effeithiol , at ddibenion Rhan II o 'r Ddeddf  - 
< /sentence>
< /p>
< p elementid="20032527w-p191">
< sentence type="heading" elementid="20032527w-s556">
< roundbracket elementid="20032527w-rb400">
(  i )
< /roundbracket>
fel petai 'n benderfyniad i fabwysiadu cynnig i ganiatáu cais am gofrestriad ar gyfer asiantaeth nyrsys ;
< /sentence>
< sentence type="normal" elementid="20032527w-s557">
< roundbracket elementid="20032527w-rb401">
(  ii )
< /roundbracket>
fel petai wedi dod yn effeithiol yn unol ag adran 19
< roundbracket elementid="20032527w-rb402">
(  5 )
< /roundbracket>
o 'r Ddeddf .
< /sentence>
< /p>
< p elementid="20032527w-p192">
< sentence type="heading" elementid="20032527w-s558">
< strong elementid="20032527w-st46">
Y cyfnod tra 'n aros am benderfyniad yngl n â dileu
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s559">
3 .
< /sentence>
< sentence type="heading" elementid="20032527w-s560">
Os yw 'r awdurdod wedi dirymu trwydded y darparydd presennol mewn perthynas â 'r ymgymeriad presennol , a naill ai  - 
< /sentence>
< /p>
< p elementid="20032527w-p193">
< sentence type="heading" elementid="20032527w-s561">
< roundbracket elementid="20032527w-rb403">
(  a )
< /roundbracket>
bod y cyfnod ar gyfer apelio yn erbyn y dirymiad heb ddod i ben ; neu
< /sentence>
< sentence type="heading" elementid="20032527w-s562">
< roundbracket elementid="20032527w-rb404">
(  b )
< /roundbracket>
bod y darparydd presennol wedi apelio o dan adran 2
< roundbracket elementid="20032527w-rb405">
(  4 )
< /roundbracket>
o Ddeddf 1957 ac nad yw 'r apêl wedi 'i phenderfynu nac wedi 'i gollwng,
< /sentence>
< /p>
< p elementid="20032527w-p194">
< sentence type="normal" elementid="20032527w-s563">
rhaid peidio ag ymdrin â 'r darparydd presennol , at ddibenion Rhan II o 'r Ddeddf , fel petai 'r cofrestriad wedi 'i ganiatáu ar gyfer yr ymgymeriad presennol hwnnw .
< /sentence>
< sentence type="heading" elementid="20032527w-s564">
< strong elementid="20032527w-st47">
Y cyfnod tra 'n aros am gynnig yngl n â dileu
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s565">
4 .
< /sentence>
< sentence type="heading" elementid="20032527w-s566">
 - 
< roundbracket elementid="20032527w-rb406">
(  1 )
< /roundbracket>
Mae 'r is-baragraff hwn yn gymwys os , yn union cyn y dyddiad effeithiol  - 
< /sentence>
< /p>
< p elementid="20032527w-p195">
< sentence type="heading" elementid="20032527w-s567">
< roundbracket elementid="20032527w-rb407">
(  a )
< /roundbracket>
y mae 'r awdurdod neu 'r Cynulliad Cenedlaethol yn pwyso a mesur a ddylid dirymu trwydded y darparydd presennol ar gyfer yr ymgymeriad presennol ;
< /sentence>
< sentence type="heading" elementid="20032527w-s568">
< roundbracket elementid="20032527w-rb408">
(  b )
< /roundbracket>
yn unol ag adran 2
< roundbracket elementid="20032527w-rb409">
(  5 )
< /roundbracket>
o Ddeddf 1957 , y rhoddwyd cyfle i 'r darparydd bresennol gael gwrandawiad ; ac
< /sentence>
< sentence type="normal" elementid="20032527w-s569">
< roundbracket elementid="20032527w-rb410">
(  c )
< /roundbracket>
nad yw 'r awdurdod na 'r Cynulliad Cenedlaethol wedi penderfynu ar y mater .
< /sentence>
< /p>
< p elementid="20032527w-p196">
< sentence type="heading" elementid="20032527w-s570">
< roundbracket elementid="20032527w-rb411">
(  2 )
< /roundbracket>
Os yw is-baragraff
< roundbracket elementid="20032527w-rb412">
(  1 )
< /roundbracket>
yn gymwys  - 
< /sentence>
< /p>
< p elementid="20032527w-p197">
< sentence type="heading" elementid="20032527w-s571">
< roundbracket elementid="20032527w-rb413">
(  a )
< /roundbracket>
yn ddarostyngedig i baragraff
< roundbracket elementid="20032527w-rb414">
(  b )
< /roundbracket>
o 'r is-baragraff hwn , rhaid ymdrin â 'r hysbysiad sy 'n rhoi gwybod i 'r darparydd presennol y mae i gael cyfle i gael gwrandawiad , at ddibenion Rhan II o 'r Ddeddf , ac er gwaethaf y ffaith nad yw 'n cydymffurfio â 'r gofynion ar gyfer hysbysiad o 'r fath o dan y Ddeddf , fel petai 'n hysbysiad o gynnig a roddwyd o dan adran 17
< roundbracket elementid="20032527w-rb415">
(  4 )
< /roundbracket>
o 'r Ddeddf i ddileu 'r cofrestriad o 'r dyddiad effeithiol ymlaen
< roundbracket elementid="20032527w-rb416">
(  ac eithrio yn unol â chais o dan adran 15
< roundbracket elementid="20032527w-rb417">
(  1 )
< /roundbracket>
< roundbracket elementid="20032527w-rb418">
(  b )
< /roundbracket>
)
< /roundbracket>
, ar gyfer ymgymeriad presennol ;
< /sentence>
< sentence type="heading" elementid="20032527w-s572">
< roundbracket elementid="20032527w-rb419">
(  b )
< /roundbracket>
bydd adran 18
< roundbracket elementid="20032527w-rb420">
(  2 )
< /roundbracket>
o 'r Ddeddf yn effeithiol fel petai  - 
< /sentence>
< sentence type="heading" elementid="20032527w-s573">
< roundbracket elementid="20032527w-rb421">
(  i )
< /roundbracket>
y gair
< doublequotation elementid="20032527w-dq66">
"
< english>
written
< /english>
"
< /doublequotation>
wedi 'i hepgor ym mharagraff
< roundbracket elementid="20032527w-rb422">
(  a )
< /roundbracket>
;
< /sentence>
< sentence type="heading" elementid="20032527w-s574">
< roundbracket elementid="20032527w-rb423">
(  ii )
< /roundbracket>
y paragraff canlynol wedi 'i roi yn lle paragraff
< roundbracket elementid="20032527w-rb424">
(  c )
< /roundbracket>
 - 
< /sentence>
< sentence type="normal" elementid="20032527w-s575">
< english>
< roundbracket elementid="20032527w-rb425">
(  c )
< /roundbracket>
the person who is registered in respect of the agency has been given an opportunity to make oral or written representations to the National Assembly concerning the matter within a reasonable period and has failed to make them within that period .
< /english>
< /sentence>
< /p>
< p elementid="20032527w-p198">
< sentence type="heading" elementid="20032527w-s576">
< strong elementid="20032527w-st48">
Trosglwyddo gwybodaeth a dogfennau
< /strong>
< /sentence>
< sentence type="normal" elementid="20032527w-s577">
5 .
< /sentence>
< sentence type="heading" elementid="20032527w-s578">
Rhaid i awdurdod drosglwyddo i 'r Cynulliad Cenedlaethol  - 
< /sentence>
< /p>
< p elementid="20032527w-p199">
< sentence type="heading" elementid="20032527w-s579">
< roundbracket elementid="20032527w-rb426">
(  a )
< /roundbracket>
yr holl wybodaeth a 'r dogfennau sydd yn ei feddiant ac sy 'n ymwneud â thrwyddedu unrhyw asiantaeth nyrsys ar unwaith wrth i 'r Rheoliadau hyn ddod i rym ac yr oedd yn arfer swyddogaethau 'r awdurdod trwyddedu o dan Ddeddf 1957 mewn perthynas â hwy yn union cyn i 'r rheoliadau hyn ddod i rym ;
< /sentence>
< sentence type="normal" elementid="20032527w-s580">
< roundbracket elementid="20032527w-rb427">
(  b )
< /roundbracket>
cyn gynted ag y bo 'n ymarferol , yr holl wybodaeth neu 'r holl ddogfennau hynny sy 'n dod i 'w meddiant ar ôl i 'r Rheoliadau hyn ddod i rym .
< /sentence>
< /p>
< p elementid="20032527w-p200">
< sentence type="heading" elementid="20032527w-s581">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20032527w-s582">
< em elementid="20032527w-em26">
< roundbracket elementid="20032527w-rb428">
(  Nid yw 'r nodyn hwn yn rhan o 'r Rheoliadau )
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20032527w-s583">
Mae 'r Rheoliadau hyn yn cael eu gwneud o dan Ddeddf Safonau Gofal 2000
< roundbracket elementid="20032527w-rb429">
(
< doublequotation elementid="20032527w-dq67">
"  y Ddeddf "
< /doublequotation>
)
< /roundbracket>
, ac maent yn gymwys i asiantaethau nyrsys yng Nghymru yn unig .
< /sentence>
< sentence type="normal" elementid="20032527w-s584">
Mae Rhannau I a II o 'r Ddeddf yn darparu mai Cynulliad Cenedlaethol Cymru , mewn perthynas â Chymru a fydd yn cofrestru ac yn archwilio sefydliadau ac asiantaethau , gan gynnwys asiantaethau nyrsys .
< /sentence>
< sentence type="normal" elementid="20032527w-s585">
Mae 'r Ddeddf hefyd yn darparu mai 'r Cynulliad fydd yn gwneud rheoliadau sy 'n llywodraethu 'r ffordd y mae sefydliadau ac asiantaethau yn cael eu rhedeg .
< /sentence>
< sentence type="normal" elementid="20032527w-s586">
Yn ôl rheoliad 4 , rhaid i bob asiantaeth baratoi datganiad o ddiben yngl n â 'r materion a nodir yn Atodlen 1 ac arweiniad defnyddiwr gwasanaeth i 'r asiantaeth
< roundbracket elementid="20032527w-rb430">
(  rheoliad 5 )
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20032527w-s587">
Rhaid i 'r asiantaeth gael ei rhedeg mewn modd sy 'n gyson â 'r datganiad o ddiben .
< /sentence>
< sentence type="normal" elementid="20032527w-s588">
Mae rheoliadau 7 i 11 yn gwneud darpariaeth yngl n â ffitrwydd y personau sy 'n rhedeg ac yn rheoli asiantaeth ac yn ei gwneud yn ofynnol i wybodaeth foddhaol gael ei sicrhau am y materion a bennir yn Atodlen 2 .
< /sentence>
< sentence type="normal" elementid="20032527w-s589">
Os corff yw 'r darparydd , rhaid iddo enwi unigolyn cyfrifol y mae 'n rhaid i 'r wybodaeth hon fod ar gael amdano
< roundbracket elementid="20032527w-rb431">
(  rheoliad 7 )
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20032527w-s590">
Mae rheoliad 8 yn rhagnodi o dan ba amgylchiadau y mae 'n rhaid penodi rheolwr ar gyfer yr asiantaeth , ac mae rheoliad 9 yn gwneud darpariaeth yngl n â ffitrwydd y rheolwr .
< /sentence>
< sentence type="normal" elementid="20032527w-s591">
Mae rheoliad 10 yn gosod gofynion cyffredinol yngl n ag ymddygiad priodol yr asiantaeth , a 'r angen am hyfforddiant priodol .
< /sentence>
< sentence type="normal" elementid="20032527w-s592">
Mae Rhan III yn gwneud darpariaeth yngl n ag ymddygiad asiantaethau , yn enwedig am ansawdd y gwasanaethau sydd i 'w darparu gan asiantaeth .
< /sentence>
< sentence type="normal" elementid="20032527w-s593">
Mae rheoliad 12 yn gwneud darpariaeth am ffitrwydd pob nyrs sy 'n cael ei chyflenwi gan asiantaeth .
< /sentence>
< sentence type="normal" elementid="20032527w-s594">
Mae rheoliad 13 yn nodi 'r polisïau a 'r gweithdrefnau y mae 'n rhaid i 'r person cofrestredig eu llunio a 'u gweithredu os yw 'r asiantaeth yn gweithredu fel busnes cyflogi .
< /sentence>
< sentence type="normal" elementid="20032527w-s595">
Yn ychwanegol , mae darpariaeth yn cael ei gwneud yngl n â staffio
< roundbracket elementid="20032527w-rb432">
(  rheoliad 14 )
< /roundbracket>
, darparu gwybodaeth i ddefnyddwyr gwasanaeth
< roundbracket elementid="20032527w-rb433">
(  rheoliad 16 )
< /roundbracket>
, cadw cofnodion
< roundbracket elementid="20032527w-rb434">
(  rheoliad 17 ac Atodlen 4 )
< /roundbracket>
a chwynion
< roundbracket elementid="20032527w-rb435">
(  rheoliad 18 )
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20032527w-s596">
Mae darpariaeth yn cael ei gwneud hefyd am addasrwydd safleoedd
< roundbracket elementid="20032527w-rb436">
(  rheoliad 20 )
< /roundbracket>
a 'r rheolaeth ariannol ar yr asiantaeth
< roundbracket elementid="20032527w-rb437">
(  rheoliad 21 )
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20032527w-s597">
Mae rheoliadau 22 i 25 yn ymdrin â rhoi hysbysiadau i 'r Cynulliad .
< /sentence>
< sentence type="normal" elementid="20032527w-s598">
Mae Rhan IV yn ymdrin â materion amrywiol .
< /sentence>
< sentence type="normal" elementid="20032527w-s599">
Yn benodol , mae rheoliad 27 yn darparu ar gyfer tramgwyddau .
< /sentence>
< sentence type="normal" elementid="20032527w-s600">
Gellir cael bod torri rheoliadau 4 i 23 yn dramgwydd gan y person cofrestredig .
< /sentence>
< sentence type="normal" elementid="20032527w-s601">
Mae rheoliad 28 yn diwygio Rheoliadau Rheoleiddio Gofal Cymdeithasol a Gofal Iechyd Annibynnol
< roundbracket elementid="20032527w-rb438">
(  Ffioedd )
< /roundbracket>
< roundbracket elementid="20032527w-rb439">
(  Cymru )
< /roundbracket>
2002 drwy ragnodi 'r ffi flynyddol ar gyfer cofrestru asiantaethau nyrsys .
< /sentence>
< sentence type="normal" elementid="20032527w-s602">
Mae rheoliad 29 yn gwneud diwygiadau i Reoliadau Cofrestru Gofal Cymdeithasol a Gofal Iechyd Annibynnol
< roundbracket elementid="20032527w-rb440">
(  Cymru )
< /roundbracket>
2002 i gynnwys asiantaethau nyrsys ac mae rheoliad 30 yn ymdrin â threfniadau trosiannol .
< /sentence>
< sentence type="heading" elementid="20032527w-s603">
< em elementid="20032527w-em27">
< english>
Notes
< /english>
:
< /em>
< /sentence>
< sentence type="normal" elementid="20032527w-s604">
< squarebracket elementid="20032527w-sb18">
[  1 ]
< /squarebracket>
2000 p.14 .
< /sentence>
< sentence type="normal" elementid="20032527w-s605">
Mae 'r pwerau yn arferadwy gan
< doublequotation elementid="20032527w-dq68">
"
< english>
the appropriate Minister
< /english>
"
< /doublequotation>
, sydd wedi 'i ddiffinio yn adran 121
< roundbracket elementid="20032527w-rb441">
(  1 )
< /roundbracket>
< roundbracket elementid="20032527w-rb442">
(  o 'i darllen gyda adran 5
< roundbracket elementid="20032527w-rb443">
(  1 )
< /roundbracket>
< roundbracket elementid="20032527w-rb444">
(  b )
< /roundbracket>
)
< /roundbracket>
, mewn perthynas â Chymru fel Cynulliad Cenedlaethol Cymru ac , mewn perthynas â Lloegr , yr Alban a Gogledd Iwerddon , fel yr Ysgrifennydd Gwladol .
< /sentence>
< sentence type="normal" elementid="20032527w-s606">
Mae
< doublequotation elementid="20032527w-dq69">
"
< english>
prescribed
< /english>
"
< /doublequotation>
a
< doublequotation elementid="20032527w-dq70">
"
< english>
regulations
< /english>
"
< /doublequotation>
wedi 'u diffinio yn adran 121
< roundbracket elementid="20032527w-rb445">
(  1 )
< /roundbracket>
o 'r Ddeddf .
< /sentence>
< sentence type="heading" elementid="20032527w-s607">
< a type="href" elementid="20032527w-a92">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20032527w-p201">
< sentence type="normal" elementid="20032527w-s608">
< squarebracket elementid="20032527w-sb19">
[  2 ]
< /squarebracket>
< em elementid="20032527w-em28">
Gweler
< /em>
adran 22
< roundbracket elementid="20032527w-rb446">
(  9 )
< /roundbracket>
o Ddeddf Safonau Gofal 2000 ynglyn â 'r gofyniad i ymgynghori .
< /sentence>
< sentence type="heading" elementid="20032527w-s609">
< a type="href" elementid="20032527w-a93">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20032527w-p202">
< sentence type="normal" elementid="20032527w-s610">
< squarebracket elementid="20032527w-sb20">
[  3 ]
< /squarebracket>
1957 p.16 .
< /sentence>
< sentence type="heading" elementid="20032527w-s611">
< a type="href" elementid="20032527w-a94">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20032527w-p203">
< sentence type="normal" elementid="20032527w-s612">
< squarebracket elementid="20032527w-sb21">
[  4 ]
< /squarebracket>
< em elementid="20032527w-em29">
Gweler
< /em>
Deddf Dehongli 1978
< roundbracket elementid="20032527w-rb447">
(  p.30 )
< /roundbracket>
, Atodlen 1 .
< /sentence>
< sentence type="normal" elementid="20032527w-s613">
Mewnosodwyd diffiniad o
< doublequotation elementid="20032527w-dq71">
"
< english>
registered
< /english>
"
< /doublequotation>
mewn perthynas â nyrsys , bydwragedd ac ymwelwyr iechyd gan Ddeddf Nyrsys , Bydwragedd ac Ymwelwyr Iechyd 1979
< roundbracket elementid="20032527w-rb448">
(  p.36 )
< /roundbracket>
, adran 23
< roundbracket elementid="20032527w-rb449">
(  4 )
< /roundbracket>
ac Atodlen 7 , paragraff 30 , fel y 'u hamnewidiwyd gan erthygl 54
< roundbracket elementid="20032527w-rb450">
(  3 )
< /roundbracket>
o Orchymyn Nyrsio a Bydwreigiaeth 2001
< roundbracket elementid="20032527w-rb451">
(  O.S. 2002/ 253 )
< /roundbracket>
, ac Atodlen 5 , paragraff 7 iddi , ar ddyddiad sydd i 'w bennu .
< /sentence>
< sentence type="heading" elementid="20032527w-s614">
< a type="href" elementid="20032527w-a95">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20032527w-p204">
< sentence type="normal" elementid="20032527w-s615">
< squarebracket elementid="20032527w-sb22">
[  5 ]
< /squarebracket>
1990 p .
< /sentence>
< sentence type="normal" elementid="20032527w-s616">
19 .
< /sentence>
< sentence type="normal" elementid="20032527w-s617">
< em elementid="20032527w-em30">
Gweler
< /em>
adran 5 o 'r Ddeddf honno fel y 'i diwygiwyd gan baragraff 69 o Atodlen 1 i Ddeddf Awdurdodau Iechyd 1995  p .
< /sentence>
< sentence type="normal" elementid="20032527w-s618">
17 ) ac adrannau 13
< roundbracket elementid="20032527w-rb452">
(  1 )
< /roundbracket>
a 14 o Ddeddf Iechyd 1999  p .
< /sentence>
< sentence type="normal" elementid="20032527w-s619">
8 ) .
< /sentence>
< sentence type="heading" elementid="20032527w-s620">
< a type="href" elementid="20032527w-a96">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20032527w-p205">
< sentence type="normal" elementid="20032527w-s621">
< squarebracket elementid="20032527w-sb23">
[  6 ]
< /squarebracket>
< em elementid="20032527w-em31">
Gweler
< /em>
adran 121
< roundbracket elementid="20032527w-rb453">
(  1 )
< /roundbracket>
o 'r Ddeddf i weld y diffiniad o
< doublequotation elementid="20032527w-dq72">
"
< english>
employment business
< /english>
"
< /doublequotation> .
< /sentence>
< sentence type="heading" elementid="20032527w-s622">
< a type="href" elementid="20032527w-a97">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20032527w-p206">
< sentence type="normal" elementid="20032527w-s623">
< squarebracket elementid="20032527w-sb24">
[  7 ]
< /squarebracket>
Sefydlwyd y Cyngor Nyrsio a Bydwreigiaeth o dan erthygl 3 o Orchymyn Nyrsio a Bydwreigiaeth 2001
< roundbracket elementid="20032527w-rb454">
(  O.S. 2002/ 253 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20032527w-s624">
< a type="href" elementid="20032527w-a98">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20032527w-p207">
< sentence type="normal" elementid="20032527w-s625">
< squarebracket elementid="20032527w-sb25">
[  8 ]
< /squarebracket>
O.S. 2002/ 921 fel y 'i diwygiwyd gan O.S.2003/ 237 ag O.S. 2003/ 781 .
< /sentence>
< sentence type="heading" elementid="20032527w-s626">
< a type="href" elementid="20032527w-a99">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20032527w-p208">
< sentence type="normal" elementid="20032527w-s627">
< squarebracket elementid="20032527w-sb26">
[  9 ]
< /squarebracket>
O.S. 2002/ 919  Cy .
< /sentence>
< sentence type="normal" elementid="20032527w-s628">
107 ) fel y 'i diwygiwyd gan O.S. 2002/ 237 .
< /sentence>
< sentence type="heading" elementid="20032527w-s629">
< a type="href" elementid="20032527w-a100">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20032527w-p209">
< sentence type="heading" elementid="20032527w-s630">
< squarebracket elementid="20032527w-sb27">
[  10 ]
< /squarebracket>
O.S. 
< a type="href" elementid="20032527w-a101">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20032527w-p210">
< sentence type="normal" elementid="20032527w-s631">
< squarebracket elementid="20032527w-sb28">
[  11 ]
< /squarebracket>
1998 p.38 .
< /sentence>
< sentence type="heading" elementid="20032527w-s632">
< a type="href" elementid="20032527w-a102">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20032527w-p211">
< sentence type="normal" elementid="20032527w-s633">
< squarebracket elementid="20032527w-sb29">
[  12 ]
< /squarebracket>
Mewnosodwyd adran 115
< roundbracket elementid="20032527w-rb455">
(  5 )
< /roundbracket>
< roundbracket elementid="20032527w-rb456">
(  ea )
< /roundbracket>
gan Ddeddf Safonau Gofal 2000 , adran 104 .
< /sentence>
< sentence type="heading" elementid="20032527w-s634">
< a type="href" elementid="20032527w-a103">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20032527w-p212">
< sentence type="normal" elementid="20032527w-s635">
< squarebracket elementid="20032527w-sb30">
[  13 ]
< /squarebracket>
Mae swydd o fewn adran 115
< roundbracket elementid="20032527w-rb457">
(  3 )
< /roundbracket>
os yw 'n golygu bod y person wrthi 'n rheolaidd yn gofalu am bersonau o dan 18 oed , yn eu hyfforddi , yn eu goruchwylio , neu os yw 'r personau hynny o dan ei ofal ef yn unig .
< /sentence>
< sentence type="normal" elementid="20032527w-s636">
Mae swydd o fewn adran 115
< roundbracket elementid="20032527w-rb458">
(  4 )
< /roundbracket>
os yw 'n fath a bennir mewn rheoliadau ac os yw 'n golygu bod y person wrthi 'n rheolaidd yn gofalu am bersonau sy 'n 18 oed neu drosodd , yn eu hyfforddi , yn eu goruchwylio , neu os yw 'r personau hynny o dan ei ofal ef yn unig .
< /sentence>
< sentence type="heading" elementid="20032527w-s637">
< a type="href" elementid="20032527w-a104">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20032527w-p213">
< sentence type="normal" elementid="20032527w-s638">
< squarebracket elementid="20032527w-sb31">
[  14 ]
< /squarebracket>
Mae adran 113
< roundbracket elementid="20032527w-rb459">
(  3A )
< /roundbracket>
ac 115
< roundbracket elementid="20032527w-rb460">
(  6A )
< /roundbracket>
wedi 'u hychwanegu at Ddeddf yr Heddlu 1997 gan adran 8 o Ddeddf Amddiffyn Plant 1999
< roundbracket elementid="20032527w-rb461">
(  p.14 )
< /roundbracket>
, ac wedi 'u diwygio gan adrannau 104 a 116 o Ddeddf Safonau Gofal 2000 a pharagraff 25 o Atodlen 4 iddi .
< /sentence>
< sentence type="normal" elementid="20032527w-s639">
Mae adrannau 113
< roundbracket elementid="20032527w-rb462">
(  3C )
< /roundbracket>
a 115
< roundbracket elementid="20032527w-rb463">
(  6B )
< /roundbracket>
i 'w hychwanegu at Ddeddf yr Heddlu 1997 gan adran 90 o Ddeddf Safonau Gofal 2000 ar ddyddiad sydd i 'w bennu .
< /sentence>
< sentence type="heading" elementid="20032527w-s640">
< a type="href" elementid="20032527w-a105">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20032527w-p214">
< sentence type="normal" elementid="20032527w-s641">
< squarebracket elementid="20032527w-sb32">
[  15 ]
< /squarebracket>
Mae 'r gofrestr yn cael ei chadw yn unol â pharagraff 10 o Atodlen 2 i Orchymyn Nyrsio a Bydwreigiaeth 2001
< roundbracket elementid="20032527w-rb464">
(  O.S. 2002/ 253 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20032527w-s642">
< a type="href" elementid="20032527w-a106">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20032527w-p215">
< sentence type="normal" elementid="20032527w-s643">
< squarebracket elementid="20032527w-sb33">
[  16 ]
< /squarebracket>
1974 p.53 .
< /sentence>
< sentence type="heading" elementid="20032527w-s644">
< a type="href" elementid="20032527w-a107">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20032527w-p216">
< sentence type="normal" elementid="20032527w-s645">
< squarebracket elementid="20032527w-sb34">
[  17 ]
< /squarebracket>
O.S. 1975/ 1023 .
< /sentence>
< sentence type="normal" elementid="20032527w-s646">
Mae diwygiadau perthnasol wedi 'u gwneud gan O.S. 1986/ 1249 , O.S. 1986/ 2268 ac O.S. 2001/ 1192 .
< /sentence>
< sentence type="heading" elementid="20032527w-s647">
< a type="href" elementid="20032527w-a108">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20032527w-p217">
< sentence type="normal" elementid="20032527w-s648">
< squarebracket elementid="20032527w-sb35">
[  18 ]
< /squarebracket>
O.S. 1975/ 1023 .
< /sentence>
< sentence type="normal" elementid="20032527w-s649">
Mae diwygiadau perthnasol wedi 'u gwneud gan O.S. 1986/ 1249 , 1986/ 2268 , 2001/ 1192 a 2002/ 441 .
< /sentence>
< sentence type="heading" elementid="20032527w-s650">
< a type="href" elementid="20032527w-a109">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20032527w-p218">
< sentence type="heading" elementid="20032527w-s651">
< a type="href" elementid="20032527w-a110">
< english>
English version
< /english>
< /a>
< /sentence>
< sentence type="heading" elementid="20032527w-s652">
ISBN 0 11090787 6
< /sentence>
< /p>
< table elementid="20032527w-tb37">
< td elementid="20032527w-td75">
< p elementid="20032527w-p219">
< sentence type="heading" elementid="20032527w-s653">
< a type="href" elementid="20032527w-a111">
< english>
Other UK SIs
< /english>
< /a>
< a type="href" elementid="20032527w-a112">
< english>
Home
< /english>
< /a>
< a type="href" elementid="20032527w-a113">
< english>
National Assembly for Wales Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20032527w-a114">
< english>
Scottish Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20032527w-a115">
< english>
Statutory Rules of Northern Ireland
< /english>
< /a>
< a type="href" elementid="20032527w-a116">
< english>
Her Majesty 's Stationery Office
< /english>
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td76">
< p elementid="20032527w-p220">
< sentence type="heading" elementid="20032527w-s654">
< english>
< strong elementid="20032527w-st49">
We welcome your 
< a type="href" elementid="20032527w-a117">
comments
< /a>
on this site
< /strong>
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td77">
< p elementid="20032527w-p221">
< sentence type="heading" elementid="20032527w-s655">
< english>
© Crown copyright 2003
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20032527w-td78">
< p elementid="20032527w-p222">
< sentence type="heading" elementid="20032527w-s656">
< english>
Prepared 9 October 2003
< /english>
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>9989< /wordcount>
< /file>
< file elementid="20033054w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Rheoliadau Asiantaethau Nyrsys ( Cymru ) ( Diwygio ) 2003< /pagetitle>
< doc_class>Statutory Instrument - Wales< /doc_class>
< year>2003< /year>
< number>3054< /number>
< legislation_code>30102< /legislation_code>
< legislation_type>Statutory Instrument - Wales< /legislation_type>
< subject>NYRSYS, BYDWRAGEDD AC YMWELWYR IECHYD, CYMRU< /subject>
< laid_date>_EMPTY< /laid_date>
< made_date>20031126< /made_date>
< coming_into_force>20031201< /coming_into_force>
< isbn>011090821X< /isbn>
< title>Rheoliadau Asiantaethau Nyrsys (Cymru) (Diwygio) 2003< /title>
< signatory>D. Elis-Thomas< /signatory>
< signatory_title>Llywydd y Cynulliad Cenedlaethol< /signatory_title>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20033054w-p1">
< sentence type="heading" elementid="20033054w-s1">
Offerynnau Statudol 2003 Rhif 3054
< roundbracket elementid="20033054w-rb1">
(  Cy.292 )
< /roundbracket>
< /sentence>
< /p>
< p elementid="20033054w-p2">
< sentence type="heading" elementid="20033054w-s2">
< strong elementid="20033054w-st1">
Rheoliadau Asiantaethau Nyrsys
< roundbracket elementid="20033054w-rb2">
(  Cymru )
< /roundbracket>
< roundbracket elementid="20033054w-rb3">
(  Diwygio )
< /roundbracket>
2003
< /strong>
< /sentence>
< /p>
< p elementid="20033054w-p3">
< sentence type="heading" elementid="20033054w-s3">
© Hawlfraint y Goron 2003
< /sentence>
< /p>
< p elementid="20033054w-p4">
< sentence type="normal" elementid="20033054w-s4">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20033054w-p5">
< sentence type="normal" elementid="20033054w-s5">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20033054w-s6">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20033054w-p6">
< sentence type="normal" elementid="20033054w-s7">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20033054w-s8">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20033054w-s9">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20033054w-p7">
< sentence type="normal" elementid="20033054w-s10">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20033054w-s11">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan
< english>
The Stationery Office Limited
< /english>
fel
< strong elementid="20033054w-st2">
Rheoliadau Asiantaethau Nyrsys
< roundbracket elementid="20033054w-rb4">
(  Cymru )
< /roundbracket>
< roundbracket elementid="20033054w-rb5">
(  Diwygio )
< /roundbracket>
2003
< /strong>
, ISBN 011090821X. Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20033054w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20033054w-s12">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20033054w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20033054w-p8">
< sentence type="normal" elementid="20033054w-s13">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20033054w-a3">
< english>
Frequently Asked Questions
< /english>
< /a> .
< /sentence>
< /p>
< p elementid="20033054w-p9">
< sentence type="normal" elementid="20033054w-s14">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20033054w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20033054w-s15">
Pan welwch fotwm
< doublequotation elementid="20033054w-dq2">
"
< english>
continue
< /english>
"
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20033054w-p10">
< sentence type="heading" elementid="20033054w-s16">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="heading" elementid="20033054w-s17">
< strong elementid="20033054w-st3">
2003 Rhif 3054
< roundbracket elementid="20033054w-rb6">
(  Cy.292 )
< /roundbracket>
< /strong>
< /sentence>
< sentence type="heading" elementid="20033054w-s18">
< strong elementid="20033054w-st4">
NYRSYS , BYDWRAGEDD AC YMWELWYR IECHYD , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20033054w-s19">
Rheoliadau Asiantaethau Nyrsys
< roundbracket elementid="20033054w-rb7">
(  Cymru )
< /roundbracket>
< roundbracket elementid="20033054w-rb8">
(  Diwygio )
< /roundbracket>
2003
< /sentence>
< /p>
< table elementid="20033054w-tb1">
< td elementid="20033054w-td1">
< p elementid="20033054w-p11">
< sentence type="heading" elementid="20033054w-s20">
< em elementid="20033054w-em1">
Wedi 'u gwneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20033054w-td2">
< p elementid="20033054w-p12">
< sentence type="heading" elementid="20033054w-s21">
< em elementid="20033054w-em2">
26 Tachwedd 2003
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20033054w-td3">
< p elementid="20033054w-p13">
< sentence type="heading" elementid="20033054w-s22">
< em elementid="20033054w-em3">
Yn dod i rym
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20033054w-td4">
< p elementid="20033054w-p14">
< sentence type="heading" elementid="20033054w-s23">
< em elementid="20033054w-em4">
1 Rhagfyr 2003
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20033054w-p15">
< sentence type="heading" elementid="20033054w-s24">
Mae Cynulliad Cenedlaethol Cymru , drwy arfer y pwerau a roddwyd iddo gan adrannau 4
< roundbracket elementid="20033054w-rb9">
(  6 )
< /roundbracket>
, 16
< roundbracket elementid="20033054w-rb10">
(  3 )
< /roundbracket>
, 22
< roundbracket elementid="20033054w-rb11">
(  1 )
< /roundbracket>
a
< roundbracket elementid="20033054w-rb12">
(  2 )
< /roundbracket>
< roundbracket elementid="20033054w-rb13">
(  b )
< /roundbracket>
, 25 , 34
< roundbracket elementid="20033054w-rb14">
(  1 )
< /roundbracket>
, 35 a 118
< roundbracket elementid="20033054w-rb15">
(  5 )
< /roundbracket>
 - 
< roundbracket elementid="20033054w-rb16">
(  7 )
< /roundbracket>
o Ddeddf Safonau Gofal 2000
< squarebracket elementid="20033054w-sb1">
[ 
< a type="href" elementid="20033054w-a4">
1
< /a>
]
< /squarebracket>
drwy hyn yn gwneud y Rheoliadau canlynol :  - 
< /sentence>
< sentence type="heading" elementid="20033054w-s25">
< strong elementid="20033054w-st5">
Enwi , cychwyn a chymhwyso
< /strong>
< /sentence>
< sentence type="normal" elementid="20033054w-s26">
1 .
< /sentence>
< sentence type="normal" elementid="20033054w-s27">
 - 
< roundbracket elementid="20033054w-rb17">
(  1 )
< /roundbracket>
Enw 'r Rheoliadau hyn yw Rheoliadau Asiantaethau Nyrsys
< roundbracket elementid="20033054w-rb18">
(  Cymru )
< /roundbracket>
< roundbracket elementid="20033054w-rb19">
(  Diwygio )
< /roundbracket>
2003 a deuant i rym ar 1 Rhagfyr 2003 .
< /sentence>
< sentence type="normal" elementid="20033054w-s28">
< roundbracket elementid="20033054w-rb20">
(  2 )
< /roundbracket>
Mae 'r Rheoliadau hyn yn gymwys i asiantaethau nyrsys yng Nghymru yn unig .
< /sentence>
< sentence type="heading" elementid="20033054w-s29">
< strong elementid="20033054w-st6">
Diwygio Rheoliadau Asiantaethau Nyrsys
< roundbracket elementid="20033054w-rb21">
(  Cymru )
< /roundbracket>
< roundbracket elementid="20033054w-rb22">
(  Diwygio )
< /roundbracket>
2003
< /strong>
< /sentence>
< sentence type="normal" elementid="20033054w-s30">
2 .
< /sentence>
< sentence type="heading" elementid="20033054w-s31">
Diwygir Rheoliadau Asiantaethau Nyrsys
< roundbracket elementid="20033054w-rb23">
(  Cymru )
< /roundbracket>
2003
< squarebracket elementid="20033054w-sb2">
[ 
< a type="href" elementid="20033054w-a5">
2
< /a>
]
< /squarebracket>
fel a ganlyn :  - 
< /sentence>
< sentence type="heading" elementid="20033054w-s32">
< roundbracket elementid="20033054w-rb24">
(  ch )
< /roundbracket>
bod gwybodaeth lawn a boddhaol ar gael amdano/ amdani  - 
< /sentence>
< sentence type="heading" elementid="20033054w-s33">
< roundbracket elementid="20033054w-rb25">
(  i )
< /roundbracket>
ac eithrio lle mae paragraff
< roundbracket elementid="20033054w-rb26">
(  3 )
< /roundbracket>
yn gymwys , mewn perthynas â phob mater a bennir ym mharagraffau 1 i 4 a 6 i 13 o Atodlen 3 ;
< /sentence>
< sentence type="normal" elementid="20033054w-s34">
< roundbracket elementid="20033054w-rb27">
(  ii )
< /roundbracket>
lle mae paragraff
< roundbracket elementid="20033054w-rb28">
(  3 )
< /roundbracket>
yn gymwys , mewn perthynas â phob mater a bennir ym mharagraffau 1 i 3 a 5 i 13 o Atodlen 3 .
< /sentence>
< /p>
< p elementid="20033054w-p16">
< sentence type="normal" elementid="20033054w-s35">
Llofnodwyd ar ran Cynulliad Cenedlaethol Cymru o dan adran 66
< roundbracket elementid="20033054w-rb29">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20033054w-sb3">
[ 
< a type="href" elementid="20033054w-a6">
3
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="heading" elementid="20033054w-s36">
< em elementid="20033054w-em5">
D. Elis-Thomas
< /em>
< /sentence>
< sentence type="heading" elementid="20033054w-s37">
Llywydd y Cynulliad Cenedlaethol
< /sentence>
< sentence type="heading" elementid="20033054w-s38">
26 Tachwedd 2003
< /sentence>
< /p>
< p elementid="20033054w-p17">
< sentence type="heading" elementid="20033054w-s39">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20033054w-s40">
< em elementid="20033054w-em6">
< roundbracket elementid="20033054w-rb30">
(  Nid yw 'r nodyn hwn yn rhan o 'r Rheoliadau )
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20033054w-s41">
Mae 'r Rheoliadau hyn yn diwygio Rheoliadau Asiantaethau Nyrsys
< roundbracket elementid="20033054w-rb31">
(  Cymru )
< /roundbracket>
< roundbracket elementid="20033054w-rb32">
(  Diwygio )
< /roundbracket>
2003
< roundbracket elementid="20033054w-rb33">
(
< doublequotation elementid="20033054w-dq3">
"  Rheoliadau 2003 "
< /doublequotation>
)
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20033054w-s42">
Mae Rheoliad 12 o Reoliadau 2003 yn ei gwneud yn ofynnol bod rhaid i 'r person sydd wedi 'i gofrestru o dan Ddeddf Safonau Gofal 2000 mewn perthynas ag asiantaeth nyrsys sicrhau na fydd unrhyw nyrs yn cael ei chyflenwi gan yr asiantaeth oni bai ei bod yn
< doublequotation elementid="20033054w-dq4">
"  ffit "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20033054w-s43">
Un o ofynion ffitrwydd yw bod yn rhaid i wybodaeth a dogfennau penodol fod ar gael mewn perthynas â 'r nyrsys hyn .
< /sentence>
< sentence type="normal" elementid="20033054w-s44">
Ymhlith y dogfennau y mae 'n rhaid iddynt fod ar gael y mae tystysgrifau record droseddol a roddir naill ai o dan adran 113 neu 115 o Ddeddf yr Heddlu 1997 ac adroddiad o wiriad gan yr heddlu .
< /sentence>
< sentence type="normal" elementid="20033054w-s45">
Mae 'r diwygiadau i Reoliadau 2003 yn darparu nad oes rhaid i wiriad gan yr heddlu fod ar gael ond lle bydd cais wedi 'i wneud am dystysgrif o dan Ddeddf yr Heddlu ond nad yw 'r dystysgrif wedi 'i rhoi .
< /sentence>
< sentence type="heading" elementid="20033054w-s46">
< em elementid="20033054w-em7">
< english>
Notes
< /english>
:
< /em>
< /sentence>
< sentence type="normal" elementid="20033054w-s47">
< squarebracket elementid="20033054w-sb4">
[  1 ]
< /squarebracket>
2000 p.14 .
< /sentence>
< sentence type="normal" elementid="20033054w-s48">
Mae 'r pwerau yn arferadwy gan y Gweinidog priodol , a ddiffinnir yn adran 121
< roundbracket elementid="20033054w-rb34">
(  1 )
< /roundbracket>
fel y 'i darllenir gydag adran 5
< roundbracket elementid="20033054w-rb35">
(  1 )
< /roundbracket>
< roundbracket elementid="20033054w-rb36">
(  b )
< /roundbracket>
, mewn perthynas â Chymru fel Cynulliad Cenedlaethol Cymru .
< /sentence>
< sentence type="heading" elementid="20033054w-s49">
< a type="href" elementid="20033054w-a7">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20033054w-p18">
< sentence type="normal" elementid="20033054w-s50">
< squarebracket elementid="20033054w-sb5">
[  2 ]
< /squarebracket>
O.S. 2003/ 2527
< roundbracket elementid="20033054w-rb37">
(  Cy.242 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20033054w-s51">
< a type="href" elementid="20033054w-a8">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20033054w-p19">
< sentence type="normal" elementid="20033054w-s52">
< squarebracket elementid="20033054w-sb6">
[  3 ]
< /squarebracket>
1998 p.38 .
< /sentence>
< sentence type="heading" elementid="20033054w-s53">
< a type="href" elementid="20033054w-a9">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20033054w-p20">
< sentence type="heading" elementid="20033054w-s54">
< a type="href" elementid="20033054w-a10">
< english>
English version
< /english>
< /a>
< /sentence>
< sentence type="heading" elementid="20033054w-s55">
ISBN 0 11090821 X
< /sentence>
< /p>
< table elementid="20033054w-tb2">
< td elementid="20033054w-td5">
< p elementid="20033054w-p21">
< sentence type="heading" elementid="20033054w-s56">
< a type="href" elementid="20033054w-a11">
< english>
Other UK SIs
< /english>
< /a>
< a type="href" elementid="20033054w-a12">
< english>
Home
< /english>
< /a>
< a type="href" elementid="20033054w-a13">
< english>
National Assembly for Wales Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20033054w-a14">
< english>
Scottish Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20033054w-a15">
< english>
Statutory Rules of Northern Ireland
< /english>
< /a>
< a type="href" elementid="20033054w-a16">
< english>
Her Majesty 's Stationery Office
< /english>
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20033054w-td6">
< p elementid="20033054w-p22">
< sentence type="heading" elementid="20033054w-s57">
< english>
< strong elementid="20033054w-st7">
We welcome your 
< a type="href" elementid="20033054w-a17">
comments
< /a>
on this site
< /strong>
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20033054w-td7">
< p elementid="20033054w-p23">
< sentence type="heading" elementid="20033054w-s58">
< english>
© Crown copyright 2003
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20033054w-td8">
< p elementid="20033054w-p24">
< sentence type="heading" elementid="20033054w-s59">
< english>
Prepared 4 December 2003
< /english>
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>783< /wordcount>
< /file>
< /corpus>

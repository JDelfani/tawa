< ?xml version = "1.0" encoding="UTF-8"?>
< corpus topic="land" language="Welsh">
< file elementid="20043238w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Gorchymyn Deddf Ymddygiad Gwrthgymdeithasol 2003 ( Cychwyn Rhif 3 ) ( Cymru ) 2004< /pagetitle>
< doc_class>Statutory Instrument - Wales< /doc_class>
< year>2004< /year>
< number>3238< /number>
< legislation_code>30102< /legislation_code>
< legislation_type>Statutory Instrument - Wales< /legislation_type>
< subject>TIR, CYMRU^YMDDYGIAD GWRTHGYMDEITHASOL, CYMRU< /subject>
< laid_date>_EMPTY< /laid_date>
< made_date>20041208< /made_date>
< coming_into_force>_EMPTY< /coming_into_force>
< isbn>0110910486< /isbn>
< title>Gorchymyn Deddf Ymddygiad Gwrthgymdeithasol 2003 (Cychwyn Rhif 3) (Cymru) 2004< /title>
< signatory>D. Elis-Thomas< /signatory>
< signatory_title>Llywydd y Cynulliad Cenedlaethol< /signatory_title>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20043238w-p1">
< sentence type="heading" elementid="20043238w-s1">
Offerynnau Statudol 2004 Rhif 3238
< roundbracket elementid="20043238w-rb1">
(  Cy.281 )
< /roundbracket>
< roundbracket elementid="20043238w-rb2">
(  C.144 )
< /roundbracket>
< /sentence>
< /p>
< p elementid="20043238w-p2">
< sentence type="heading" elementid="20043238w-s2">
< strong elementid="20043238w-st1">
Gorchymyn Deddf Ymddygiad Gwrthgymdeithasol 2003
< roundbracket elementid="20043238w-rb3">
(  Cychwyn Rhif 3 )
< /roundbracket>
< roundbracket elementid="20043238w-rb4">
(  Cymru )
< /roundbracket>
2004
< /strong>
< /sentence>
< /p>
< p elementid="20043238w-p3">
< sentence type="heading" elementid="20043238w-s3">
© Hawlfraint y Goron 2004
< /sentence>
< /p>
< p elementid="20043238w-p4">
< sentence type="normal" elementid="20043238w-s4">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20043238w-p5">
< sentence type="normal" elementid="20043238w-s5">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20043238w-s6">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20043238w-p6">
< sentence type="normal" elementid="20043238w-s7">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20043238w-s8">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20043238w-s9">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20043238w-p7">
< sentence type="normal" elementid="20043238w-s10">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20043238w-s11">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan
< english>
The Stationery Office Limited
< /english>
fel
< strong elementid="20043238w-st2">
Gorchymyn Deddf Ymddygiad Gwrthgymdeithasol 2003
< roundbracket elementid="20043238w-rb5">
(  Cychwyn Rhif 3 )
< /roundbracket>
< roundbracket elementid="20043238w-rb6">
(  Cymru )
< /roundbracket>
2004
< /strong>
, ISBN 0110910486 .
< /sentence>
< sentence type="normal" elementid="20043238w-s12">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20043238w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20043238w-s13">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20043238w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20043238w-p8">
< sentence type="normal" elementid="20043238w-s14">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20043238w-a3">
< english>
Frequently Asked Questions
< /english>
< /a> .
< /sentence>
< /p>
< p elementid="20043238w-p9">
< sentence type="normal" elementid="20043238w-s15">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20043238w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20043238w-s16">
Pan welwch fotwm
< doublequotation elementid="20043238w-dq2">
"  continue "
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20043238w-p10">
< sentence type="heading" elementid="20043238w-s17">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="heading" elementid="20043238w-s18">
< strong elementid="20043238w-st3">
2004 Rhif 3238
< roundbracket elementid="20043238w-rb7">
(  Cy.281 )
< /roundbracket>
< roundbracket elementid="20043238w-rb8">
(  C.144 )
< /roundbracket>
< /strong>
< /sentence>
< sentence type="heading" elementid="20043238w-s19">
< strong elementid="20043238w-st4">
TIR , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20043238w-s20">
< strong elementid="20043238w-st5">
YMDDYGIAD GWRTHGYMDEITHASOL , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20043238w-s21">
Gorchymyn Deddf Ymddygiad Gwrthgymdeithasol 2003
< roundbracket elementid="20043238w-rb9">
(  Cychwyn Rhif 3 )
< /roundbracket>
< roundbracket elementid="20043238w-rb10">
(  Cymru )
< /roundbracket>
2004
< /sentence>
< /p>
< table elementid="20043238w-tb1">
< td elementid="20043238w-td1">
< p elementid="20043238w-p11">
< sentence type="heading" elementid="20043238w-s22">
< em elementid="20043238w-em1">
Wedi 'i wneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td2">
< p elementid="20043238w-p12">
< sentence type="heading" elementid="20043238w-s23">
< em elementid="20043238w-em2">
8 Rhagfyr 2004
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20043238w-p13">
< sentence type="heading" elementid="20043238w-s24">
Mae Cynulliad Cenedlaethol Cymru , drwy arfer y pwerau a roddwyd iddo gan adran 93
< roundbracket elementid="20043238w-rb11">
(  3 )
< /roundbracket>
< roundbracket elementid="20043238w-rb12">
(  b )
< /roundbracket>
o Ddeddf Ymddygiad Gwrthgymdeithasol 2003
< roundbracket elementid="20043238w-rb13">
(
< doublequotation elementid="20043238w-dq3">
"  y Ddeddf "
< /doublequotation>
)
< /roundbracket>
< squarebracket elementid="20043238w-sb1">
[ 
< a type="href" elementid="20043238w-a4">
1
< /a>
]
< /squarebracket>
, drwy hyn yn gwneud y Gorchymyn a ganlyn :
< /sentence>
< sentence type="heading" elementid="20043238w-s25">
< strong elementid="20043238w-st6">
Enwi a chymhwyso
< /strong>
< /sentence>
< sentence type="normal" elementid="20043238w-s26">
1 .
< /sentence>
< sentence type="normal" elementid="20043238w-s27">
 - 
< roundbracket elementid="20043238w-rb14">
(  1 )
< /roundbracket>
Enw 'r Gorchymyn hwn yw Gorchymyn Deddf Ymddygiad Gwrthgymdeithasol 2003
< roundbracket elementid="20043238w-rb15">
(  Cychwyn Rhif 3 )
< /roundbracket>
< roundbracket elementid="20043238w-rb16">
(  Cymru )
< /roundbracket>
2004 .
< /sentence>
< sentence type="normal" elementid="20043238w-s28">
< roundbracket elementid="20043238w-rb17">
(  2 )
< /roundbracket>
Mae 'r Gorchymyn hwn yn gymwys i gŵynion am wrychoedd neu berthi
< squarebracket elementid="20043238w-sb2">
[ 
< a type="href" elementid="20043238w-a5">
2
< /a>
]
< /squarebracket>
yng Nghymru .
< /sentence>
< sentence type="heading" elementid="20043238w-s29">
Y darpariaethau sydd i 'w cychwyn
< /sentence>
< sentence type="normal" elementid="20043238w-s30">
2 .
< /sentence>
< sentence type="normal" elementid="20043238w-s31">
Daw Rhan 8 o 'r Ddeddf
< roundbracket elementid="20043238w-rb18">
(  Gwrychoedd neu Berthi Uchel )
< /roundbracket>
i rym ar 31 Rhagfyr 2004 .
< /sentence>
< sentence type="normal" elementid="20043238w-s32">
Llofnodwyd ar ran Cynulliad Cenedlaethol Cymru yn unol ag adran 66
< roundbracket elementid="20043238w-rb19">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20043238w-sb3">
[ 
< a type="href" elementid="20043238w-a6">
3
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="heading" elementid="20043238w-s33">
< em elementid="20043238w-em3">
D. Elis-Thomas
< /em>
< /sentence>
< sentence type="heading" elementid="20043238w-s34">
Llywydd y Cynulliad Cenedlaethol
< /sentence>
< sentence type="heading" elementid="20043238w-s35">
8 Rhagfyr 2004
< /sentence>
< sentence type="heading" elementid="20043238w-s36">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20043238w-s37">
< em elementid="20043238w-em4">
< roundbracket elementid="20043238w-rb20">
(  Nid yw 'r nodyn hwn yn rhan o 'r Gorchymyn )
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20043238w-s38">
Mae 'r Gorchymyn hwn yn dod â darpariaethau Rhan 8 o Ddeddf Ymddygiad Gwrthgymdeithasol 2003
< roundbracket elementid="20043238w-rb21">
(
< doublequotation elementid="20043238w-dq4">
"  y Ddeddf "
< /doublequotation>
)
< /roundbracket>
i rym , mewn perthynas â gwrychoedd neu berthi yng Nghymru , ar 31 Rhagfyr 2004 .
< /sentence>
< sentence type="normal" elementid="20043238w-s39">
Mae Rhan 8 o 'r Ddeddf yn rhoi 'r pŵer i awdurdodau lleol ymdrin â chwynion am wrychoedd neu berthi uchel sy 'n cael effaith andwyol ar allu cymydog i fwynhau ei eiddo .
< /sentence>
< sentence type="normal" elementid="20043238w-s40">
Mae hefyd yn rhoi 'r pŵer i Gynulliad Cenedlaethol Cymru
< roundbracket elementid="20043238w-rb22">
(
< doublequotation elementid="20043238w-dq5">
"  y Cynulliad Cenedlaethol "
< /doublequotation>
)
< /roundbracket>
wneud rheoliadau mewn perthynas â gwrychoedd neu berthi yng Nghymru , i osod mwyafswm y ffï y gall awdurdod lleol ei chodi ar gyfer ymdrin â chais o dan y Ddeddf , ac i bennu 'r gweithdrefnau y dylid eu dilyn pan wneir apêl i 'r Cynulliad Cenedlaethol o dan adran 71 o 'r Ddeddf .
< /sentence>
< sentence type="heading" elementid="20043238w-s41">
< strong elementid="20043238w-st7">
NODYN AM ORCHMYNION CYCHWYN BLAENOROL
< /strong>
< /sentence>
< sentence type="heading" elementid="20043238w-s42">
< em elementid="20043238w-em5">
< roundbracket elementid="20043238w-rb23">
(  Nid yw 'r nodyn hwn yn rhan o 'r Gorchymyn )
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20043238w-s43">
Daethpwyd â darpariaethau 'r Ddeddf y cyfeirir atynt yn Nhabl 1 isod i rym mewn perthynas â Chymru a Lloegr drwy Orchmynion Cychwyn a wnaed cyn dyddiad y Gorchymyn hwn .
< /sentence>
< sentence type="normal" elementid="20043238w-s44">
Daethpwyd â darpariaethau 'r Ddeddf y cyfeirir atynt yn Nhabl 2 isod i rym mewn perthynas â Chymru drwy Orchmynion Cychwyn a wnaed cyn dyddiad y Gorchymyn hwn .
< /sentence>
< sentence type="heading" elementid="20043238w-s45">
< strong elementid="20043238w-st8">
TABL 1
< /strong>
< /sentence>
< /p>
< table elementid="20043238w-tb2">
< td elementid="20043238w-td3">
< p elementid="20043238w-p14">
< sentence type="heading" elementid="20043238w-s46">
< em elementid="20043238w-em6">
Adran
< roundbracket elementid="20043238w-rb24">
(  nau )
< /roundbracket>
neu Atodlen
< roundbracket elementid="20043238w-rb25">
(  ni )
< /roundbracket>
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td4">
< p elementid="20043238w-p15">
< sentence type="heading" elementid="20043238w-s47">
< em elementid="20043238w-em7">
Dyddiad cychwyn
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td5">
< p elementid="20043238w-p16">
< sentence type="heading" elementid="20043238w-s48">
< em elementid="20043238w-em8">
Rhif yr O.S.
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td6">
< p elementid="20043238w-p17">
< sentence type="heading" elementid="20043238w-s49">
1 i 11
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td7">
< p elementid="20043238w-p18">
< sentence type="heading" elementid="20043238w-s50">
20 Ionawr 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td8">
< p elementid="20043238w-p19">
< sentence type="heading" elementid="20043238w-s51">
2003/ 3300
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td9">
< p elementid="20043238w-p20">
< sentence type="heading" elementid="20043238w-s52">
18
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td10">
< p elementid="20043238w-p21">
< sentence type="heading" elementid="20043238w-s53">
27 Chwefror 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td11">
< p elementid="20043238w-p22">
< sentence type="heading" elementid="20043238w-s54">
2003/ 3300
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td12">
< p elementid="20043238w-p23">
< sentence type="heading" elementid="20043238w-s55">
23
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td13">
< p elementid="20043238w-p24">
< sentence type="heading" elementid="20043238w-s56">
27 Chwefror 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td14">
< p elementid="20043238w-p25">
< sentence type="heading" elementid="20043238w-s57">
2003/ 3300
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td15">
< p elementid="20043238w-p26">
< sentence type="heading" elementid="20043238w-s58">
25 i 29
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td16">
< p elementid="20043238w-p27">
< sentence type="heading" elementid="20043238w-s59">
27 Chwefror 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td17">
< p elementid="20043238w-p28">
< sentence type="heading" elementid="20043238w-s60">
2003/ 3300
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td18">
< p elementid="20043238w-p29">
< sentence type="heading" elementid="20043238w-s61">
30 i 38
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td19">
< p elementid="20043238w-p30">
< sentence type="heading" elementid="20043238w-s62">
20 Ionawr 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td20">
< p elementid="20043238w-p31">
< sentence type="heading" elementid="20043238w-s63">
2003/ 3300
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td21">
< p elementid="20043238w-p32">
< sentence type="heading" elementid="20043238w-s64">
39
< roundbracket elementid="20043238w-rb26">
(  1 )
< /roundbracket>
a
< roundbracket elementid="20043238w-rb27">
(  2 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td22">
< p elementid="20043238w-p33">
< sentence type="heading" elementid="20043238w-s65">
20 Ionawr 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td23">
< p elementid="20043238w-p34">
< sentence type="heading" elementid="20043238w-s66">
2003/ 3300
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td24">
< p elementid="20043238w-p35">
< sentence type="heading" elementid="20043238w-s67">
39
< roundbracket elementid="20043238w-rb28">
(  3 )
< /roundbracket>
< roundbracket elementid="20043238w-rb29">
(  yn rhannol )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td25">
< p elementid="20043238w-p36">
< sentence type="heading" elementid="20043238w-s68">
20 Ionawr 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td26">
< p elementid="20043238w-p37">
< sentence type="heading" elementid="20043238w-s69">
2003/ 3300
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td27">
< p elementid="20043238w-p38">
< sentence type="heading" elementid="20043238w-s70">
39
< roundbracket elementid="20043238w-rb30">
(  3 )
< /roundbracket>
< roundbracket elementid="20043238w-rb31">
(  y gweddill )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td28">
< p elementid="20043238w-p39">
< sentence type="heading" elementid="20043238w-s71">
30 Ebrill 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td29">
< p elementid="20043238w-p40">
< sentence type="heading" elementid="20043238w-s72">
2003/ 3300
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td30">
< p elementid="20043238w-p41">
< sentence type="heading" elementid="20043238w-s73">
39
< roundbracket elementid="20043238w-rb32">
(  4 )
< /roundbracket>
i
< roundbracket elementid="20043238w-rb33">
(  6 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td31">
< p elementid="20043238w-p42">
< sentence type="heading" elementid="20043238w-s74">
20 Ionawr 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td32">
< p elementid="20043238w-p43">
< sentence type="heading" elementid="20043238w-s75">
2003/ 3300
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td33">
< p elementid="20043238w-p44">
< sentence type="heading" elementid="20043238w-s76">
46
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td34">
< p elementid="20043238w-p45">
< sentence type="heading" elementid="20043238w-s77">
31 Mawrth 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td35">
< p elementid="20043238w-p46">
< sentence type="heading" elementid="20043238w-s78">
2004/ 690
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td36">
< p elementid="20043238w-p47">
< sentence type="heading" elementid="20043238w-s79">
53
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td37">
< p elementid="20043238w-p48">
< sentence type="heading" elementid="20043238w-s80">
20 Ionawr 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td38">
< p elementid="20043238w-p49">
< sentence type="heading" elementid="20043238w-s81">
2003/ 3300
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td39">
< p elementid="20043238w-p50">
< sentence type="heading" elementid="20043238w-s82">
54
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td40">
< p elementid="20043238w-p51">
< sentence type="heading" elementid="20043238w-s83">
31 Mawrth 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td41">
< p elementid="20043238w-p52">
< sentence type="heading" elementid="20043238w-s84">
2004/ 690
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td42">
< p elementid="20043238w-p53">
< sentence type="heading" elementid="20043238w-s85">
57 i 59
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td43">
< p elementid="20043238w-p54">
< sentence type="heading" elementid="20043238w-s86">
20 Ionawr 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td44">
< p elementid="20043238w-p55">
< sentence type="heading" elementid="20043238w-s87">
2003/ 3300
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td45">
< p elementid="20043238w-p56">
< sentence type="heading" elementid="20043238w-s88">
60 i 64
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td46">
< p elementid="20043238w-p57">
< sentence type="heading" elementid="20043238w-s89">
27 Chwefror 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td47">
< p elementid="20043238w-p58">
< sentence type="heading" elementid="20043238w-s90">
2003/ 3300
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td48">
< p elementid="20043238w-p59">
< sentence type="heading" elementid="20043238w-s91">
85
< roundbracket elementid="20043238w-rb34">
(  1 )
< /roundbracket>
i
< roundbracket elementid="20043238w-rb35">
(  3 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td49">
< p elementid="20043238w-p60">
< sentence type="heading" elementid="20043238w-s92">
20 Ionawr 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td50">
< p elementid="20043238w-p61">
< sentence type="heading" elementid="20043238w-s93">
2003/ 3300
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td51">
< p elementid="20043238w-p62">
< sentence type="heading" elementid="20043238w-s94">
85
< roundbracket elementid="20043238w-rb36">
(  4 )
< /roundbracket>
< roundbracket elementid="20043238w-rb37">
(  yn rhannol )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td52">
< p elementid="20043238w-p63">
< sentence type="heading" elementid="20043238w-s95">
20 Ionawr 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td53">
< p elementid="20043238w-p64">
< sentence type="heading" elementid="20043238w-s96">
2003/ 3300
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td54">
< p elementid="20043238w-p65">
< sentence type="heading" elementid="20043238w-s97">
85
< roundbracket elementid="20043238w-rb38">
(  4 )
< /roundbracket>
< roundbracket elementid="20043238w-rb39">
(  y gweddill )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td55">
< p elementid="20043238w-p66">
< sentence type="heading" elementid="20043238w-s98">
31 Mawrth 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td56">
< p elementid="20043238w-p67">
< sentence type="heading" elementid="20043238w-s99">
2004/ 690
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td57">
< p elementid="20043238w-p68">
< sentence type="heading" elementid="20043238w-s100">
85
< roundbracket elementid="20043238w-rb40">
(  5 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td58">
< p elementid="20043238w-p69">
< sentence type="heading" elementid="20043238w-s101">
31 Mawrth 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td59">
< p elementid="20043238w-p70">
< sentence type="heading" elementid="20043238w-s102">
2004/ 690
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td60">
< p elementid="20043238w-p71">
< sentence type="heading" elementid="20043238w-s103">
85
< roundbracket elementid="20043238w-rb41">
(  6 )
< /roundbracket>
< roundbracket elementid="20043238w-rb42">
(  yn rhannol )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td61">
< p elementid="20043238w-p72">
< sentence type="heading" elementid="20043238w-s104">
31 Mawrth 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td62">
< p elementid="20043238w-p73">
< sentence type="heading" elementid="20043238w-s105">
2004/ 690
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td63">
< p elementid="20043238w-p74">
< sentence type="heading" elementid="20043238w-s106">
85
< roundbracket elementid="20043238w-rb43">
(  7 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td64">
< p elementid="20043238w-p75">
< sentence type="heading" elementid="20043238w-s107">
20 Ionawr 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td65">
< p elementid="20043238w-p76">
< sentence type="heading" elementid="20043238w-s108">
2003/ 3300
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td66">
< p elementid="20043238w-p77">
< sentence type="heading" elementid="20043238w-s109">
85
< roundbracket elementid="20043238w-rb44">
(  8 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td67">
< p elementid="20043238w-p78">
< sentence type="heading" elementid="20043238w-s110">
27 Chwefror 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td68">
< p elementid="20043238w-p79">
< sentence type="heading" elementid="20043238w-s111">
2003/ 3300
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td69">
< p elementid="20043238w-p80">
< sentence type="heading" elementid="20043238w-s112">
85
< roundbracket elementid="20043238w-rb45">
(  9 )
< /roundbracket>
i
< roundbracket elementid="20043238w-rb46">
(  11 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td70">
< p elementid="20043238w-p81">
< sentence type="heading" elementid="20043238w-s113">
31 Mawrth 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td71">
< p elementid="20043238w-p82">
< sentence type="heading" elementid="20043238w-s114">
2004/ 690
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td72">
< p elementid="20043238w-p83">
< sentence type="heading" elementid="20043238w-s115">
86
< roundbracket elementid="20043238w-rb47">
(  1 )
< /roundbracket>
a
< roundbracket elementid="20043238w-rb48">
(  2 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td73">
< p elementid="20043238w-p84">
< sentence type="heading" elementid="20043238w-s116">
31 Mawrth 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td74">
< p elementid="20043238w-p85">
< sentence type="heading" elementid="20043238w-s117">
2004/ 690
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td75">
< p elementid="20043238w-p86">
< sentence type="heading" elementid="20043238w-s118">
86
< roundbracket elementid="20043238w-rb49">
(  3 )
< /roundbracket>
< roundbracket elementid="20043238w-rb50">
(  yn rhannol )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td76">
< p elementid="20043238w-p87">
< sentence type="heading" elementid="20043238w-s119">
20 Ionawr 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td77">
< p elementid="20043238w-p88">
< sentence type="heading" elementid="20043238w-s120">
2003/ 3300
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td78">
< p elementid="20043238w-p89">
< sentence type="heading" elementid="20043238w-s121">
86
< roundbracket elementid="20043238w-rb51">
(  3 )
< /roundbracket>
< roundbracket elementid="20043238w-rb52">
(  y gweddill )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td79">
< p elementid="20043238w-p90">
< sentence type="heading" elementid="20043238w-s122">
31 Mawrth 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td80">
< p elementid="20043238w-p91">
< sentence type="heading" elementid="20043238w-s123">
2004/ 690
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td81">
< p elementid="20043238w-p92">
< sentence type="heading" elementid="20043238w-s124">
86
< roundbracket elementid="20043238w-rb53">
(  4 )
< /roundbracket>
i
< roundbracket elementid="20043238w-rb54">
(  6 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td82">
< p elementid="20043238w-p93">
< sentence type="heading" elementid="20043238w-s125">
20 Ionawr 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td83">
< p elementid="20043238w-p94">
< sentence type="heading" elementid="20043238w-s126">
2003/ 3300
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td84">
< p elementid="20043238w-p95">
< sentence type="heading" elementid="20043238w-s127">
87
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td85">
< p elementid="20043238w-p96">
< sentence type="heading" elementid="20043238w-s128">
20 Ionawr 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td86">
< p elementid="20043238w-p97">
< sentence type="heading" elementid="20043238w-s129">
2003/ 3300
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td87">
< p elementid="20043238w-p98">
< sentence type="heading" elementid="20043238w-s130">
89
< roundbracket elementid="20043238w-rb55">
(  1 )
< /roundbracket>
i
< roundbracket elementid="20043238w-rb56">
(  4 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td88">
< p elementid="20043238w-p99">
< sentence type="heading" elementid="20043238w-s131">
20 Ionawr 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td89">
< p elementid="20043238w-p100">
< sentence type="heading" elementid="20043238w-s132">
2003/ 3300
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td90">
< p elementid="20043238w-p101">
< sentence type="heading" elementid="20043238w-s133">
89
< roundbracket elementid="20043238w-rb57">
(  5 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td91">
< p elementid="20043238w-p102">
< sentence type="heading" elementid="20043238w-s134">
31 Mawrth 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td92">
< p elementid="20043238w-p103">
< sentence type="heading" elementid="20043238w-s135">
2004/ 690
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td93">
< p elementid="20043238w-p104">
< sentence type="heading" elementid="20043238w-s136">
89
< roundbracket elementid="20043238w-rb58">
(  6 )
< /roundbracket>
a
< roundbracket elementid="20043238w-rb59">
(  7 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td94">
< p elementid="20043238w-p105">
< sentence type="heading" elementid="20043238w-s137">
20 Ionawr 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td95">
< p elementid="20043238w-p106">
< sentence type="heading" elementid="20043238w-s138">
2003/ 3300
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td96">
< p elementid="20043238w-p107">
< sentence type="heading" elementid="20043238w-s139">
90
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td97">
< p elementid="20043238w-p108">
< sentence type="heading" elementid="20043238w-s140">
30 Gorffennaf 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td98">
< p elementid="20043238w-p109">
< sentence type="heading" elementid="20043238w-s141">
2004/ 1502
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td99">
< p elementid="20043238w-p110">
< sentence type="heading" elementid="20043238w-s142">
92
< roundbracket elementid="20043238w-rb60">
(  yn rhannol )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td100">
< p elementid="20043238w-p111">
< sentence type="heading" elementid="20043238w-s143">
20 Ionawr 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td101">
< p elementid="20043238w-p112">
< sentence type="heading" elementid="20043238w-s144">
2003/ 3300
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td102">
< p elementid="20043238w-p113">
< sentence type="heading" elementid="20043238w-s145">
92
< roundbracket elementid="20043238w-rb61">
(  yn rhannol )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td103">
< p elementid="20043238w-p114">
< sentence type="heading" elementid="20043238w-s146">
31 Mawrth 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td104">
< p elementid="20043238w-p115">
< sentence type="heading" elementid="20043238w-s147">
2004/ 690
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td105">
< p elementid="20043238w-p116">
< sentence type="heading" elementid="20043238w-s148">
Atodlen 3
< roundbracket elementid="20043238w-rb62">
(  yn rhannol )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td106">
< p elementid="20043238w-p117">
< sentence type="heading" elementid="20043238w-s149">
20 Ionawr 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td107">
< p elementid="20043238w-p118">
< sentence type="heading" elementid="20043238w-s150">
2003/ 3300
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td108">
< p elementid="20043238w-p119">
< sentence type="heading" elementid="20043238w-s151">
Atodlen 3
< roundbracket elementid="20043238w-rb63">
(  yn rhannol )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td109">
< p elementid="20043238w-p120">
< sentence type="heading" elementid="20043238w-s152">
31 Mawrth 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td110">
< p elementid="20043238w-p121">
< sentence type="heading" elementid="20043238w-s153">
2004/ 690
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20043238w-p122">
< sentence type="heading" elementid="20043238w-s154">
< strong elementid="20043238w-st9">
TABL 2
< /strong>
< /sentence>
< /p>
< table elementid="20043238w-tb3">
< td elementid="20043238w-td111">
< p elementid="20043238w-p123">
< sentence type="heading" elementid="20043238w-s155">
< em elementid="20043238w-em9">
Adran
< roundbracket elementid="20043238w-rb64">
(  nau )
< /roundbracket>
neu Atodlen
< roundbracket elementid="20043238w-rb65">
(  ni )
< /roundbracket>
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td112">
< p elementid="20043238w-p124">
< sentence type="heading" elementid="20043238w-s156">
< em elementid="20043238w-em10">
Dyddiad cychwyn
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td113">
< p elementid="20043238w-p125">
< sentence type="heading" elementid="20043238w-s157">
< em elementid="20043238w-em11">
Rhif yr O.S.
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td114">
< p elementid="20043238w-p126">
< sentence type="heading" elementid="20043238w-s158">
13
< roundbracket elementid="20043238w-rb66">
(  gydag arbedion )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td115">
< p elementid="20043238w-p127">
< sentence type="heading" elementid="20043238w-s159">
30 Medi 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td116">
< p elementid="20043238w-p128">
< sentence type="heading" elementid="20043238w-s160">
2004/ 2557
< roundbracket elementid="20043238w-rb67">
(  Cy.228 )
< /roundbracket>
< roundbracket elementid="20043238w-rb68">
(  C.107 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td117">
< p elementid="20043238w-p129">
< sentence type="heading" elementid="20043238w-s161">
14
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td118">
< p elementid="20043238w-p130">
< sentence type="heading" elementid="20043238w-s162">
30 Medi 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td119">
< p elementid="20043238w-p131">
< sentence type="heading" elementid="20043238w-s163">
2004/ 2557
< roundbracket elementid="20043238w-rb69">
(  Cy.228 )
< /roundbracket>
< roundbracket elementid="20043238w-rb70">
(  C.107 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td120">
< p elementid="20043238w-p132">
< sentence type="heading" elementid="20043238w-s164">
16
< roundbracket elementid="20043238w-rb71">
(  gydag arbedion )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td121">
< p elementid="20043238w-p133">
< sentence type="heading" elementid="20043238w-s165">
30 Medi 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td122">
< p elementid="20043238w-p134">
< sentence type="heading" elementid="20043238w-s166">
2004/ 2557
< roundbracket elementid="20043238w-rb72">
(  Cy.228 )
< /roundbracket>
< roundbracket elementid="20043238w-rb73">
(  C.107 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td123">
< p elementid="20043238w-p135">
< sentence type="heading" elementid="20043238w-s167">
17
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td124">
< p elementid="20043238w-p136">
< sentence type="heading" elementid="20043238w-s168">
30 Medi 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td125">
< p elementid="20043238w-p137">
< sentence type="heading" elementid="20043238w-s169">
2004/ 2557
< roundbracket elementid="20043238w-rb74">
(  Cy.228 )
< /roundbracket>
< roundbracket elementid="20043238w-rb75">
(  C.107 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td126">
< p elementid="20043238w-p138">
< sentence type="heading" elementid="20043238w-s170">
40 i 45
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td127">
< p elementid="20043238w-p139">
< sentence type="heading" elementid="20043238w-s171">
31 Mawrth 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td128">
< p elementid="20043238w-p140">
< sentence type="heading" elementid="20043238w-s172">
2004/ 999
< roundbracket elementid="20043238w-rb76">
(  Cy.105 )
< /roundbracket>
< roundbracket elementid="20043238w-rb77">
(  C.43 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td129">
< p elementid="20043238w-p141">
< sentence type="heading" elementid="20043238w-s173">
47 i 52
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td130">
< p elementid="20043238w-p142">
< sentence type="heading" elementid="20043238w-s174">
31 Mawrth 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td131">
< p elementid="20043238w-p143">
< sentence type="heading" elementid="20043238w-s175">
2004/ 999
< roundbracket elementid="20043238w-rb78">
(  Cy.105 )
< /roundbracket>
< roundbracket elementid="20043238w-rb79">
(  C.43 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td132">
< p elementid="20043238w-p144">
< sentence type="heading" elementid="20043238w-s176">
55 a 56
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td133">
< p elementid="20043238w-p145">
< sentence type="heading" elementid="20043238w-s177">
31 Mawrth 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td134">
< p elementid="20043238w-p146">
< sentence type="heading" elementid="20043238w-s178">
2004/ 999
< roundbracket elementid="20043238w-rb80">
(  Cy.105 )
< /roundbracket>
< roundbracket elementid="20043238w-rb81">
(  C.43 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td135">
< p elementid="20043238w-p147">
< sentence type="heading" elementid="20043238w-s179">
91
< roundbracket elementid="20043238w-rb82">
(  gydag arbedion )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td136">
< p elementid="20043238w-p148">
< sentence type="heading" elementid="20043238w-s180">
30 Medi 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td137">
< p elementid="20043238w-p149">
< sentence type="heading" elementid="20043238w-s181">
2004/ 2557
< roundbracket elementid="20043238w-rb83">
(  Cy.228 )
< /roundbracket>
< roundbracket elementid="20043238w-rb84">
(  C.107 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td138">
< p elementid="20043238w-p150">
< sentence type="heading" elementid="20043238w-s182">
Atodlen 1
< roundbracket elementid="20043238w-rb85">
(  yn rhannol )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td139">
< p elementid="20043238w-p151">
< sentence type="heading" elementid="20043238w-s183">
30 Medi 2004
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td140">
< p elementid="20043238w-p152">
< sentence type="heading" elementid="20043238w-s184">
2004/ 2557
< roundbracket elementid="20043238w-rb86">
(  Cy.228 )
< /roundbracket>
< roundbracket elementid="20043238w-rb87">
(  C.107 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20043238w-p153">
< sentence type="heading" elementid="20043238w-s185">
< em elementid="20043238w-em12">
< english>
Notes
< /english>
:
< /em>
< /sentence>
< sentence type="normal" elementid="20043238w-s186">
< squarebracket elementid="20043238w-sb4">
[  1 ]
< /squarebracket>
2003 p.38 .
< /sentence>
< sentence type="heading" elementid="20043238w-s187">
< a type="href" elementid="20043238w-a7">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20043238w-p154">
< sentence type="normal" elementid="20043238w-s188">
< squarebracket elementid="20043238w-sb5">
[  2 ]
< /squarebracket>
< em elementid="20043238w-em13">
Gweler
< /em>
adran 66
< roundbracket elementid="20043238w-rb88">
(  6 )
< /roundbracket>
o 'r Ddeddf am ystyr
< doublequotation elementid="20043238w-dq6">
"
< english>
high hedge
< /english>
"
< /doublequotation> .
< /sentence>
< sentence type="heading" elementid="20043238w-s189">
< a type="href" elementid="20043238w-a8">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20043238w-p155">
< sentence type="normal" elementid="20043238w-s190">
< squarebracket elementid="20043238w-sb6">
[  3 ]
< /squarebracket>
1998 p.38 .
< /sentence>
< sentence type="heading" elementid="20043238w-s191">
< a type="href" elementid="20043238w-a9">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20043238w-p156">
< sentence type="heading" elementid="20043238w-s192">
< a type="href" elementid="20043238w-a10">
< english>
English version
< /english>
< /a>
< /sentence>
< sentence type="heading" elementid="20043238w-s193">
ISBN 0 11 091048 6
< /sentence>
< /p>
< table elementid="20043238w-tb4">
< td elementid="20043238w-td141">
< p elementid="20043238w-p157">
< sentence type="heading" elementid="20043238w-s194">
< a type="href" elementid="20043238w-a11">
< english>
Other UK SIs
< /english>
< /a>
< a type="href" elementid="20043238w-a12">
< english>
Home
< /english>
< /a>
< a type="href" elementid="20043238w-a13">
< english>
National Assembly for Wales Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20043238w-a14">
< english>
Scottish Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20043238w-a15">
< english>
Statutory Rules of Northern Ireland
< /english>
< /a>
< a type="href" elementid="20043238w-a16">
< english>
Her Majesty 's Stationery Office
< /english>
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td142">
< p elementid="20043238w-p158">
< sentence type="heading" elementid="20043238w-s195">
< strong elementid="20043238w-st10">
< english>
We welcome your 
< a type="href" elementid="20043238w-a17">
comments
< /a>
on this site
< /strong>
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td143">
< p elementid="20043238w-p159">
< sentence type="heading" elementid="20043238w-s196">
< english>
© Crown copyright 2004
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20043238w-td144">
< p elementid="20043238w-p160">
< sentence type="heading" elementid="20043238w-s197">
< english>
Prepared 15 December 2004
< /english>
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>1122< /wordcount>
< /file>
< file elementid="20043240w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Rheoliadau Gwrychoedd neu Berthi Uchel ( Apelau ) ( Cymru ) 2004< /pagetitle>
< doc_class>Statutory Instrument - Wales< /doc_class>
< year>2004< /year>
< number>3240< /number>
< legislation_code>30102< /legislation_code>
< legislation_type>Statutory Instrument - Wales< /legislation_type>
< subject>TIR, CYMRU^YMDDYGIAD GWRTHGYMDEITHASOL, CYMRU< /subject>
< laid_date>_EMPTY< /laid_date>
< made_date>20041208< /made_date>
< coming_into_force>20041231< /coming_into_force>
< isbn>0110910508< /isbn>
< title>Rheoliadau Gwrychoedd neu Berthi Uchel (Apelau) (Cymru) 2004< /title>
< signatory>D. Elis-Thomas< /signatory>
< signatory_title>Llywydd y Cynulliad Cenedlaethol< /signatory_title>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20043240w-p1">
< sentence type="heading" elementid="20043240w-s1">
Offerynnau Statudol 2004 RHIF 3240
< roundbracket elementid="20043240w-rb1">
(  CY.282 )
< /roundbracket>
< /sentence>
< /p>
< p elementid="20043240w-p2">
< sentence type="heading" elementid="20043240w-s2">
< strong elementid="20043240w-st1">
Rheoliadau Gwrychoedd neu Berthi Uchel
< roundbracket elementid="20043240w-rb2">
(  Apelau )
< /roundbracket>
< roundbracket elementid="20043240w-rb3">
(  Cymru )
< /roundbracket>
2004
< /strong>
< /sentence>
< /p>
< p elementid="20043240w-p3">
< sentence type="heading" elementid="20043240w-s3">
© Hawlfraint y Goron 2004
< /sentence>
< /p>
< p elementid="20043240w-p4">
< sentence type="normal" elementid="20043240w-s4">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20043240w-p5">
< sentence type="normal" elementid="20043240w-s5">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20043240w-s6">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20043240w-p6">
< sentence type="normal" elementid="20043240w-s7">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20043240w-s8">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20043240w-s9">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20043240w-p7">
< sentence type="normal" elementid="20043240w-s10">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20043240w-s11">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan
< english>
The Stationery Office Limited
< /english>
fel
< strong elementid="20043240w-st2">
Rheoliadau Gwrychoedd neu Berthi Uchel
< roundbracket elementid="20043240w-rb4">
(  Apelau )
< /roundbracket>
< roundbracket elementid="20043240w-rb5">
(  Cymru )
< /roundbracket>
2004
< /strong>
, ISBN 0110910508 .
< /sentence>
< sentence type="normal" elementid="20043240w-s12">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20043240w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20043240w-s13">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20043240w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20043240w-p8">
< sentence type="normal" elementid="20043240w-s14">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20043240w-a3">
< english>
Frequently Asked Questions
< /english>
< /a> .
< /sentence>
< /p>
< p elementid="20043240w-p9">
< sentence type="normal" elementid="20043240w-s15">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20043240w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20043240w-s16">
Pan welwch fotwm
< doublequotation elementid="20043240w-dq2">
"
< english>
continue
< /english>
"
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20043240w-p10">
< sentence type="heading" elementid="20043240w-s17">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="heading" elementid="20043240w-s18">
< strong elementid="20043240w-st3">
2004 RHIF 3240
< roundbracket elementid="20043240w-rb6">
(  CY.282 )
< /roundbracket>
< /strong>
< /sentence>
< sentence type="heading" elementid="20043240w-s19">
< strong elementid="20043240w-st4">
TIR , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20043240w-s20">
< strong elementid="20043240w-st5">
YMDDYGIAD GWRTHGYMDEITHASOL , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20043240w-s21">
Rheoliadau Gwrychoedd neu Berthi Uchel
< roundbracket elementid="20043240w-rb7">
(  Apelau )
< /roundbracket>
< roundbracket elementid="20043240w-rb8">
(  Cymru )
< /roundbracket>
2004
< /sentence>
< /p>
< table elementid="20043240w-tb1">
< td elementid="20043240w-td1">
< p elementid="20043240w-p11">
< sentence type="heading" elementid="20043240w-s22">
< em elementid="20043240w-em1">
Wedi 'u gwneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20043240w-td2">
< p elementid="20043240w-p12">
< sentence type="heading" elementid="20043240w-s23">
< em elementid="20043240w-em2">
8 Rhagfyr 2004
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20043240w-td3">
< p elementid="20043240w-p13">
< sentence type="heading" elementid="20043240w-s24">
< em elementid="20043240w-em3">
Yn dod i rym
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20043240w-td4">
< p elementid="20043240w-p14">
< sentence type="heading" elementid="20043240w-s25">
< em elementid="20043240w-em4">
31 Rhagfyr 2004
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20043240w-p15">
< sentence type="heading" elementid="20043240w-s26">
Mae Cynulliad Cenedlaethol Cymru
< roundbracket elementid="20043240w-rb9">
(
< doublequotation elementid="20043240w-dq3">
"  y Cynulliad Cenedlaethol "
< /doublequotation>
)
< /roundbracket>
, drwy arfer y pwerau a roddwyd iddo gan adran 72 o Ddeddf Ymddygiad Gwrthgymdeithasol 2003
< roundbracket elementid="20043240w-rb10">
(
< doublequotation elementid="20043240w-dq4">
"  y Ddeddf "
< /doublequotation>
)
< /roundbracket>
< squarebracket elementid="20043240w-sb1">
[ 
< a type="href" elementid="20043240w-a4">
1
< /a>
]
< /squarebracket>
, drwy hyn yn gwneud y Rheoliadau canlynol :
< /sentence>
< sentence type="heading" elementid="20043240w-s27">
< strong elementid="20043240w-st6">
Enwi , cychwyn a chymhwyso
< /strong>
< /sentence>
< sentence type="normal" elementid="20043240w-s28">
1 .
< /sentence>
< sentence type="normal" elementid="20043240w-s29">
 - 
< roundbracket elementid="20043240w-rb11">
(  1 )
< /roundbracket>
Enw 'r Rheoliadau hyn yw Rheoliadau Gwrychoedd neu Berthi Uchel
< roundbracket elementid="20043240w-rb12">
(  Apelau )
< /roundbracket>
< roundbracket elementid="20043240w-rb13">
(  Cymru )
< /roundbracket>
2004 a deuant i rym ar 31 Rhagfyr 2004 .
< /sentence>
< sentence type="normal" elementid="20043240w-s30">
< roundbracket elementid="20043240w-rb14">
(  2 )
< /roundbracket>
Mae 'r Rheoliadau hyn yn gymwys i apelau a wneir i 'r Cynulliad Cenedlaethol o dan adran 71 o 'r Ddeddf mewn perthynas â gwrychoedd neu berthi sydd wedi 'u lleoli yng Nghymru .
< /sentence>
< sentence type="heading" elementid="20043240w-s31">
< strong elementid="20043240w-st7">
Dehongli
< /strong>
< /sentence>
< sentence type="normal" elementid="20043240w-s32">
2 .
< /sentence>
< sentence type="heading" elementid="20043240w-s33">
Yn y Rheoliadau hyn - 
< /sentence>
< sentence type="heading" elementid="20043240w-s34">
ystyr
< doublequotation elementid="20043240w-dq5">
"  apêl "
< /doublequotation>
< roundbracket elementid="20043240w-rb15">
(
< doublequotation elementid="20043240w-dq6">
"
< em elementid="20043240w-em5">
< english>
appeal
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw apêl o dan adran 71 o 'r Ddeddf ; ystyr
< doublequotation elementid="20043240w-dq7">
"  dyddiad cychwyn "
< /doublequotation>
< roundbracket elementid="20043240w-rb16">
(
< doublequotation elementid="20043240w-dq8">
"
< em elementid="20043240w-em6">
< english>
starting date
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
mewn perthynas ag apêl , yw 'r dyddiad y rhoddir hysbysiad ohono gan y Cynulliad Cenedlaethol o dan reoliad 10 ; ystyr
< doublequotation elementid="20043240w-dq9">
"  ffurflen apêl "
< /doublequotation>
< roundbracket elementid="20043240w-rb17">
(
< doublequotation elementid="20043240w-dq10">
"
< em elementid="20043240w-em7">
< english>
appeal form
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw dogfen ar ffurf yr un a gyflenwir gan y Cynulliad Cenedlaethol at ddibenion rheithdrefn o dan y Rheoliadau hyn neu ddogfen sy 'n cynnwys yr holl wybodaeth y mae 'r ffurflen honno 'n ei gwneud yn ofynnol ; ystyr
< doublequotation elementid="20043240w-dq11">
"  holiadur "
< /doublequotation>
< roundbracket elementid="20043240w-rb18">
(
< doublequotation elementid="20043240w-dq12">
"
< english>
questionnaire
< /english>
"
< /doublequotation>
)
< /roundbracket>
yw dogfen ar ffurf yr un a gyflenwir gan y Cynulliad Cenedlaethol at ddibenion rheithdrefn o dan y Rheoliadau hyn neu ddogfen sy 'n cynnwys yr holl wybodaeth y mae 'r ffurflen honno 'n ei gwneud yn ofynnol ; ystyr
< doublequotation elementid="20043240w-dq13">
"  y partïon "
< /doublequotation>
< roundbracket elementid="20043240w-rb19">
(
< doublequotation elementid="20043240w-dq14">
"
< em elementid="20043240w-em8">
< english>
the parties
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw - 
< roundbracket elementid="20043240w-rb20">
(  a )
< /roundbracket>
yr apelydd ;
< /sentence>
< sentence type="heading" elementid="20043240w-s35">
< roundbracket elementid="20043240w-rb21">
(  b )
< /roundbracket>
yr awdurdod perthnasol ;
< /sentence>
< sentence type="heading" elementid="20043240w-s36">
< roundbracket elementid="20043240w-rb22">
(  c )
< /roundbracket>
pob person arall sy 'n achwynydd mewn perthynas â 'r gŵyn y rhoddwyd hysbysiad adfer mewn cysylltiad â hi ; ac
< /sentence>
< sentence type="heading" elementid="20043240w-s37">
< roundbracket elementid="20043240w-rb23">
(  ch )
< /roundbracket>
pob perchennog neu feddiannydd y tir y mae 'r gwrych neu 'r berth uchel wedi 'u lleoli arno ;
< /sentence>
< sentence type="normal" elementid="20043240w-s38">
ystyr
< doublequotation elementid="20043240w-dq15">
"  person penodedig "
< /doublequotation>
< roundbracket elementid="20043240w-rb24">
(
< doublequotation elementid="20043240w-dq16">
"
< em elementid="20043240w-em9">
< english>
appointed person
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw person a benodwyd o dan adran 72
< roundbracket elementid="20043240w-rb25">
(  3 )
< /roundbracket>
o 'r Ddeddf ; ac ystyr
< doublequotation elementid="20043240w-dq17">
"  terfynau amser perthnasol "
< /doublequotation>
< roundbracket elementid="20043240w-rb26">
(
< doublequotation elementid="20043240w-dq18">
"
< em elementid="20043240w-em10">
< english>
relevant time limits
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw 'r terfynau amser a ragnodir yn y Rheoliadau hyn neu , os bydd y Cynulliad Cenedlaethol wedi arfer ei bŵer o dan reoliad 22 , unrhyw derfyn amser diweddarach sydd wedi 'i benderfynu o dan y rheoliad hwnnw .
< /sentence>
< /p>
< p elementid="20043240w-p16">
< sentence type="heading" elementid="20043240w-s39">
< strong elementid="20043240w-st8">
Seiliau dros apelio yn erbyn dyroddi hysbysiadau adfer
< /strong>
< /sentence>
< sentence type="normal" elementid="20043240w-s40">
3 .
< /sentence>
< sentence type="heading" elementid="20043240w-s41">
 - 
< roundbracket elementid="20043240w-rb27">
(  1 )
< /roundbracket>
Gellir gwneud apêl o dan adran 71
< roundbracket elementid="20043240w-rb28">
(  1 )
< /roundbracket>
< roundbracket elementid="20043240w-rb29">
(  a )
< /roundbracket>
o 'r Ddeddf yn erbyn dyroddi hysbysiad adfer gan yr awdurdod perthnasol ar y sail - 
< /sentence>
< /p>
< p elementid="20043240w-p17">
< sentence type="heading" elementid="20043240w-s42">
< roundbracket elementid="20043240w-rb30">
(  a )
< /roundbracket>
nad yw uchder y gwrych neu 'r berth uchel a bennir yn yr hysbysiad adfer yn effeithio 'n andwyol ar y mwynhad rhesymol a gaiff yr achwynydd o 'r eiddo domestig a bennir felly ;
< /sentence>
< sentence type="heading" elementid="20043240w-s43">
< roundbracket elementid="20043240w-rb31">
(  b )
< /roundbracket>
na ddylid cymryd unrhyw gamau mewn perthynas â 'r gwrych neu 'r berth uchel a bennir yn yr hysbysiad adfer i wneud iawn am yr effaith andwyol ar y mwynhad rhesymol a gaiff yr achwynydd o 'r eiddo domestig a bennir felly neu i atal ailadrodd yr effaith andwyol honno ;
< /sentence>
< sentence type="heading" elementid="20043240w-s44">
< roundbracket elementid="20043240w-rb32">
(  c )
< /roundbracket>
bod y camau a bennir yn yr hysbysiad adfer yn fwy na 'r hyn sydd ei angen i wneud iawn am effaith andwyol y gwrych neu 'r berth uchel neu i atal ailadrodd yr effaith andwyol honno ; neu
< /sentence>
< sentence type="normal" elementid="20043240w-s45">
< roundbracket elementid="20043240w-rb33">
(  ch )
< /roundbracket>
bod y cyfnod a bennir yn yr hysbysiad adfer ar gyfer cymryd y camau a bennir felly yn llai na 'r cyfnod y dylid yn rhesymol ei ganiatáu .
< /sentence>
< /p>
< p elementid="20043240w-p18">
< sentence type="normal" elementid="20043240w-s46">
< roundbracket elementid="20043240w-rb34">
(  2 )
< /roundbracket>
Gellir gwneud apêl o dan adran 71
< roundbracket elementid="20043240w-rb35">
(  1 )
< /roundbracket>
< roundbracket elementid="20043240w-rb36">
(  a )
< /roundbracket>
o 'r Ddeddf gan achwynydd mewn perthynas â hysbysiad adfer a ddyroddwyd gan yr awdurdod perthnasol ar y sail bod y camau a bennir yn yr hysbysiad adfer yn annigonol i wneud iawn am effaith andwyol y gwrych neu 'r berth uchel ar y mwynhad rhesymol a gaiff yr achwynydd o 'r eiddo domestig a bennir felly neu i atal ailadrodd yr effaith andwyol .
< /sentence>
< sentence type="heading" elementid="20043240w-s47">
< strong elementid="20043240w-st9">
Seiliau dros apelio yn erbyn tynnu 'n ôl , hepgor neu lacio hysbysiad adfer
< /strong>
< /sentence>
< sentence type="normal" elementid="20043240w-s48">
4 .
< /sentence>
< sentence type="normal" elementid="20043240w-s49">
 - 
< roundbracket elementid="20043240w-rb37">
(  1 )
< /roundbracket>
Cyhyd ag y bodlonir yr amodau a bennir ym mharagraff
< roundbracket elementid="20043240w-rb38">
(  2 )
< /roundbracket>
, gellir gwneud apêl o dan adran 71
< roundbracket elementid="20043240w-rb39">
(  1 )
< /roundbracket>
< roundbracket elementid="20043240w-rb40">
(  b )
< /roundbracket>
neu
< roundbracket elementid="20043240w-rb41">
(  c )
< /roundbracket>
o 'r Ddeddf yn erbyn tynnu 'n ôl hysbysiad adfer , neu hepgor neu lacio ei ofynion , a hynny ar unrhyw un o 'r seiliau a bennir ym mharagraff
< roundbracket elementid="20043240w-rb42">
(  3 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20043240w-s50">
< roundbracket elementid="20043240w-rb43">
(  2 )
< /roundbracket>
Dyma 'r amodau y cyfeirir atynt ym mharagraff
< roundbracket elementid="20043240w-rb44">
(  1 )
< /roundbracket>
 - 
< /sentence>
< /p>
< p elementid="20043240w-p19">
< sentence type="heading" elementid="20043240w-s51">
< roundbracket elementid="20043240w-rb45">
(  a )
< /roundbracket>
nad yw 'r awdurdod perthnasol wedi dyroddi hysbysiad adfer arall mewn perthynas â 'r un gwrych neu berth uchel ; a
< /sentence>
< sentence type="normal" elementid="20043240w-s52">
< roundbracket elementid="20043240w-rb46">
(  b )
< /roundbracket>
na chydsyniodd y person sy 'n dymuno apelio â thynnu 'n ôl yr hysbysiad adfer neu
< roundbracket elementid="20043240w-rb47">
(  yn ôl y digwydd )
< /roundbracket>
â hepgor neu lacio gofynion yr hysbysiad hwnnw .
< /sentence>
< /p>
< p elementid="20043240w-p20">
< sentence type="heading" elementid="20043240w-s53">
< roundbracket elementid="20043240w-rb48">
(  3 )
< /roundbracket>
Dyma 'r seiliau y cyfeirir atynt ym mharagraff
< roundbracket elementid="20043240w-rb49">
(  1 )
< /roundbracket>
 - 
< /sentence>
< /p>
< p elementid="20043240w-p21">
< sentence type="heading" elementid="20043240w-s54">
< roundbracket elementid="20043240w-rb50">
(  a )
< /roundbracket>
na fu unrhyw newid sylweddol mewn amgylchiadau ers gwneud y gŵyn , y dyroddwyd yr hysbysiad adfer mewn cysylltiad â hi , sef newid a fyddai 'n cyfiawnhau tynnu 'n ôl yr hysbysiad adfer neu
< roundbracket elementid="20043240w-rb51">
(  yn ôl y digwydd )
< /roundbracket>
hepgor neu lacio ei ofynion ;
< /sentence>
< sentence type="heading" elementid="20043240w-s55">
< roundbracket elementid="20043240w-rb52">
(  b )
< /roundbracket>
bod gofynion yr hysbysiad adfer , fel y maent wedi 'u hepgor neu wedi 'u llacio , yn annigonol i wneud iawn am effaith andwyol y gwrych neu 'r berth uchel ar y mwynhad rhesymol a gaiff yr achwynydd o 'r eiddo domestig a bennir yn yr hysbysiad adfer neu i atal ailadrodd yr effaith andwyol honno ; neu
< /sentence>
< sentence type="normal" elementid="20043240w-s56">
< roundbracket elementid="20043240w-rb53">
(  c )
< /roundbracket>
yn achos apêl gan berchennog neu feddiannydd y tir cyfagos , bod gofynion yr hysbysiad adfer , fel y maent wedi 'u hepgor neu wedi 'u llacio , yn fwy na 'r hyn sy 'n angenrheidiol i wneud iawn am effaith andwyol y gwrych neu 'r berth uchel ar y mwynhad rhesymol a gaiff yr achwynydd o 'r eiddo domestig a bennir yn yr hysbysiad neu i atal ailadrodd yr effaith andwyol honno .
< /sentence>
< /p>
< p elementid="20043240w-p22">
< sentence type="heading" elementid="20043240w-s57">
< strong elementid="20043240w-st10">
Seiliau dros apelio pan fo 'r awdurdod perthnasol yn penderfynu na ddylai cwyn fynd yn ei blaen
< /strong>
< /sentence>
< sentence type="normal" elementid="20043240w-s58">
5 .
< /sentence>
< sentence type="normal" elementid="20043240w-s59">
Gall apêl gan achwynydd o dan adran 71
< roundbracket elementid="20043240w-rb54">
(  3 )
< /roundbracket>
o 'r Ddeddf
< roundbracket elementid="20043240w-rb55">
(  pan na fydd yr awdurdod perthnasol yn penderfynu 'r naill neu 'r llall o 'r materion a bennir yn adran 68
< roundbracket elementid="20043240w-rb56">
(  3 )
< /roundbracket>
o 'r Ddeddf , neu 'r ddau ohonynt , o blaid yr achwynydd )
< /roundbracket>
gael ei gwneud ar y sail bod uchder y gwrych neu 'r berth uchel a bennir yn y gŵyn yn effeithio 'n andwyol ar y mwynhad rhesymol a gaiff yr achwynydd o 'r eiddo domestig a bennir felly ac y dylid cymryd camau gyda 'r bwriad o wneud iawn am effaith andwyol y gwrych neu 'r berth uchel a bennir yn y gwyn ar y mwynhad rhesymol a gaiff yr achwynydd o 'r eiddo domestig a bennir felly neu o atal ailadrodd yr effaith honno .
< /sentence>
< sentence type="heading" elementid="20043240w-s60">
< strong elementid="20043240w-st11">
Penderfynu ar apelau gan y Cynulliad Cenedlaethol
< /strong>
< /sentence>
< sentence type="normal" elementid="20043240w-s61">
6 .
< /sentence>
< sentence type="normal" elementid="20043240w-s62">
 - 
< roundbracket elementid="20043240w-rb57">
(  1 )
< /roundbracket>
Wrth ystyried apêl , mae gan y Cynulliad Cenedlaethol , neu unrhyw berson a benodir ganddo o dan adran 72
< roundbracket elementid="20043240w-rb58">
(  3 )
< /roundbracket>
o 'r Ddeddf i wrando 'r apêl a 'i phenderfynu ar ei ran , hawl i ystyried unrhyw dystiolaeth neu unrhyw fater arall ni waeth a oedd y dystiolaeth honno neu 'r mater arall hwnnw heb eu hystyried gan yr awdurdod perthnasol neu nad oedd modd iddo eu hystyried pan ddaeth i 'r penderfyniad y mae 'r apêl yn berthnasol iddo .
< /sentence>
< sentence type="normal" elementid="20043240w-s63">
< roundbracket elementid="20043240w-rb59">
(  2 )
< /roundbracket>
Rhaid i 'r person sy 'n gwneud yr apêl gadarnhau sail neu seiliau 'r apêl ac , os yw 'r person hwnnw yn methu gwneud hynny , rhaid i 'r apêl gael ei gwrthod .
< /sentence>
< sentence type="heading" elementid="20043240w-s64">
< strong elementid="20043240w-st12">
Penodi person penodedig a dirymu ei benodiad
< /strong>
< /sentence>
< sentence type="normal" elementid="20043240w-s65">
7 .
< /sentence>
< sentence type="heading" elementid="20043240w-s66">
 - 
< roundbracket elementid="20043240w-rb60">
(  1 )
< /roundbracket>
Rhaid i benodiad o dan adran 72
< roundbracket elementid="20043240w-rb61">
(  3 )
< /roundbracket>
o 'r Ddeddf fod yn ysgrifenedig a chaniateir iddo - 
< /sentence>
< /p>
< p elementid="20043240w-p23">
< sentence type="heading" elementid="20043240w-s67">
< roundbracket elementid="20043240w-rb62">
(  a )
< /roundbracket>
ymwneud ag unrhyw apêl neu fater penodol a bennir yn y penodiad neu ag apelau neu faterion o ddisgrifiad a bennir felly ;
< /sentence>
< sentence type="heading" elementid="20043240w-s68">
< roundbracket elementid="20043240w-rb63">
(  b )
< /roundbracket>
darparu bod unrhyw swyddogaeth y mae 'n ymwneud â hi yn arferadwy gan y person penodedig naill ai 'n ddiamod neu 'n ddarostyngedig i gyflawni unrhyw amodau a bennir yn y penodiad ; ac
< /sentence>
< sentence type="normal" elementid="20043240w-s69">
< roundbracket elementid="20043240w-rb64">
(  c )
< /roundbracket>
cael ei ddirymu ar unrhyw adeg gan yr awdurdod apelau , drwy roi hysbysiad ysgrifenedig i 'r person penodedig , mewn perthynas ag unrhyw apêl neu fater nad ydynt wedi 'u penderfynu gan y person penodedig cyn yr amser hwnnw .
< /sentence>
< /p>
< p elementid="20043240w-p24">
< sentence type="heading" elementid="20043240w-s70">
< roundbracket elementid="20043240w-rb65">
(  2 )
< /roundbracket>
Mae gan berson penodedig , mewn perthynas ag unrhyw apêl neu fater y mae 'r penodiad yn ymwneud â hwy , yr un pwerau a dyletswyddau â 'r awdurdod apelau , ac eithrio unrhyw swyddogaeth o ran - 
< /sentence>
< /p>
< p elementid="20043240w-p25">
< sentence type="heading" elementid="20043240w-s71">
< roundbracket elementid="20043240w-rb66">
(  a )
< /roundbracket>
gwneud rheoliadau ;
< /sentence>
< sentence type="heading" elementid="20043240w-s72">
< roundbracket elementid="20043240w-rb67">
(  b )
< /roundbracket>
cynnal gwrandawiad ; neu
< /sentence>
< sentence type="heading" elementid="20043240w-s73">
< roundbracket elementid="20043240w-rb68">
(  c )
< /roundbracket>
penodi person er mwyn - 
< /sentence>
< sentence type="heading" elementid="20043240w-s74">
< roundbracket elementid="20043240w-rb69">
(  i )
< /roundbracket>
galluogi personau i fod yn bresennol mewn gwrandawiad a gynhelir gan y person a benodir felly a chymryd rhan ynddo , neu
< /sentence>
< sentence type="normal" elementid="20043240w-s75">
< roundbracket elementid="20043240w-rb70">
(  ii )
< /roundbracket>
cyfeirio unrhyw gwestiwn neu fater at y person hwnnw .
< /sentence>
< /p>
< p elementid="20043240w-p26">
< sentence type="normal" elementid="20043240w-s76">
< roundbracket elementid="20043240w-rb71">
(  3 )
< /roundbracket>
Pan fo 'r awdurdod apelau , o dan baragraff
< roundbracket elementid="20043240w-rb72">
(  1 )
< /roundbracket>
< roundbracket elementid="20043240w-rb73">
(  c )
< /roundbracket>
, yn dirymu penodiad mewn perthynas ag unrhyw apêl neu fater , rhaid i 'r awdurdod apelau , onid yw 'n bwriadu penderfynu 'r apêl neu 'r mater ei hun , benodi person arall i benderfynu 'r apêl neu 'r mater yn ei le .
< /sentence>
< sentence type="normal" elementid="20043240w-s77">
< roundbracket elementid="20043240w-rb74">
(  4 )
< /roundbracket>
Pan gaiff penodiad newydd ei wneud , rhaid i 'r broses o ystyried yr apêl neu 'r mater , neu unrhyw wrandawiad mewn cysylltiad â 'r penodiad , gael ei dechrau o 'r newydd ; ond , os dyna 'r sefyllfa , nid oes hawl gan neb i wneud sylwadau newydd neu i addasu neu dynnu 'n ôl unrhyw sylwadau a wnaed eisoes .
< /sentence>
< sentence type="heading" elementid="20043240w-s78">
< roundbracket elementid="20043240w-rb75">
(  5 )
< /roundbracket>
Rhaid i unrhyw beth sydd wedi 'i wneud neu sydd heb ei wneud gan berson penodedig wrth iddo arfer , neu honni arfer , unrhyw swyddogaeth y mae 'r penodiad yn ymwneud â hi neu mewn cysylltiad ag arfer neu honni arfer y swyddogaeth honno , gael ei drin i bob pwrpas fel rhywbeth sydd wedi 'i wneud neu heb ei wneud gan yr awdurdod apelau , ac eithrio i 'r graddau y mae 'r weithred honno neu 'r anwaith hwnnw yn ymwneud - 
< /sentence>
< /p>
< p elementid="20043240w-p27">
< sentence type="heading" elementid="20043240w-s79">
< roundbracket elementid="20043240w-rb76">
(  a )
< /roundbracket>
â chymaint o unrhyw gontract a wnaed rhwng yr awdurdod apelau a 'r person penodedig ag sy 'n ymwneud ag arfer y swyddogaeth ; neu
< /sentence>
< sentence type="normal" elementid="20043240w-s80">
< roundbracket elementid="20043240w-rb77">
(  b )
< /roundbracket>
ag unrhyw achos troseddol sydd wedi 'i ddwyn mewn perthynas ag unrhyw beth a gafodd ei wneud neu na chafodd ei wneud fel a grybwyllir yn y paragraff hwn .
< /sentence>
< /p>
< p elementid="20043240w-p28">
< sentence type="heading" elementid="20043240w-s81">
< strong elementid="20043240w-st13">
Hysbysiad o Apêl
< /strong>
< /sentence>
< sentence type="normal" elementid="20043240w-s82">
8 .
< /sentence>
< sentence type="normal" elementid="20043240w-s83">
 - 
< roundbracket elementid="20043240w-rb78">
(  1 )
< /roundbracket>
Rhaid i berson sy 'n dymuno apelio roi hysbysiad o 'r apêl honno i 'r Cynulliad Cenedlaethol drwy anfon i 'r Cynulliad Cenedlaethol , fel y daw i law o fewn y cyfnod a bennir yn adran 71
< roundbracket elementid="20043240w-rb79">
(  4 )
< /roundbracket>
o 'r Ddeddf , y ffurflen apêl ynghyd â chopi o unrhyw ddogfennau ategol .
< /sentence>
< sentence type="normal" elementid="20043240w-s84">
< roundbracket elementid="20043240w-rb80">
(  2 )
< /roundbracket>
Rhaid i 'r apelydd , cyn gynted ag y bo 'n rhesymol ymarferol ar ôl hynny , anfon at yr awdurdod perthnasol gopi o 'r ffurflen apêl ac unrhyw ddogfennau eraill y cyfeirir atynt ym mharagraff
< roundbracket elementid="20043240w-rb81">
(  1 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20043240w-s85">
< strong elementid="20043240w-st14">
Gwybodaeth Gychwynnol
< /strong>
< /sentence>
< sentence type="normal" elementid="20043240w-s86">
9 .
< /sentence>
< sentence type="heading" elementid="20043240w-s87">
Rhaid i 'r awdurdod perthnasol , pan gaiff hysbysiad o apêl o dan reoliad 8 , roi gwybod ar unwaith i 'r Cynulliad Cenedlaethol a 'r apelydd - 
< /sentence>
< /p>
< p elementid="20043240w-p29">
< sentence type="heading" elementid="20043240w-s88">
< roundbracket elementid="20043240w-rb82">
(  a )
< /roundbracket>
beth yw enw a chyfeiriad pob person , ar wahân i 'r apelydd , sy 'n bartïon i 'r apêl ; a
< /sentence>
< sentence type="normal" elementid="20043240w-s89">
< roundbracket elementid="20043240w-rb83">
(  b )
< /roundbracket>
a yw 'n dymuno bod yr apêl yn cael ei phenderfynu drwy gyfnewid sylwadau ysgrifenedig neu ar ôl cynnal gwrandawiad .
< /sentence>
< /p>
< p elementid="20043240w-p30">
< sentence type="heading" elementid="20043240w-s90">
< strong elementid="20043240w-st15">
Dyddiad Cychwyn
< /strong>
< /sentence>
< sentence type="normal" elementid="20043240w-s91">
10 .
< /sentence>
< sentence type="heading" elementid="20043240w-s92">
Rhaid i 'r Cynulliad Cenedlaethol , cyn gynted ag y bo 'n ymarferol  - 
< /sentence>
< /p>
< p elementid="20043240w-p31">
< sentence type="heading" elementid="20043240w-s93">
< roundbracket elementid="20043240w-rb84">
(  a )
< /roundbracket>
ar ôl iddo gael yr hysbysiad o apêl a roddwyd yn unol â rheoliad 8 a 'r wybodaeth gychwynnol o dan reoliad 9 , hysbysu 'r partïon - 
< /sentence>
< sentence type="heading" elementid="20043240w-s94">
< roundbracket elementid="20043240w-rb85">
(  i )
< /roundbracket>
o 'r Rhif cyfeirnod a ddyrannwyd i 'r apêl,
< /sentence>
< sentence type="heading" elementid="20043240w-s95">
< roundbracket elementid="20043240w-rb86">
(  ii )
< /roundbracket>
a yw 'r apêl i gael ei phenderfynu drwy gyfnewid sylwadau ysgrifenedig neu , pan fo naill ai 'r apelydd neu 'r awdurdod perthnasol
< roundbracket elementid="20043240w-rb87">
(  neu 'r ddau )
< /roundbracket>
wedi gofyn am hynny , ar ôl cynnal gwrandawiad , a
< /sentence>
< sentence type="heading" elementid="20043240w-s96">
< roundbracket elementid="20043240w-rb88">
(  iii )
< /roundbracket>
o 'r cyfeiriad y mae gohebiaeth ysgrifenedig i 'r Cynulliad Cenedlaethol ynghylch yr apêl i 'w hanfon iddo ; a
< /sentence>
< /p>
< p elementid="20043240w-p32">
< sentence type="normal" elementid="20043240w-s97">
< roundbracket elementid="20043240w-rb89">
(  b )
< /roundbracket>
ar ôl iddo gael digon o wybodaeth i 'w alluogi i ystyried yr apêl , hysbysu 'r partïon o 'r dyddiad cychwyn .
< /sentence>
< /p>
< p elementid="20043240w-p33">
< sentence type="heading" elementid="20043240w-s98">
< strong elementid="20043240w-st16">
Hysbysiad i bersonau â buddiant
< /strong>
< /sentence>
< sentence type="normal" elementid="20043240w-s99">
11 .
< /sentence>
< sentence type="normal" elementid="20043240w-s100">
 - 
< roundbracket elementid="20043240w-rb90">
(  1 )
< /roundbracket>
Rhaid i 'r awdurdod perthnasol roi hysbysiad o 'r apêl o fewn 2 wythnos i 'r dyddiad cychwyn i unrhyw bersonau , ac eithrio 'r partïon , sydd wedi gwneud sylwadau i 'r awdurdod perthnasol ynghylch y penderfyniad y mae 'r apêl yn ymwneud ag ef .
< /sentence>
< sentence type="heading" elementid="20043240w-s101">
< roundbracket elementid="20043240w-rb91">
(  2 )
< /roundbracket>
Rhaid i hysbysiad o dan baragraff
< roundbracket elementid="20043240w-rb92">
(  1 )
< /roundbracket>
 - 
< /sentence>
< /p>
< p elementid="20043240w-p34">
< sentence type="heading" elementid="20043240w-s102">
< roundbracket elementid="20043240w-rb93">
(  a )
< /roundbracket>
nodi enw 'r apelydd a chyfeiriad y tir lle y mae 'r gwrych neu 'r berth uchel y mae 'r apêl yn ymwneud â hwy wedi 'u lleoli ;
< /sentence>
< sentence type="heading" elementid="20043240w-s103">
< roundbracket elementid="20043240w-rb94">
(  b )
< /roundbracket>
nodi 'r materion yr hysbyswyd y partïon ohonynt gan y Cynulliad Cenedlaethol o dan reoliad 10 ;
< /sentence>
< sentence type="heading" elementid="20043240w-s104">
< roundbracket elementid="20043240w-rb95">
(  c )
< /roundbracket>
disgrifio natur yr apêl a 'r seiliau dros wneud yr apêl ;
< /sentence>
< sentence type="heading" elementid="20043240w-s105">
< roundbracket elementid="20043240w-rb96">
(  ch )
< /roundbracket>
datgan y bydd copïau o unrhyw sylwadau sydd wedi 'u gwneud gan unrhyw berson y crybwyllir ei enw ym mharagraff
< roundbracket elementid="20043240w-rb97">
(  1 )
< /roundbracket>
o 'r rheoliad hwn yn cael eu hanfon i 'r Cynulliad Cenedlaethol ac at bob un o 'r partïon ;
< /sentence>
< sentence type="heading" elementid="20043240w-s106">
< roundbracket elementid="20043240w-rb98">
(  d )
< /roundbracket>
datgan y caniateir anfon sylwadau ysgrifenedig ychwanegol i 'r Cynulliad Cenedlaethol fel y byddant yn dod i law o fewn 6 wythnos i 'r dyddiad cychwyn , a phan fo sylwadau o 'r fath yn cael eu hanfon , ei bod yn ofynnol anfon tri chopi ohonynt ;
< /sentence>
< sentence type="heading" elementid="20043240w-s107">
< roundbracket elementid="20043240w-rb99">
(  dd )
< /roundbracket>
datgan y bydd unrhyw sylwadau o 'r fath yn cael eu hystyried gan y Cynulliad Cenedlaethol pan fydd yn penderfynu ar yr apêl oni fydd unrhyw berson yn eu tynnu 'n ôl o fewn 6 wythnos i 'r dyddiad cychwyn ; ac
< /sentence>
< sentence type="normal" elementid="20043240w-s108">
< roundbracket elementid="20043240w-rb100">
(  e )
< /roundbracket>
datgan y weithdrefn sydd i 'w defnyddio i benderfynu ar yr apêl .
< /sentence>
< /p>
< p elementid="20043240w-p35">
< sentence type="normal" elementid="20043240w-s109">
< roundbracket elementid="20043240w-rb101">
(  3 )
< /roundbracket>
Rhaid i 'r Cynulliad Cenedlaethol , cyn gynted ag y bo 'n ymarferol ar ôl iddo eu cael , anfon copi o unrhyw sylwadau ychwanegol a gyflwynwyd o dan baragraff
< roundbracket elementid="20043240w-rb102">
(  2 )
< /roundbracket>
< roundbracket elementid="20043240w-rb103">
(  d )
< /roundbracket>
at bob un o 'r partïon .
< /sentence>
< sentence type="heading" elementid="20043240w-s110">
< strong elementid="20043240w-st17">
Holiadur
< /strong>
< /sentence>
< sentence type="normal" elementid="20043240w-s111">
12 .
< /sentence>
< sentence type="heading" elementid="20043240w-s112">
 - 
< roundbracket elementid="20043240w-rb104">
(  1 )
< /roundbracket>
Rhaid i 'r awdurdod perthnasol anfon y canlynol i 'r Cynulliad Cenedlaethol , ac anfon copi ohonynt at bob un o 'r partïon eraill , fel y byddant yn dod i law yn y naill achos a 'r llall o fewn 2 wythnos i 'r dyddiad cychwyn  - 
< /sentence>
< /p>
< p elementid="20043240w-p36">
< sentence type="heading" elementid="20043240w-s113">
< roundbracket elementid="20043240w-rb105">
(  a )
< /roundbracket>
holiadur wedi 'i gwblhau ; a
< /sentence>
< sentence type="normal" elementid="20043240w-s114">
< roundbracket elementid="20043240w-rb106">
(  b )
< /roundbracket>
copi o bob un o 'r dogfennau y cyfeirir atynt ynddo .
< /sentence>
< /p>
< p elementid="20043240w-p37">
< sentence type="normal" elementid="20043240w-s115">
< roundbracket elementid="20043240w-rb107">
(  2 )
< /roundbracket>
Rhaid i 'r holiadur ddatgan y dyddiad y cafodd ei anfon i 'r Cynulliad Cenedlaethol .
< /sentence>
< sentence type="heading" elementid="20043240w-s116">
< strong elementid="20043240w-st18">
Cyfnewid tystiolaeth
< /strong>
< /sentence>
< sentence type="normal" elementid="20043240w-s117">
13 .
< /sentence>
< sentence type="heading" elementid="20043240w-s118">
 - 
< roundbracket elementid="20043240w-rb108">
(  1 )
< /roundbracket>
Mae 'r rheoliad hwn yn gymwys pan fydd apêl i fod i gael ei phenderfynu drwy sylwadau ysgrifenedig neu ar ôl cynnal gwrandawiad ac , yn ychwanegol , pan fydd apêl i fod i gael ei phenderfynu - 
< /sentence>
< /p>
< p elementid="20043240w-p38">
< sentence type="heading" elementid="20043240w-s119">
< roundbracket elementid="20043240w-rb109">
(  a )
< /roundbracket>
drwy sylwadau ysgrifenedig , mae rheoliadau 14 a 19 i 22 yn gymwys ;
< /sentence>
< sentence type="normal" elementid="20043240w-s120">
< roundbracket elementid="20043240w-rb110">
(  b )
< /roundbracket>
ar ôl cynnal gwrandawiad , mae rheoliadau 15 i 22 yn gymwys .
< /sentence>
< /p>
< p elementid="20043240w-p39">
< sentence type="heading" elementid="20043240w-s121">
< roundbracket elementid="20043240w-rb111">
(  2 )
< /roundbracket>
Os bydd unrhyw un o 'r partïon yn dymuno cyflwyno unrhyw sylwadau yn ychwanegol at - 
< /sentence>
< /p>
< p elementid="20043240w-p40">
< sentence type="heading" elementid="20043240w-s122">
< roundbracket elementid="20043240w-rb112">
(  a )
< /roundbracket>
yr hysbysiad o apêl ac unrhyw ddogfennau sy 'n ategol iddo ; neu
< /sentence>
< sentence type="heading" elementid="20043240w-s123">
< roundbracket elementid="20043240w-rb113">
(  b )
< /roundbracket>
yr holiadur ac unrhyw ddogfennau a gyflwynir gydag ef,
< /sentence>
< /p>
< p elementid="20043240w-p41">
< sentence type="normal" elementid="20043240w-s124">
rhaid i 'r parti hwnnw anfon tri chopi o 'r sylwadau ychwanegol hynny i 'r Cynulliad Cenedlaethol fel y byddant yn dod i law o fewn 6 wythnos i 'r dyddiad cychwyn a rhaid i 'r Cynulliad Cenedlaethol , cyn gynted ag y bo 'n ymarferol ar ôl iddynt ddod i law , anfon copi o unrhyw sylwadau ychwanegol at bob un o 'r partïon eraill .
< /sentence>
< sentence type="normal" elementid="20043240w-s125">
< roundbracket elementid="20043240w-rb114">
(  3 )
< /roundbracket>
Os bydd unrhyw barti yn dymuno gwneud sylwadau ar unrhyw sylwadau gan unrhyw barti arall , neu ar unrhyw sylwadau a wnaed yn unol â rheoliad 11
< roundbracket elementid="20043240w-rb115">
(  2 )
< /roundbracket>
< roundbracket elementid="20043240w-rb116">
(  d )
< /roundbracket>
, rhaid i 'r parti hwnnw anfon tri chopi o 'r rhain i 'r Cynulliad Cenedlaethol , fel y byddant yn dod i law o fewn 9 wythnos i 'r dyddiad cychwyn .
< /sentence>
< sentence type="normal" elementid="20043240w-s126">
< roundbracket elementid="20043240w-rb117">
(  4 )
< /roundbracket>
Caiff y Cynulliad Cenedlaethol anwybyddu unrhyw wybodaeth ychwanegol gan unrhyw un o 'r partïon nad yw wedi 'i chael o fewn 9 wythnos i 'r dyddiad cychwyn oni wnaeth y Cynulliad Cenedlaethol ofyn am yr wybodaeth ychwanegol honno a bod y Cynulliad Cenedlaethol yn cael yr wybodaeth honno y gofynnodd amdani o fewn y cyfnod a bennodd yn ysgrifenedig pan wnaeth y cais .
< /sentence>
< sentence type="heading" elementid="20043240w-s127">
< strong elementid="20043240w-st19">
Apelau a benderfynir drwy gyfnewid sylwadau ysgrifenedig
< /strong>
< /sentence>
< sentence type="normal" elementid="20043240w-s128">
14 .
< /sentence>
< sentence type="normal" elementid="20043240w-s129">
 - 
< roundbracket elementid="20043240w-rb118">
(  1 )
< /roundbracket>
Caiff y Cynulliad Cenedlaethol , ar ôl diwedd unrhyw derfynau amser erbyn pryd y mae 'n ofynnol neu y caniateir i unrhyw gam gael ei gymryd yn unol â 'r Rheoliadau hyn , ac ar ôl rhoi i 'r apelydd a 'r awdurdod perthnasol hysbysiad ysgrifenedig o 'i fwriad i wneud hynny , fynd rhagddo i benderfynu ar yr apêl drwy gymryd i ystyriaeth ddim ond y sylwadau a 'r dogfennau eraill hynny a gyflwynwyd i 'r Cynulliad Cenedlaethol o fewn y terfynau amser perthnasol .
< /sentence>
< sentence type="normal" elementid="20043240w-s130">
< roundbracket elementid="20043240w-rb119">
(  2 )
< /roundbracket>
Caiff y Cynulliad Cenedlaethol fynd rhagddo i benderfynu ar apêl , drwy gymryd i ystyriaeth ddim ond y sylwadau ysgrifenedig hynny a gafwyd o fewn y terfynau amser perthnasol .
< /sentence>
< sentence type="normal" elementid="20043240w-s131">
< roundbracket elementid="20043240w-rb120">
(  3 )
< /roundbracket>
Caiff y Cynulliad Cenedlaethol , ar ôl hysbysu 'r partïon o 'i fwriad i wneud hynny , fynd rhagddo i benderfynu ar apêl , er nad oes unrhyw sylwadau ysgrifenedig wedi 'u gwneud o fewn y terfynau amser perthnasol , os ymddengys i 'r Cynulliad Cenedlaethol fod ganddo ddigon o ddeunydd ger ei fron i 'w alluogi i ddod i benderfyniad ar rinweddau 'r achos .
< /sentence>
< sentence type="heading" elementid="20043240w-s132">
< strong elementid="20043240w-st20">
Apelau a benderfynir ar ôl cynnal gwrandawiad
< /strong>
< /sentence>
< sentence type="normal" elementid="20043240w-s133">
15 .
< /sentence>
< sentence type="heading" elementid="20043240w-s134">
 - 
< roundbracket elementid="20043240w-rb121">
(  1 )
< /roundbracket>
Rhaid i 'r Cynulliad Cenedlaethol wneud y canlynol - 
< /sentence>
< /p>
< p elementid="20043240w-p42">
< sentence type="heading" elementid="20043240w-s135">
< roundbracket elementid="20043240w-rb122">
(  a )
< /roundbracket>
cyn gynted ag y bo 'n ymarferol , hysbysu 'r awdurdod perthnasol yn ysgrifenedig fod rhaid i 'r awdurdod perthnasol hysbysu 'r partïon , ac unrhyw berson arall y mae wedi 'i hysbysu yn unol â rheoliad 11 , o enw 'r person penodedig a fydd yn cynnal y gwrandawiad ;
< /sentence>
< sentence type="heading" elementid="20043240w-s136">
< roundbracket elementid="20043240w-rb123">
(  b )
< /roundbracket>
cyn gynted ag y bo 'n ymarferol , ar ôl unrhyw newid o ran pwy yw 'r person penodedig , hysbysu 'r awdurdod perthnasol yn ysgrifenedig fod rhaid i 'r awdurdod perthnasol hysbysu 'r personau hynny y mae ganddynt hawl i gael eu hysbysu yn unol ag is-baragraff
< roundbracket elementid="20043240w-rb124">
(  a )
< /roundbracket>
o 'r newid hwnnw , ac eithrio os nad yw 'n rhesymol ymarferol gwneud hynny cyn bod y gwrandawiad yn cael ei gynnal , ac os nad ydyw , rhaid cyhoeddi enw 'r person penodedig a 'r ffaith ei fod wedi 'i benodi ar ddechrau 'r gwrandawiad ; ac
< /sentence>
< sentence type="normal" elementid="20043240w-s137">
< roundbracket elementid="20043240w-rb125">
(  c )
< /roundbracket>
oni chytunir ar gyfnod hysbysu llai gyda 'r partïon , sicrhau bod yr awdurdod perthnasol yn rhoi hysbysiad ysgrifenedig nad yw 'n llai na 4 wythnos i 'r personau a hysbysir yn unol ag is-baragraff
< roundbracket elementid="20043240w-rb126">
(  a )
< /roundbracket>
o 'r dyddiad , yr amser a 'r lle a drefnwyd ar gyfer y gwrandawiad .
< /sentence>
< /p>
< p elementid="20043240w-p43">
< sentence type="heading" elementid="20043240w-s138">
< roundbracket elementid="20043240w-rb127">
(  2 )
< /roundbracket>
Rhaid i bob hysbysiad o wrandawiad a roddir yn unol â pharagraff
< roundbracket elementid="20043240w-rb128">
(  1 )
< /roundbracket>
< roundbracket elementid="20043240w-rb129">
(  c )
< /roundbracket>
gynnwys - 
< /sentence>
< /p>
< p elementid="20043240w-p44">
< sentence type="heading" elementid="20043240w-s139">
< roundbracket elementid="20043240w-rb130">
(  a )
< /roundbracket>
datganiad o ddyddiad , amser a lle 'r gwrandawiad ac o 'r pwerau sy 'n galluogi 'r Cynulliad Cenedlaethol i benderfynu 'r apêl o dan sylw ;
< /sentence>
< sentence type="heading" elementid="20043240w-s140">
< roundbracket elementid="20043240w-rb131">
(  b )
< /roundbracket>
disgrifiad ysgrifenedig o 'r tir sy 'n ddigonol i ddynodi ei leoliad a 'i hyd a 'i led ;
< /sentence>
< sentence type="heading" elementid="20043240w-s141">
< roundbracket elementid="20043240w-rb132">
(  c )
< /roundbracket>
disgrifiad byr o bwnc yr apêl ; ac
< /sentence>
< sentence type="normal" elementid="20043240w-s142">
< roundbracket elementid="20043240w-rb133">
(  ch )
< /roundbracket>
manylion ynghylch ble a phryd y gellir archwilio copïau o 'r dogfennau sy 'n berthnasol i 'r apêl .
< /sentence>
< /p>
< p elementid="20043240w-p45">
< sentence type="normal" elementid="20043240w-s143">
< roundbracket elementid="20043240w-rb134">
(  3 )
< /roundbracket>
Er gwaethaf paragraff
< roundbracket elementid="20043240w-rb135">
(  1 )
< /roundbracket>
, caiff y Cynulliad Cenedlaethol amrywio 'r dyddiad a bennwyd ar gyfer cynnal y gwrandawiad , p 'un a yw 'r dyddiad fel y 'i hamrywiwyd o fewn y cyfnod sydd fel arall yn ofynnol gan y paragraff hwnnw neu beidio ; ac mae paragraff
< roundbracket elementid="20043240w-rb136">
(  1 )
< /roundbracket>
< roundbracket elementid="20043240w-rb137">
(  c )
< /roundbracket>
yn gymwys i amrywiad ar ddyddiad fel yr oedd yn gymwys i 'r dyddiad a bennwyd yn wreiddiol .
< /sentence>
< sentence type="normal" elementid="20043240w-s144">
< roundbracket elementid="20043240w-rb138">
(  4 )
< /roundbracket>
Caiff y Cynulliad Cenedlaethol amrywio 'r amser neu 'r lle a bennwyd ar gyfer cynnal y gwrandawiad a rhaid iddo roi 'r rhybudd sy 'n ymddangos yn rhesymol iddo am unrhyw amrywiad .
< /sentence>
< sentence type="heading" elementid="20043240w-s145">
< strong elementid="20043240w-st21">
Hawl i fod yn bresennol mewn gwrandawiad a chymryd rhan ynddo
< /strong>
< /sentence>
< sentence type="normal" elementid="20043240w-s146">
16 .
< /sentence>
< sentence type="normal" elementid="20043240w-s147">
Mae gan y partïon hawl i fod yn bresennol a chymryd rhan mewn gwrandawiad , a gall y person penodedig ganiatáu i unrhyw bersonau eraill wneud hynny
< roundbracket elementid="20043240w-rb139">
(  naill ai ar eu rhan eu hunain neu ar ran unrhyw berson arall )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20043240w-s148">
< strong elementid="20043240w-st22">
Y weithdrefn mewn gwrandawiad
< /strong>
< /sentence>
< sentence type="normal" elementid="20043240w-s149">
17 .
< /sentence>
< sentence type="normal" elementid="20043240w-s150">
 - 
< roundbracket elementid="20043240w-rb140">
(  1 )
< /roundbracket>
Oni ddarperir fel arall yn y Rheoliadau hyn , caiff y person penodedig benderfynu 'r weithdrefn mewn gwrandawiad .
< /sentence>
< sentence type="normal" elementid="20043240w-s151">
< roundbracket elementid="20043240w-rb141">
(  2 )
< /roundbracket>
Mae gwrandawiad i fod ar ffurf trafodaeth sy 'n cael ei llywio gan y person penodedig ac ni ddylid caniatáu croesholi onid yw 'r person penodedig yn credu ei fod yn angenrheidiol i sicrhau archwiliad priodol o 'r pynciau sy 'n berthnasol i 'r apêl .
< /sentence>
< sentence type="normal" elementid="20043240w-s152">
< roundbracket elementid="20043240w-rb142">
(  3 )
< /roundbracket>
Ar ddechrau 'r gwrandawiad , rhaid i 'r person penodedig nodi 'r pynciau y mae 'n ymddangos i 'r person penodedig mai hwy yw 'r prif bynciau i 'w hystyried yn y gwrandawiad ac unrhyw faterion y mae angen i 'r person penodedig gael esboniad pellach arnynt gan unrhyw berson a chanddo hawl neu ganiatâd i gymryd rhan .
< /sentence>
< sentence type="normal" elementid="20043240w-s153">
< roundbracket elementid="20043240w-rb143">
(  4 )
< /roundbracket>
Nid oes dim ym mharagraff
< roundbracket elementid="20043240w-rb144">
(  3 )
< /roundbracket>
i atal unrhyw berson sydd â hawl neu ganiatâd i gymryd rhan yn y gwrandawiad rhag cyfeirio at bynciau y mae 'n barnu eu bod yn berthnasol ar gyfer ystyried yr apêl ond nad oeddent yn bynciau a nodwyd gan y person penodedig yn unol â 'r paragraff hwnnw .
< /sentence>
< sentence type="normal" elementid="20043240w-s154">
< roundbracket elementid="20043240w-rb145">
(  5 )
< /roundbracket>
Yn ddarostyngedig i 'r darpariaethau sydd wedi 'u cynnwys yn y rheoliad hwn , caiff person sydd â hawl i gymryd rhan mewn gwrandawiad , alw tystiolaeth ond , fel arall , bydd galw tystiolaeth yn dibynnu ar ddisgresiwn y person penodedig .
< /sentence>
< sentence type="normal" elementid="20043240w-s155">
< roundbracket elementid="20043240w-rb146">
(  6 )
< /roundbracket>
Caiff y person penodedig wrthod caniatáu i dystiolaeth lafar gael ei rhoi neu i unrhyw fater arall y mae 'r person penodedig yn barnu ei fod yn amherthnasol neu 'n ailadroddus , gael ei gyflwyno ond , os bydd y person penodedig yn gwrthod caniatáu i dystiolaeth lafar gael ei rhoi , caiff y person sy 'n dymuno rhoi 'r dystiolaeth gyflwyno unrhyw dystiolaeth neu fater arall yn ysgrifenedig i 'r person penodedig cyn diwedd y gwrandawiad .
< /sentence>
< sentence type="heading" elementid="20043240w-s156">
< roundbracket elementid="20043240w-rb147">
(  7 )
< /roundbracket>
Caiff y person penodedig - 
< /sentence>
< /p>
< p elementid="20043240w-p46">
< sentence type="heading" elementid="20043240w-s157">
< roundbracket elementid="20043240w-rb148">
(  a )
< /roundbracket>
ei gwneud yn ofynnol i unrhyw berson sy 'n bresennol mewn gwrandawiad neu sy 'n cymryd rhan ynddo ac sydd , ym marn y person penodedig , yn ymddwyn mewn modd aflonyddgar , ymadael ; a
< /sentence>
< sentence type="heading" elementid="20043240w-s158">
< roundbracket elementid="20043240w-rb149">
(  b )
< /roundbracket>
gwrthod caniatáu i 'r person hwnnw ddychwelyd neu ddim ond caniatáu i 'r person hwnnw ddychwelyd o dan yr amodau a bennir gan y person penodedig,
< /sentence>
< /p>
< p elementid="20043240w-p47">
< sentence type="normal" elementid="20043240w-s159">
ond caiff unrhyw berson o 'r fath gyflwyno unrhyw dystiolaeth neu fater arall yn ysgrifenedig i 'r person penodedig cyn diwedd y gwrandawiad .
< /sentence>
< sentence type="normal" elementid="20043240w-s160">
< roundbracket elementid="20043240w-rb150">
(  8 )
< /roundbracket>
Caiff y person penodedig ganiatáu i unrhyw berson newid datganiad neu ychwanegu ato i 'r graddau y bo hynny 'n angenrheidiol at ddibenion y gwrandawiad , ond rhaid i 'r person penodedig
< roundbracket elementid="20043240w-rb151">
(  drwy ohirio 'r gwrandawiad os bydd angen )
< /roundbracket>
roi cyfle digonol i bob person arall sydd â hawl i gymryd rhan yn y gwrandawiad , ac sydd mewn gwirionedd yn cymryd rhan ynddo , ystyried unrhyw fater newydd neu ddogfen newydd .
< /sentence>
< sentence type="normal" elementid="20043240w-s161">
< roundbracket elementid="20043240w-rb152">
(  9 )
< /roundbracket>
Caiff y person penodedig fwrw ymlaen â gwrandawiad yn absenoldeb unrhyw berson a chanddo hawl neu ganiatâd i gymryd rhan ynddo .
< /sentence>
< sentence type="normal" elementid="20043240w-s162">
< roundbracket elementid="20043240w-rb153">
(  10 )
< /roundbracket>
Caiff y person penodedig gymryd i ystyriaeth unrhyw sylw neu dystiolaeth ysgrifenedig neu unrhyw ddogfen ysgrifenedig arall a ddaeth i law 'r person penodedig oddi wrth unrhyw berson cyn dechrau 'r gwrandawiad neu yn ystod y gwrandawiad ar yr amod bod y person penodedig yn eu datgelu yn y gwrandawiad .
< /sentence>
< sentence type="normal" elementid="20043240w-s163">
< roundbracket elementid="20043240w-rb154">
(  11 )
< /roundbracket>
Caiff y person penodedig o dro i dro ohirio gwrandawiad ac , os cyhoeddir dyddiad , amser a lle 'r gwrandawiad gohiriedig yn y gwrandawiad cyn y gohiriad , ni fydd yn ofynnol cael unrhyw hysbysiad pellach .
< /sentence>
< sentence type="heading" elementid="20043240w-s164">
< strong elementid="20043240w-st23">
Penderfyniad ar ôl cynnal gwrandawiad
< /strong>
< /sentence>
< sentence type="normal" elementid="20043240w-s165">
18 .
< /sentence>
< sentence type="normal" elementid="20043240w-s166">
 - 
< roundbracket elementid="20043240w-rb155">
(  1 )
< /roundbracket>
Caiff y person penodedig anwybyddu unrhyw sylwadau , tystiolaeth neu ddogfennau ysgrifenedig eraill sy 'n dod i law ar ôl diwedd y gwrandawiad .
< /sentence>
< sentence type="heading" elementid="20043240w-s167">
< roundbracket elementid="20043240w-rb156">
(  2 )
< /roundbracket>
Os bydd y person penodedig , ar ôl diwedd y gwrandawiad , yn bwriadu cymryd i ystyriaeth unrhyw dystiolaeth newydd neu unrhyw fater newydd o ffaith
< roundbracket elementid="20043240w-rb157">
(  nad yw 'n fater o bolisi 'r Cynulliad Cenedlaethol )
< /roundbracket>
na chafodd ei godi yn y gwrandawiad ac y mae 'r person penodedig yn credu ei fod yn berthnasol i 'r penderfyniad , rhaid i 'r person penodedig beidio â gwneud hynny heb yn gyntaf - 
< /sentence>
< /p>
< p elementid="20043240w-p48">
< sentence type="heading" elementid="20043240w-s168">
< roundbracket elementid="20043240w-rb158">
(  a )
< /roundbracket>
hysbysu 'r personau sydd â hawl i gymryd rhan yn y gwrandawiad
< roundbracket elementid="20043240w-rb159">
(  p 'un a gwnaethant hynny neu beidio )
< /roundbracket>
o 'r mater o dan sylw ; a
< /sentence>
< sentence type="heading" elementid="20043240w-s169">
< roundbracket elementid="20043240w-rb160">
(  b )
< /roundbracket>
rhoi cyfle iddynt wneud sylwadau ysgrifenedig neu ofyn am ailagor y gwrandawiad,
< /sentence>
< /p>
< p elementid="20043240w-p49">
< sentence type="normal" elementid="20043240w-s170">
ar yr amod bod y Cynulliad Cenedlaethol yn cael y sylwadau ysgrifenedig hynny neu 'r cais am ailagor y gwrandawiad o fewn 3 wythnos i ddyddiad yr hysbysiad .
< /sentence>
< sentence type="heading" elementid="20043240w-s171">
< roundbracket elementid="20043240w-rb161">
(  3 )
< /roundbracket>
Caiff person penodedig beri bod gwrandawiad yn cael ei ailagor a rhaid iddo wneud hynny os gofynnir iddo ei wneud gan yr apelydd neu 'r atebydd o dan yr amgylchiadau , ac o fewn y cyfnod a grybwyllir ym mharagraff
< roundbracket elementid="20043240w-rb162">
(  2 )
< /roundbracket>
; ac os bydd gwrandawiad yn cael ei ailagor - 
< /sentence>
< /p>
< p elementid="20043240w-p50">
< sentence type="heading" elementid="20043240w-s172">
< roundbracket elementid="20043240w-rb163">
(  a )
< /roundbracket>
rhaid i 'r person penodedig anfon at y personau sydd â hawl i gymryd rhan yn y gwrandawiad , ac a gymerodd rhan ynddo , ddatganiad ysgrifenedig o 'r materion y gwahoddir tystiolaeth bellach amdanynt ; a
< /sentence>
< sentence type="normal" elementid="20043240w-s173">
< roundbracket elementid="20043240w-rb164">
(  b )
< /roundbracket>
mae rheoliad 15
< roundbracket elementid="20043240w-rb165">
(  1 )
< /roundbracket>
< roundbracket elementid="20043240w-rb166">
(  c )
< /roundbracket>
yn gymwys fel pe bai 'r cyfeiriadau at wrandawiad yn gyfeiriadau at wrandawiad a ailagorwyd .
< /sentence>
< /p>
< p elementid="20043240w-p51">
< sentence type="heading" elementid="20043240w-s174">
< strong elementid="20043240w-st24">
Tynnu apêl yn ôl
< /strong>
< /sentence>
< sentence type="normal" elementid="20043240w-s175">
19 .
< /sentence>
< sentence type="normal" elementid="20043240w-s176">
 - 
< roundbracket elementid="20043240w-rb167">
(  1 )
< /roundbracket>
Caiff yr apelydd dynnu apêl yn ôl drwy roi hysbysiad ysgrifenedig i 'r Cynulliad Cenedlaethol .
< /sentence>
< sentence type="normal" elementid="20043240w-s177">
< roundbracket elementid="20043240w-rb168">
(  2 )
< /roundbracket>
Rhaid i 'r Cynulliad Cenedlaethol , cyn gynted ag y bo 'n rhesymol ymarferol ar ôl iddo gael hysbysiad o dynnu apêl yn ôl , hysbysu 'r awdurdod perthnasol o 'r ffaith honno ; a chyn gynted ag y bo 'n ymarferol ar ôl cael hysbysiad o 'r fath , rhaid i 'r awdurdod perthnasol roi gwybod i 'r partïon ac unrhyw bersonau eraill a gyflwynodd sylwadau o dan reoliad 11 .
< /sentence>
< sentence type="heading" elementid="20043240w-s178">
< strong elementid="20043240w-st25">
Arolygu safleoedd
< /strong>
< /sentence>
< sentence type="normal" elementid="20043240w-s179">
20 .
< /sentence>
< sentence type="normal" elementid="20043240w-s180">
 - 
< roundbracket elementid="20043240w-rb169">
(  1 )
< /roundbracket>
Caiff y person penodedig arolygu 'r tir ar unrhyw adeg heb rywun yn gwmni iddo a heb hysbysu 'r apelydd na 'r atebydd o 'i fwriad i wneud hynny .
< /sentence>
< sentence type="heading" elementid="20043240w-s181">
< roundbracket elementid="20043240w-rb170">
(  2 )
< /roundbracket>
Yn ystod , neu ar ôl diwedd , gwrandawiad - 
< /sentence>
< /p>
< p elementid="20043240w-p52">
< sentence type="heading" elementid="20043240w-s182">
< roundbracket elementid="20043240w-rb171">
(  a )
< /roundbracket>
caiff y person penodedig , ar ôl iddo gyhoeddi yn ystod y gwrandawiad y dyddiad a 'r amser y bwriedir gwneud yr arolygiad , arolygu 'r tir yng nghwmni 'r partïon ac unrhyw berson arall y rhoddwyd caniatâd iddo fod yn bresennol a chymryd rhan yn y gwrandawiad ; a
< /sentence>
< sentence type="normal" elementid="20043240w-s183">
< roundbracket elementid="20043240w-rb172">
(  b )
< /roundbracket>
rhaid i 'r person penodedig wneud arolygiad o 'r fath os gofynnir iddo ei wneud gan y partïon cyn neu yn ystod gwrandawiad .
< /sentence>
< /p>
< p elementid="20043240w-p53">
< sentence type="heading" elementid="20043240w-s184">
< roundbracket elementid="20043240w-rb173">
(  3 )
< /roundbracket>
Os yw apêl yn cael ei phenderfynu drwy sylwadau ysgrifenedig - 
< /sentence>
< /p>
< p elementid="20043240w-p54">
< sentence type="heading" elementid="20043240w-s185">
< roundbracket elementid="20043240w-rb174">
(  a )
< /roundbracket>
caiff y person penodedig , ar ôl iddo roi rhybudd rhesymol mewn ysgrifen i 'r partïon o 'i fwriad i wneud hynny , arolygu 'r tir yng nghwmni 'r partïon ac unrhyw berson arall y mae 'r arolygydd yn credu y byddai 'n rhesymol ei wahodd ; a
< /sentence>
< sentence type="normal" elementid="20043240w-s186">
< roundbracket elementid="20043240w-rb175">
(  b )
< /roundbracket>
rhaid i 'r person penodedig wneud arolygiad o 'r fath , os gofynnir iddo ei wneud gan y partïon , cyn iddo wneud penderfyniad .
< /sentence>
< /p>
< p elementid="20043240w-p55">
< sentence type="normal" elementid="20043240w-s187">
< roundbracket elementid="20043240w-rb176">
(  4 )
< /roundbracket>
Rhaid i apelydd gymryd y camau sy 'n rhesymol o fewn pŵer yr apelydd er mwyn galluogi 'r person penodedig i gael mynediad i 'r tir sydd i 'w arolygu .
< /sentence>
< sentence type="normal" elementid="20043240w-s188">
< roundbracket elementid="20043240w-rb177">
(  5 )
< /roundbracket>
Nid yw 'n ofynnol i 'r person penodedig ohirio arolygiad o 'r math y cyfeirir ato ym mharagraff
< roundbracket elementid="20043240w-rb178">
(  2 )
< /roundbracket>
neu
< roundbracket elementid="20043240w-rb179">
(  3 )
< /roundbracket>
os na fydd unrhyw berson a grybwyllir yn y paragraffau hynny yn bresennol ar yr amser penodedig .
< /sentence>
< sentence type="heading" elementid="20043240w-s189">
< strong elementid="20043240w-st26">
Hysbysu o benderfyniad ar apêl
< /strong>
< /sentence>
< sentence type="normal" elementid="20043240w-s190">
21 .
< /sentence>
< sentence type="heading" elementid="20043240w-s191">
 - 
< roundbracket elementid="20043240w-rb180">
(  1 )
< /roundbracket>
Rhaid rhoi hysbysiad ysgrifenedig o benderfyniad y Cynulliad Cenedlaethol neu , yn ôl y digwydd , o benderfyniad y person penodedig , a 'r rhesymau dros y penderfyniad , i 'r personau canlynol  - 
< /sentence>
< /p>
< p elementid="20043240w-p56">
< sentence type="heading" elementid="20043240w-s192">
< roundbracket elementid="20043240w-rb181">
(  a )
< /roundbracket>
y partïon ;
< /sentence>
< sentence type="heading" elementid="20043240w-s193">
< roundbracket elementid="20043240w-rb182">
(  b )
< /roundbracket>
unrhyw berson sydd , ar ôl cymryd rhan yn y gwrandawiad , wedi gofyn am gael ei hysbysu o 'r penderfyniad ; ac
< /sentence>
< sentence type="normal" elementid="20043240w-s194">
< roundbracket elementid="20043240w-rb183">
(  c )
< /roundbracket>
unrhyw berson arall y rhoddwyd hysbysiad iddo yn unol â rheoliad 11 ac sydd wedi gofyn am gael ei hysbysu o 'r penderfyniad .
< /sentence>
< /p>
< p elementid="20043240w-p57">
< sentence type="normal" elementid="20043240w-s195">
< roundbracket elementid="20043240w-rb184">
(  2 )
< /roundbracket>
Caiff unrhyw berson a chanddo hawl i gael ei hysbysu o 'r penderfyniad o dan baragraff
< roundbracket elementid="20043240w-rb185">
(  1 )
< /roundbracket>
wneud cais i 'r Cynulliad Cenedlaethol , yn ysgrifenedig , am gael cyfle i fwrw golwg dros unrhyw ddogfennau a restrir yn yr hysbysiad a rhaid i 'r Cynulliad Cenedlaethol roi 'r cyfle hwnnw i 'r person hwnnw .
< /sentence>
< sentence type="normal" elementid="20043240w-s196">
< roundbracket elementid="20043240w-rb186">
(  3 )
< /roundbracket>
Rhaid i unrhyw berson sy 'n gwneud cais o dan baragraff
< roundbracket elementid="20043240w-rb187">
(  2 )
< /roundbracket>
sicrhau bod y Cynulliad Cenedlaethol yn ei gael o fewn 6 wythnos i ddyddiad y penderfyniad ar yr apêl .
< /sentence>
< sentence type="normal" elementid="20043240w-s197">
< roundbracket elementid="20043240w-rb188">
(  4 )
< /roundbracket>
Mae 'r penderfyniad yr hysbysir y partïon ohono o dan baragraff
< roundbracket elementid="20043240w-rb189">
(  1 )
< /roundbracket>
yn rhwymo 'r partïon .
< /sentence>
< sentence type="heading" elementid="20043240w-s198">
< strong elementid="20043240w-st27">
Caniatáu mwy o amser
< /strong>
< /sentence>
< sentence type="normal" elementid="20043240w-s199">
22 .
< /sentence>
< sentence type="normal" elementid="20043240w-s200">
Caiff y Cynulliad Cenedlaethol benderfynu bod terfynau amser sy 'n ddiweddarach na 'r rhai a ragnodir yn y Rheoliadau i fod yn gymwys a rhaid iddo roi hysbysiad ysgrifenedig o unrhyw benderfyniad o 'r fath i bob un o 'r partïon .
< /sentence>
< sentence type="normal" elementid="20043240w-s201">
Llofnodwyd ar ran Cynulliad Cenedlaethol Cymru o dan adran 66
< roundbracket elementid="20043240w-rb190">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20043240w-sb2">
[ 
< a type="href" elementid="20043240w-a5">
2
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="heading" elementid="20043240w-s202">
< em elementid="20043240w-em11">
D. Elis-Thomas
< /em>
< /sentence>
< sentence type="heading" elementid="20043240w-s203">
Llywydd y Cynulliad Cenedlaethol
< /sentence>
< sentence type="heading" elementid="20043240w-s204">
8 Rhagfyr 2004
< /sentence>
< /p>
< p elementid="20043240w-p58">
< sentence type="heading" elementid="20043240w-s205">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20043240w-s206">
< em elementid="20043240w-em12">
< roundbracket elementid="20043240w-rb191">
(  Nid yw 'r nodyn hwn yn rhan o 'r Rheoliadau )
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20043240w-s207">
Mae Rhan 8 o Ddeddf Ymddygiad Gwrthgymdeithasol 2003
< roundbracket elementid="20043240w-rb192">
(
< doublequotation elementid="20043240w-dq19">
"  y Ddeddf "
< /doublequotation>
)
< /roundbracket>
yn rhoi 'r pŵer i awdurdodau lleol i ymdrin â chwynion ynghylch gwrychoedd neu berthi uchel sy 'n effeithio 'n andwyol ar y mwynhad a gaiff cymydog o 'i eiddo .
< /sentence>
< sentence type="normal" elementid="20043240w-s208">
Caiff perchennog neu feddiannydd eiddo domestig
< roundbracket elementid="20043240w-rb193">
(  yr
< doublequotation elementid="20043240w-dq20">
"  achwynydd "
< /doublequotation>
)
< /roundbracket>
wneud cwyn ar y sail bod uchder gwrych neu berth sydd wedi 'u lleoli ar dir y mae person arall yn berchen arno neu 'n ei feddiannu yn effeithio 'n andwyol ar y mwynhad rhesymol a gaiff yr achwynydd o 'r eiddo .
< /sentence>
< sentence type="normal" elementid="20043240w-s209">
Rhaid gwneud cwyn i 'r awdurdod lleol y mae 'r tir y mae 'r gwrych neu 'r berth wedi 'u lleoli arno yn ei ardal a rhaid amgáu gyda 'r gŵyn ffi a bennir gan yr awdurdod lleol
< roundbracket elementid="20043240w-rb194">
(  yn ddarostyngedig i uchafswm a bennir gan Gynulliad Cenedlaethol Cymru
< roundbracket elementid="20043240w-rb195">
(
< doublequotation elementid="20043240w-dq21">
"  y Cynulliad Cenedlaethol "
< /doublequotation>
)
< /roundbracket>
, mewn perthynas â gwrychoedd neu berthi sydd wedi 'u lleoli yng Nghymru )
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20043240w-s210">
Mae adran 71 o 'r Ddeddf yn nodi 'r amrywiol hawliau i apelio yn erbyn penderfyniadau awdurdod lleol o dan adran 68 o 'r Ddeddf
< roundbracket elementid="20043240w-rb196">
(  gweithdrefn ar gyfer ymdrin â chwynion )
< /roundbracket>
ac o dan adran 70 o 'r Ddeddf
< roundbracket elementid="20043240w-rb197">
(  tynnu 'n ôl , hepgor neu lacio hysbysiadau adfer )
< /roundbracket>
ac yn erbyn unrhyw hysbysiad adfer a ddyroddir gan yr awdurdod lleol .
< /sentence>
< sentence type="normal" elementid="20043240w-s211">
Mewn perthynas â gwrychoedd neu berthi sydd wedi 'u lleoli yng Nghymru , rhaid gwneud apêl i 'r Cynulliad Cenedlaethol
< roundbracket elementid="20043240w-rb198">
(
< doublequotation elementid="20043240w-dq22">
"  yr awdurdod apelau "
< /doublequotation>
)
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20043240w-s212">
Yn ei swyddogaeth fel yr awdurdod apelau mewn perthynas â gwrychoedd neu berthi sydd wedi 'u lleoli yng Nghymru , mae adran 72 o 'r Ddeddf yn rhoi i 'r Cynulliad Cenedlaethol y pŵer i wneud rheoliadau - 
< /sentence>
< sentence type="heading" elementid="20043240w-s213">
< roundbracket elementid="20043240w-rb199">
(  a )
< /roundbracket>
i ddarparu gweithdrefn ar gyfer ymdrin ag apelau o dan Ran 8 o 'r Ddeddf
< roundbracket elementid="20043240w-rb200">
(  gan gynnwys pennu ar ba seiliau y caniateir i apelau gael eu gwneud )
< /roundbracket>
;
< /sentence>
< sentence type="heading" elementid="20043240w-s214">
< roundbracket elementid="20043240w-rb201">
(  b )
< /roundbracket>
i benodi person arall i wrando ar apelau a phenderfynu arnynt
< roundbracket elementid="20043240w-rb202">
(
< doublequotation elementid="20043240w-dq23">
"  person penodedig "
< /doublequotation>
)
< /roundbracket>
; ac
< /sentence>
< sentence type="normal" elementid="20043240w-s215">
< roundbracket elementid="20043240w-rb203">
(  c )
< /roundbracket>
i fynnu bod person penodedig yn cyflawni holl swyddogaethau 'r Cynulliad Cenedlaethol neu unrhyw rai ohonynt mewn perthynas ag apelau o 'r fath .
< /sentence>
< /p>
< p elementid="20043240w-p59">
< sentence type="normal" elementid="20043240w-s216">
Caiff y Cynulliad Cenedlaethol ganiatáu neu wrthod apêl , yn llwyr neu 'n rhannol .
< /sentence>
< sentence type="normal" elementid="20043240w-s217">
Os bydd y Cynulliad Cenedlaethol yn caniatáu apêl , caiff ddiddymu neu amrywio 'r hysbysiad adfer y mae 'r apêl yn berthnasol iddo a chaiff hefyd ddyroddi hysbysiad adfer os bydd yr awdurdod lleol , pan oedd yn ymdrin â 'r gŵyn wreiddiol , wedi penderfynu peidio â gwneud hynny .
< /sentence>
< sentence type="normal" elementid="20043240w-s218">
Ni waeth beth fydd ei benderfyniad ar yr apêl , caiff y Cynulliad Cenedlaethol gywiro unrhyw ddiffyg , camgymeriad neu gamddisgrifiad yn yr hysbysiad adfer gwreiddiol os yw o 'r farn na fydd hynny 'n peri anghyfiawnder .
< /sentence>
< /p>
< p elementid="20043240w-p60">
< sentence type="heading" elementid="20043240w-s219">
< em elementid="20043240w-em13">
< english>
Notes
< /english>
:
< /em>
< /sentence>
< sentence type="normal" elementid="20043240w-s220">
< squarebracket elementid="20043240w-sb3">
[  1 ]
< /squarebracket>
2003 p.38 .
< /sentence>
< sentence type="heading" elementid="20043240w-s221">
< a type="href" elementid="20043240w-a6">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20043240w-p61">
< sentence type="normal" elementid="20043240w-s222">
< squarebracket elementid="20043240w-sb4">
[  2 ]
< /squarebracket>
1998 p.38 .
< /sentence>
< sentence type="heading" elementid="20043240w-s223">
< a type="href" elementid="20043240w-a7">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20043240w-p62">
< sentence type="heading" elementid="20043240w-s224">
< a type="href" elementid="20043240w-a8">
< english>
English version
< /english>
< /a>
< /sentence>
< sentence type="heading" elementid="20043240w-s225">
ISBN 0 11 091050 8
< /sentence>
< /p>
< table elementid="20043240w-tb2">
< td elementid="20043240w-td5">
< p elementid="20043240w-p63">
< sentence type="heading" elementid="20043240w-s226">
< a type="href" elementid="20043240w-a9">
< english>
Other UK SIs
< /english>
< /a>
< a type="href" elementid="20043240w-a10">
< english>
Home
< /english>
< /a>
< a type="href" elementid="20043240w-a11">
< english>
National Assembly for Wales Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20043240w-a12">
< english>
Scottish Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20043240w-a13">
< english>
Statutory Rules of Northern Ireland
< /english>
< /a>
< a type="href" elementid="20043240w-a14">
< english>
Her Majesty 's Stationery Office
< /english>
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20043240w-td6">
< p elementid="20043240w-p64">
< sentence type="heading" elementid="20043240w-s227">
< strong elementid="20043240w-st28">
< english>
We welcome your 
< a type="href" elementid="20043240w-a15">
comments
< /a>
on this site
< /english>
< /strong>
< /sentence>
< /p>
< /td>
< td elementid="20043240w-td7">
< p elementid="20043240w-p65">
< sentence type="heading" elementid="20043240w-s228">
< english>
© Crown copyright 2004
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20043240w-td8">
< p elementid="20043240w-p66">
< sentence type="heading" elementid="20043240w-s229">
< english>
Prepared 16 December 2004
< /english>
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>5479< /wordcount>
< /file>
< file elementid="20043241w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Rheoliadau Gwrychoedd neu Berthi Uchel ( Ffioedd ) ( Cymru ) 2004< /pagetitle>
< doc_class>Statutory Instrument - Wales< /doc_class>
< year>2004< /year>
< number>3241< /number>
< legislation_code>30102< /legislation_code>
< legislation_type>Statutory Instrument - Wales< /legislation_type>
< subject>TIR, CYMRU^YMDDYGIAD GWRTHGYMDEITHASOL, CYMRU< /subject>
< laid_date>_EMPTY< /laid_date>
< made_date>20041208< /made_date>
< coming_into_force>20041231< /coming_into_force>
< isbn>0110910516< /isbn>
< title>Rheoliadau Gwrychoedd neu Berthi Uchel (Ffioedd) (Cymru) 2004< /title>
< signatory>D. Elis-Thomas< /signatory>
< signatory_title>Llywydd y Cynulliad Cenedlaethol< /signatory_title>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20043241w-p1">
< sentence type="heading" elementid="20043241w-s1">
Offerynnau Statudol 2004 Rhif 3241
< roundbracket elementid="20043241w-rb1">
(  Cy.283 )
< /roundbracket>
< /sentence>
< /p>
< p elementid="20043241w-p2">
< sentence type="heading" elementid="20043241w-s2">
< strong elementid="20043241w-st1">
Rheoliadau Gwrychoedd neu Berthi Uchel
< roundbracket elementid="20043241w-rb2">
(  Ffioedd )
< /roundbracket>
< roundbracket elementid="20043241w-rb3">
(  Cymru )
< /roundbracket>
2004
< /strong>
< /sentence>
< /p>
< p elementid="20043241w-p3">
< sentence type="heading" elementid="20043241w-s3">
© Hawlfraint y Goron 2004
< /sentence>
< /p>
< p elementid="20043241w-p4">
< sentence type="normal" elementid="20043241w-s4">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20043241w-p5">
< sentence type="normal" elementid="20043241w-s5">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20043241w-s6">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20043241w-p6">
< sentence type="normal" elementid="20043241w-s7">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20043241w-s8">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20043241w-s9">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20043241w-p7">
< sentence type="normal" elementid="20043241w-s10">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20043241w-s11">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan
< english>
The Stationery Office Limited
< /english>
fel
< strong elementid="20043241w-st2">
Rheoliadau Gwrychoedd neu Berthi Uchel
< roundbracket elementid="20043241w-rb4">
(  Ffioedd )
< /roundbracket>
< roundbracket elementid="20043241w-rb5">
(  Cymru )
< /roundbracket>
2004
< /strong>
, ISBN 0110910516 .
< /sentence>
< sentence type="normal" elementid="20043241w-s12">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20043241w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20043241w-s13">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20043241w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20043241w-p8">
< sentence type="normal" elementid="20043241w-s14">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20043241w-a3">
< english>
Frequently Asked Questions
< /english>
< /a> .
< /sentence>
< /p>
< p elementid="20043241w-p9">
< sentence type="normal" elementid="20043241w-s15">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20043241w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20043241w-s16">
Pan welwch fotwm
< doublequotation elementid="20043241w-dq2">
"
< english>
continue
< /english>
"
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20043241w-p10">
< sentence type="heading" elementid="20043241w-s17">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="heading" elementid="20043241w-s18">
< strong elementid="20043241w-st3">
2004 Rhif 3241
< roundbracket elementid="20043241w-rb6">
(  Cy.283 )
< /roundbracket>
< /strong>
< /sentence>
< sentence type="heading" elementid="20043241w-s19">
< strong elementid="20043241w-st4">
TIR , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20043241w-s20">
< strong elementid="20043241w-st5">
YMDDYGIAD GWRTHGYMDEITHASOL , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20043241w-s21">
Rheoliadau Gwrychoedd neu Berthi Uchel
< roundbracket elementid="20043241w-rb7">
(  Ffioedd )
< /roundbracket>
< roundbracket elementid="20043241w-rb8">
(  Cymru )
< /roundbracket>
2004
< /sentence>
< /p>
< table elementid="20043241w-tb1">
< td elementid="20043241w-td1">
< p elementid="20043241w-p11">
< sentence type="heading" elementid="20043241w-s22">
< em elementid="20043241w-em1">
Wedi 'u gwneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20043241w-td2">
< p elementid="20043241w-p12">
< sentence type="heading" elementid="20043241w-s23">
< em elementid="20043241w-em2">
8 Rhagfyr 2004
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20043241w-td3">
< p elementid="20043241w-p13">
< sentence type="heading" elementid="20043241w-s24">
< em elementid="20043241w-em3">
Yn dod i rym
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20043241w-td4">
< p elementid="20043241w-p14">
< sentence type="heading" elementid="20043241w-s25">
< em elementid="20043241w-em4">
31 Rhagfyr 2004
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20043241w-p15">
< sentence type="heading" elementid="20043241w-s26">
Mae Cynulliad Cenedlaethol Cymru , drwy arfer y pŵer a roddwyd iddo gan adran 68
< roundbracket elementid="20043241w-rb9">
(  7 )
< /roundbracket>
< roundbracket elementid="20043241w-rb10">
(  b )
< /roundbracket>
o Ddeddf Ymddygiad Gwrthgymdeithasol 2003
< roundbracket elementid="20043241w-rb11">
(
< doublequotation elementid="20043241w-dq3">
"  y Ddeddf "
< /doublequotation>
)
< /roundbracket>
< squarebracket elementid="20043241w-sb1">
[ 
< a type="href" elementid="20043241w-a4">
1
< /a>
]
< /squarebracket>
, drwy hyn yn gwneud y Rheoliadau canlynol :
< /sentence>
< sentence type="heading" elementid="20043241w-s27">
< strong elementid="20043241w-st6">
Enwi , cychwyn a chymhwyso
< /strong>
< /sentence>
< sentence type="normal" elementid="20043241w-s28">
1 .
< /sentence>
< sentence type="normal" elementid="20043241w-s29">
 - 
< roundbracket elementid="20043241w-rb12">
(  1 )
< /roundbracket>
Enw 'r Rheoliadau hyn yw Rheoliadau Gwrychoedd neu Berthi Uchel
< roundbracket elementid="20043241w-rb13">
(  Ffioedd )
< /roundbracket>
< roundbracket elementid="20043241w-rb14">
(  Cymru )
< /roundbracket>
2004 a deuant i rym ar 31 Rhagfyr 2004 .
< /sentence>
< sentence type="normal" elementid="20043241w-s30">
< roundbracket elementid="20043241w-rb15">
(  2 )
< /roundbracket>
Mae 'r Rheoliadau hyn yn gymwys i gŵynion a wneir , mewn perthynas â gwrychoedd neu berthi sydd wedi 'u lleoli yng Nghymru
< squarebracket elementid="20043241w-sb2">
[ 
< a type="href" elementid="20043241w-a5">
2
< /a>
]
< /squarebracket>
, o dan adran 68 o 'r Ddeddf i awdurdod perthnasol
< squarebracket elementid="20043241w-sb3">
[ 
< a type="href" elementid="20043241w-a6">
3
< /a>
]
< /squarebracket>
ar y dyddiad y daw 'r Rheoliadau hyn i rym neu ar ôl y dyddiad hwnnw .
< /sentence>
< sentence type="heading" elementid="20043241w-s31">
Uchafswm rhagnodedig
< /sentence>
< sentence type="normal" elementid="20043241w-s32">
2 .
< /sentence>
< sentence type="normal" elementid="20043241w-s33">
Rhaid i 'r ffi a bennir gan yr awdurdod perthnasol o dan adran 68
< roundbracket elementid="20043241w-rb16">
(  1 )
< /roundbracket>
< roundbracket elementid="20043241w-rb17">
(  b )
< /roundbracket>
o 'r Ddeddf beidio â bod yn fwy na £ 320 .
< /sentence>
< sentence type="normal" elementid="20043241w-s34">
Llofnodwyd ar ran Cynulliad Cenedlaethol Cymru yn unol ag adran 66
< roundbracket elementid="20043241w-rb18">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20043241w-sb4">
[ 
< a type="href" elementid="20043241w-a7">
4
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="heading" elementid="20043241w-s35">
< em elementid="20043241w-em5">
D. Elis-Thomas
< /em>
< /sentence>
< sentence type="heading" elementid="20043241w-s36">
Llywydd y Cynulliad Cenedlaethol
< /sentence>
< sentence type="heading" elementid="20043241w-s37">
8 Rhagfyr 2004
< /sentence>
< sentence type="heading" elementid="20043241w-s38">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20043241w-s39">
< em elementid="20043241w-em6">
< roundbracket elementid="20043241w-rb19">
(  Nid yw 'r nodyn hwn yn rhan o 'r Rheoliadau )
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20043241w-s40">
Mae Rhan 8 o Ddeddf Ymddygiad Gwrthgymdeithasol 2003 yn rhoi 'r pŵer i awdurdodau lleol ymdrin â chwynion ynghylch gwrychoedd neu berthi uchel sy 'n effeithio 'n andwyol ar y mwynhad a gaiff cymydog o 'i eiddo .
< /sentence>
< sentence type="normal" elementid="20043241w-s41">
Caiff perchennog neu feddiannydd eiddo domestig wneud cwyn os bod uchder gwrych neu berth a leolir ar dir y mae person arall yn berchen arno neu 'n ei feddiannu yn effeithio 'n andwyol ar y mwynhad rhesymol a gaiff y perchennog neu 'r meddiannydd o 'r eiddo hwnnw .
< /sentence>
< sentence type="normal" elementid="20043241w-s42">
Rhaid gwneud cwyn i 'r awdurdod lleol y mae 'r tir y mae 'r gwrych neu 'r berth wedi 'u lleoli arno yn ei ardal a rhaid amgáu gyda 'r gŵyn ffi a bennir gan yr awdurdod lleol
< roundbracket elementid="20043241w-rb20">
(  yn ddarostyngedig i uchafswm a ragnodir mewn rheoliadau a wneir gan Gynulliad Cenedlaethol Cymru mewn perthynas â gwrychoedd neu berthi sydd wedi 'u lleoli yng Nghymru )
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20043241w-s43">
Mae 'r Rheoliadau hyn yn rhagnodi 'r uchafswm hwnnw .
< /sentence>
< sentence type="heading" elementid="20043241w-s44">
< em elementid="20043241w-em7">
< english>
Notes
< /english>
:
< /em>
< /sentence>
< sentence type="normal" elementid="20043241w-s45">
< squarebracket elementid="20043241w-sb5">
[  1 ]
< /squarebracket>
2003 p.38 .
< /sentence>
< sentence type="heading" elementid="20043241w-s46">
< a type="href" elementid="20043241w-a8">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20043241w-p16">
< sentence type="normal" elementid="20043241w-s47">
< squarebracket elementid="20043241w-sb6">
[  2 ]
< /squarebracket>
< em elementid="20043241w-em8">
Gweler
< /em>
adran 66 o 'r Ddeddf ar gyfer ystyr
< doublequotation elementid="20043241w-dq4">
"
< english>
high hedge
< /english>
"
< /doublequotation> .
< /sentence>
< sentence type="heading" elementid="20043241w-s48">
< a type="href" elementid="20043241w-a9">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20043241w-p17">
< sentence type="normal" elementid="20043241w-s49">
< squarebracket elementid="20043241w-sb7">
[  3 ]
< /squarebracket>
Mae adran 65
< roundbracket elementid="20043241w-rb21">
(  5 )
< /roundbracket>
o 'r Ddeddf yn diffinio 'r
< doublequotation elementid="20043241w-dq5">
"
< english>
relevant authority
< /english>
"
< /doublequotation>
fel yr awdurdod lleol y mae 'r tir y mae 'r gwrych neu 'r berth uchel wedi 'u lleoli arno yn ei ardal .
< /sentence>
< sentence type="normal" elementid="20043241w-s50">
Mae adran 82 o 'r Ddeddf yn diffinio
< doublequotation elementid="20043241w-dq6">
"
< english>
local authority
< /english>
"
< /doublequotation>
, mewn perthynas â Chymru , fel y sir neu 'r cyngor bwrdeistref sirol .
< /sentence>
< sentence type="heading" elementid="20043241w-s51">
< a type="href" elementid="20043241w-a10">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20043241w-p18">
< sentence type="normal" elementid="20043241w-s52">
< squarebracket elementid="20043241w-sb8">
[  4 ]
< /squarebracket>
1998 p.38 .
< /sentence>
< sentence type="heading" elementid="20043241w-s53">
< a type="href" elementid="20043241w-a11">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20043241w-p19">
< sentence type="heading" elementid="20043241w-s54">
< a type="href" elementid="20043241w-a12">
< english>
English version
< /english>
< /a>
< /sentence>
< sentence type="heading" elementid="20043241w-s55">
ISBN 0 11 091051 6
< /sentence>
< /p>
< table elementid="20043241w-tb2">
< td elementid="20043241w-td5">
< p elementid="20043241w-p20">
< sentence type="heading" elementid="20043241w-s56">
< a type="href" elementid="20043241w-a13">
< english>
Other UK SIs
< /english>
< /a>
< a type="href" elementid="20043241w-a14">
< english>
Home
< /english>
< /a>
< a type="href" elementid="20043241w-a15">
< english>
National Assembly for Wales Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20043241w-a16">
< english>
Scottish Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20043241w-a17">
< english>
Statutory Rules of Northern Ireland
< /english>
< /a>
< a type="href" elementid="20043241w-a18">
< english>
Her Majesty 's Stationery Office
< /english>
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20043241w-td6">
< p elementid="20043241w-p21">
< sentence type="heading" elementid="20043241w-s57">
< strong elementid="20043241w-st7">
< english>
We welcome your 
< a type="href" elementid="20043241w-a19">
comments
< /a>
on this site
< /english>
< /strong>
< /sentence>
< /p>
< /td>
< td elementid="20043241w-td7">
< p elementid="20043241w-p22">
< sentence type="heading" elementid="20043241w-s58">
< english>
© Crown copyright 2004
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20043241w-td8">
< p elementid="20043241w-p23">
< sentence type="heading" elementid="20043241w-s59">
< english>
Prepared 16 December 2004
< /english>
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>792< /wordcount>
< /file>
< /corpus>

< ?xml version = "1.0" encoding="UTF-8"?>
< corpus topic="national-assistance-services" language="Welsh">
< file elementid="20001145w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Offerynnau Statudol 2000 Rhif 1145 ( Cy.81 )< /pagetitle>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20001145w-p1">
< sentence type="heading" elementid="20001145w-s1">
Offerynnau Statudol 2000 Rhif 1145
< roundbracket elementid="20001145w-rb1">
(  Cy.81 )
< /roundbracket>
< /sentence>
< /p>
< p elementid="20001145w-p2">
< sentence type="heading" elementid="20001145w-s2">
< strong elementid="20001145w-st1">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20001145w-rb2">
(  Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20001145w-rb3">
(  Cymru )
< /roundbracket>
2000
< /strong>
< /sentence>
< /p>
< p elementid="20001145w-p3">
< sentence type="heading" elementid="20001145w-s3">
< english>
© Crown Copyright 2000
< /english>
< /sentence>
< /p>
< p elementid="20001145w-p4">
< sentence type="normal" elementid="20001145w-s4">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20001145w-p5">
< sentence type="normal" elementid="20001145w-s5">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20001145w-s6">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20001145w-p6">
< sentence type="normal" elementid="20001145w-s7">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20001145w-s8">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20001145w-s9">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20001145w-p7">
< sentence type="normal" elementid="20001145w-s10">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20001145w-s11">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan
< english>
The Stationery Office Limited
< /english>
fel
< strong elementid="20001145w-st2">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20001145w-rb4">
(  Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20001145w-rb5">
(  Cymru )
< /roundbracket>
2000
< /strong>
, ISBN 0 11 090063 4 .
< /sentence>
< sentence type="normal" elementid="20001145w-s12">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20001145w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20001145w-s13">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20001145w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20001145w-p8">
< sentence type="normal" elementid="20001145w-s14">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20001145w-a3">
< english>
Frequently Asked Questions
< /english>
< /a> .
< /sentence>
< /p>
< p elementid="20001145w-p9">
< sentence type="normal" elementid="20001145w-s15">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20001145w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20001145w-s16">
Pan welwch fotwm
< doublequotation elementid="20001145w-dq2">
"
< english>
continue
< /continue>
"
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20001145w-p10">
< sentence type="heading" elementid="20001145w-s17">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="heading" elementid="20001145w-s18">
< strong elementid="20001145w-st3">
2000 Rhif 1145
< roundbracket elementid="20001145w-rb6">
(  Cy.81 )
< /roundbracket>
< /strong>
< /sentence>
< sentence type="heading" elementid="20001145w-s19">
< strong elementid="20001145w-st4">
GWASANAETHAU CYMORTH GWLADOL , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20001145w-s20">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20001145w-rb7">
(  Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20001145w-rb8">
(  Cymru )
< /roundbracket>
2000
< /sentence>
< /p>
< table elementid="20001145w-tb1">
< td elementid="20001145w-td1">
< p elementid="20001145w-p11">
< sentence type="heading" elementid="20001145w-s21">
< em elementid="20001145w-em1">
Wedi 'u gwneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20001145w-td2">
< p elementid="20001145w-p12">
< sentence type="heading" elementid="20001145w-s22">
< em elementid="20001145w-em2">
6 Ebrill 2000
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20001145w-td3">
< p elementid="20001145w-p13">
< sentence type="heading" elementid="20001145w-s23">
< em elementid="20001145w-em3">
Yn dod i rym
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20001145w-td4">
< p elementid="20001145w-p14">
< sentence type="heading" elementid="20001145w-s24">
< em elementid="20001145w-em4">
10 Ebrill 2000
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20001145w-p15">
< sentence type="heading" elementid="20001145w-s25">
Mae Cynulliad Cenedlaethol Cymru yn gwneud y Rheoliadau canlynol drwy arfer y pwerau a roddwyd i 'r Ysgrifennydd Gwladol gan adran 22
< roundbracket elementid="20001145w-rb9">
(  4 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948
< squarebracket elementid="20001145w-sb1">
[ 
< a type="href" elementid="20001145w-a4">
1
< /a>
]
< /squarebracket>
a phob pwer arall sy 'n galluogi 'r Ysgrifennydd Gwladol yn y cyswllt hwnnw , ac a freiniwyd bellach yng Nghynulliad Cenedlaethol Cymru
< squarebracket elementid="20001145w-sb2">
[ 
< a type="href" elementid="20001145w-a5">
2
< /a>
]
< /squarebracket>
:-
< /sentence>
< sentence type="heading" elementid="20001145w-s26">
< strong elementid="20001145w-st5">
Enwi , cychwyn a chymhwyso
< /strong>
< /sentence>
< sentence type="normal" elementid="20001145w-s27">
1 .
< /sentence>
< sentence type="normal" elementid="20001145w-s28">
 - 
< roundbracket elementid="20001145w-rb10">
(  1 )
< /roundbracket>
Enw 'r Rheoliadau hyn yw Rheoliadau Cymorth Gwladol
< roundbracket elementid="20001145w-rb11">
(  Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20001145w-rb12">
(  Cymru )
< /roundbracket>
2000 , a deuant i rym ar 10 Ebrill 2000 .
< /sentence>
< sentence type="normal" elementid="20001145w-s29">
< roundbracket elementid="20001145w-rb13">
(  2 )
< /roundbracket>
Mae 'r Rheoliadau hyn yn gymwys i Gymru yn unig .
< /sentence>
< sentence type="heading" elementid="20001145w-s30">
< strong elementid="20001145w-st6">
Y swm y mae ei angen at anghenion personol
< /strong>
< /sentence>
< sentence type="normal" elementid="20001145w-s31">
2 .
< /sentence>
< sentence type="normal" elementid="20001145w-s32">
Y swm y mae 'n rhaid i awdurdod lleol ragdybio o dan adran 22
< roundbracket elementid="20001145w-rb14">
(  4 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 y bydd ar berson ei angen at anghenion personol y person hwnnw fydd £ 15.45 yr wythnos .
< /sentence>
< sentence type="heading" elementid="20001145w-s33">
< strong elementid="20001145w-st7">
Diddymu
< /strong>
< /sentence>
< sentence type="normal" elementid="20001145w-s34">
3 .
< /sentence>
< sentence type="normal" elementid="20001145w-s35">
Mae Rheoliadau Cymorth Gwladol
< roundbracket elementid="20001145w-rb15">
(  Symiau at Anghenion Personol )
< /roundbracket>
1999
< squarebracket elementid="20001145w-sb3">
[ 
< a type="href" elementid="20001145w-a6">
3
< /a>
]
< /squarebracket>
, i 'r graddau y maent yn gymwys i Gymru , drwy hyn wedi 'u diddymu .
< /sentence>
< sentence type="normal" elementid="20001145w-s36">
Llofnodwyd ar ran Cynulliad Cenedlaethol Cymru o dan adran 66
< roundbracket elementid="20001145w-rb16">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20001145w-sb4">
[ 
< a type="href" elementid="20001145w-a7">
4
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="heading" elementid="20001145w-s37">
< em elementid="20001145w-em5">
D. Elis Thomas
< /em>
< /sentence>
< sentence type="heading" elementid="20001145w-s38">
Llywydd y Cynulliad Cenedlaethol
< /sentence>
< sentence type="heading" elementid="20001145w-s39">
6 Ebrill 2000
< /sentence>
< sentence type="heading" elementid="20001145w-s40">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20001145w-s41">
< em elementid="20001145w-em6">
< roundbracket elementid="20001145w-rb17">
Nid yw 'r nodyn hwn yn rhan o 'r Rheoliadau.)
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20001145w-s42">
Mae 'r Rheoliadau hyn yn nodi 'r swm wythnosol y mae 'r awdurdodau lleol yng Nghymru i ragdybio , yn niffyg amgylchiadau arbennig , y bydd ar breswylwyr sydd mewn llety a drefnwyd o dan Ran III o Ddeddf Cymorth Gwladol 1948 ei angen at eu hanghenion personol .
< /sentence>
< sentence type="normal" elementid="20001145w-s43">
O 10 Ebrill 2000 ymlaen , rhagdybir y bydd ar bob preswylydd o 'r fath angen £ 15.45 yr wythnos at eu hanghenion personol .
< /sentence>
< sentence type="normal" elementid="20001145w-s44">
Mae 'r Rheoliadau hyn yn disodli , yng Nghymru , Reoliadau Cymorth Gwladol
< roundbracket elementid="20001145w-rb18">
(  Symiau at Anghenion Personol )
< /roundbracket>
1999
< roundbracket elementid="20001145w-rb19">
(  a oedd yn darparu ar gyfer y swm y rhagdybid bod ar breswylwyr ei angen o 12 Ebrill 1999 ymlaen )
< /roundbracket>
sy 'n cael eu diddymu i 'r graddau y maent yn gymwys i Gymru .
< /sentence>
< sentence type="heading" elementid="20001145w-s45">
< em elementid="20001145w-em7">
< english>
Notes
< /english>
:
< /em>
< /sentence>
< sentence type="normal" elementid="20001145w-s46">
< squarebracket elementid="20001145w-sb5">
[  1 ]
< /squarebracket>
1948 p.29 .
< /sentence>
< sentence type="normal" elementid="20001145w-s47">
Gweler adrannau 35
< roundbracket elementid="20001145w-rb20">
(  1 )
< /roundbracket>
a 64
< roundbracket elementid="20001145w-rb21">
(  1 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 i gael diffiniadau o
< english>
the Minister
< /english>
a
< english>
prescribed
< /english>
yn y drefn honno , ac erthygl 2 o Orchymyn yr Ysgrifennydd Gwladol dros Wasanaethau Cymdeithasol 1968
< roundbracket elementid="20001145w-rb22">
(  O.S. 1968/ 1699 )
< /roundbracket>
a drosglwyddodd holl swyddogaethau 'r Gweinidog Iechyd i 'r Ysgrifennydd Gwladol .
< /sentence>
< sentence type="heading" elementid="20001145w-s48">
< a type="href" elementid="20001145w-a8">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20001145w-p16">
< sentence type="normal" elementid="20001145w-s49">
< squarebracket elementid="20001145w-sb6">
[  2 ]
< /squarebracket>
Trosglwyddwyd swyddogaethau 'r Ysgrifennydd Gwladol o dan adran 22
< roundbracket elementid="20001145w-rb23">
(  4 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 i Gynulliad Cenedlaethol Cymru gan Orchymyn Cynulliad Cenedlaethol Cymru
< roundbracket elementid="20001145w-rb24">
(  Trosglwyddo Swyddogaethau )
< /roundbracket>
1999
< roundbracket elementid="20001145w-rb25">
(  O.S. 1999/ 672 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20001145w-s50">
< a type="href" elementid="20001145w-a9">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20001145w-p17">
< sentence type="normal" elementid="20001145w-s51">
< squarebracket elementid="20001145w-sb7">
[  3 ]
< /squarebracket>
O.S.1999/ 549 .
< /sentence>
< sentence type="heading" elementid="20001145w-s52">
< a type="href" elementid="20001145w-a10">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20001145w-p18">
< sentence type="normal" elementid="20001145w-s53">
< squarebracket elementid="20001145w-sb8">
[  4 ]
< /squarebracket>
1998 p.38 .
< /sentence>
< sentence type="heading" elementid="20001145w-s54">
< a type="href" elementid="20001145w-a11">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20001145w-p19">
< sentence type="heading" elementid="20001145w-s55">
< a type="href" elementid="20001145w-a12">
< english>
English version
< /english>
< /a>
< /sentence>
< sentence type="heading" elementid="20001145w-s56">
ISBN 0 11 090063 4
< /sentence>
< /p>
< table elementid="20001145w-tb2">
< td elementid="20001145w-td5">
< p elementid="20001145w-p20">
< sentence type="heading" elementid="20001145w-s57">
< a type="href" elementid="20001145w-a13">
< english>
Other UK SIs
< /english>
< /a>
< a type="href" elementid="20001145w-a14">
< english>
Home
< /english>
< /a>
< a type="href" elementid="20001145w-a15">
< english>
National Assembly for Wales Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20001145w-a16">
< english>
Scottish Statutory Instrument
< /english>
< /a>
< a type="href" elementid="20001145w-a17">
< english>
Statutory Rules of Northern Ireland
< /english>
< /a>
< a type="href" elementid="20001145w-a18">
< english>
Her Majesty 's Stationery Office
< /english>
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20001145w-td6">
< p elementid="20001145w-p21">
< sentence type="heading" elementid="20001145w-s58">
< english>
< strong elementid="20001145w-st8">
We welcome your 
< a type="href" elementid="20001145w-a19">
comments
< /a>
on this site
< /strong>
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20001145w-td7">
< p elementid="20001145w-p22">
< sentence type="heading" elementid="20001145w-s59">
< english>
© Crown copyright 2000
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20001145w-td8">
< p elementid="20001145w-p23">
< sentence type="heading" elementid="20001145w-s60">
< english>
Prepared 1 June 2000
< /english>
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>795< /wordcount>
< /file>
< file elementid="20010276w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Offerynnau Statudol 2001 Rhif 276 ( Cy . 12 )< /pagetitle>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20010276w-p1">
< sentence type="normal" elementid="20010276w-s1">
Offerynnau Statudol 2001 Rhif 276  Cy .
< /sentence>
< sentence type="heading" elementid="20010276w-s2">
12 )
< /sentence>
< /p>
< p elementid="20010276w-p2">
< sentence type="heading" elementid="20010276w-s3">
< strong elementid="20010276w-st1">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20010276w-rb1">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20010276w-rb2">
(  Diwygio )
< /roundbracket>
< roundbracket elementid="20010276w-rb3">
(  Cymru )
< /roundbracket>
2001
< /strong>
< /sentence>
< /p>
< p elementid="20010276w-p3">
< sentence type="heading" elementid="20010276w-s4">
< english>
© Crown Copyright 2001
< /english>
< /sentence>
< /p>
< p elementid="20010276w-p4">
< sentence type="normal" elementid="20010276w-s5">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20010276w-p5">
< sentence type="normal" elementid="20010276w-s6">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20010276w-s7">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20010276w-p6">
< sentence type="normal" elementid="20010276w-s8">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20010276w-s9">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20010276w-s10">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20010276w-p7">
< sentence type="normal" elementid="20010276w-s11">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20010276w-s12">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan
< english>
The Stationery Office Limited
< /english>
fel
< strong elementid="20010276w-st2">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20010276w-rb4">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20010276w-rb5">
(  Diwygio )
< /roundbracket>
< roundbracket elementid="20010276w-rb6">
(  Cymru )
< /roundbracket>
2001
< /strong>
, ISBN 0-11-090176-3 .
< /sentence>
< sentence type="normal" elementid="20010276w-s13">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20010276w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20010276w-s14">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20010276w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20010276w-p8">
< sentence type="normal" elementid="20010276w-s15">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20010276w-a3">
< english>
Frequently Asked Questions
< /english>
< /a> .
< /sentence>
< /p>
< p elementid="20010276w-p9">
< sentence type="normal" elementid="20010276w-s16">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20010276w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20010276w-s17">
Pan welwch fotwm
< doublequotation elementid="20010276w-dq2">
"
< english>
continue
< /continue>
"
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20010276w-p10">
< sentence type="heading" elementid="20010276w-s18">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="normal" elementid="20010276w-s19">
2001 Rhif 276  Cy .
< /sentence>
< sentence type="heading" elementid="20010276w-s20">
12 )
< /sentence>
< sentence type="heading" elementid="20010276w-s21">
< strong elementid="20010276w-st3">
GWASANAETHAU CYMORTH GWLADOL , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20010276w-s22">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20010276w-rb7">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20010276w-rb8">
(  Diwygio )
< /roundbracket>
< roundbracket elementid="20010276w-rb9">
(  Cymru )
< /roundbracket>
2001
< /sentence>
< /p>
< table elementid="20010276w-tb1">
< td elementid="20010276w-td1">
< p elementid="20010276w-p11">
< sentence type="heading" elementid="20010276w-s23">
< em elementid="20010276w-em1">
Wedi 'u gwneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20010276w-td2">
< p elementid="20010276w-p12">
< sentence type="heading" elementid="20010276w-s24">
< em elementid="20010276w-em2">
30 Ionawr 2001
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20010276w-td3">
< p elementid="20010276w-p13">
< sentence type="heading" elementid="20010276w-s25">
< em elementid="20010276w-em3">
Yn dod i rym
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20010276w-td4">
< p elementid="20010276w-p14">
< sentence type="heading" elementid="20010276w-s26">
< em elementid="20010276w-em4">
1 Chwefror 2001
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20010276w-p15">
< sentence type="heading" elementid="20010276w-s27">
Mae Cynulliad Cenedlaethol Cymru yn gwneud y Rheoliadau canlynol drwy arfer y pwerau a roddwyd i 'r Ysgrifennydd Gwladol gan adran 22
< roundbracket elementid="20010276w-rb10">
(  5 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948
< squarebracket elementid="20010276w-sb1">
[ 
< a type="href" elementid="20010276w-a4">
1
< /a>
]
< /squarebracket>
ac sydd bellach wedi 'u breinio yng Nghynulliad Cenedlaethol Cymru
< squarebracket elementid="20010276w-sb2">
[ 
< a type="href" elementid="20010276w-a5">
2
< /a>
]
< /squarebracket>
:
< /sentence>
< sentence type="heading" elementid="20010276w-s28">
< strong elementid="20010276w-st4">
Enwi , cychwyn , dehongli a chymhwyso
< /strong>
< /sentence>
< sentence type="normal" elementid="20010276w-s29">
1 .
< /sentence>
< sentence type="normal" elementid="20010276w-s30">
 - 
< roundbracket elementid="20010276w-rb11">
(  1 )
< /roundbracket>
Enw 'r Rheoliadau hyn yw Rheoliadau Cymorth Gwladol
< roundbracket elementid="20010276w-rb12">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20010276w-rb13">
(  Diwygio )
< /roundbracket>
< roundbracket elementid="20010276w-rb14">
(  Cymru )
< /roundbracket>
2001 a deuant i rym ar 1 Chwefror 2001 .
< /sentence>
< sentence type="normal" elementid="20010276w-s31">
< roundbracket elementid="20010276w-rb15">
(  2 )
< /roundbracket>
Yn y Rheoliadau hyn , ystyr
< doublequotation elementid="20010276w-dq3">
"  y prif Reoliadau "
< /doublequotation>
< roundbracket elementid="20010276w-rb16">
(
< doublequotation elementid="20010276w-dq4">
"
< english>
the principal Regulations
< /english>
"
< /doublequotation>
)
< /roundbracket>
yw Rheoliadau Cymorth Gwladol
< roundbracket elementid="20010276w-rb17">
(  Asesu Adnoddau )
< /roundbracket>
1992
< squarebracket elementid="20010276w-sb3">
[ 
< a type="href" elementid="20010276w-a6">
3
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="normal" elementid="20010276w-s32">
< roundbracket elementid="20010276w-rb18">
(  3 )
< /roundbracket>
Bydd y Rheoliadau hyn yn gymwys i Gymru yn unig .
< /sentence>
< sentence type="heading" elementid="20010276w-s33">
Diwygio Atodlen 4 i 'r prif Reoliadau
< /sentence>
< sentence type="normal" elementid="20010276w-s34">
2 .
< /sentence>
< sentence type="heading" elementid="20010276w-s35">
Ar ddiwedd Atodlen 4 i 'r prif Reoliadau
< roundbracket elementid="20010276w-rb19">
(  cyfalaf sydd i 'w anwybyddu )
< /roundbracket>
ychwanegir y paragraff canlynol  - 
< /sentence>
< sentence type="normal" elementid="20010276w-s36">
20 .
< /sentence>
< sentence type="normal" elementid="20010276w-s37">
< english>
Any amount which would be disregarded under paragraph 61 of Schedule 10 to the Income Support Regulations  ex-gratia payment made by the Secretary of State in consequence of a person 's imprisonment or internment by the Japanese during the Second World War
< /english>
 ). [ 
< a type="href" elementid="20010276w-a7">
4
< /a>
]  .
< /sentence>
< /p>
< p elementid="20010276w-p16">
< sentence type="heading" elementid="20010276w-s38">
Llofnodwyd ar ran Cynulliad Cenedlaethol Cymru o dan adran 66
< roundbracket elementid="20010276w-rb20">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20010276w-sb4">
[ 
< a type="href" elementid="20010276w-a8">
5
< /a>
]
< /squarebracket>
)
< /sentence>
< sentence type="heading" elementid="20010276w-s39">
< em elementid="20010276w-em5">
D.Elis Thomas
< /em>
< /sentence>
< sentence type="heading" elementid="20010276w-s40">
Llywydd y Cynulliad Cenedlaethol
< /sentence>
< sentence type="heading" elementid="20010276w-s41">
30 Ionawr 2001
< /sentence>
< /p>
< p elementid="20010276w-p17">
< sentence type="heading" elementid="20010276w-s42">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20010276w-s43">
< em elementid="20010276w-em6">
< roundbracket elementid="20010276w-rb21">
(  Nid yw 'r nodyn hwn yn rhan o 'r Rheoliadau )
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20010276w-s44">
Mae 'r Rheoliadau hyn yn gwneud diwygiadau pellach i Reoliadau Cymorth Gwladol
< roundbracket elementid="20010276w-rb22">
(  Asesu Adnoddau )
< /roundbracket>
1992
< roundbracket elementid="20010276w-rb23">
(
< doublequotation elementid="20010276w-dq5">
"  y prif Reoliadau "
< /doublequotation>
)
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20010276w-s45">
Mae 'r prif Reoliadau yn ymwneud ag asesu gallu person
< roundbracket elementid="20010276w-rb24">
(
< doublequotation elementid="20010276w-dq6">
"  y preswylydd "
< /doublequotation>
)
< /roundbracket>
i dalu am lety sydd wedi 'i drefnu gan awdurdodau lleol o dan Ran III o Ddeddf Cymorth Gwladol 1948 .
< /sentence>
< sentence type="normal" elementid="20010276w-s46">
Mae llety Rhan III yn cael ei drefnu ar gyfer personau 18 oed neu drosodd y mae arnynt , oherwydd oedran , salwch , anabledd neu unrhyw amgylchiadau eraill , angen gofal a sylw nad ydynt ar gael iddynt fel arall , ac ar gyfer mamau sy 'n disgwyl plentyn a mamau sy 'n magu ac sydd mewn angen tebyg .
< /sentence>
< sentence type="normal" elementid="20010276w-s47">
Mae 'r prif Reoliadau yn darparu bod rhaid peidio ag asesu unrhyw breswylydd fel un sy 'n methu â thalu am lety Rhan III yn ôl y gyfradd safonol os yw cyfalaf y preswylydd hwnnw , o 'i gyfrifo yn unol â 'r prif Reoliadau , yn fwy nag £ 16,000 .
< /sentence>
< sentence type="heading" elementid="20010276w-s48">
Mae 'r Rheoliadau hyn yn diwygio 'r prif Reoliadau fel bod taliad ex-gratia o £ 10,000 sy 'n cael ei wneud gan yr Adran Nawdd Cymdeithasol ar 1 Chwefror 2001 neu wedyn yn cael ei anwybyddu , at ddibenion asesu cyfalaf preswylydd , os yw 'r taliad yn cael ei wneud yn sgil carcharu neu gaethiwo unrhyw rai o 'r canlynol gan y Japaneaid yn ystod yr Ail Ryfel Byd  - 
< /sentence>
< sentence type="heading" elementid="20010276w-s49">
< roundbracket elementid="20010276w-rb25">
(  a )
< /roundbracket>
y preswylydd ;
< /sentence>
< sentence type="heading" elementid="20010276w-s50">
< roundbracket elementid="20010276w-rb26">
(  b )
< /roundbracket>
partner y preswylydd ;
< /sentence>
< sentence type="heading" elementid="20010276w-s51">
< roundbracket elementid="20010276w-rb27">
(  c )
< /roundbracket>
priod marw y preswylydd ; neu
< /sentence>
< sentence type="normal" elementid="20010276w-s52">
< roundbracket elementid="20010276w-rb28">
(  ch )
< /roundbracket>
priod marw partner y preswylydd .
< /sentence>
< sentence type="heading" elementid="20010276w-s53">
< em elementid="20010276w-em7">
< english>
Notes
< /english>
:
< /em>
< /sentence>
< sentence type="normal" elementid="20010276w-s54">
< squarebracket elementid="20010276w-sb5">
[  1 ]
< /squarebracket>
1948 p.29 ; diwygiwyd adran 22
< roundbracket elementid="20010276w-rb29">
(  5 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 gan adran 39
< roundbracket elementid="20010276w-rb30">
(  1 )
< /roundbracket>
o Ddeddf y Weinyddiaeth Nawdd Cymdeithasol 1966
< roundbracket elementid="20010276w-rb31">
(  p.20 )
< /roundbracket>
a pharagraff 6 o Atodlen 6 iddi , gan adran 35
< roundbracket elementid="20010276w-rb32">
(  2 )
< /roundbracket>
o Ddeddf Budd-daliadau Atodol 1976
< roundbracket elementid="20010276w-rb33">
(  p.71 )
< /roundbracket>
a pharagraff 3
< roundbracket elementid="20010276w-rb34">
(  b )
< /roundbracket>
o Atodlen 7 iddi , gan adran 20 o Ddeddf Nawdd Cymdeithasol 1980
< roundbracket elementid="20010276w-rb35">
(  p.30 )
< /roundbracket>
a pharagraff 2 o Atodlen 4 iddi , a chan adran 86 o Ddeddf Nawdd Cymdeithasol 1986
< roundbracket elementid="20010276w-rb36">
(  p.50 )
< /roundbracket>
a pharagraff 32 o Atodlen 10 iddi .
< /sentence>
< sentence type="heading" elementid="20010276w-s55">
< a type="href" elementid="20010276w-a9">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20010276w-p18">
< sentence type="normal" elementid="20010276w-s56">
< squarebracket elementid="20010276w-sb6">
[  2 ]
< /squarebracket>
Cafodd swyddogaethau 'r Ysgrifennydd Gwladol o dan adran 22
< roundbracket elementid="20010276w-rb37">
(  5 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 eu trosglwyddo i Gynulliad Cenedlaethol Cymru gan Orchymyn Cynulliad Cenedlaethol Cymru
< roundbracket elementid="20010276w-rb38">
(  Trosglwyddo Swyddogaethau )
< /roundbracket>
1999
< roundbracket elementid="20010276w-rb39">
(  O.S. 1999/ 672 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20010276w-s57">
< a type="href" elementid="20010276w-a10">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20010276w-p19">
< sentence type="normal" elementid="20010276w-s58">
< squarebracket elementid="20010276w-sb7">
[  3 ]
< /squarebracket>
O.S. 1992/ 2977 ; yr offeryn diwygio perthnasol yw O.S. 1993/ 964 .
< /sentence>
< sentence type="heading" elementid="20010276w-s59">
< a type="href" elementid="20010276w-a11">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20010276w-p20">
< sentence type="heading" elementid="20010276w-s60">
< squarebracket elementid="20010276w-sb8">
[  4 ]
< /squarebracket>
Ychwanegwyd paragraff 61 gan O.S. 2001/ 22 
< a type="href" elementid="20010276w-a12">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20010276w-p21">
< sentence type="heading" elementid="20010276w-s61">
< squarebracket elementid="20010276w-sb9">
[  5 ]
< /squarebracket>
1998 p.38 
< a type="href" elementid="20010276w-a13">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20010276w-p22">
< sentence type="heading" elementid="20010276w-s62">
< a type="href" elementid="20010276w-a14">
< english>
English version
< /english>
< /a>
< /sentence>
< sentence type="heading" elementid="20010276w-s63">
ISBN 0-11-090176-3
< /sentence>
< /p>
< table elementid="20010276w-tb2">
< td elementid="20010276w-td5">
< p elementid="20010276w-p23">
< sentence type="heading" elementid="20010276w-s64">
< a type="href" elementid="20010276w-a15">
< english>
Other UK SIs
< /english>
< /a>
< a type="href" elementid="20010276w-a16">
< english>
Home
< /english>
< /a>
< a type="href" elementid="20010276w-a17">
< english>
National Assembly for Wales Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20010276w-a18">
< english>
Scottish Statutory Instrument
< /english>
< /a>
< a type="href" elementid="20010276w-a19">
< english>
Statutory Rules of Northern Ireland
< /english>
< /a>
< a type="href" elementid="20010276w-a20">
< english>
Her Majesty 's Stationery Office
< /english>
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20010276w-td6">
< p elementid="20010276w-p24">
< sentence type="heading" elementid="20010276w-s65">
< english>
< strong elementid="20010276w-st5">
We welcome your 
< a type="href" elementid="20010276w-a21">
comments
< /a>
on this site
< /strong>
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20010276w-td7">
< p elementid="20010276w-p25">
< sentence type="heading" elementid="20010276w-s66">
< english>
© Crown copyright 2001
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20010276w-td8">
< p elementid="20010276w-p26">
< sentence type="heading" elementid="20010276w-s67">
< english>
Prepared 26 March 2001
< /english>
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>962< /wordcount>
< /file>
< file elementid="20011408w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Rheoliadau Cymorth Gwladol ( Symiau at Anghenion Personol ) ( Cymru ) 2001< /pagetitle>
< doc_class>Statutory Instrument - Wales< /doc_class>
< year>2001< /year>
< number>1408< /number>
< legislation_code>30102< /legislation_code>
< legislation_type>Statutory Instrument - Wales< /legislation_type>
< subject>GWASANAETHAU CYMORTH GWLADOL, CYMRU< /subject>
< laid_date>_EMPTY< /laid_date>
< made_date>20011204< /made_date>
< coming_into_force>20011209< /coming_into_force>
< isbn>0110902041< /isbn>
< title>Rheoliadau Cymorth Gwladol (Symiau at Anghenion Personol) (Cymru) 2001< /title>
< signatory>Dafydd Elis-Thomas< /signatory>
< signatory_title>Llywydd Cynulliad Cenedlaethol Cymru< /signatory_title>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20011408w-p1">
< sentence type="normal" elementid="20011408w-s1">
Offerynnau Statudol 2001 Rhif 1408  Cy .
< /sentence>
< sentence type="heading" elementid="20011408w-s2">
94 )
< /sentence>
< /p>
< p elementid="20011408w-p2">
< sentence type="heading" elementid="20011408w-s3">
< strong elementid="20011408w-st1">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20011408w-rb1">
(  Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20011408w-rb2">
(  Cymru )
< /roundbracket>
2001
< /strong>
< /sentence>
< /p>
< p elementid="20011408w-p3">
< sentence type="heading" elementid="20011408w-s4">
< english>
© Crown Copyright 2001
< /english>
< /sentence>
< /p>
< p elementid="20011408w-p4">
< sentence type="normal" elementid="20011408w-s5">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20011408w-p5">
< sentence type="normal" elementid="20011408w-s6">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20011408w-s7">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20011408w-p6">
< sentence type="normal" elementid="20011408w-s8">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20011408w-s9">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20011408w-s10">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20011408w-p7">
< sentence type="normal" elementid="20011408w-s11">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20011408w-s12">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan
< english>
The Stationery Office Limited
< /english>
fel
< strong elementid="20011408w-st2">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20011408w-rb3">
(  Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20011408w-rb4">
(  Cymru )
< /roundbracket>
2001
< /strong>
, ISBN 0-11-090204-1 .
< /sentence>
< sentence type="normal" elementid="20011408w-s13">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20011408w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20011408w-s14">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20011408w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20011408w-p8">
< sentence type="normal" elementid="20011408w-s15">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20011408w-a3">
< english>
Frequently Asked Questions
< /english>
< /a> .
< /sentence>
< /p>
< p elementid="20011408w-p9">
< sentence type="normal" elementid="20011408w-s16">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20011408w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20011408w-s17">
Pan welwch fotwm
< doublequotation elementid="20011408w-dq2">
"
< english>
continue
< /continue>
"
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20011408w-p10">
< sentence type="heading" elementid="20011408w-s18">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="normal" elementid="20011408w-s19">
2001 Rhif 1408  Cy .
< /sentence>
< sentence type="heading" elementid="20011408w-s20">
94 )
< /sentence>
< sentence type="heading" elementid="20011408w-s21">
< strong elementid="20011408w-st3">
GWASANAETHAU CYMORTH GWLADOL , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20011408w-s22">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20011408w-rb5">
(  Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20011408w-rb6">
(  Cymru )
< /roundbracket>
2001
< /sentence>
< /p>
< table elementid="20011408w-tb1">
< td elementid="20011408w-td1">
< p elementid="20011408w-p11">
< sentence type="heading" elementid="20011408w-s23">
< em elementid="20011408w-em1">
Wedi 'u gwneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20011408w-td2">
< p elementid="20011408w-p12">
< sentence type="heading" elementid="20011408w-s24">
< em elementid="20011408w-em2">
4 Ebrill 2001
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20011408w-td3">
< p elementid="20011408w-p13">
< sentence type="heading" elementid="20011408w-s25">
< em elementid="20011408w-em3">
Yn dod i rym
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20011408w-td4">
< p elementid="20011408w-p14">
< sentence type="heading" elementid="20011408w-s26">
< em elementid="20011408w-em4">
9 Ebrill 2001
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20011408w-p15">
< sentence type="heading" elementid="20011408w-s27">
Mae Cynulliad Cenedlaethol Cymru yn gwneud y Rheoliadau canlynol drwy arfer y pwerau a roddwyd i 'r Ysgrifennydd Gwladol gan adran 22
< roundbracket elementid="20011408w-rb7">
(  4 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948
< squarebracket elementid="20011408w-sb1">
[ 
< a type="href" elementid="20011408w-a4">
1
< /a>
]
< /squarebracket>
a phob p er arall sy 'n galluogi 'r Ysgrifennydd Gwladol yn y cyswllt hwnnw , ac a freiniwyd bellach yng Nghynulliad Cenedlaethol Cymru
< squarebracket elementid="20011408w-sb2">
[ 
< a type="href" elementid="20011408w-a5">
2
< /a>
]
< /squarebracket>
:  - 
< /sentence>
< sentence type="heading" elementid="20011408w-s28">
< strong elementid="20011408w-st4">
Enwi , cychwyn a chymhwyso
< /strong>
< /sentence>
< sentence type="normal" elementid="20011408w-s29">
1 .
< /sentence>
< sentence type="normal" elementid="20011408w-s30">
 - 
< roundbracket elementid="20011408w-rb8">
(  1 )
< /roundbracket>
Enw 'r Rheoliadau hyn yw Rheoliadau Cymorth Gwladol
< roundbracket elementid="20011408w-rb9">
(  Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20011408w-rb10">
(  Cymru )
< /roundbracket>
2001 , a deuant i rym ar 9 Ebrill 2001 .
< /sentence>
< sentence type="normal" elementid="20011408w-s31">
< roundbracket elementid="20011408w-rb11">
(  2 )
< /roundbracket>
Mae 'r Rheoliadau hyn yn gymwys i Gymru yn unig .
< /sentence>
< sentence type="heading" elementid="20011408w-s32">
< strong elementid="20011408w-st5">
Y swm y mae ei angen at anghenion personol
< /strong>
< /sentence>
< sentence type="normal" elementid="20011408w-s33">
2 .
< /sentence>
< sentence type="normal" elementid="20011408w-s34">
Y swm y mae 'n rhaid i awdurdod lleol ragdybio o dan adran 22
< roundbracket elementid="20011408w-rb12">
(  4 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 y bydd ar berson ei angen at anghenion personol y person hwnnw fydd £ 16.05 yr wythnos .
< /sentence>
< sentence type="heading" elementid="20011408w-s35">
< strong elementid="20011408w-st6">
Diddymu
< /strong>
< /sentence>
< sentence type="normal" elementid="20011408w-s36">
3 .
< /sentence>
< sentence type="normal" elementid="20011408w-s37">
Mae Rheoliadau Cymorth Gwladol
< roundbracket elementid="20011408w-rb13">
(  Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20011408w-rb14">
(  Cymru )
< /roundbracket>
2000
< squarebracket elementid="20011408w-sb3">
[ 
< a type="href" elementid="20011408w-a6">
3
< /a>
]
< /squarebracket>
, drwy hyn wedi 'u diddymu .
< /sentence>
< sentence type="normal" elementid="20011408w-s38">
Llofnodwyd ar ran Cynulliad Cenedlaethol Cymru o dan adran 66
< roundbracket elementid="20011408w-rb15">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20011408w-sb4">
[ 
< a type="href" elementid="20011408w-a7">
4
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="heading" elementid="20011408w-s39">
< em elementid="20011408w-em5">
Dafydd Elis-Thomas
< /em>
< /sentence>
< sentence type="heading" elementid="20011408w-s40">
Llywydd Cynulliad Cenedlaethol Cymru
< /sentence>
< sentence type="heading" elementid="20011408w-s41">
4 ydd Ebrill 2001
< /sentence>
< sentence type="heading" elementid="20011408w-s42">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20011408w-s43">
< em elementid="20011408w-em6">
< roundbracket elementid="20011408w-rb16">
Nid yw 'r nodyn hwn yn rhan o 'r Rheoliadau.)
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20011408w-s44">
Mae 'r Rheoliadau hyn yn nodi 'r swm wythnosol y mae 'r awdurdodau lleol yng Nghymru i ragdybio , yn niffyg amgylchiadau arbennig , y bydd ar breswylwyr sydd mewn llety a drefnwyd o dan Ran III o Ddeddf Cymorth Gwladol 1948 ei angen at eu hanghenion personol .
< /sentence>
< sentence type="normal" elementid="20011408w-s45">
0 9 Ebrill 2001 ymlaen , rhagdybir y bydd ar bob preswylydd o 'r fath angen £ 16.05 yr wythnos at eu hanghenion personol .
< /sentence>
< sentence type="normal" elementid="20011408w-s46">
Mae 'r Rheoliadau hyn yn disodli Rheoliadau Cymorth Gwladol
< roundbracket elementid="20011408w-rb17">
(  Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20011408w-rb18">
(  Cymru )
< /roundbracket>
2000
< roundbracket elementid="20011408w-rb19">
(  a oedd yn darparu ar gyfer y swm y rhagdybid bod ar breswylwyr ei angen o 10 Ebrill 2000 ymlaen )
< /roundbracket>
sy 'n cael ei diddymu .
< /sentence>
< sentence type="heading" elementid="20011408w-s47">
< em elementid="20011408w-em7">
< english>
Notes
< /english>
:
< /em>
< /sentence>
< sentence type="normal" elementid="20011408w-s48">
< squarebracket elementid="20011408w-sb5">
[  1 ]
< /squarebracket>
1948 p.29 .
< /sentence>
< sentence type="normal" elementid="20011408w-s49">
Gweler adrannau 35
< roundbracket elementid="20011408w-rb20">
(  1 )
< /roundbracket>
a 64
< roundbracket elementid="20011408w-rb21">
(  1 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 i gael diffiniadau o
< doublequotation elementid="20011408w-dq3">
"
< english>
the Minister
< /english>
"
< /doublequotation>
a
< doublequotation elementid="20011408w-dq4">
"
< english>
prescribed
< /english>
"
< /doublequotation>
yn y drefn honno , ac erthygl 2 o Orchymyn yr Ysgrifennydd Gwladol dros Wasanaethau Cymdeithasol 1968
< roundbracket elementid="20011408w-rb22">
(  O.S. 1968/ 1699 )
< /roundbracket>
a drosglwyddodd holl swyddogaethau 'r Gweinidog Iechyd i 'r Ysgrifennydd Gwladol .
< /sentence>
< sentence type="heading" elementid="20011408w-s50">
< a type="href" elementid="20011408w-a8">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20011408w-p16">
< sentence type="normal" elementid="20011408w-s51">
< squarebracket elementid="20011408w-sb6">
[  2 ]
< /squarebracket>
Trosglwyddwyd swyddogaethau 'r Ysgrifennydd Gwladol o dan adran 22
< roundbracket elementid="20011408w-rb23">
(  4 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 i Gynulliad Cenedlaethol Cymru gan Orchymyn Cynulliad Cenedlaethol Cymru
< roundbracket elementid="20011408w-rb24">
(  Trosglwyddo Swyddogaethau )
< /roundbracket>
1999
< roundbracket elementid="20011408w-rb25">
(  O.S. 1999/ 672 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20011408w-s52">
< a type="href" elementid="20011408w-a9">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20011408w-p17">
< sentence type="normal" elementid="20011408w-s53">
< squarebracket elementid="20011408w-sb7">
[  3 ]
< /squarebracket>
O.S. 2000/ 1145 .
< /sentence>
< sentence type="heading" elementid="20011408w-s54">
< a type="href" elementid="20011408w-a10">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20011408w-p18">
< sentence type="normal" elementid="20011408w-s55">
< squarebracket elementid="20011408w-sb8">
[  4 ]
< /squarebracket>
1998 p.38 .
< /sentence>
< sentence type="heading" elementid="20011408w-s56">
< a type="href" elementid="20011408w-a11">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20011408w-p19">
< sentence type="heading" elementid="20011408w-s57">
< a type="href" elementid="20011408w-a12">
< english>
English version
< /english>
< /a>
< /sentence>
< sentence type="heading" elementid="20011408w-s58">
ISBN 0-11-090204-1
< /sentence>
< /p>
< table elementid="20011408w-tb2">
< td elementid="20011408w-td5">
< p elementid="20011408w-p20">
< sentence type="heading" elementid="20011408w-s59">
< a type="href" elementid="20011408w-a13">
< english>
Other UK SIs
< /english>
< /a>
< a type="href" elementid="20011408w-a14">
< english>
Home
< /english>
< /a>
< a type="href" elementid="20011408w-a15">
< english>
National Assembly for Wales Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20011408w-a16">
< english>
Scottish Statutory Instrument
< /english>
< /a>
< a type="href" elementid="20011408w-a17">
< english>
Statutory Rules of Northern Ireland
< /english>
< /a>
< a type="href" elementid="20011408w-a18">
< english>
Her Majesty 's Stationery Office
< /english>
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20011408w-td6">
< p elementid="20011408w-p21">
< sentence type="heading" elementid="20011408w-s60">
< english>
< strong elementid="20011408w-st7">
We welcome your 
< a type="href" elementid="20011408w-a19">
comments
< /a>
on this site
< /strong>
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20011408w-td7">
< p elementid="20011408w-p22">
< sentence type="heading" elementid="20011408w-s61">
< english>
© Crown copyright 2001
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20011408w-td8">
< p elementid="20011408w-p23">
< sentence type="heading" elementid="20011408w-s62">
< english>
Prepared 1 June 2001
< /english>
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>775< /wordcount>
< /file>
< file elementid="20011409w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Rheoliadau Cymorth Gwladol ( Asesu Adnoddau ) ( Diwygio Rhif 2 ) ( Cymru ) 2001< /pagetitle>
< doc_class>Statutory Instrument - Wales< /doc_class>
< year>2001< /year>
< number>1409< /number>
< legislation_code>30102< /legislation_code>
< legislation_type>Statutory Instrument - Wales< /legislation_type>
< subject>GWASANAETHAU CYMORTH GWLADOL, CYMRU< /subject>
< laid_date>_EMPTY< /laid_date>
< made_date>20011204< /made_date>
< coming_into_force>20011209< /coming_into_force>
< isbn>0110902211< /isbn>
< title>Rheoliadau Cymorth Gwladol (Asesu Adnoddau) (Diwygio Rhif 2) (Cymru) 2001< /title>
< signatory>Dafydd Elis Thomas< /signatory>
< signatory_title>Llywydd y Cynulliad Cenedlaethol< /signatory_title>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20011409w-p1">
< sentence type="normal" elementid="20011409w-s1">
Offerynnau Statudol 2001 Rhif 1409  Cy .
< /sentence>
< sentence type="heading" elementid="20011409w-s2">
95 )
< /sentence>
< /p>
< p elementid="20011409w-p2">
< sentence type="heading" elementid="20011409w-s3">
< strong elementid="20011409w-st1">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20011409w-rb1">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20011409w-rb2">
(  Diwygio Rhif 2 )
< /roundbracket>
< roundbracket elementid="20011409w-rb3">
(  Cymru )
< /roundbracket>
2001
< /strong>
< /sentence>
< /p>
< p elementid="20011409w-p3">
< sentence type="heading" elementid="20011409w-s4">
< english>
© Crown Copyright 2001
< /english>
< /sentence>
< /p>
< p elementid="20011409w-p4">
< sentence type="normal" elementid="20011409w-s5">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20011409w-p5">
< sentence type="normal" elementid="20011409w-s6">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20011409w-s7">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20011409w-p6">
< sentence type="normal" elementid="20011409w-s8">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20011409w-s9">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20011409w-s10">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20011409w-p7">
< sentence type="normal" elementid="20011409w-s11">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20011409w-s12">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan
< english>
The Stationery Office Limited
< /english>
fel
< strong elementid="20011409w-st2">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20011409w-rb4">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20011409w-rb5">
(  Diwygio Rhif 2 )
< /roundbracket>
< roundbracket elementid="20011409w-rb6">
(  Cymru )
< /roundbracket>
2001
< /strong>
, ISBN 0-11-090221-1 .
< /sentence>
< sentence type="normal" elementid="20011409w-s13">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20011409w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20011409w-s14">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20011409w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20011409w-p8">
< sentence type="normal" elementid="20011409w-s15">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20011409w-a3">
< english>
Frequently Asked Questions
< /english>
< /a> .
< /sentence>
< /p>
< p elementid="20011409w-p9">
< sentence type="normal" elementid="20011409w-s16">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20011409w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20011409w-s17">
Pan welwch fotwm
< doublequotation elementid="20011409w-dq2">
"
< english>
continue
< /continue>
"
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20011409w-p10">
< sentence type="heading" elementid="20011409w-s18">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="normal" elementid="20011409w-s19">
2001 Rhif 1409  Cy .
< /sentence>
< sentence type="heading" elementid="20011409w-s20">
95 )
< /sentence>
< sentence type="heading" elementid="20011409w-s21">
< strong elementid="20011409w-st3">
GWASANAETHAU CYMORTH GWLADOL , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20011409w-s22">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20011409w-rb7">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20011409w-rb8">
(  Diwygio Rhif 2 )
< /roundbracket>
< roundbracket elementid="20011409w-rb9">
(  Cymru )
< /roundbracket>
2001
< /sentence>
< /p>
< table elementid="20011409w-tb1">
< td elementid="20011409w-td1">
< p elementid="20011409w-p11">
< sentence type="heading" elementid="20011409w-s23">
< em elementid="20011409w-em1">
Wedi 'u gwneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20011409w-td2">
< p elementid="20011409w-p12">
< sentence type="heading" elementid="20011409w-s24">
< em elementid="20011409w-em2">
4 Ebrill 2001
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20011409w-td3">
< p elementid="20011409w-p13">
< sentence type="heading" elementid="20011409w-s25">
< em elementid="20011409w-em3">
Yn dod i rym
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20011409w-td4">
< p elementid="20011409w-p14">
< sentence type="heading" elementid="20011409w-s26">
< em elementid="20011409w-em4">
at ddiben Rheoliad 2
< roundbracket elementid="20011409w-rb10">
(  6 )
< /roundbracket>
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20011409w-td5">
< p elementid="20011409w-p15">
< sentence type="heading" elementid="20011409w-s27">
< em elementid="20011409w-em5">
12 Ebrill 2001
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20011409w-td6">
< p elementid="20011409w-p16">
< sentence type="heading" elementid="20011409w-s28">
< em elementid="20011409w-em6">
at bob diben arall
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20011409w-td7">
< p elementid="20011409w-p17">
< sentence type="heading" elementid="20011409w-s29">
< em elementid="20011409w-em7">
9 Ebrill 2001
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20011409w-p18">
< sentence type="heading" elementid="20011409w-s30">
Mae Cynulliad Cenedlaethol Cymru yn gwneud y Rheoliadau canlynol drwy arfer y pwerau a roddwyd i 'r Ysgrifennydd Gwladol gan adran 22
< roundbracket elementid="20011409w-rb11">
(  5 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948
< squarebracket elementid="20011409w-sb1">
[ 
< a type="href" elementid="20011409w-a4">
1
< /a>
]
< /squarebracket>
ac sydd bellach wedi 'u breinio yng Nghynulliad Cenedlaethol Cymru
< squarebracket elementid="20011409w-sb2">
[ 
< a type="href" elementid="20011409w-a5">
2
< /a>
]
< /squarebracket>
:
< /sentence>
< sentence type="heading" elementid="20011409w-s31">
< strong elementid="20011409w-st4">
Enwi , cychwyn , dehongli a chymhwyso
< /strong>
< /sentence>
< sentence type="normal" elementid="20011409w-s32">
1 .
< /sentence>
< sentence type="normal" elementid="20011409w-s33">
 - 
< roundbracket elementid="20011409w-rb12">
(  1 )
< /roundbracket>
Enw 'r Rheoliadau hyn yw Rheoliadau Cymorth Gwladol
< roundbracket elementid="20011409w-rb13">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20011409w-rb14">
(  Diwygio Rhif 2 )
< /roundbracket>
< roundbracket elementid="20011409w-rb15">
(  Cymru )
< /roundbracket>
2001 a deuant i rym at ddibenion Rheoliad 2
< roundbracket elementid="20011409w-rb16">
(  6 )
< /roundbracket>
ar 12 Ebrill 2001 ac at bob diben arall ar 9 Ebrill 2001 .
< /sentence>
< sentence type="normal" elementid="20011409w-s34">
< roundbracket elementid="20011409w-rb17">
(  2 )
< /roundbracket>
Yn y Rheoliadau hyn , ystyr
< doublequotation elementid="20011409w-dq3">
"  y prif Reoliadau "
< /doublequotation>
< roundbracket elementid="20011409w-rb18">
(
< doublequotation elementid="20011409w-dq4">
"
< english>
the principal Regulations
< /english>
"
< /doublequotation>
)
< /roundbracket>
yw Rheoliadau Cymorth Gwladol
< roundbracket elementid="20011409w-rb19">
(  Asesu Adnoddau )
< /roundbracket>
1992
< squarebracket elementid="20011409w-sb3">
[ 
< a type="href" elementid="20011409w-a6">
3
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="normal" elementid="20011409w-s35">
< roundbracket elementid="20011409w-rb20">
(  3 )
< /roundbracket>
Bydd y Rheoliadau hyn yn gymwys i Gymru yn unig .
< /sentence>
< sentence type="heading" elementid="20011409w-s36">
Diwygio 'r prif Reoliadau
< /sentence>
< sentence type="normal" elementid="20011409w-s37">
2 .
< /sentence>
< sentence type="normal" elementid="20011409w-s38">
 - 
< roundbracket elementid="20011409w-rb21">
(  1 )
< /roundbracket>
Diwygir y prif Reoliadau yn unol â pharagraffau canlynol y rheoliad hwn .
< /sentence>
< sentence type="heading" elementid="20011409w-s39">
< roundbracket elementid="20011409w-rb22">
(  2 )
< /roundbracket>
Yn rheoliad 2
< roundbracket elementid="20011409w-rb23">
(  1 )
< /roundbracket>
< roundbracket elementid="20011409w-rb24">
(  dehongli )
< /roundbracket>
mewnosodir ar ôl y diffiniad o
< doublequotation elementid="20011409w-dq5">
"  partner "
< /doublequotation>
, y diffiniad canlynol - 
< /sentence>
< sentence type="normal" elementid="20011409w-s40">
< doublequotation elementid="20011409w-dq6">
"  "
< /doublequotation>
< english>
permanent resident  means a resident who is not a temporary resident
< /english>
 .
< /sentence>
< sentence type="normal" elementid="20011409w-s41">
.
< /sentence>
< /p>
< p elementid="20011409w-p19">
< sentence type="normal" elementid="20011409w-s42">
< roundbracket elementid="20011409w-rb25">
(  3 )
< /roundbracket>
Yn rheoliad 20
< roundbracket elementid="20011409w-rb26">
(  terfyn cyfalaf )
< /roundbracket>
yn lle 'r ffigur
< doublequotation elementid="20011409w-dq7">
"  £ 16,000 "
< /doublequotation>
rhoddir y ffigur
< doublequotation elementid="20011409w-dq8">
"  £ 18,500 "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20011409w-s43">
< roundbracket elementid="20011409w-rb27">
(  4 )
< /roundbracket>
Yn rheoliad 28
< roundbracket elementid="20011409w-rb28">
(  1 )
< /roundbracket>
< roundbracket elementid="20011409w-rb29">
(  cyfrifo incwm tariff o gyfalaf )
< /roundbracket>
yn lle 'r ffigur
< doublequotation elementid="20011409w-dq9">
"  £ 10,000 "
< /doublequotation>
, lle mae 'n ymddangos , rhoddir y ffigur
< doublequotation elementid="20011409w-dq10">
"  £ 11,500 "
< /doublequotation>
ac yn lle 'r ffigur
< doublequotation elementid="20011409w-dq11">
"  £ 16,000 "
< /doublequotation>
rhoddir y ffigur
< doublequotation elementid="20011409w-dq12">
"  £ 18,500 "
< /doublequotation> .
< /sentence>
< sentence type="heading" elementid="20011409w-s44">
< roundbracket elementid="20011409w-rb30">
(  5 )
< /roundbracket>
Yn Atodlen 4 i 'r prif Reoliadau , ar ôl paragraff 1 , mewnosodir y paragraff canlynol - 
< /sentence>
< /p>
< p elementid="20011409w-p20">
< sentence type="normal" elementid="20011409w-s45">
< english>
< strong elementid="20011409w-st5">
1A.
< /strong>
 - 
< roundbracket elementid="20011409w-rb31">
(  1 )
< /roundbracket>
In the case of a resident who becomes a permanent resident on or after 9 April 2001
< roundbracket elementid="20011409w-rb32">
(
< doublequotation elementid="20011409w-dq13">
" qualifying resident "
< /doublequotation>
)
< /roundbracket>
in respect of the first period of permanent residence the value of any dwelling which he would otherwise normally occupy as his only or main residence
< roundbracket elementid="20011409w-rb33">
(
< doublequotation elementid="20011409w-dq14">
"  his home "
< /doublequotation>
)
< /roundbracket>
for a period of 12 weeks beginning with the day on which the first period of residence begins .
< /english>
< /sentence>
< sentence type="heading" elementid="20011409w-s46">
< english>
< roundbracket elementid="20011409w-rb34">
(  2 )
< /roundbracket>
In the case of a qualifying resident
< /english>
< /sentence>
< sentence type="heading" elementid="20011409w-s47">
< english>
< roundbracket elementid="20011409w-rb35">
(  a )
< /roundbracket>
who ceases to be a permanent resident , and
< /english>
< /sentence>
< sentence type="heading" elementid="20011409w-s48">
< english>
< roundbracket elementid="20011409w-rb36">
(  b )
< /roundbracket>
who subsequently becomes a permanent resident again at any time within the period of 52 weeks from the end of the first period of permanent residence,
< /english>
< /sentence>
< /p>
< p elementid="20011409w-p21">
< sentence type="normal" elementid="20011409w-s49">
< english>
the value of his home for such period
< roundbracket elementid="20011409w-rb37">
(  if any )
< /roundbracket>
which when added to the period disregarded under sub-paragraph
< roundbracket elementid="20011409w-rb38">
(  1 )
< /roundbracket>
in respect of his first period of permanent residence does not exceed 12 weeks in total .
< /english>
< /sentence>
< sentence type="heading" elementid="20011409w-s50">
< english>
< roundbracket elementid="20011409w-rb39">
(  3 )
< /roundbracket>
In the case of a qualifying resident
< /english>
< /sentence>
< /p>
< p elementid="20011409w-p22">
< sentence type="heading" elementid="20011409w-s51">
< english>
< roundbracket elementid="20011409w-rb40">
(  a )
< /roundbracket>
who ceases to be a permanent resident and is not a person to whom sub-paragraph
< roundbracket elementid="20011409w-rb41">
(  2 )
< /roundbracket>
has applied , and
< /english>
< /sentence>
< sentence type="heading" elementid="20011409w-s52">
< english>
< roundbracket elementid="20011409w-rb42">
(  b )
< /roundbracket>
who subsequently becomes a permanent resident again at any time after a period of more than 52 weeks from the end of the first period of residence,
< /english>
< /sentence>
< /p>
< p elementid="20011409w-p23">
< sentence type="normal" elementid="20011409w-s53">
< english>
the value of his home for a period of 12 weeks beginning with the day on which the second period of permanent residence begins .
< /english>
< /sentence>
< sentence type="normal" elementid="20011409w-s54">
< english>
< roundbracket elementid="20011409w-rb43">
(  4 )
< /roundbracket>
In this paragraph
< doublequotation elementid="20011409w-dq15">
"  the first period of permanent residence "
< /doublequotation>
means the period of permanent residence beginning on or after 9th April 2001 and
< doublequotation elementid="20011409w-dq16">
"  the second period of permanent residence "
< /doublequotation>
means the period of permanent residence beginning at anytime after the period of 52 weeks referred to in subparagraph
< roundbracket elementid="20011409w-rb44">
(  3 )
< /roundbracket>
< roundbracket elementid="20011409w-rb45">
(  b )
< /roundbracket> .
< /english>
< /sentence>
< /p>
< p elementid="20011409w-p24">
< sentence type="heading" elementid="20011409w-s55">
< roundbracket elementid="20011409w-rb46">
(  6 )
< /roundbracket>
Yn yr Atodlen 4 a enwyd , ar ôl paragraff 20 , mewnosodir y paragraff canlynol - 
< /sentence>
< /p>
< p elementid="20011409w-p25">
< sentence type="normal" elementid="20011409w-s56">
21 .
< /sentence>
< sentence type="normal" elementid="20011409w-s57">
< english>
Any payment which would be disregarded under paragraph 64 of Schedule 10 to the Income Support Regulations
< roundbracket elementid="20011409w-rb47">
(  payments under a trust established out of funds provided by the Secretary of State in respect of persons who suffered or are suffering from variant Creutzfeld-Jacob disease )
< /roundbracket> .
< /english>
< /sentence>
< sentence type="normal" elementid="20011409w-s58">
.
< /sentence>
< /p>
< p elementid="20011409w-p26">
< sentence type="heading" elementid="20011409w-s59">
Llofnodwyd ar ran Cynulliad Cenedlaethol Cymru o dan adran 66
< roundbracket elementid="20011409w-rb48">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20011409w-sb4">
[ 
< a type="href" elementid="20011409w-a7">
4
< /a>
]
< /squarebracket>
< /sentence>
< sentence type="heading" elementid="20011409w-s60">
< em elementid="20011409w-em8">
Dafydd Elis Thomas
< /em>
< /sentence>
< sentence type="heading" elementid="20011409w-s61">
Llywydd y Cynulliad Cenedlaethol
< /sentence>
< sentence type="heading" elementid="20011409w-s62">
4 Ebrill 2001
< /sentence>
< /p>
< p elementid="20011409w-p27">
< sentence type="heading" elementid="20011409w-s63">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20011409w-s64">
< em elementid="20011409w-em9">
< roundbracket elementid="20011409w-rb49">
(  Nid yw 'r nodyn hwn yn rhan o 'r Rheoliadau )
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20011409w-s65">
Mae 'r Rheoliadau hyn yn gwneud diwygiadau pellach i Reoliadau Cymorth Gwladol
< roundbracket elementid="20011409w-rb50">
(  Asesu Adnoddau )
< /roundbracket>
1992
< roundbracket elementid="20011409w-rb51">
(
< doublequotation elementid="20011409w-dq17">
"  y prif Reoliadau "
< /doublequotation>
)
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20011409w-s66">
Mae 'r prif Reoliadau yn ymwneud ag asesu gallu person
< roundbracket elementid="20011409w-rb52">
(
< doublequotation elementid="20011409w-dq18">
"  y preswylydd "
< /doublequotation>
)
< /roundbracket>
i dalu am lety sydd wedi 'i drefnu gan awdurdodau lleol o dan Ran III o Ddeddf Cymorth Gwladol 1948 .
< /sentence>
< sentence type="normal" elementid="20011409w-s67">
Mae llety Rhan III yn cael ei drefnu ar gyfer personau 18 oed neu drosodd y mae arnynt , oherwydd oedran , salwch , anabledd neu unrhyw amgylchiadau eraill , angen gofal a sylw nad ydynt ar gael iddynt fel arall , ac ar gyfer mamau sy 'n disgwyl plentyn a mamau sy 'n magu ac sydd mewn angen tebyg .
< /sentence>
< sentence type="normal" elementid="20011409w-s68">
Mae 'r prif Reoliadau yn darparu bod rhaid peidio ag asesu unrhyw breswylydd fel un sy 'n methu â thalu am lety Rhan III yn ôl y gyfradd safonol os yw cyfalaf y preswylydd hwnnw , o 'i gyfrifo yn unol â 'r prif Reoliadau , yn fwy nag £ 16,000 .
< /sentence>
< sentence type="normal" elementid="20011409w-s69">
Mae 'r Rheoliadau hyn yn diwygio 'r prif Reoliadau er mwyn cynyddu 'r terfyn cyfalaf o £ 16,000 i £ 18,500 .
< /sentence>
< sentence type="normal" elementid="20011409w-s70">
Mae 'r prif Reoliadau yn darparu hefyd ar gyfer cyfrifo incwm preswylydd i gymryd i ystyriaeth gyfalaf sy 'n cael ei drin fel swm sy 'n cyfateb i incwm .
< /sentence>
< sentence type="normal" elementid="20011409w-s71">
Mae 'r Rheoliadau hyn yn diwygio terfynau uchaf ac isaf cyfalaf o 'r fath fel bod pob £ 250 cyflawn rhyngddynt , neu unrhyw ran ohono nad yw 'n £ 250 cyflawn , yn cael ei drin fel swm sy 'n cyfateb i incwm wythnosol o £ 1 .
< /sentence>
< sentence type="normal" elementid="20011409w-s72">
Mae 'r Rheoliadau yn cyflwyno categorïau pellach o gyfalaf hefyd sydd i 'w hanwybyddu wrth asesu adnoddau preswylydd .
< /sentence>
< sentence type="normal" elementid="20011409w-s73">
Mae gwerth unrhyw annedd a fyddai fel arall yn cael ei feddiannu gan y preswylydd fel ei unig neu brif breswylfa i 'w anwybyddu am gyfnod o 12 wythnos o dan yr amgylchiadau a bennir yn y Rheoliadau .
< /sentence>
< sentence type="normal" elementid="20011409w-s74">
Yn ychwanegol , mae 'r Rheoliadau yn darparu ar gyfer anwybyddu taliadau sy 'n cael eu gwneud o dan ymddiriedolaeth a sefydlwyd â chyllid a ddarparwyd gan yr Ysgrifennydd Gwladol dros Iechyd mewn perthynas â phersonau a ddioddefodd neu sy 'n dioddef gan glefyd amrywiadol Creutzfeldt-Jakob .
< /sentence>
< sentence type="heading" elementid="20011409w-s75">
< em elementid="20011409w-em10">
< english>
Notes
< /english>
:
< /em>
< /sentence>
< sentence type="normal" elementid="20011409w-s76">
< squarebracket elementid="20011409w-sb5">
[  1 ]
< /squarebracket>
1948 p.29 ; diwygiwyd adran 22
< roundbracket elementid="20011409w-rb53">
(  5 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 gan adran 39
< roundbracket elementid="20011409w-rb54">
(  1 )
< /roundbracket>
o Ddeddf y Weinyddiaeth Nawdd Cymdeithasol 1966
< roundbracket elementid="20011409w-rb55">
(  p.20 )
< /roundbracket>
a pharagraff 6 o Atodlen 6 iddi , gan adran 35
< roundbracket elementid="20011409w-rb56">
(  2 )
< /roundbracket>
o Ddeddf Budd-daliadau Atodol 1976
< roundbracket elementid="20011409w-rb57">
(  p.71 )
< /roundbracket>
a pharagraff 3
< roundbracket elementid="20011409w-rb58">
(  b )
< /roundbracket>
o Atodlen 7 iddi , gan adran 20 o Ddeddf Nawdd Cymdeithasol 1986
< roundbracket elementid="20011409w-rb59">
(  p.50 )
< /roundbracket>
a pharagraff 2 o Atodlen 4 iddi .
< /sentence>
< sentence type="heading" elementid="20011409w-s77">
< a type="href" elementid="20011409w-a8">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20011409w-p28">
< sentence type="normal" elementid="20011409w-s78">
< squarebracket elementid="20011409w-sb6">
[  2 ]
< /squarebracket>
Cafodd swyddogaethau 'r Ysgrifennydd Gwladol o dan adran 22
< roundbracket elementid="20011409w-rb60">
(  5 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 eu trosglwyddo i Gynulliad Cenedlaethol Cymru gan Orchymyn Cynulliad Cenedlaethol Cymru
< roundbracket elementid="20011409w-rb61">
(  Trosglwyddo Swyddogaethau )
< /roundbracket>
1999
< roundbracket elementid="20011409w-rb62">
(  O.S. 1999/ 672 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20011409w-s79">
< a type="href" elementid="20011409w-a9">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20011409w-p29">
< sentence type="normal" elementid="20011409w-s80">
< squarebracket elementid="20011409w-sb7">
[  3 ]
< /squarebracket>
O.S. 1992/ 2977 fel y 'i diwygiwyd gan O.S. 1993/ 964 , 1993/ 2230 , 1994/ 825 , 1994/ 2386 , 1995/ 858 , 1995/ 3054 , 1996/ 602 , 1997/ 485 , 1998/ 497 , 1998/ 1730 , 2001/ 276
< roundbracket elementid="20011409w-rb63">
(  Cy.12 )
< /roundbracket>
, ac , mewn perthynas â Lloegr yn unig , gan O.S. 2001/ 58 .
< /sentence>
< sentence type="heading" elementid="20011409w-s81">
< a type="href" elementid="20011409w-a10">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20011409w-p30">
< sentence type="normal" elementid="20011409w-s82">
< squarebracket elementid="20011409w-sb8">
[  4 ]
< /squarebracket>
1998 p.38 .
< /sentence>
< sentence type="heading" elementid="20011409w-s83">
< a type="href" elementid="20011409w-a11">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20011409w-p31">
< sentence type="heading" elementid="20011409w-s84">
< a type="href" elementid="20011409w-a12">
< english>
English version
< /english>
< /a>
< /sentence>
< sentence type="heading" elementid="20011409w-s85">
ISBN 0-11-090221-1
< /sentence>
< /p>
< table elementid="20011409w-tb2">
< td elementid="20011409w-td8">
< p elementid="20011409w-p32">
< sentence type="heading" elementid="20011409w-s86">
< a type="href" elementid="20011409w-a13">
< english>
Other UK SIs
< /english>
< /a>
< a type="href" elementid="20011409w-a14">
< english>
Home
< /english>
< /a>
< a type="href" elementid="20011409w-a15">
< english>
National Assembly for Wales Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20011409w-a16">
< english>
Scottish Statutory Instrument
< /english>
< /a>
< a type="href" elementid="20011409w-a17">
< english>
Statutory Rules of Northern Ireland
< /english>
< /a>
< a type="href" elementid="20011409w-a18">
< english>
Her Majesty 's Stationery Office
< /english>
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20011409w-td9">
< p elementid="20011409w-p33">
< sentence type="heading" elementid="20011409w-s87">
< english>
< strong elementid="20011409w-st6">
We welcome your 
< a type="href" elementid="20011409w-a19">
comments
< /a>
on this site
< /strong>
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20011409w-td10">
< p elementid="20011409w-p34">
< sentence type="heading" elementid="20011409w-s88">
< english>
© Crown copyright 2001
< english>
< /sentence>
< /p>
< /td>
< td elementid="20011409w-td11">
< p elementid="20011409w-p35">
< sentence type="heading" elementid="20011409w-s89">
< english>
Prepared 8 June 2001
< /english>
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>1488< /wordcount>
< /file>
< file elementid="20040103w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Gorchymyn Deddf Iechyd a Gofal Cymdeithasol 2001 ( Cychwyn Rhif 6 ) ( Cymru ) 2004< /pagetitle>
< doc_class>Statutory Instrument - Wales< /doc_class>
< year>2004< /year>
< number>0103< /number>
< legislation_code>30102< /legislation_code>
< legislation_type>Statutory Instrument - Wales< /legislation_type>
< subject>GWASANAETHAU CYMORTH GWLADOL, CYMRU^Y GWASANAETH IECHYD GWLADOL, CYMRU< /subject>
< laid_date>_EMPTY< /laid_date>
< made_date>20040120< /made_date>
< coming_into_force>_EMPTY< /coming_into_force>
< isbn>0110908597< /isbn>
< title>Gorchymyn Deddf Iechyd a Gofal Cymdeithasol 2001 (Cychwyn Rhif 6) (Cymru) 2004< /title>
< signatory>D. Elis-Thomas< /signatory>
< signatory_title>Llywydd y Cynulliad Cenedlaethol< /signatory_title>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20040103w-p1">
< sentence type="heading" elementid="20040103w-s1">
Offerynnau Statudol 2004 Rhif 103
< roundbracket elementid="20040103w-rb1">
(  Cy.10 )
< /roundbracket>
< roundbracket elementid="20040103w-rb2">
(  C.6 )
< /roundbracket>
< /sentence>
< /p>
< p elementid="20040103w-p2">
< sentence type="heading" elementid="20040103w-s2">
< strong elementid="20040103w-st1">
Gorchymyn Deddf Iechyd a Gofal Cymdeithasol 2001
< roundbracket elementid="20040103w-rb3">
(  Cychwyn Rhif 6 )
< /roundbracket>
< roundbracket elementid="20040103w-rb4">
(  Cymru )
< /roundbracket>
2004
< /strong>
< /sentence>
< /p>
< p elementid="20040103w-p3">
< sentence type="heading" elementid="20040103w-s3">
© Hawlfraint y Goron 2004
< /sentence>
< /p>
< p elementid="20040103w-p4">
< sentence type="normal" elementid="20040103w-s4">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20040103w-p5">
< sentence type="normal" elementid="20040103w-s5">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20040103w-s6">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20040103w-p6">
< sentence type="normal" elementid="20040103w-s7">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20040103w-s8">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20040103w-s9">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20040103w-p7">
< sentence type="normal" elementid="20040103w-s10">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20040103w-s11">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan
< english>
The Stationery Office Limited
< /english>
fel
< strong elementid="20040103w-st2">
Gorchymyn Deddf Iechyd a Gofal Cymdeithasol 2001
< roundbracket elementid="20040103w-rb5">
(  Cychwyn Rhif 6 )
< /roundbracket>
< roundbracket elementid="20040103w-rb6">
(  Cymru )
< /roundbracket>
2004
< /strong>
, ISBN 0110908597 .
< /sentence>
< sentence type="normal" elementid="20040103w-s12">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20040103w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20040103w-s13">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20040103w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20040103w-p8">
< sentence type="normal" elementid="20040103w-s14">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20040103w-a3">
< english>
Frequently Asked Questions
< /english>
< /a> .
< /sentence>
< /p>
< p elementid="20040103w-p9">
< sentence type="normal" elementid="20040103w-s15">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20040103w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20040103w-s16">
Pan welwch fotwm
< doublequotation elementid="20040103w-dq2">
"
< english>
continue
< /continue>
"
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20040103w-p10">
< sentence type="heading" elementid="20040103w-s17">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="heading" elementid="20040103w-s18">
< strong elementid="20040103w-st3">
2004 Rhif 103
< roundbracket elementid="20040103w-rb7">
(  Cy.10 )
< /roundbracket>
< roundbracket elementid="20040103w-rb8">
(  C.6 )
< /roundbracket>
< /strong>
< /sentence>
< sentence type="heading" elementid="20040103w-s19">
< strong elementid="20040103w-st4">
GWASANAETHAU CYMORTH GWLADOL , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20040103w-s20">
< strong elementid="20040103w-st5">
Y GWASANAETH IECHYD GWLADOL , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20040103w-s21">
Gorchymyn Deddf Iechyd a Gofal Cymdeithasol 2001
< roundbracket elementid="20040103w-rb9">
(  Cychwyn Rhif 6 )
< /roundbracket>
< roundbracket elementid="20040103w-rb10">
(  Cymru )
< /roundbracket>
2004
< /sentence>
< /p>
< table elementid="20040103w-tb1">
< td elementid="20040103w-td1">
< p elementid="20040103w-p11">
< sentence type="heading" elementid="20040103w-s22">
< em elementid="20040103w-em1">
Wedi 'i wneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td2">
< p elementid="20040103w-p12">
< sentence type="heading" elementid="20040103w-s23">
< em elementid="20040103w-em2">
20 Ionawr 2004
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20040103w-p13">
< sentence type="heading" elementid="20040103w-s24">
Mae Cynulliad Cenedlaethol Cymru drwy arfer y pwerau a roddwyd iddo gan adrannau 64
< roundbracket elementid="20040103w-rb11">
(  6 )
< /roundbracket>
a 70
< roundbracket elementid="20040103w-rb12">
(  2 )
< /roundbracket>
o Ddeddf Iechyd a Gofal Cymdeithasol 2001
< squarebracket elementid="20040103w-sb1">
[ 
< a type="href" elementid="20040103w-a4">
1
< /a>
]
< /squarebracket>
, drwy hyn yn gwneud y Gorchymyn canlynol :
< /sentence>
< sentence type="heading" elementid="20040103w-s25">
< strong elementid="20040103w-st6">
Enwi , dehongli a chymhwyso
< /strong>
< /sentence>
< sentence type="normal" elementid="20040103w-s26">
1 .
< /sentence>
< sentence type="normal" elementid="20040103w-s27">
 - 
< roundbracket elementid="20040103w-rb13">
(  1 )
< /roundbracket>
Enw 'r Gorchymyn hwn yw Gorchymyn Deddf Iechyd a Gofal Cymdeithasol 2001
< roundbracket elementid="20040103w-rb14">
(  Cychwyn Rhif 6 )
< /roundbracket>
< roundbracket elementid="20040103w-rb15">
(  Cymru )
< /roundbracket>
2004 .
< /sentence>
< sentence type="normal" elementid="20040103w-s28">
< roundbracket elementid="20040103w-rb16">
(  2 )
< /roundbracket>
Yn Gorchymyn hwn , cyfeirir at Ddeddf Iechyd a Gofal Cymdeithasol 2001 fel
< doublequotation elementid="20040103w-dq3">
"  y Ddeddf "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20040103w-s29">
< roundbracket elementid="20040103w-rb17">
(  3 )
< /roundbracket>
Mae 'r Gorchymyn hwn yn gymwys i Gymru yn unig .
< /sentence>
< sentence type="heading" elementid="20040103w-s30">
< strong elementid="20040103w-st7">
Y diwrnod penodedig mewn perthynas ag adran 6
< roundbracket elementid="20040103w-rb18">
(  3 )
< /roundbracket>
o 'r Ddeddf
< /strong>
< /sentence>
< sentence type="normal" elementid="20040103w-s31">
2 .
< /sentence>
< sentence type="normal" elementid="20040103w-s32">
24 Ionawr 2004 yw 'r diwrnod penodedig i adran 6
< roundbracket elementid="20040103w-rb19">
(  3 )
< /roundbracket>
o 'r Ddeddf ddod i rym .
< /sentence>
< sentence type="heading" elementid="20040103w-s33">
< strong elementid="20040103w-st8">
Y diwrnod penodedig mewn perthynas ag adran 12 o 'r Ddeddf
< /strong>
< /sentence>
< sentence type="normal" elementid="20040103w-s34">
3 .
< /sentence>
< sentence type="normal" elementid="20040103w-s35">
31 Ionawr 2004 yw 'r diwrnod penodedig i adran 12 o 'r Ddeddf ddod i rym .
< /sentence>
< sentence type="heading" elementid="20040103w-s36">
< strong elementid="20040103w-st9">
Y diwrnod penodedig mewn perthynas ag adran 49 o 'r Ddeddf
< /strong>
< /sentence>
< sentence type="normal" elementid="20040103w-s37">
4 .
< /sentence>
< sentence type="normal" elementid="20040103w-s38">
1 Ebrill 2004 yw 'r diwrnod penodedig i adran 49 o 'r Ddeddf ddod i rym i 'r graddau na chafodd yr adran honno ei dwyn i rym eisoes .
< /sentence>
< sentence type="normal" elementid="20040103w-s39">
Llofnodwyd ar ran Cynulliad Cenedlaethol Cymru o dan adran 66
< roundbracket elementid="20040103w-rb20">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20040103w-sb2">
[ 
< a type="href" elementid="20040103w-a5">
2
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="heading" elementid="20040103w-s40">
< em elementid="20040103w-em3">
D. Elis-Thomas
< /em>
< /sentence>
< sentence type="heading" elementid="20040103w-s41">
Llywydd y Cynulliad Cenedlaethol
< /sentence>
< sentence type="heading" elementid="20040103w-s42">
20 Ionawr 2004
< /sentence>
< sentence type="heading" elementid="20040103w-s43">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20040103w-s44">
< em elementid="20040103w-em4">
< roundbracket elementid="20040103w-rb21">
(  Nid yw 'r nodyn hwn yn rhan o 'r Gorchymyn )
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20040103w-s45">
Dyma 'r chweched Gorchymyn Cychwyn a wnaed o dan Ddeddf Iechyd a Gofal Cymdeithasol 2001
< roundbracket elementid="20040103w-rb22">
(
< doublequotation elementid="20040103w-dq4">
"  y Ddeddf "
< /doublequotation>
)
< /roundbracket>
mewn perthynas â Chymru .
< /sentence>
< sentence type="normal" elementid="20040103w-s46">
Mae erthygl 2 yn pennu 24 Ionawr 2004 fel y diwrnod y daw adran 6
< roundbracket elementid="20040103w-rb23">
(  3 )
< /roundbracket>
o 'r Ddeddf i rym yng Nghymru .
< /sentence>
< sentence type="normal" elementid="20040103w-s47">
Mae erthygl 3 yn pennu 31 Ionawr 2004 fel y diwrnod y daw adran 12 o 'r Ddeddf i rym yng Nghymru .
< /sentence>
< sentence type="normal" elementid="20040103w-s48">
Mae erthygl 4 yn pennu 1 Ebrill 2004 fel y diwrnod y daw adran 49 o 'r Ddeddf i rym yng Nghymru , i 'r graddau nad yw 'r adran honno eisoes mewn grym .
< /sentence>
< sentence type="normal" elementid="20040103w-s49">
Mae adran 6
< roundbracket elementid="20040103w-rb24">
(  3 )
< /roundbracket>
yn mewnosod diwygiadau ym mharagraff 16 o Atodlen 2 i Ddeddf y Gwasanaeth Iechyd Gwladol a Gofal Cymunedol 1990 sy 'n galluogi Cynulliad Cenedlaethol Cymru i wneud rheoliadau a rhoi cyfarwyddiadau i Ymddiriedolaethau GIG yng Nghymru ynghylch tâl a thelerau ac amodau eraill y maent yn cyflogi staff odanynt ac yn gyffredinol mewn cysylltiad â materion ynghylch cyflogi staff .
< /sentence>
< sentence type="normal" elementid="20040103w-s50">
Mae adran 12 yn mewnosod adran 19A newydd yn Neddf y Gwasanaeth Iechyd Gwladol 1977 .
< /sentence>
< sentence type="normal" elementid="20040103w-s51">
Mae 'r ddarpariaeth newydd hon yn ei gwneud yn ofynnol i 'r Cynulliad Cenedlaethol drefnu , i 'r graddau y mae 'n ystyried sy 'n angenrheidiol i fodloni holl ofynion rhesymol , i ddarparu gwasanaethau eirioli annibynnol ar gyfer pobl sy 'n dymuno cwyno am y gwasanaeth y maent
< roundbracket elementid="20040103w-rb25">
(  neu y mae rhywun y maent yn gofalu amdano )
< /roundbracket>
wedi ei gael oddi wrth y GIG.
< /sentence>
< sentence type="normal" elementid="20040103w-s52">
Mae adran 49 yn eithrio gofal nyrsio gan nyrs gofrestredig o 'r gwasanaethau y gall yr awdurdodau lleol eu darparu yn unol â deddfiadau sy 'n ymwneud â darparu gwasanaethau gofal cymunedol .
< /sentence>
< sentence type="heading" elementid="20040103w-s53">
< strong elementid="20040103w-st10">
NODYN AM ORCHMYNION CYCHWYN BLAENOROL
< /strong>
< /sentence>
< sentence type="heading" elementid="20040103w-s54">
< em elementid="20040103w-em5">
< roundbracket elementid="20040103w-rb26">
(  Nid yw 'r nodyn hwn yn rhan o 'r Gorchymyn )
< /roundbracket>
< /em>
< /sentence>
< /p>
< table elementid="20040103w-tb2">
< td elementid="20040103w-td3">
< p elementid="20040103w-p14">
< sentence type="heading" elementid="20040103w-s55">
Y ddarpariaeth
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td4">
< p elementid="20040103w-p15">
< sentence type="heading" elementid="20040103w-s56">
Dyddiad Cychwyn
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td5">
< p elementid="20040103w-p16">
< sentence type="heading" elementid="20040103w-s57">
O.S. Rhif
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td6">
< p elementid="20040103w-p17">
< sentence type="heading" elementid="20040103w-s58">
Adran 3
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td7">
< p elementid="20040103w-p18">
< sentence type="heading" elementid="20040103w-s59">
1 Gorffennaf 2002
< roundbracket elementid="20040103w-rb27">
(  yn rhannol )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td8">
< p elementid="20040103w-p19">
< sentence type="heading" elementid="20040103w-s60">
2002/ 1475
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td9">
< p elementid="20040103w-p20">
< sentence type="heading" elementid="20040103w-s61">
Adran 5
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td10">
< p elementid="20040103w-p21">
< sentence type="heading" elementid="20040103w-s62">
1 Gorffennaf 2002
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td11">
< p elementid="20040103w-p22">
< sentence type="heading" elementid="20040103w-s63">
2002/ 1475
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td12">
< p elementid="20040103w-p23">
< sentence type="heading" elementid="20040103w-s64">
Adran 11
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td13">
< p elementid="20040103w-p24">
< sentence type="heading" elementid="20040103w-s65">
1 Rhagfyr 2002
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td14">
< p elementid="20040103w-p25">
< sentence type="heading" elementid="20040103w-s66">
2002/ 1475
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td15">
< p elementid="20040103w-p26">
< sentence type="heading" elementid="20040103w-s67">
Adran 13
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td16">
< p elementid="20040103w-p27">
< sentence type="heading" elementid="20040103w-s68">
17 Mawrth 2003
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td17">
< p elementid="20040103w-p28">
< sentence type="heading" elementid="20040103w-s69">
2003/ 713
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td18">
< p elementid="20040103w-p29">
< sentence type="heading" elementid="20040103w-s70">
Adran 16
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td19">
< p elementid="20040103w-p30">
< sentence type="heading" elementid="20040103w-s71">
26 Awst 2002
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td20">
< p elementid="20040103w-p31">
< sentence type="heading" elementid="20040103w-s72">
2002/ 1919
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td21">
< p elementid="20040103w-p32">
< sentence type="heading" elementid="20040103w-s73">
Adran 19  -  26
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td22">
< p elementid="20040103w-p33">
< sentence type="heading" elementid="20040103w-s74">
1 Gorffennaf 2002
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td23">
< p elementid="20040103w-p34">
< sentence type="heading" elementid="20040103w-s75">
2002/ 1475
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td24">
< p elementid="20040103w-p35">
< sentence type="heading" elementid="20040103w-s76">
Adran 27
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td25">
< p elementid="20040103w-p36">
< sentence type="heading" elementid="20040103w-s77">
26 Awst 2002
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td26">
< p elementid="20040103w-p37">
< sentence type="heading" elementid="20040103w-s78">
2002/ 1919
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td27">
< p elementid="20040103w-p38">
< sentence type="heading" elementid="20040103w-s79">
Adran 28  -  39
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td28">
< p elementid="20040103w-p39">
< sentence type="heading" elementid="20040103w-s80">
1 Gorffennaf 2002
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td29">
< p elementid="20040103w-p40">
< sentence type="heading" elementid="20040103w-s81">
2002/ 1475
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td30">
< p elementid="20040103w-p41">
< sentence type="heading" elementid="20040103w-s82">
Adran 41  -  43
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td31">
< p elementid="20040103w-p42">
< sentence type="heading" elementid="20040103w-s83">
1 Gorffennaf 2002
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td32">
< p elementid="20040103w-p43">
< sentence type="heading" elementid="20040103w-s84">
2002/ 1475
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td33">
< p elementid="20040103w-p44">
< sentence type="heading" elementid="20040103w-s85">
Adran 49
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td34">
< p elementid="20040103w-p45">
< sentence type="heading" elementid="20040103w-s86">
3 Rhagfyr 2001
< roundbracket elementid="20040103w-rb28">
(  yn rhannol )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td35">
< p elementid="20040103w-p46">
< sentence type="heading" elementid="20040103w-s87">
2001/ 3807
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td36">
< p elementid="20040103w-p47">
< sentence type="heading" elementid="20040103w-s88">
Adran 50
< roundbracket elementid="20040103w-rb29">
(  1 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td37">
< p elementid="20040103w-p48">
< sentence type="heading" elementid="20040103w-s89">
8 Ebrill 2002
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td38">
< p elementid="20040103w-p49">
< sentence type="heading" elementid="20040103w-s90">
2001/ 3752
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td39">
< p elementid="20040103w-p50">
< sentence type="heading" elementid="20040103w-s91">
Adran 50
< roundbracket elementid="20040103w-rb30">
(  2 )
< /roundbracket>
i
< roundbracket elementid="20040103w-rb31">
(  10 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td40">
< p elementid="20040103w-p51">
< sentence type="heading" elementid="20040103w-s92">
19 Rhagfyr 2001
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td41">
< p elementid="20040103w-p52">
< sentence type="heading" elementid="20040103w-s93">
2001/ 3807
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td42">
< p elementid="20040103w-p53">
< sentence type="heading" elementid="20040103w-s94">
Adran 51
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td43">
< p elementid="20040103w-p54">
< sentence type="heading" elementid="20040103w-s95">
8 Tachwedd 2001
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td44">
< p elementid="20040103w-p55">
< sentence type="heading" elementid="20040103w-s96">
2001/ 3752
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td45">
< p elementid="20040103w-p56">
< sentence type="heading" elementid="20040103w-s97">
Adran 52
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td46">
< p elementid="20040103w-p57">
< sentence type="heading" elementid="20040103w-s98">
8 Tachwedd 2001
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td47">
< p elementid="20040103w-p58">
< sentence type="heading" elementid="20040103w-s99">
2001/ 3752
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td48">
< p elementid="20040103w-p59">
< sentence type="heading" elementid="20040103w-s100">
Adran 53
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td49">
< p elementid="20040103w-p60">
< sentence type="heading" elementid="20040103w-s101">
1 Ebrill 2003
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td50">
< p elementid="20040103w-p61">
< sentence type="heading" elementid="20040103w-s102">
2003/ 939
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td51">
< p elementid="20040103w-p62">
< sentence type="heading" elementid="20040103w-s103">
Adran 54
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td52">
< p elementid="20040103w-p63">
< sentence type="heading" elementid="20040103w-s104">
1 Ebrill 2003
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td53">
< p elementid="20040103w-p64">
< sentence type="heading" elementid="20040103w-s105">
2003/ 939
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td54">
< p elementid="20040103w-p65">
< sentence type="heading" elementid="20040103w-s106">
Adran 55
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td55">
< p elementid="20040103w-p66">
< sentence type="heading" elementid="20040103w-s107">
1 Ebrill 2003
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td56">
< p elementid="20040103w-p67">
< sentence type="heading" elementid="20040103w-s108">
2003/ 939
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td57">
< p elementid="20040103w-p68">
< sentence type="heading" elementid="20040103w-s109">
Adran 67
< roundbracket elementid="20040103w-rb32">
(  1 )
< /roundbracket>
ac Atodlen 5 ;
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td58">
< p elementid="20040103w-p69">
< sentence type="heading" elementid="20040103w-s110">
1 Ebrill 2002 , 1 Gorffennaf 2002 a 26 Awst 2002
< roundbracket elementid="20040103w-rb33">
(  yn rhannol )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td59">
< p elementid="20040103w-p70">
< sentence type="normal" elementid="20040103w-s111">
2002/ 1095 , 2002/ 1475 a 2002/ 1919 .
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td60">
< p elementid="20040103w-p71">
< sentence type="heading" elementid="20040103w-s112">
Adran 67
< roundbracket elementid="20040103w-rb34">
(  2 )
< /roundbracket>
ac Atodlen 6 ;
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td61">
< p elementid="20040103w-p72">
< sentence type="heading" elementid="20040103w-s113">
1 Ebrill 2002 , 15 Ebrill 2002 , 1 Gorffennaf 2002 a 26 Awst 2002
< roundbracket elementid="20040103w-rb35">
(  yn rhannol )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td62">
< p elementid="20040103w-p73">
< sentence type="heading" elementid="20040103w-s114">
2002/ 1095 , 2002/ 1312 , 2002/ 1475 a 2002/ 1919
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td63">
< p elementid="20040103w-p74">
< sentence type="heading" elementid="20040103w-s115">
Atodlen 2
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td64">
< p elementid="20040103w-p75">
< sentence type="heading" elementid="20040103w-s116">
1 Gorffennaf 2002
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td65">
< p elementid="20040103w-p76">
< sentence type="heading" elementid="20040103w-s117">
2002/ 1475
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td66">
< p elementid="20040103w-p77">
< sentence type="heading" elementid="20040103w-s118">
Atodlen 3
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td67">
< p elementid="20040103w-p78">
< sentence type="heading" elementid="20040103w-s119">
1 Gorffennaf 2002
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td68">
< p elementid="20040103w-p79">
< sentence type="heading" elementid="20040103w-s120">
2002/ 1475
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20040103w-p80">
< sentence type="normal" elementid="20040103w-s121">
Mae amryw o ddarpariaethau 'r Ddeddf wedi 'u dwyn i rym mewn perthynas â Lloegr gan yr Offerynnau Statudol canlynol : O.S. 2001/ 2804
< roundbracket elementid="20040103w-rb36">
(  C.95 )
< /roundbracket>
; O.S. 2001/ 3167
< roundbracket elementid="20040103w-rb37">
(  C.101 )
< /roundbracket>
; O.S. 2001/ 3294
< roundbracket elementid="20040103w-rb38">
(  C.107 )
< /roundbracket>
; O.S. 2001/ 3619
< roundbracket elementid="20040103w-rb39">
(  C.117 )
< /roundbracket>
; O.S. 2001/ 3752
< roundbracket elementid="20040103w-rb40">
(  C.122 )
< /roundbracket>
; O.S. 2001/ 3738
< roundbracket elementid="20040103w-rb41">
(  C.121 )
< /roundbracket>
; O.S.2001/ 4149
< roundbracket elementid="20040103w-rb42">
(  C.133 )
< /roundbracket>
; O.S. 2002/ 1095
< roundbracket elementid="20040103w-rb43">
(  C.26 )
< /roundbracket>
; O.S. 2002/ 1312
< roundbracket elementid="20040103w-rb44">
(  C.36 )
< /roundbracket>
; O.S. 2002/ 2363
< roundbracket elementid="20040103w-rb45">
(  C.77 )
< /roundbracket>
; O.S. 2003/ 53
< roundbracket elementid="20040103w-rb46">
(  C.3 )
< /roundbracket>
; O.S. 2003/ 850
< roundbracket elementid="20040103w-rb47">
(  C.46 )
< /roundbracket>
ac O.S. 2003/ 2245
< roundbracket elementid="20040103w-rb48">
(  C.87 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20040103w-s122">
< em elementid="20040103w-em6">
< english>
Notes
< /english>
:
< /em>
< /sentence>
< sentence type="normal" elementid="20040103w-s123">
< squarebracket elementid="20040103w-sb3">
[  1 ]
< /squarebracket>
2001 p.15 ; y Cynulliad Cenedlaethol yw 'r awdurdod perthnasol ar gyfer gwneud y Rheoliadau hyn yn rhinwedd adran 66 o 'r Ddeddf .
< /sentence>
< sentence type="heading" elementid="20040103w-s124">
< a type="href" elementid="20040103w-a6">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20040103w-p81">
< sentence type="normal" elementid="20040103w-s125">
< squarebracket elementid="20040103w-sb4">
[  2 ]
< /squarebracket>
1998 p.38 .
< /sentence>
< sentence type="heading" elementid="20040103w-s126">
< a type="href" elementid="20040103w-a7">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20040103w-p82">
< sentence type="heading" elementid="20040103w-s127">
< a type="href" elementid="20040103w-a8">
< english>
English version
< /english>
< /a>
< /sentence>
< sentence type="heading" elementid="20040103w-s128">
ISBN 0 11090859 7
< /sentence>
< /p>
< table elementid="20040103w-tb3">
< td elementid="20040103w-td69">
< p elementid="20040103w-p83">
< sentence type="heading" elementid="20040103w-s129">
< a type="href" elementid="20040103w-a9">
< english>
Other UK SIs
< /english>
< /a>
< a type="href" elementid="20040103w-a10">
< english>
Home
< /english>
< /a>
< a type="href" elementid="20040103w-a11">
< english>
National Assembly for Wales Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20040103w-a12">
< english>
Scottish Statutory Instrument
< /english>
< /a>
< a type="href" elementid="20040103w-a13">
< english>
Statutory Rules of Northern Ireland
< /english>
< /a>
< a type="href" elementid="20040103w-a14">
< english>
Her Majesty 's Stationery Office
< /english>
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td70">
< p elementid="20040103w-p84">
< sentence type="heading" elementid="20040103w-s130">
< strong elementid="20040103w-st11">
< english>
We welcome your 
< a type="href" elementid="20040103w-a15">
comments
< /a>
on this site
< /strong>
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td71">
< p elementid="20040103w-p85">
< sentence type="heading" elementid="20040103w-s131">
< english>
© Crown copyright 2004
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20040103w-td72">
< p elementid="20040103w-p86">
< sentence type="heading" elementid="20040103w-s132">
< english>
Prepared 6 February 2004
< /english>
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>1198< /wordcount>
< /file>
< file elementid="20041023w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Rheoliadau Cymorth Gwladol ( Asesu Adnoddau ) ( Diwygio ) ( Cymru ) 2004< /pagetitle>
< doc_class>Statutory Instrument - Wales< /doc_class>
< year>2004< /year>
< number>1023< /number>
< legislation_code>30102< /legislation_code>
< legislation_type>Statutory Instrument - Wales< /legislation_type>
< subject>GWASANAETHAU CYMORTH GWLADOL, CYMRU< /subject>
< laid_date>_EMPTY< /laid_date>
< made_date>20040330< /made_date>
< coming_into_force>20040412< /coming_into_force>
< isbn>0110909216< /isbn>
< title>Rheoliadau Cymorth Gwladol (Asesu Adnoddau) (Diwygio) (Cymru) 2004< /title>
< signatory>D. Elis-Thomas< /signatory>
< signatory_title>Llywydd y Cynulliad Cenedlaethol< /signatory_title>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20041023w-p1">
< sentence type="heading" elementid="20041023w-s1">
Offerynnau Statudol 2004 Rhif 1023
< roundbracket elementid="20041023w-rb1">
(  Cy.120 )
< /roundbracket>
< /sentence>
< /p>
< p elementid="20041023w-p2">
< sentence type="heading" elementid="20041023w-s2">
< strong elementid="20041023w-st1">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20041023w-rb2">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20041023w-rb3">
(  Diwygio )
< /roundbracket>
< roundbracket elementid="20041023w-rb4">
(  Cymru )
< /roundbracket>
2004
< /strong>
< /sentence>
< /p>
< p elementid="20041023w-p3">
< sentence type="heading" elementid="20041023w-s3">
© Hawlfraint y Goron 2004
< /sentence>
< /p>
< p elementid="20041023w-p4">
< sentence type="normal" elementid="20041023w-s4">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20041023w-p5">
< sentence type="normal" elementid="20041023w-s5">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20041023w-s6">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20041023w-p6">
< sentence type="normal" elementid="20041023w-s7">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20041023w-s8">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20041023w-s9">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20041023w-p7">
< sentence type="normal" elementid="20041023w-s10">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20041023w-s11">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan
< english>
The Stationery Office Limited
< /english>
fel
< strong elementid="20041023w-st2">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20041023w-rb5">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20041023w-rb6">
(  Diwygio )
< /roundbracket>
< roundbracket elementid="20041023w-rb7">
(  Cymru )
< /roundbracket>
2004
< /strong>
, ISBN 0110909216 .
< /sentence>
< sentence type="normal" elementid="20041023w-s12">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20041023w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20041023w-s13">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20041023w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20041023w-p8">
< sentence type="normal" elementid="20041023w-s14">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20041023w-a3">
< english>
Frequently Asked Questions
< /english>
< /a> .
< /sentence>
< /p>
< p elementid="20041023w-p9">
< sentence type="normal" elementid="20041023w-s15">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20041023w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20041023w-s16">
Pan welwch fotwm
< doublequotation elementid="20041023w-dq2">
"
< english>
continue
< /continue>
"
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20041023w-p10">
< sentence type="heading" elementid="20041023w-s17">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="heading" elementid="20041023w-s18">
< strong elementid="20041023w-st3">
2004 Rhif 1023
< roundbracket elementid="20041023w-rb8">
(  Cy.120 )
< /roundbracket>
< /strong>
< /sentence>
< sentence type="heading" elementid="20041023w-s19">
< strong elementid="20041023w-st4">
GWASANAETHAU CYMORTH GWLADOL , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20041023w-s20">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20041023w-rb9">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20041023w-rb10">
(  Diwygio )
< /roundbracket>
< roundbracket elementid="20041023w-rb11">
(  Cymru )
< /roundbracket>
2004
< /sentence>
< /p>
< table elementid="20041023w-tb1">
< td elementid="20041023w-td1">
< p elementid="20041023w-p11">
< sentence type="heading" elementid="20041023w-s21">
< em elementid="20041023w-em1">
Wedi 'u gwneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20041023w-td2">
< p elementid="20041023w-p12">
< sentence type="heading" elementid="20041023w-s22">
< em elementid="20041023w-em2">
30 Mawrth 2004
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20041023w-td3">
< p elementid="20041023w-p13">
< sentence type="heading" elementid="20041023w-s23">
< em elementid="20041023w-em3">
Yn dod i rym
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20041023w-td4">
< p elementid="20041023w-p14">
< sentence type="heading" elementid="20041023w-s24">
< em elementid="20041023w-em4">
12 Ebrill 2004
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20041023w-p15">
< sentence type="normal" elementid="20041023w-s25">
Mae Cynulliad Cenedlaethol Cymru yn gwneud y Rheoliadau canlynol drwy arfer y pwerau a roddwyd i 'r Ysgrifennydd Gwladol gan adran 22
< roundbracket elementid="20041023w-rb12">
(  5 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948
< squarebracket elementid="20041023w-sb1">
[ 
< a type="href" elementid="20041023w-a4">
1
< /a>
]
< /squarebracket>
ac sydd wedi 'u breinio bellach yng Nghynulliad Cenedlaethol Cymru
< squarebracket elementid="20041023w-sb2">
[ 
< a type="href" elementid="20041023w-a5">
2
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="heading" elementid="20041023w-s26">
< strong elementid="20041023w-st5">
Enwi , cychwyn , dehongli a chymhwyso
< /strong>
< /sentence>
< sentence type="normal" elementid="20041023w-s27">
1 .
< /sentence>
< sentence type="normal" elementid="20041023w-s28">
 - 
< roundbracket elementid="20041023w-rb13">
(  1 )
< /roundbracket>
Enw 'r rheoliadau hyn yw Rheoliadau Cymorth Gwladol
< roundbracket elementid="20041023w-rb14">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20041023w-rb15">
(  Diwygio )
< /roundbracket>
< roundbracket elementid="20041023w-rb16">
(  Cymru )
< /roundbracket>
2004 a deuant i rym ar 12 Ebrill 2004 .
< /sentence>
< sentence type="normal" elementid="20041023w-s29">
< roundbracket elementid="20041023w-rb17">
(  2 )
< /roundbracket>
Yn y rheoliadau hyn ystyr
< doublequotation elementid="20041023w-dq3">
"  y Prif Reoliadau "
< /doublequotation>
< roundbracket elementid="20041023w-rb18">
(
< doublequotation elementid="20041023w-dq4">
"
< em elementid="20041023w-em5">
< english>
the Principal Regulations
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw Rheoliadau Cymorth Gwladol
< roundbracket elementid="20041023w-rb19">
(  Asesu Adnoddau )
< /roundbracket>
1992
< squarebracket elementid="20041023w-sb3">
[ 
< a type="href" elementid="20041023w-a6">
3
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="normal" elementid="20041023w-s30">
< roundbracket elementid="20041023w-rb20">
(  3 )
< /roundbracket>
Mae 'r Rheoliadau hyn yn gymwys i Gymru yn unig .
< /sentence>
< sentence type="heading" elementid="20041023w-s31">
Diwygio 'r Prif Reoliadau  -  Terfynau Cyfalaf
< /sentence>
< sentence type="normal" elementid="20041023w-s32">
2 .
< /sentence>
< sentence type="normal" elementid="20041023w-s33">
 - 
< roundbracket elementid="20041023w-rb21">
(  1 )
< /roundbracket>
Diwygir y Prif Reoliadau yn unol â pharagraffau canlynol y rheoliad hwn .
< /sentence>
< sentence type="normal" elementid="20041023w-s34">
< roundbracket elementid="20041023w-rb22">
(  2 )
< /roundbracket>
Yn rheoliad 20
< roundbracket elementid="20041023w-rb23">
(  Terfyn cyfalaf )
< /roundbracket>
, yn lle 'r ffigur
< doublequotation elementid="20041023w-dq5">
"  £ 20,000 "
< /doublequotation>
< squarebracket elementid="20041023w-sb4">
[ 
< a type="href" elementid="20041023w-a7">
4
< /a>
]
< /squarebracket>
, rhodder y ffigur
< doublequotation elementid="20041023w-dq6">
"  £ 20,500 "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20041023w-s35">
< roundbracket elementid="20041023w-rb24">
(  3 )
< /roundbracket>
Yn rheoliad 28
< roundbracket elementid="20041023w-rb25">
(  1 )
< /roundbracket>
< roundbracket elementid="20041023w-rb26">
(  Cyfrifo incwm tariff o 'r cyfalaf )
< /roundbracket>
, yn lle 'r ffigur
< doublequotation elementid="20041023w-dq7">
"  £ 12,250 "
< /doublequotation>
< squarebracket elementid="20041023w-sb5">
[ 
< a type="href" elementid="20041023w-a8">
5
< /a>
]
< /squarebracket>
, lle mae 'n ymddangos , rhodder y ffigur
< doublequotation elementid="20041023w-dq8">
"  £ 13,500 "
< /doublequotation>
ac yn lle 'r ffigur
< doublequotation elementid="20041023w-dq9">
"  £ 20,000 "
< /doublequotation>
< squarebracket elementid="20041023w-sb6">
[ 
< a type="href" elementid="20041023w-a9">
6
< /a>
]
< /squarebracket>
, rhodder y ffigur
< doublequotation elementid="20041023w-dq10">
"  £ 20,500 "
< /doublequotation> .
< /sentence>
< sentence type="heading" elementid="20041023w-s36">
Diwygio Atodlen 3 o 'r Prif Reoliadau  -  Symiau a anwybyddir wrth gyfrifo incwm heblaw enillion
< /sentence>
< sentence type="normal" elementid="20041023w-s37">
3 .
< /sentence>
< sentence type="heading" elementid="20041023w-s38">
Ym mharagraff 28H o Atodlen 3 o 'r Prif Reoliadau - 
< /sentence>
< sentence type="heading" elementid="20041023w-s39">
< roundbracket elementid="20041023w-rb27">
(  a )
< /roundbracket>
yn lle 'r ffigur
< doublequotation elementid="20041023w-dq11">
"  £ 4.50 "
< /doublequotation>
rhodder y ffigur
< doublequotation elementid="20041023w-dq12">
"  £ 4.75 "
< /doublequotation>
ble bynnnag y mae 'n ymddangos ;
< /sentence>
< sentence type="heading" elementid="20041023w-s40">
< roundbracket elementid="20041023w-rb28">
(  b )
< /roundbracket>
yn is-baragraffau
< roundbracket elementid="20041023w-rb29">
(  3 )
< /roundbracket>
a
< roundbracket elementid="20041023w-rb30">
(  4 )
< /roundbracket>
, yn lle 'r ffigur
< doublequotation elementid="20041023w-dq13">
"  £ 6.75 "
< /doublequotation>
rhodder y ffigur
< doublequotation elementid="20041023w-dq14">
"  £ 6.95 "
< /doublequotation>
ble bynnag y mae 'n ymddangos ;
< /sentence>
< sentence type="heading" elementid="20041023w-s41">
< roundbracket elementid="20041023w-rb31">
(  c )
< /roundbracket>
dileer is-baragraff
< roundbracket elementid="20041023w-rb32">
(  5 )
< /roundbracket>
ac yn ei le rhodder yr is-baragraff canlynol - 
< /sentence>
< sentence type="heading" elementid="20041023w-s42">
< english>
< roundbracket elementid="20041023w-rb33">
(  5 )
< /roundbracket>
Where,
< /english>
< /sentence>
< sentence type="heading" elementid="20041023w-s43">
< english>
< roundbracket elementid="20041023w-rb34">
(  a )
< /roundbracket>
the sum referred to in sub-paragraph
< roundbracket elementid="20041023w-rb35">
(  4 )
< /roundbracket>
has been disregarded in the assessment of the resident 's partner 's income under these Regulations , or,
< /english>
< /sentence>
< sentence type="heading" elementid="20041023w-s44">
< english>
< roundbracket elementid="20041023w-rb36">
(  b )
< /roundbracket>
the partner referred to in paragraph
< roundbracket elementid="20041023w-rb37">
(  4 )
< /roundbracket>
< roundbracket elementid="20041023w-rb38">
(  a )
< /roundbracket>
is in receipt of savings credit,
< /english>
< /sentence>
< /p>
< p elementid="20041023w-p16">
< sentence type="normal" elementid="20041023w-s45">
< english>
sub-paragraph
< roundbracket elementid="20041023w-rb39">
(  4 )
< /roundbracket>
does not apply to the resident .
< /english>
< /sentence>
< sentence type="normal" elementid="20041023w-s46">
.
< /sentence>
< /p>
< p elementid="20041023w-p17">
< sentence type="heading" elementid="20041023w-s47">
< strong elementid="20041023w-st6">
Dirymu
< /strong>
< /sentence>
< sentence type="normal" elementid="20041023w-s48">
Dirymir rheoliad 2 o Reoliadau Cymorth Gwladol
< roundbracket elementid="20041023w-rb40">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20041023w-rb41">
(  Diwygio )
< /roundbracket>
< roundbracket elementid="20041023w-rb42">
(  Cymru )
< /roundbracket>
2003
< squarebracket elementid="20041023w-sb7">
[ 
< a type="href" elementid="20041023w-a10">
7
< /a>
]
< /squarebracket>
) .
< /sentence>
< sentence type="normal" elementid="20041023w-s49">
Llofnodwyd ar ran Cynulliad Cenedlaethol Cymru o dan adran 66
< roundbracket elementid="20041023w-rb43">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20041023w-sb8">
[ 
< a type="href" elementid="20041023w-a11">
8
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="heading" elementid="20041023w-s50">
< em elementid="20041023w-em6">
D. Elis-Thomas
< /em>
< /sentence>
< sentence type="heading" elementid="20041023w-s51">
Llywydd y Cynulliad Cenedlaethol
< /sentence>
< sentence type="heading" elementid="20041023w-s52">
30 Mawrth 2004
< /sentence>
< /p>
< p elementid="20041023w-p18">
< sentence type="heading" elementid="20041023w-s53">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20041023w-s54">
< em elementid="20041023w-em7">
< roundbracket elementid="20041023w-rb44">
(  Nid yw 'r nodyn hwn yn rhan o 'r Rheoliadau )
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20041023w-s55">
Mae 'r rheoliadau hyn yn gwneud diwygiadau pellach i Reoliadau Cymorth Gwladol
< roundbracket elementid="20041023w-rb45">
(  Asesu Adnoddau )
< /roundbracket>
1992
< roundbracket elementid="20041023w-rb46">
(
< doublequotation elementid="20041023w-dq15">
"  y Prif Reoliadau "
< /doublequotation>
)
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20041023w-s56">
Mae 'r Prif Reoliadau yn ymwneud ag asesu gallu person
< roundbracket elementid="20041023w-rb47">
(
< doublequotation elementid="20041023w-dq16">
"  y preswylydd "
< /doublequotation>
)
< /roundbracket>
i dalu am y llety a drefnir gan awdurdodau lleol o dan Ran III o Ddeddf Cymorth Gwladol 1948 .
< /sentence>
< sentence type="normal" elementid="20041023w-s57">
Trefnir llety o dan Ran III i bobl 18 oed neu drosodd sydd , oblegid eu hoedran , afiechyd , anabledd neu unrhyw amgylchiadau eraill , mewn angen gofal a sylw nad ydynt fel arall ar gael iddynt , ac i famau sy 'n disgwyl neu 'n sy 'n magu ac sydd mewn angen tebyg .
< /sentence>
< sentence type="normal" elementid="20041023w-s58">
Mae 'r Prif Reoliadau yn darparu bod rhaid asesu preswylydd fel un sy 'n gallu talu am lety Rhan III yn ôl y gyfradd safonol os yw cyfalaf y preswylydd hwnnw , o 'i gyfrifo yn unol â 'r prif Reoliadau , yn fwy na therfyn cyfalaf uchaf o £ 20,000 .
< /sentence>
< sentence type="normal" elementid="20041023w-s59">
Mae 'r rheoliadau hyn yn diwygio 'r Prif Reoliadau i gynyddu 'r terfyn cyfalaf uchaf o £ 20,000 i £ 20,500 .
< /sentence>
< sentence type="normal" elementid="20041023w-s60">
Mae 'r Prif Reoliadau yn darparu hefyd ar gyfer cyfrifo incwm preswylydd i gymryd i ystyriaeth gyfalaf sydd o fewn band rhwng y terfyn cyfalaf uchaf a 'r terfyn cyfalaf isaf .
< /sentence>
< sentence type="normal" elementid="20041023w-s61">
Mae 'r Rheoliadau hyn yn diwygio 'r terfynau cyfalaf uchaf ac isaf .
< /sentence>
< sentence type="normal" elementid="20041023w-s62">
Mae pob £ 250 neu ran o £ 250 o fewn y band hwn yn cael ei drin fel swm sy 'n cyfateb i incwm wythnosol o £ 1 .
< /sentence>
< sentence type="normal" elementid="20041023w-s63">
Mae rheoliad 3 yn cynyddu swm y Credyd Cynilion sydd i 'w anwybyddu wrth gyfrifo incwm preswylydd .
< /sentence>
< sentence type="normal" elementid="20041023w-s64">
Mae hefyd yn gwneud mân ddiwygiad er mwyn symud effaith na fwriadwyd mohono yn y rheoliad fel y deddfwyd ef gyntaf .
< /sentence>
< sentence type="normal" elementid="20041023w-s65">
Mae rheoliad 4 yn dirymu 'r rheoliad a ddiwygiodd y terfynau cyfalaf yn 2003 .
< /sentence>
< sentence type="heading" elementid="20041023w-s66">
< em elementid="20041023w-em8">
< english>
Notes
< /english>
:
< /em>
< /sentence>
< sentence type="normal" elementid="20041023w-s67">
< squarebracket elementid="20041023w-sb9">
[  1 ]
< /squarebracket>
1948 p .
< /sentence>
< sentence type="normal" elementid="20041023w-s68">
29 ; diwygiwyd adran 22
< roundbracket elementid="20041023w-rb48">
(  5 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 gan adran 39
< roundbracket elementid="20041023w-rb49">
(  1 )
< /roundbracket>
o Ddeddf y Weinyddiaeth Nawdd Cymdeithasol 1966
< roundbracket elementid="20041023w-rb50">
(  p.20 )
< /roundbracket>
a pharagraff 6 o Atodlen 6 iddi , gan adran 35
< roundbracket elementid="20041023w-rb51">
(  2 )
< /roundbracket>
o Ddeddf Budd-daliadau Atodol 1976
< roundbracket elementid="20041023w-rb52">
(  p.71 )
< /roundbracket>
a pharagraff 3
< roundbracket elementid="20041023w-rb53">
(  b )
< /roundbracket>
o Atodlen 7 iddi , gan adran 20 o Ddeddf Nawdd Cymdeithasol 1980
< roundbracket elementid="20041023w-rb54">
(  p.30 )
< /roundbracket>
a pharagraff 2 o Atodlen 4 iddi , a chan adran 86 o Ddeddf Nawdd Cymdeithasol 1986
< roundbracket elementid="20041023w-rb55">
(  p.50 )
< /roundbracket>
a pharagraff 32 o Atodlen 10 iddi .
< /sentence>
< sentence type="heading" elementid="20041023w-s69">
< a type="href" elementid="20041023w-a12">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20041023w-p19">
< sentence type="normal" elementid="20041023w-s70">
< squarebracket elementid="20041023w-sb10">
[  2 ]
< /squarebracket>
Trosglwyddwyd swyddogaethau Ysgrifennydd Gwladol Cymru o dan adran 22
< roundbracket elementid="20041023w-rb56">
(  5 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 i Gynulliad Cenedlaethol Cymru gan Orchymyn Cynulliad Cenedlaethol Cymru
< roundbracket elementid="20041023w-rb57">
(  Trosglwyddo Swyddogaethau )
< /roundbracket>
1999
< roundbracket elementid="20041023w-rb58">
(  O.S. 1999/ 672 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20041023w-s71">
< a type="href" elementid="20041023w-a13">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20041023w-p20">
< sentence type="normal" elementid="20041023w-s72">
< squarebracket elementid="20041023w-sb11">
[  3 ]
< /squarebracket>
O.S. 1992/ 2977 ; yr offerynnau diwygio perthnasol yw O.S. 1996/ 602 , O.S. 2002/ 814  Cy .
< /sentence>
< sentence type="normal" elementid="20041023w-s73">
94 ) ac O.S.2003/ 897  Cy .
< /sentence>
< sentence type="normal" elementid="20041023w-s74">
117 ) .
< /sentence>
< sentence type="heading" elementid="20041023w-s75">
< a type="href" elementid="20041023w-a14">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20041023w-p21">
< sentence type="normal" elementid="20041023w-s76">
< squarebracket elementid="20041023w-sb12">
[  4 ]
< /squarebracket>
Mewnosodwyd y ffigur hwn mewn perthynas â Chymru gan O.S.2003/ 897
< roundbracket elementid="20041023w-rb59">
(  Cy.117 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20041023w-s77">
< a type="href" elementid="20041023w-a15">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20041023w-p22">
< sentence type="heading" elementid="20041023w-s78">
< squarebracket elementid="20041023w-sb13">
[  5 ]
< /squarebracket>
Mewnosodwyd y ffigur hwn mewn perthynas â Chymru gan O.S.2003/ 897
< roundbracket elementid="20041023w-rb60">
(  Cy.117 )
< /roundbracket>

< a type="href" elementid="20041023w-a16">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20041023w-p23">
< sentence type="heading" elementid="20041023w-s79">
< squarebracket elementid="20041023w-sb14">
[  6 ]
< /squarebracket>
Mewnosodwyd y ffigur hwn mewn perthynas â Chymru gan O.S.2003/ 897
< roundbracket elementid="20041023w-rb61">
(  Cy.117 )
< /roundbracket>

< a type="href" elementid="20041023w-a17">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20041023w-p24">
< sentence type="normal" elementid="20041023w-s80">
< squarebracket elementid="20041023w-sb15">
[  7 ]
< /squarebracket>
O.S. 2003/ 897
< roundbracket elementid="20041023w-rb62">
(  Cy.117 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20041023w-s81">
< a type="href" elementid="20041023w-a18">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20041023w-p25">
< sentence type="normal" elementid="20041023w-s82">
< squarebracket elementid="20041023w-sb16">
[  8 ]
< /squarebracket>
1998 p.38 .
< /sentence>
< sentence type="heading" elementid="20041023w-s83">
< a type="href" elementid="20041023w-a19">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20041023w-p26">
< sentence type="heading" elementid="20041023w-s84">
< a type="href" elementid="20041023w-a20">
< english>
English version
< /english>
< /a>
< /sentence>
< sentence type="heading" elementid="20041023w-s85">
ISBN 0 11 090921 6
< /sentence>
< /p>
< table elementid="20041023w-tb2">
< td elementid="20041023w-td5">
< p elementid="20041023w-p27">
< sentence type="heading" elementid="20041023w-s86">
< a type="href" elementid="20041023w-a21">
< english>
Other UK SIs
< /english>
< /a>
< a type="href" elementid="20041023w-a22">
< english>
Home
< /english>
< /a>
< a type="href" elementid="20041023w-a23">
< english>
National Assembly for Wales Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20041023w-a24">
< english>
Scottish Statutory Instrument
< /english>
< /a>
< a type="href" elementid="20041023w-a25">
< english>
Statutory Rules of Northern Ireland
< /english>
< /a>
< a type="href" elementid="20041023w-a26">
< english>
Her Majesty 's Stationery Office
< /english>
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20041023w-td6">
< p elementid="20041023w-p28">
< sentence type="heading" elementid="20041023w-s87">
< english>
< strong elementid="20041023w-st7">
We welcome your 
< a type="href" elementid="20041023w-a27">
comments
< /a>
on this site
< /strong>
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20041023w-td7">
< p elementid="20041023w-p29">
< sentence type="heading" elementid="20041023w-s88">
< english>
© Crown copyright 2004
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20041023w-td8">
< p elementid="20041023w-p30">
< sentence type="heading" elementid="20041023w-s89">
< english>
Prepared 7 April 2004
< /english>
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>1202< /wordcount>
< /file>
< file elementid="20041024w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Rheoliadau Cymorth Gwladol ( Symiau at Anghenion Personol ) ( Cymru ) 2004< /pagetitle>
< doc_class>Statutory Instrument - Wales< /doc_class>
< year>2004< /year>
< number>1024< /number>
< legislation_code>30102< /legislation_code>
< legislation_type>Statutory Instrument - Wales< /legislation_type>
< subject>GWASANAETHAU CYMORTH GWLADOL, CYMRU< /subject>
< laid_date>_EMPTY< /laid_date>
< made_date>20040330< /made_date>
< coming_into_force>20040412< /coming_into_force>
< isbn>0110909232< /isbn>
< title>Rheoliadau Cymorth Gwladol (Symiau at Anghenion Personol) (Cymru) 2004< /title>
< signatory>D. Elis-Thomas< /signatory>
< signatory_title>Llywydd y Cynulliad Cenedlaethol< /signatory_title>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20041024w-p1">
< sentence type="heading" elementid="20041024w-s1">
Offerynnau Statudol 2004 Rhif 1024
< roundbracket elementid="20041024w-rb1">
(  Cy.121 )
< /roundbracket>
< /sentence>
< /p>
< p elementid="20041024w-p2">
< sentence type="heading" elementid="20041024w-s2">
< strong elementid="20041024w-st1">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20041024w-rb2">
(  Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20041024w-rb3">
(  Cymru )
< /roundbracket>
2004
< /strong>
< /sentence>
< /p>
< p elementid="20041024w-p3">
< sentence type="heading" elementid="20041024w-s3">
© Hawlfraint y Goron 2004
< /sentence>
< /p>
< p elementid="20041024w-p4">
< sentence type="normal" elementid="20041024w-s4">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20041024w-p5">
< sentence type="normal" elementid="20041024w-s5">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20041024w-s6">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20041024w-p6">
< sentence type="normal" elementid="20041024w-s7">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20041024w-s8">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20041024w-s9">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20041024w-p7">
< sentence type="normal" elementid="20041024w-s10">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20041024w-s11">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan
< english>
The Stationery Office Limited
< /english>
fel
< strong elementid="20041024w-st2">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20041024w-rb4">
(  Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20041024w-rb5">
(  Cymru )
< /roundbracket>
2004
< /strong>
, ISBN 0110909232 .
< /sentence>
< sentence type="normal" elementid="20041024w-s12">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20041024w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20041024w-s13">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20041024w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20041024w-p8">
< sentence type="normal" elementid="20041024w-s14">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20041024w-a3">
< english>
Frequently Asked Questions
< /english>
< /a> .
< /sentence>
< /p>
< p elementid="20041024w-p9">
< sentence type="normal" elementid="20041024w-s15">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20041024w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20041024w-s16">
Pan welwch fotwm
< doublequotation elementid="20041024w-dq2">
"
< english>
continue
< /continue>
"
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20041024w-p10">
< sentence type="heading" elementid="20041024w-s17">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="heading" elementid="20041024w-s18">
< strong elementid="20041024w-st3">
2004 Rhif 1024
< roundbracket elementid="20041024w-rb6">
(  Cy.121 )
< /roundbracket>
< /strong>
< /sentence>
< sentence type="heading" elementid="20041024w-s19">
< strong elementid="20041024w-st4">
GWASANAETHAU CYMORTH GWLADOL , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20041024w-s20">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20041024w-rb7">
(  Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20041024w-rb8">
(  Cymru )
< /roundbracket>
2004
< /sentence>
< /p>
< table elementid="20041024w-tb1">
< td elementid="20041024w-td1">
< p elementid="20041024w-p11">
< sentence type="heading" elementid="20041024w-s21">
< em elementid="20041024w-em1">
Wedi 'u gwneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20041024w-td2">
< p elementid="20041024w-p12">
< sentence type="heading" elementid="20041024w-s22">
< em elementid="20041024w-em2">
30 Mawrth 2004
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20041024w-td3">
< p elementid="20041024w-p13">
< sentence type="heading" elementid="20041024w-s23">
< em elementid="20041024w-em3">
Yn dod i rym
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20041024w-td4">
< p elementid="20041024w-p14">
< sentence type="heading" elementid="20041024w-s24">
< em elementid="20041024w-em4">
12 Ebrill 2004
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20041024w-p15">
< sentence type="normal" elementid="20041024w-s25">
Mae Cynulliad Cenedlaethol Cymru yn gwneud y Rheoliadau canlynol drwy arfer y pwerau a roddwyd i 'r Ysgrifennydd Gwladol gan adran 22
< roundbracket elementid="20041024w-rb9">
(  4 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948
< squarebracket elementid="20041024w-sb1">
[ 
< a type="href" elementid="20041024w-a4">
1
< /a>
]
< /squarebracket>
ac sydd wedi 'u breinio bellach yng Nghynulliad Cenedlaethol Cymru
< squarebracket elementid="20041024w-sb2">
[ 
< a type="href" elementid="20041024w-a5">
2
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="heading" elementid="20041024w-s26">
< strong elementid="20041024w-st5">
Enwi , cychwyn a chymhwyso
< /strong>
< /sentence>
< sentence type="normal" elementid="20041024w-s27">
1 .
< /sentence>
< sentence type="normal" elementid="20041024w-s28">
 - 
< roundbracket elementid="20041024w-rb10">
(  1 )
< /roundbracket>
Enw 'r rheoliadau hyn yw Rheoliadau Cymorth Gwladol
< roundbracket elementid="20041024w-rb11">
(  Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20041024w-rb12">
(  Cymru )
< /roundbracket>
2004 a deuant i rym ar 12 Ebrill 2004 .
< /sentence>
< sentence type="normal" elementid="20041024w-s29">
< roundbracket elementid="20041024w-rb13">
(  2 )
< /roundbracket>
Mae 'r Rheoliadau hyn yn gymwys i Gymru yn unig .
< /sentence>
< sentence type="heading" elementid="20041024w-s30">
< strong elementid="20041024w-st6">
Y swm y mae ei angen at anghenion personol
< /strong>
< /sentence>
< sentence type="normal" elementid="20041024w-s31">
2 .
< /sentence>
< sentence type="normal" elementid="20041024w-s32">
Y swm y mae 'n rhaid i awdurdod lleol ragdybio o dan adran 22
< roundbracket elementid="20041024w-rb14">
(  4 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 y bydd ar berson ei angen at anghenion personol y person hwnnw yw £ 18.40 yr wythnos .
< /sentence>
< sentence type="heading" elementid="20041024w-s33">
< strong elementid="20041024w-st7">
Dirymu
< /strong>
< /sentence>
< sentence type="normal" elementid="20041024w-s34">
3 .
< /sentence>
< sentence type="normal" elementid="20041024w-s35">
Mae Rheoliadau Cymorth Gwladol
< roundbracket elementid="20041024w-rb15">
(  Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20041024w-rb16">
(  Cymru )
< /roundbracket>
2003
< squarebracket elementid="20041024w-sb3">
[ 
< a type="href" elementid="20041024w-a6">
3
< /a>
]
< /squarebracket>
drwy hyn wedi 'u dirymu .
< /sentence>
< sentence type="normal" elementid="20041024w-s36">
Llofnodwyd ar ran Cynulliad Cenedlaethol Cymru o dan adran 66
< roundbracket elementid="20041024w-rb17">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20041024w-sb4">
[ 
< a type="href" elementid="20041024w-a7">
4
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="heading" elementid="20041024w-s37">
< em elementid="20041024w-em5">
D. Elis-Thomas
< /em>
< /sentence>
< sentence type="heading" elementid="20041024w-s38">
Llywydd y Cynulliad Cenedlaethol
< /sentence>
< sentence type="heading" elementid="20041024w-s39">
30 Mawrth 2004
< /sentence>
< sentence type="heading" elementid="20041024w-s40">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20041024w-s41">
< em elementid="20041024w-em6">
< roundbracket elementid="20041024w-rb18">
(  Nid yw 'r nodyn hwn yn rhan o 'r Rheoliadau )
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20041024w-s42">
Mae 'r Rheoliadau hyn yn nodi 'r swm wythnosol y mae awdurdodau lleol yng Nghymru i ragdybio , yn niffyg amgylchiadau arbennig , y bydd ar breswylwyr sydd mewn llety a drefnwyd o dan Ran III o Ddeddf Cymorth Gwladol 1948 ei angen at eu hanghenion personol .
< /sentence>
< sentence type="normal" elementid="20041024w-s43">
O 12 Ebrill 2004 ymlaen , rhagdybir y bydd ar bob preswylydd o 'r fath angen £ 18.40 yr wythnos at eu hanghenion personol .
< /sentence>
< sentence type="normal" elementid="20041024w-s44">
Mae 'r Rheoliadau hyn yn disodli Rheoliadau Cymorth Gwladol
< roundbracket elementid="20041024w-rb19">
(  Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20041024w-rb20">
(  Cymru )
< /roundbracket>
2003 sy 'n cael eu dirymu , ac oedd yn rhagnodi 'r swm wythnosol o £ 17.80 .
< /sentence>
< sentence type="heading" elementid="20041024w-s45">
< em elementid="20041024w-em7">
< english>
Notes
< /english>
:
< /em>
< /sentence>
< sentence type="normal" elementid="20041024w-s46">
< squarebracket elementid="20041024w-sb5">
[  1 ]
< /squarebracket>
1948 p.29 .
< /sentence>
< sentence type="normal" elementid="20041024w-s47">
< em elementid="20041024w-em8">
Gweler
< /em>
adrannau 35
< roundbracket elementid="20041024w-rb21">
(  1 )
< /roundbracket>
a 64
< roundbracket elementid="20041024w-rb22">
(  1 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 ar gyfer y diffiniadau o
< doublequotation elementid="20041024w-dq3">
"
< english>
the Minister
< /english>
"
< /doublequotation>
a
< doublequotation elementid="20041024w-dq4">
"
< english>
prescribed
< /english>
"
< /doublequotation>
yn eu trefn , ac erthygl 2 o Orchymyn yr Ysgrifennydd Gwladol dros Wasanaethau Cymdeithasol 1968
< roundbracket elementid="20041024w-rb23">
(  O.S. 1968/ 1699 )
< /roundbracket>
a drosglwyddodd holl swyddogaethau 'r Gweinidog Iechyd i 'r Ysgrifennydd Gwladol .
< /sentence>
< sentence type="heading" elementid="20041024w-s48">
< a type="href" elementid="20041024w-a8">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20041024w-p16">
< sentence type="normal" elementid="20041024w-s49">
< squarebracket elementid="20041024w-sb6">
[  2 ]
< /squarebracket>
Trosglwyddwyd swyddogaethau Ysgrifennydd Gwladol Cymru o dan adran 22
< roundbracket elementid="20041024w-rb24">
(  4 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 i Gynulliad Cenedlaethol Cymru gan Orchymyn Cynulliad Cenedlaethol Cymru
< roundbracket elementid="20041024w-rb25">
(  Trosglwyddo Swyddogaethau )
< /roundbracket>
1999
< roundbracket elementid="20041024w-rb26">
(  O.S. 1999/ 672 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20041024w-s50">
< a type="href" elementid="20041024w-a9">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20041024w-p17">
< sentence type="normal" elementid="20041024w-s51">
< squarebracket elementid="20041024w-sb7">
[  3 ]
< /squarebracket>
O.S. 2003/ 892
< roundbracket elementid="20041024w-rb27">
(  Cy.112 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20041024w-s52">
< a type="href" elementid="20041024w-a10">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20041024w-p18">
< sentence type="normal" elementid="20041024w-s53">
< squarebracket elementid="20041024w-sb8">
[  4 ]
< /squarebracket>
1998 p.38 .
< /sentence>
< sentence type="heading" elementid="20041024w-s54">
< a type="href" elementid="20041024w-a11">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20041024w-p19">
< sentence type="heading" elementid="20041024w-s55">
< a type="href" elementid="20041024w-a12">
< english>
English version
< /english>
< /a>
< /sentence>
< sentence type="heading" elementid="20041024w-s56">
ISBN 0 11 090923 2
< /sentence>
< /p>
< table elementid="20041024w-tb2">
< td elementid="20041024w-td5">
< p elementid="20041024w-p20">
< sentence type="heading" elementid="20041024w-s57">
< a type="href" elementid="20041024w-a13">
< english>
Other UK SIs
< /english>
< /a>
< a type="href" elementid="20041024w-a14">
< english>
Home
< /english>
< /a>
< a type="href" elementid="20041024w-a15">
< english>
National Assembly for Wales Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20041024w-a16">
< english>
Scottish Statutory Instrument
< /english>
< /a>
< a type="href" elementid="20041024w-a17">
< english>
Statutory Rules of Northern Ireland
< /english>
< /a>
< a type="href" elementid="20041024w-a18">
< english>
Her Majesty 's Stationery Office
< /english>
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20041024w-td6">
< p elementid="20041024w-p21">
< sentence type="heading" elementid="20041024w-s58">
< english>
< strong elementid="20041024w-st8">
We welcome your 
< a type="href" elementid="20041024w-a19">
comments
< /a>
on this site
< /strong>
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20041024w-td7">
< p elementid="20041024w-p22">
< sentence type="heading" elementid="20041024w-s59">
< english>
© Crown copyright 2004
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20041024w-td8">
< p elementid="20041024w-p23">
< sentence type="heading" elementid="20041024w-s60">
< english>
Prepared 7 April 2004
< /english>
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>752< /wordcount>
< /file>
< file elementid="20042879w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Rheoliadau Cymorth Gwladol ( Asesu Adnoddau ) ( Diwygio Rhif 2 ) ( Cymru ) 2004< /pagetitle>
< doc_class>Statutory Instrument - Wales< /doc_class>
< year>2004< /year>
< number>2879< /number>
< legislation_code>30102< /legislation_code>
< legislation_type>Statutory Instrument - Wales< /legislation_type>
< subject>GWASANAETHAU CYMORTH GWLADOL, CYMRU< /subject>
< laid_date>_EMPTY< /laid_date>
< made_date>20041102< /made_date>
< coming_into_force>20041103< /coming_into_force>
< isbn>0110910176< /isbn>
< title>Rheoliadau Cymorth Gwladol (Asesu Adnoddau) (Diwygio Rhif 2) (Cymru) 2004< /title>
< signatory>D. Elis-Thomas< /signatory>
< signatory_title>Llywydd y Cynulliad Cenedlaethol< /signatory_title>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20042879w-p1">
< sentence type="heading" elementid="20042879w-s1">
Offerynnau Statudol 2004 Rhif 2879
< roundbracket elementid="20042879w-rb1">
(  Cy.249 )
< /roundbracket>
< /sentence>
< /p>
< p elementid="20042879w-p2">
< sentence type="heading" elementid="20042879w-s2">
< strong elementid="20042879w-st1">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20042879w-rb2">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20042879w-rb3">
(  Diwygio Rhif 2 )
< /roundbracket>
< roundbracket elementid="20042879w-rb4">
(  Cymru )
< /roundbracket>
2004
< /strong>
< /sentence>
< /p>
< p elementid="20042879w-p3">
< sentence type="heading" elementid="20042879w-s3">
© Hawlfraint y Goron 2004
< /sentence>
< /p>
< p elementid="20042879w-p4">
< sentence type="normal" elementid="20042879w-s4">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20042879w-p5">
< sentence type="normal" elementid="20042879w-s5">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20042879w-s6">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20042879w-p6">
< sentence type="normal" elementid="20042879w-s7">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20042879w-s8">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20042879w-s9">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20042879w-p7">
< sentence type="normal" elementid="20042879w-s10">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20042879w-s11">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan
< english>
The Stationery Office Limited
< /english>
fel
< strong elementid="20042879w-st2">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20042879w-rb5">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20042879w-rb6">
(  Diwygio Rhif 2 )
< /roundbracket>
< roundbracket elementid="20042879w-rb7">
(  Cymru )
< /roundbracket>
2004
< /strong>
, ISBN 0110910176 .
< /sentence>
< sentence type="normal" elementid="20042879w-s12">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20042879w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20042879w-s13">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20042879w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20042879w-p8">
< sentence type="normal" elementid="20042879w-s14">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20042879w-a3">
< english>
Frequently Asked Questions
< /english>
< /a> .
< /sentence>
< /p>
< p elementid="20042879w-p9">
< sentence type="normal" elementid="20042879w-s15">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20042879w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20042879w-s16">
Pan welwch fotwm
< doublequotation elementid="20042879w-dq2">
"
< english>
continue
< /continue>
"
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20042879w-p10">
< sentence type="heading" elementid="20042879w-s17">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="heading" elementid="20042879w-s18">
< strong elementid="20042879w-st3">
2004 Rhif 2879
< roundbracket elementid="20042879w-rb8">
(  Cy.249 )
< /roundbracket>
< /strong>
< /sentence>
< sentence type="heading" elementid="20042879w-s19">
< strong elementid="20042879w-st4">
GWASANAETHAU CYMORTH GWLADOL , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20042879w-s20">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20042879w-rb9">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20042879w-rb10">
(  Diwygio Rhif 2 )
< /roundbracket>
< roundbracket elementid="20042879w-rb11">
(  Cymru )
< /roundbracket>
2004
< /sentence>
< /p>
< table elementid="20042879w-tb1">
< td elementid="20042879w-td1">
< p elementid="20042879w-p11">
< sentence type="heading" elementid="20042879w-s21">
< em elementid="20042879w-em1">
Wedi 'u gwneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20042879w-td2">
< p elementid="20042879w-p12">
< sentence type="heading" elementid="20042879w-s22">
< em elementid="20042879w-em2">
2 Tachwedd 2004
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20042879w-td3">
< p elementid="20042879w-p13">
< sentence type="heading" elementid="20042879w-s23">
< em elementid="20042879w-em3">
Yn dod i rym
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20042879w-td4">
< p elementid="20042879w-p14">
< sentence type="heading" elementid="20042879w-s24">
< em elementid="20042879w-em4">
3 Tachwedd 2004
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20042879w-p15">
< sentence type="heading" elementid="20042879w-s25">
Mae Cynulliad Cenedlaethol Cymru yn gwneud y Rheoliadau a ganlyn drwy arfer y pwerau a roddwyd i 'r Ysgrifennydd Gwladol gan adran 22
< roundbracket elementid="20042879w-rb12">
(  5 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948
< squarebracket elementid="20042879w-sb1">
[ 
< a type="href" elementid="20042879w-a4">
1
< /a>
]
< /squarebracket>
ac sydd bellach wedi 'u breinio yng Nghynulliad Cenedlaethol Cymru
< squarebracket elementid="20042879w-sb2">
[ 
< a type="href" elementid="20042879w-a5">
2
< /a>
]
< /squarebracket>
:
< /sentence>
< sentence type="heading" elementid="20042879w-s26">
< strong elementid="20042879w-st5">
Enwi , cychwyn , dehongli a chymhwyso
< /strong>
< /sentence>
< sentence type="normal" elementid="20042879w-s27">
1 .
< /sentence>
< sentence type="normal" elementid="20042879w-s28">
 - 
< roundbracket elementid="20042879w-rb13">
(  1 )
< /roundbracket>
Enw 'r Rheoliadau hyn yw Rheoliadau Cymorth Gwladol
< roundbracket elementid="20042879w-rb14">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20042879w-rb15">
(  Diwygio Rhif 2 )
< /roundbracket>
< roundbracket elementid="20042879w-rb16">
(  Cymru )
< /roundbracket>
2004 .
< /sentence>
< sentence type="normal" elementid="20042879w-s29">
< roundbracket elementid="20042879w-rb17">
(  2 )
< /roundbracket>
Daw 'r Rheoliadau hyn i rym ar 3 Tachwedd 2004 .
< /sentence>
< sentence type="normal" elementid="20042879w-s30">
< roundbracket elementid="20042879w-rb18">
(  3 )
< /roundbracket>
Yn y Rheoliadau hyn , ystyr
< doublequotation elementid="20042879w-dq3">
"  y Prif Reoliadau "
< /doublequotation>
< roundbracket elementid="20042879w-rb19">
(
< doublequotation elementid="20042879w-dq4">
"
< em elementid="20042879w-em5">
< english>
the Principal Regulations
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw Rheoliadau Cymorth Gwladol
< roundbracket elementid="20042879w-rb20">
(  Asesu Adnoddau )
< /roundbracket>
1992
< squarebracket elementid="20042879w-sb3">
[ 
< a type="href" elementid="20042879w-a6">
3
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="normal" elementid="20042879w-s31">
< roundbracket elementid="20042879w-rb21">
(  4 )
< /roundbracket>
Mae 'r Rheoliadau hyn yn gymwys i Gymru yn unig .
< /sentence>
< sentence type="heading" elementid="20042879w-s32">
Diwygio rheoliad 2 o 'r Prif Reoliadau
< /sentence>
< sentence type="normal" elementid="20042879w-s33">
2 .
< /sentence>
< sentence type="heading" elementid="20042879w-s34">
Ym mharagraff
< roundbracket elementid="20042879w-rb22">
(  1 )
< /roundbracket>
o reoliad 2 o 'r Prif Reoliadau
< roundbracket elementid="20042879w-rb23">
(  dehongli )
< /roundbracket>
, mewnosoder y canlynol yn ôl trefn yr wyddor - 
< /sentence>
< sentence type="normal" elementid="20042879w-s35">
< english>
< doublequotation elementid="20042879w-dq5">
"  "
< /doublequotation>
the health service
< doublequotation elementid="20042879w-dq6">
"  has the same meaning as in section 128
< roundbracket elementid="20042879w-rb24">
(  1 )
< /roundbracket>
of the National Health Service Act 1977
< squarebracket elementid="20042879w-sb4">
[ 
< a type="href" elementid="20042879w-a7">
4
< /a>
]
< /squarebracket>
"
< /doublequotation> .
< /english>
< /sentence>
< /p>
< p elementid="20042879w-p16">
< sentence type="heading" elementid="20042879w-s36">
< strong elementid="20042879w-st6">
Diwygio Atodlen 3 i 'r Prif Reoliadau
< roundbracket elementid="20042879w-rb25">
(  Symiau sydd i 'w diystyru wrth gyfrifo incwm ac eithrio enillion )
< /roundbracket>
< /strong>
< /sentence>
< sentence type="normal" elementid="20042879w-s37">
3 .
< /sentence>
< sentence type="heading" elementid="20042879w-s38">
Ar ôl paragraff 28I o Atodlen 3 i 'r Prif Reoliadau , mewnosoder y paragraff a ganlyn - 
< /sentence>
< /p>
< p elementid="20042879w-p17">
< sentence type="heading" elementid="20042879w-s39">
< english>
< strong elementid="20042879w-st7">
28J.
< /strong>
Any payment made to the resident under section 63
< roundbracket elementid="20042879w-rb26">
(  6 )
< /roundbracket>
< roundbracket elementid="20042879w-rb27">
(  b )
< /roundbracket>
of the Health Services and Public Health Act 1968
< roundbracket elementid="20042879w-rb28">
(
< doublequotation elementid="20042879w-dq7">
"  the 1968 Act "
< /doublequotation>
)
< /roundbracket>
< squarebracket elementid="20042879w-sb5">
[ 
< a type="href" elementid="20042879w-a8">
5
< /a>
]
< /squarebracket>
< roundbracket elementid="20042879w-rb29">
(  travelling and other allowances to persons availing themselves of instruction )
< /roundbracket>
for the purpose of meeting child care costs where the instruction is provided pursuant to - 
< /english>
< /sentence>
< sentence type="heading" elementid="20042879w-s40">
< english>
< roundbracket elementid="20042879w-rb30">
(  a )
< /roundbracket>
section 63
< roundbracket elementid="20042879w-rb31">
(  1 )
< /roundbracket>
< roundbracket elementid="20042879w-rb32">
(  a )
< /roundbracket>
of the 1968 Act ; or
< /english>
< /sentence>
< sentence type="normal" elementid="20042879w-s41">
< english>
< roundbracket elementid="20042879w-rb33">
(  b )
< /roundbracket>
section 63
< roundbracket elementid="20042879w-rb34">
(  1 )
< /roundbracket>
< roundbracket elementid="20042879w-rb35">
(  b )
< /roundbracket>
of the 1968 Act and where the resident is employed , or has it in contemplation to be employed , in an activity involved in or connected with a service which must or may be provided or secured as part of the health service .
< /english>
< /sentence>
< sentence type="normal" elementid="20042879w-s42">
.
< /sentence>
< /p>
< p elementid="20042879w-p18">
< sentence type="heading" elementid="20042879w-s43">
< strong elementid="20042879w-st8">
Diwygio Atodlen 4 i 'r Prif Reoliadau
< roundbracket elementid="20042879w-rb36">
(  Cyfalaf sydd i 'w ddiystyru )
< /roundbracket>
< /strong>
< /sentence>
< sentence type="normal" elementid="20042879w-s44">
4 .
< /sentence>
< sentence type="heading" elementid="20042879w-s45">
Yn Atodlen 4 i 'r Prif Reoliadau , ar ôl paragraff 24 , ychwaneger y paragraffau a ganlyn - 
< /sentence>
< /p>
< p elementid="20042879w-p19">
< sentence type="normal" elementid="20042879w-s46">
25 .
< /sentence>
< sentence type="normal" elementid="20042879w-s47">
< english>
Any payment made to the resident under section 2 or 3 of the Age-Related Payments Act 2004
< roundbracket elementid="20042879w-rb37">
(  entitlement : basic or special cases )
< /roundbracket>
< squarebracket elementid="20042879w-sb6">
[ 
< a type="href" elementid="20042879w-a9">
6
< /a>
]
< /squarebracket> .
< /english>
< /sentence>
< sentence type="normal" elementid="20042879w-s48">
26 .
< /sentence>
< sentence type="heading" elementid="20042879w-s49">
< english>
Any payment made to the resident under section 63
< roundbracket elementid="20042879w-rb38">
(  6 )
< /roundbracket>
< roundbracket elementid="20042879w-rb39">
(  b )
< /roundbracket>
of the Health Services and Public Health Act 1968
< roundbracket elementid="20042879w-rb40">
(
< doublequotation elementid="20042879w-dq8">
"  the 1968 Act "
< /doublequotation>
)
< /roundbracket>
< squarebracket elementid="20042879w-sb7">
[ 
< a type="href" elementid="20042879w-a10">
7
< /a>
]
< /squarebracket>
< roundbracket elementid="20042879w-rb41">
(  travelling and other allowances to persons availing themselves of instruction )
< /roundbracket>
for the purpose of meeting child care costs where the instruction is
< /english>
< /sentence>
< sentence type="heading" elementid="20042879w-s50">
< english>
Provided pursuant to - 
< /english>
< /sentence>
< sentence type="normal" elementid="20042879w-s51">
< english>
< roundbracket elementid="20042879w-rb42">
(  b )
< /roundbracket>
section 63
< roundbracket elementid="20042879w-rb43">
(  1 )
< /roundbracket>
< roundbracket elementid="20042879w-rb44">
(  b )
< /roundbracket>
of the 1968 Act and where the resident is employed , or has it in contemplation to be employed , in an activity involved in or connected with a service which must or may be provided or secured as part of the health service .
< /english>
< /sentence>
< sentence type="normal" elementid="20042879w-s52">
.
< /sentence>
< /p>
< p elementid="20042879w-p20">
< sentence type="heading" elementid="20042879w-s53">
Llofnodwyd ar ran Cynulliad Cenedlaethol Cymru o dan adran 66
< roundbracket elementid="20042879w-rb45">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20042879w-sb8">
[ 
< a type="href" elementid="20042879w-a11">
8
< /a>
]
< /squarebracket>
< /sentence>
< sentence type="heading" elementid="20042879w-s54">
< em elementid="20042879w-em6">
D. Elis-Thomas
< /em>
< /sentence>
< sentence type="heading" elementid="20042879w-s55">
Llywydd y Cynulliad Cenedlaethol
< /sentence>
< sentence type="heading" elementid="20042879w-s56">
2 Tachwedd 2004
< /sentence>
< /p>
< p elementid="20042879w-p21">
< sentence type="heading" elementid="20042879w-s57">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20042879w-s58">
< em elementid="20042879w-em7">
< roundbracket elementid="20042879w-rb46">
(  Nid yw 'r nodyn hwn yn rhan o 'r Rheoliadau )
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20042879w-s59">
Mae 'r Rheoliadau hyn yn gwneud diwygiadau pellach i Reoliadau Cymorth Gwladol
< roundbracket elementid="20042879w-rb47">
(  Asesu Adnoddau )
< /roundbracket>
1992
< roundbracket elementid="20042879w-rb48">
(
< doublequotation elementid="20042879w-dq9">
"  y Prif Reoliadau "
< /doublequotation>
)
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20042879w-s60">
Mae 'r Prif Reoliadau yn pennu sut y mae awdurdodau lleol yn asesu gallu person i dalu am y llety y mae awdurdodau lleol yn ei drefnu o dan Ran III o Ddeddf Cymorth Gwladol 1948 .
< /sentence>
< sentence type="normal" elementid="20042879w-s61">
Mae rheoliad 3 yn diwygio 'r Prif Reoliadau drwy gyflwyno modd o ddiystyru incwm o ran taliadau tuag at gost gofal plant o dan adran 63
< roundbracket elementid="20042879w-rb49">
(  6 )
< /roundbracket>
< roundbracket elementid="20042879w-rb50">
(  b )
< /roundbracket>
o Ddeddf Gwasanaethau Iechyd ac Iechyd y Cyhoedd 1968 a wneir i breswylydd sydd o dan hyfforddiant sy 'n gysylltiedig â 'r gwasanaeth iechyd , yn rhinwedd y trefniadau a wneir o dan yr adran honno .
< /sentence>
< sentence type="normal" elementid="20042879w-s62">
Mae rheoliad 4 yn darparu i 'r un taliadau gael eu diystyru fel cyfalaf , a hefyd yn cyflwyno modd o ddiystyru cyfalaf o ran taliadau a wneir o dan adran 2 neu 3 o Ddeddf Taliadau ar Sail Oed 2004 .
< /sentence>
< sentence type="heading" elementid="20042879w-s63">
< em elementid="20042879w-em8">
< english>
Notes
< /english>
:
< /em>
< /sentence>
< sentence type="normal" elementid="20042879w-s64">
< squarebracket elementid="20042879w-sb9">
[  1 ]
< /squarebracket>
1948 p .
< /sentence>
< sentence type="normal" elementid="20042879w-s65">
29 ; diwygiwyd adran 22
< roundbracket elementid="20042879w-rb51">
(  5 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 gan adran 39
< roundbracket elementid="20042879w-rb52">
(  1 )
< /roundbracket>
o Ddeddf y Weinyddiaeth Nawdd Cymdeithasol 1966
< roundbracket elementid="20042879w-rb53">
(  p.20 )
< /roundbracket>
a pharagraff 6 o Atodlen 6 iddi , gan adran 35
< roundbracket elementid="20042879w-rb54">
(  2 )
< /roundbracket>
o Ddeddf Budd-daliadau Atodol 1976
< roundbracket elementid="20042879w-rb55">
(  p.71 )
< /roundbracket>
a pharagraff 3
< roundbracket elementid="20042879w-rb56">
(  b )
< /roundbracket>
o Atodlen 7 iddi , gan adran 20 o Ddeddf Nawdd Cymdeithasol 1980
< roundbracket elementid="20042879w-rb57">
(  p.30 )
< /roundbracket>
a pharagraff 2 o Atodlen 4 iddi , a chan adran 86 o Ddeddf Nawdd Cymdeithasol 1986
< roundbracket elementid="20042879w-rb58">
(  p.50 )
< /roundbracket>
a pharagraff 32 o Atodlen 10 iddi .
< /sentence>
< sentence type="heading" elementid="20042879w-s66">
< a type="href" elementid="20042879w-a12">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20042879w-p22">
< sentence type="normal" elementid="20042879w-s67">
< squarebracket elementid="20042879w-sb10">
[  2 ]
< /squarebracket>
Trosglwyddwyd swyddogaethau 'r Ysgrifennydd Gwladol o dan adran 22
< roundbracket elementid="20042879w-rb59">
(  5 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 i Gynulliad Cenedlaethol Cymru gan Orchymyn Cynulliad Cenedlaethol Cymru
< roundbracket elementid="20042879w-rb60">
(  Trosglwyddo Swyddogaethau )
< /roundbracket>
1999
< roundbracket elementid="20042879w-rb61">
(  O.S. 1999/ 672 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20042879w-s68">
< a type="href" elementid="20042879w-a13">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20042879w-p23">
< sentence type="normal" elementid="20042879w-s69">
< squarebracket elementid="20042879w-sb11">
[  3 ]
< /squarebracket>
O.S. 1992/ 2977 ; ac O.S. 1996/ 602 , O.S. 2002/ 814
< roundbracket elementid="20042879w-rb62">
(  Cy.94 )
< /roundbracket>
ac O.S. 2003/ 897  Cy .
< /sentence>
< sentence type="normal" elementid="20042879w-s70">
117 ) yw 'r offerynnau diwygio perthnasol .
< /sentence>
< sentence type="heading" elementid="20042879w-s71">
< a type="href" elementid="20042879w-a14">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20042879w-p24">
< sentence type="normal" elementid="20042879w-s72">
< squarebracket elementid="20042879w-sb12">
[  4 ]
< /squarebracket>
1977 p.49 .
< /sentence>
< sentence type="heading" elementid="20042879w-s73">
< a type="href" elementid="20042879w-a15">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20042879w-p25">
< sentence type="normal" elementid="20042879w-s74">
< squarebracket elementid="20042879w-sb13">
[  5 ]
< /squarebracket>
1968 p.46 .
< /sentence>
< sentence type="normal" elementid="20042879w-s75">
Mae adran 63
< roundbracket elementid="20042879w-rb63">
(  6 )
< /roundbracket>
yn darparu i 'r Gweinidog dalu ffioedd a dyroddi grantiau mewn perthynas â hyfforddi 'r sawl a gyflogir gan y Gwasanaeth Iechyd Gwladol , neu sy 'n ystyried cael ei gyflogi ganddo , ac mae is-adran
< roundbracket elementid="20042879w-rb64">
(  6 )
< /roundbracket>
< roundbracket elementid="20042879w-rb65">
(  b )
< /roundbracket>
yn caniatáu talu lwfansau teithio a lwfansau eraill i 'r sawl sy 'n derbyn hyfforddiant .
< /sentence>
< sentence type="normal" elementid="20042879w-s76">
Datganolwyd pwerau 'r Gweinidog o dan y Ddeddf i Gynulliad Cenedlaethol Cymru gan Orchymyn Cynulliad Cenedlaethol Cymru
< roundbracket elementid="20042879w-rb66">
(  Trosglwyddo Swyddogaethau )
< /roundbracket>
1999
< roundbracket elementid="20042879w-rb67">
(  O.S. 1999/ 672 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20042879w-s77">
< a type="href" elementid="20042879w-a16">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20042879w-p26">
< sentence type="normal" elementid="20042879w-s78">
< squarebracket elementid="20042879w-sb14">
[  6 ]
< /squarebracket>
2004 p .
< /sentence>
< sentence type="normal" elementid="20042879w-s79">
10 .
< /sentence>
< sentence type="heading" elementid="20042879w-s80">
< a type="href" elementid="20042879w-a17">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20042879w-p27">
< sentence type="normal" elementid="20042879w-s81">
< squarebracket elementid="20042879w-sb15">
[  7 ]
< /squarebracket>
1968 p.46 .
< /sentence>
< sentence type="normal" elementid="20042879w-s82">
Mae adran 63
< roundbracket elementid="20042879w-rb68">
(  6 )
< /roundbracket>
yn darparu i 'r Gweinidog dalu ffioedd a dyroddi grantiau mewn perthynas â hyfforddi 'r sawl a gyflogir gan y Gwasanaeth Iechyd Gwladol , neu sy 'n ystyried cael ei gyflogi ganddo , ac mae is-adran
< roundbracket elementid="20042879w-rb69">
(  6 )
< /roundbracket>
< roundbracket elementid="20042879w-rb70">
(  b )
< /roundbracket>
yn caniatáu talu lwfansau teithio a lwfansau eraill i 'r sawl sy 'n derbyn hyfforddiant .
< /sentence>
< sentence type="normal" elementid="20042879w-s83">
Datganolwyd pwerau 'r Gweinidog o dan y Ddeddf i Gynulliad Cenedlaethol Cymru gan Orchymyn Cynulliad Cenedlaethol Cymru
< roundbracket elementid="20042879w-rb71">
(  Trosglwyddo Swyddogaethau )
< /roundbracket>
1999
< roundbracket elementid="20042879w-rb72">
(  O.S. 1999/ 672 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20042879w-s84">
< a type="href" elementid="20042879w-a18">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20042879w-p28">
< sentence type="normal" elementid="20042879w-s85">
< squarebracket elementid="20042879w-sb16">
[  8 ]
< /squarebracket>
1998 p.38 .
< /sentence>
< sentence type="heading" elementid="20042879w-s86">
< a type="href" elementid="20042879w-a19">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20042879w-p29">
< sentence type="heading" elementid="20042879w-s87">
< a type="href" elementid="20042879w-a20">
< english>
English version
< /english>
< /a>
< /sentence>
< sentence type="heading" elementid="20042879w-s88">
ISBN 0 11091017 6
< /sentence>
< /p>
< table elementid="20042879w-tb2">
< td elementid="20042879w-td5">
< p elementid="20042879w-p30">
< sentence type="heading" elementid="20042879w-s89">
< a type="href" elementid="20042879w-a21">
< english>
Other UK SIs
< /english>
< /a>
< a type="href" elementid="20042879w-a22">
< english>
Home
< /english>
< /a>
< a type="href" elementid="20042879w-a23">
< english>
National Assembly for Wales Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20042879w-a24">
< english>
Scottish Statutory Instrument
< /english>
< /a>
< a type="href" elementid="20042879w-a25">
< english>
Statutory Rules of Northern Ireland
< /english>
< /a>
< a type="href" elementid="20042879w-a26">
< english>
Her Majesty 's Stationery Office
< /english>
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20042879w-td6">
< p elementid="20042879w-p31">
< sentence type="heading" elementid="20042879w-s90">
< english>
< english>
< strong elementid="20042879w-st9">
We welcome your 
< a type="href" elementid="20042879w-a27">
comments
< /a>
on this site
< /strong>
< /english>
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20042879w-td7">
< p elementid="20042879w-p32">
< sentence type="heading" elementid="20042879w-s91">
< english>
© Crown copyright 2004
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20042879w-td8">
< p elementid="20042879w-p33">
< sentence type="heading" elementid="20042879w-s92">
< english>
Prepared 9 November 2004
< /english>
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>1337< /wordcount>
< /file>
< file elementid="20050662w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Rheoliadau Cymorth Gwladol ( Asesu Adnoddau ) ( Diwygio ) ( Cymru ) 2005< /pagetitle>
< doc_class>Statutory Instrument - Wales< /doc_class>
< year>2005< /year>
< number>0662< /number>
< legislation_code>30102< /legislation_code>
< legislation_type>Statutory Instrument - Wales< /legislation_type>
< subject>GWASANAETHAU CYMORTH GWLADOL, CYMRU< /subject>
< laid_date>_EMPTY< /laid_date>
< made_date>20050308< /made_date>
< coming_into_force>20050411< /coming_into_force>
< isbn>0110910885< /isbn>
< title>Rheoliadau Cymorth Gwladol (Asesu Adnoddau) (Diwygio) (Cymru) 2005< /title>
< signatory>D. Elis-Thomas< /signatory>
< signatory_title>Llywydd y Cynulliad Cenedlaethol< /signatory_title>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20050662w-p1">
< sentence type="heading" elementid="20050662w-s1">
Offerynnau Statudol 2005 Rhif 662
< roundbracket elementid="20050662w-rb1">
(  Cy.52 )
< /roundbracket>
< /sentence>
< /p>
< p elementid="20050662w-p2">
< sentence type="heading" elementid="20050662w-s2">
< strong elementid="20050662w-st1">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20050662w-rb2">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20050662w-rb3">
(  Diwygio )
< /roundbracket>
< roundbracket elementid="20050662w-rb4">
(  Cymru )
< /roundbracket>
2005
< /strong>
< /sentence>
< /p>
< p elementid="20050662w-p3">
< sentence type="heading" elementid="20050662w-s3">
© Hawlfraint y Goron 2005
< /sentence>
< /p>
< p elementid="20050662w-p4">
< sentence type="normal" elementid="20050662w-s4">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20050662w-p5">
< sentence type="normal" elementid="20050662w-s5">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20050662w-s6">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20050662w-p6">
< sentence type="normal" elementid="20050662w-s7">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20050662w-s8">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20050662w-s9">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20050662w-p7">
< sentence type="normal" elementid="20050662w-s10">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20050662w-s11">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan
< english>
The Stationery Office Limited
< /english>
fel
< strong elementid="20050662w-st2">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20050662w-rb5">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20050662w-rb6">
(  Diwygio )
< /roundbracket>
< roundbracket elementid="20050662w-rb7">
(  Cymru )
< /roundbracket>
2005
< /strong>
, ISBN 0110910885 .
< /sentence>
< sentence type="normal" elementid="20050662w-s12">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20050662w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20050662w-s13">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20050662w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20050662w-p8">
< sentence type="normal" elementid="20050662w-s14">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20050662w-a3">
< english>
Frequently Asked Questions
< /english>
< /a> .
< /sentence>
< /p>
< p elementid="20050662w-p9">
< sentence type="normal" elementid="20050662w-s15">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20050662w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20050662w-s16">
Pan welwch fotwm
< doublequotation elementid="20050662w-dq2">
"
< english>
continue
< /continue>
"
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20050662w-p10">
< sentence type="heading" elementid="20050662w-s17">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="heading" elementid="20050662w-s18">
< strong elementid="20050662w-st3">
2005 Rhif 662
< roundbracket elementid="20050662w-rb8">
(  Cy.52 )
< /roundbracket>
< /strong>
< /sentence>
< sentence type="heading" elementid="20050662w-s19">
< strong elementid="20050662w-st4">
GWASANAETHAU CYMORTH GWLADOL , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20050662w-s20">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20050662w-rb9">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20050662w-rb10">
(  Diwygio )
< /roundbracket>
< roundbracket elementid="20050662w-rb11">
(  Cymru )
< /roundbracket>
2005
< /sentence>
< /p>
< table elementid="20050662w-tb1">
< td elementid="20050662w-td1">
< p elementid="20050662w-p11">
< sentence type="heading" elementid="20050662w-s21">
< em elementid="20050662w-em1">
Wedi 'u gwneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20050662w-td2">
< p elementid="20050662w-p12">
< sentence type="heading" elementid="20050662w-s22">
< em elementid="20050662w-em2">
8 Mawrth 2005
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20050662w-td3">
< p elementid="20050662w-p13">
< sentence type="heading" elementid="20050662w-s23">
< em elementid="20050662w-em3">
Yn dod i rym
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20050662w-td4">
< p elementid="20050662w-p14">
< sentence type="heading" elementid="20050662w-s24">
< em elementid="20050662w-em4">
11 Ebrill 2005
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20050662w-p15">
< sentence type="normal" elementid="20050662w-s25">
Mae Cynulliad Cenedlaethol Cymru yn gwneud y Rheoliadau a ganlyn drwy arfer y pwerau a roddwyd i 'r Ysgrifennydd Gwladol gan adran 22
< roundbracket elementid="20050662w-rb12">
(  5 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948
< squarebracket elementid="20050662w-sb1">
[ 
< a type="href" elementid="20050662w-a4">
1
< /a>
]
< /squarebracket>
ac sydd bellach wedi 'u breinio yng Nghynulliad Cenedlaethol Cymru
< squarebracket elementid="20050662w-sb2">
[ 
< a type="href" elementid="20050662w-a5">
2
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="heading" elementid="20050662w-s26">
< strong elementid="20050662w-st5">
Enwi , cychwyn , dehongli a chymhwyso
< /strong>
< /sentence>
< sentence type="normal" elementid="20050662w-s27">
1 .
< /sentence>
< sentence type="normal" elementid="20050662w-s28">
 - 
< roundbracket elementid="20050662w-rb13">
(  1 )
< /roundbracket>
Enw 'r Rheoliadau hyn yw Rheoliadau Cymorth Gwladol
< roundbracket elementid="20050662w-rb14">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20050662w-rb15">
(  Diwygio )
< /roundbracket>
< roundbracket elementid="20050662w-rb16">
(  Cymru )
< /roundbracket>
2005 a deuant i rym ar 11 Ebrill 2005 .
< /sentence>
< sentence type="normal" elementid="20050662w-s29">
< roundbracket elementid="20050662w-rb17">
(  2 )
< /roundbracket>
Yn y Rheoliadau hyn ystyr
< doublequotation elementid="20050662w-dq3">
"  y Prif Reoliadau "
< /doublequotation>
< roundbracket elementid="20050662w-rb18">
(
< doublequotation elementid="20050662w-dq4">
"
< em elementid="20050662w-em5">
< english>
the Principal Regulations
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw Rheoliadau Cymorth Gwladol
< roundbracket elementid="20050662w-rb19">
(  Asesu Adnoddau )
< /roundbracket>
1992
< squarebracket elementid="20050662w-sb3">
[ 
< a type="href" elementid="20050662w-a6">
3
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="normal" elementid="20050662w-s30">
< roundbracket elementid="20050662w-rb20">
(  3 )
< /roundbracket>
Mae 'r Rheoliadau hyn yn gymwys i Gymru 'n unig .
< /sentence>
< sentence type="heading" elementid="20050662w-s31">
Diwygio 'r Prif Reoliadau  -  Terfynau Cyfalaf
< /sentence>
< sentence type="normal" elementid="20050662w-s32">
2 .
< /sentence>
< sentence type="normal" elementid="20050662w-s33">
 - 
< roundbracket elementid="20050662w-rb21">
(  1 )
< /roundbracket>
Diwygir y Prif Reoliadau yn unol â pharagraffau canlynol y rheoliad hwn .
< /sentence>
< sentence type="normal" elementid="20050662w-s34">
< roundbracket elementid="20050662w-rb22">
(  2 )
< /roundbracket>
Dirymir rheoliad 20
< roundbracket elementid="20050662w-rb23">
(  Terfyn cyfalaf )
< /roundbracket>
o ran Cymru .
< /sentence>
< sentence type="heading" elementid="20050662w-s35">
< roundbracket elementid="20050662w-rb24">
(  3 )
< /roundbracket>
Ar ôl rheoliad 20
< roundbracket elementid="20050662w-rb25">
(  Terfyn cyfalaf )
< /roundbracket>
mewnosoder y rheoliad canlynol - 
< /sentence>
< sentence type="heading" elementid="20050662w-s36">
< english>
< strong elementid="20050662w-st6">
20A.
< /strong>
< strong elementid="20050662w-st7">
Capital limit  -  Wales
< /strong>
< /english>
< /sentence>
< sentence type="normal" elementid="20050662w-s37">
< english>
< roundbracket elementid="20050662w-rb26">
(  1 )
< /roundbracket>
This regulation applies in relation to Wales .
< /english>
< /sentence>
< sentence type="normal" elementid="20050662w-s38">
< english>
< roundbracket elementid="20050662w-rb27">
(  2 )
< /roundbracket>
No resident shall be assessed as unable to pay for his accommodation at the standard rate if his capital , calculated in accordance with regulation 21 , exceeds £ 21,000 .
< /english>
< /sentence>
< sentence type="normal" elementid="20050662w-s39">
.
< /sentence>
< /p>
< p elementid="20050662w-p16">
< sentence type="normal" elementid="20050662w-s40">
< roundbracket elementid="20050662w-rb28">
(  4 )
< /roundbracket>
Dirymir rheoliad 28
< roundbracket elementid="20050662w-rb29">
(  Cyfrifo incwm tariff o gyfalaf )
< /roundbracket>
o ran Cymru .
< /sentence>
< sentence type="heading" elementid="20050662w-s41">
< roundbracket elementid="20050662w-rb30">
(  5 )
< /roundbracket>
Ar ôl rheoliad 28
< roundbracket elementid="20050662w-rb31">
(  Cyfrifo incwm tariff o gyfalaf )
< /roundbracket>
, mewnosoder y rheoliad a ganlyn - 
< /sentence>
< /p>
< p elementid="20050662w-p17">
< sentence type="heading" elementid="20050662w-s42">
< english>
< strong elementid="20050662w-st8">
28A.
< /strong>
< strong elementid="20050662w-st9">
Calculation of tariff income from capital  -  Wales
< /strong>
< /english>
< /sentence>
< sentence type="normal" elementid="20050662w-s43">
< english>
< roundbracket elementid="20050662w-rb32">
(  1 )
< /roundbracket>
This regulation applies in relation to Wales .
< /english>
< /sentence>
< sentence type="normal" elementid="20050662w-s44">
< english>
< roundbracket elementid="20050662w-rb33">
(  2 )
< /roundbracket>
Where a resident 's capital calculated in accordance with this part exceeds £ 14,750 it shall be treated as equivalent to a weekly income of £ 1 for each £ 250 in excess of £ 14,750 up to the limit of £ 21,000
< roundbracket elementid="20050662w-rb34">
(  the Capital limit )
< /roundbracket> .
< /english>
< /sentence>
< sentence type="normal" elementid="20050662w-s45">
< english>
< roundbracket elementid="20050662w-rb35">
(  3 )
< /roundbracket>
Where any part of the excess is not a complete £ 250 that part shall nevertheless be treated as equivalent to a weekly income of £ 1 .
< /english>
< /sentence>
< sentence type="normal" elementid="20050662w-s46">
< english>
< roundbracket elementid="20050662w-rb36">
(  4 )
< /roundbracket>
For the purposes of paragraph
< roundbracket elementid="20050662w-rb37">
(  2 )
< /roundbracket>
capital includes any income treated as capital under regulations 22 and 34
< roundbracket elementid="20050662w-rb38">
(  income treated as capital and liable relative payments )
< /roundbracket>
respectively .
< /english>
< /sentence>
< sentence type="normal" elementid="20050662w-s47">
< english>
< roundbracket elementid="20050662w-rb39">
(  5 )
< /roundbracket>
For the purposes of paragraph
< roundbracket elementid="20050662w-rb40">
(  2 )
< /roundbracket>
, where a resident makes additional payments as provided for in regulation 4
< roundbracket elementid="20050662w-rb41">
(  2 )
< /roundbracket>
< roundbracket elementid="20050662w-rb42">
(  b )
< /roundbracket>
of the National Assistance
< roundbracket elementid="20050662w-rb43">
(  Residential Accommodation )
< /roundbracket>
< roundbracket elementid="20050662w-rb44">
(  Additional Payments Relevant Contributions and Assessment of Resources )
< /roundbracket>
< roundbracket elementid="20050662w-rb45">
(  Amendment )
< /roundbracket>
< roundbracket elementid="20050662w-rb46">
(  Wales )
< /roundbracket>
Regulations 2003
< squarebracket elementid="20050662w-sb4">
[ 
< a type="href" elementid="20050662w-a7">
4
< /a>
]
< /squarebracket>
< roundbracket elementid="20050662w-rb47">
(  additional payments from capital not exceeding the lower capital limit )
< /roundbracket>
, the resident is to be treated as possessing capital equivalent to the amount of any additional payments .
< /english>
< /sentence>
< sentence type="normal" elementid="20050662w-s48">
.
< /sentence>
< /p>
< p elementid="20050662w-p18">
< sentence type="heading" elementid="20050662w-s49">
< strong elementid="20050662w-st10">
Diwygio Atodlen 3 i 'r Prif Reoliadau  -  Symiau a ddiystyrir wrth gyfrifo incwm nad yw 'n enillion
< /strong>
< /sentence>
< sentence type="normal" elementid="20050662w-s50">
3 .
< /sentence>
< sentence type="heading" elementid="20050662w-s51">
 - 
< roundbracket elementid="20050662w-rb48">
(  1 )
< /roundbracket>
Ym mharagraff 28H o Atodlen 3 i 'r Prif Reoliadau - 
< /sentence>
< /p>
< p elementid="20050662w-p19">
< sentence type="heading" elementid="20050662w-s52">
< roundbracket elementid="20050662w-rb49">
(  a )
< /roundbracket>
yn lle 'r ffigur
< doublequotation elementid="20050662w-dq5">
"  £ 4.75 "
< /doublequotation>
, bob tro y mae 'n ymddangos , rhodder y ffigur
< doublequotation elementid="20050662w-dq6">
"  £ 4.85 "
< /doublequotation>
;
< /sentence>
< sentence type="normal" elementid="20050662w-s53">
< roundbracket elementid="20050662w-rb50">
(  b )
< /roundbracket>
yn is-baragraffau
< roundbracket elementid="20050662w-rb51">
(  3 )
< /roundbracket>
a
< roundbracket elementid="20050662w-rb52">
(  4 )
< /roundbracket>
, yn lle 'r ffigur
< doublequotation elementid="20050662w-dq7">
"  £ 6.95 "
< /doublequotation>
, bob tro y mae 'n ymddangos , rhodder y ffigur
< doublequotation elementid="20050662w-dq8">
"  £ 7.20 "
< /doublequotation> .
< /sentence>
< /p>
< p elementid="20050662w-p20">
< sentence type="heading" elementid="20050662w-s54">
< roundbracket elementid="20050662w-rb53">
(  2 )
< /roundbracket>
Ar ôl paragraff 28J o Atodlen 3 i 'r Prif Reoliadau , mewnosoder y paragraff a ganlyn - 
< /sentence>
< /p>
< p elementid="20050662w-p21">
< sentence type="normal" elementid="20050662w-s55">
< english>
< strong elementid="20050662w-st11">
28K.
< /strong>
Any payment made pursuant to section 14F of the Children Act 1989
< squarebracket elementid="20050662w-sb5">
[ 
< a type="href" elementid="20050662w-a8">
5
< /a>
]
< /squarebracket>
< roundbracket elementid="20050662w-rb54">
(  Special Guardianship Support Services )
< /roundbracket>
to a resident who is special guardian or a prospective special guardian .
< /english>
< /sentence>
< sentence type="normal" elementid="20050662w-s56">
.
< /sentence>
< /p>
< p elementid="20050662w-p22">
< sentence type="heading" elementid="20050662w-s57">
< strong elementid="20050662w-st12">
Diwygio Atodlen 4 i 'r Prif Reoliadau  -  Symiau a ddiystyrir wrth gyfrifo cyfalaf
< /strong>
< /sentence>
< sentence type="normal" elementid="20050662w-s58">
4 .
< /sentence>
< sentence type="heading" elementid="20050662w-s59">
Ar ôl paragraff 26 o Atodlen 4 i 'r Prif Reoliadau , mewnosoder y paragraff a ganlyn - 
< /sentence>
< /p>
< p elementid="20050662w-p23">
< sentence type="normal" elementid="20050662w-s60">
27 .
< /sentence>
< sentence type="normal" elementid="20050662w-s61">
< english>
Any payment made pursuant to section 14F of the Children Act 1989
< roundbracket elementid="20050662w-rb55">
(  Special Guardianship Support Services )
< /roundbracket>
to a resident who is special guardian or a prospective special guardian .
< /english>
< /sentence>
< sentence type="normal" elementid="20050662w-s62">
.
< /sentence>
< /p>
< p elementid="20050662w-p24">
< sentence type="normal" elementid="20050662w-s63">
Llofnodwyd ar ran Cynulliad Cenedlaethol Cymru o dan adran 66
< roundbracket elementid="20050662w-rb56">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20050662w-sb6">
[ 
< a type="href" elementid="20050662w-a9">
6
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="heading" elementid="20050662w-s64">
< em elementid="20050662w-em6">
D. Elis-Thomas
< /em>
< /sentence>
< sentence type="heading" elementid="20050662w-s65">
Llywydd y Cynulliad Cenedlaethol
< /sentence>
< sentence type="heading" elementid="20050662w-s66">
8 Mawrth 2005
< /sentence>
< /p>
< p elementid="20050662w-p25">
< sentence type="heading" elementid="20050662w-s67">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20050662w-s68">
< em elementid="20050662w-em7">
< roundbracket elementid="20050662w-rb57">
Nid yw 'r nodyn hwn yn rhan o 'r Rheoliadau.)
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20050662w-s69">
Mae 'r Rheoliadau hyn yn gwneud diwygiadau pellach i Reoliadau Cymorth Gwladol
< roundbracket elementid="20050662w-rb58">
(  Asesu Adnoddau )
< /roundbracket>
1992
< roundbracket elementid="20050662w-rb59">
(
< doublequotation elementid="20050662w-dq9">
"  y Prif Reoliadau "
< /doublequotation>
)
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20050662w-s70">
Mae 'r Prif Reoliadau 'n penderfynu 'r ffordd y mae awdurdodau lleol yn asesu gallu person i dalu am y llety y mae awdurdodau lleol yn ei drefnu o dan Ran 3 o Ddeddf Cymorth Gwladol 1948 .
< /sentence>
< sentence type="normal" elementid="20050662w-s71">
Mae rheoliad 2 yn codi 'r terfynau cyfalaf yn y Prif Reoliadau .
< /sentence>
< sentence type="normal" elementid="20050662w-s72">
Mae 'r terfyn cyfalaf uchaf yn gosod y swm cyfalaf pan nad yw preswylydd , os yw ei gyfalaf yn uwch na 'r swm hwnnw , yn gymwys i dderbyn cymorth awdurdod lleol .
< /sentence>
< sentence type="normal" elementid="20050662w-s73">
Mae 'r terfyn cyfalaf isaf yn pennu 'r swm pan nad yw 'n ofynnol i breswylydd , os yw ei gyfalaf yn is na 'r swm hwnnw , gyfrannu o 'i gyfalaf at ei lety .
< /sentence>
< sentence type="normal" elementid="20050662w-s74">
Mae rheoliad 3 yn gwneud diwygiadau i Atodlen 3 i 'r Prif Reoliadau sy 'n nodi ffynonellau incwm penodol y mae 'n rhaid i awdurdodau lleol eu diystyru .
< /sentence>
< sentence type="normal" elementid="20050662w-s75">
Codir lefelau diystyriad credyd cynilion o £ 4.75 i £ 4.85 yn achos person sengl ac o £ 6.95 i £ 7.20 yn achos preswylydd sydd â phartner .
< /sentence>
< sentence type="normal" elementid="20050662w-s76">
Mae diystyriad newydd o ran taliadau a wneir o dan y trefniadau i gynorthwyo
< doublequotation elementid="20050662w-dq10">
"  gwarcheidwaid arbennig "
< /doublequotation>
o dan adran 14F o Ddeddf Plant 1989 .
< /sentence>
< sentence type="normal" elementid="20050662w-s77">
Mae rheoliad 4 yn diwygio Atodlen 4 i 'r Prif Reoliadau fel bod taliadau a wneir i
< doublequotation elementid="20050662w-dq11">
"  warcheidwaid arbennig "
< /doublequotation>
hefyd yn cael eu diystyru fel cyfalaf .
< /sentence>
< sentence type="heading" elementid="20050662w-s78">
< em elementid="20050662w-em8">
< english>
Notes
< /english>
:
< /em>
< /sentence>
< sentence type="normal" elementid="20050662w-s79">
< squarebracket elementid="20050662w-sb7">
[  1 ]
< /squarebracket>
1948 p .
< /sentence>
< sentence type="normal" elementid="20050662w-s80">
29 ; diwygiwyd adran 22
< roundbracket elementid="20050662w-rb60">
(  5 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 gan adran 39
< roundbracket elementid="20050662w-rb61">
(  1 )
< /roundbracket>
o Ddeddf y Weinyddiaeth Nawdd Cymdeithasol 1966
< roundbracket elementid="20050662w-rb62">
(  p.20 )
< /roundbracket>
a pharagraff 6 o Atodlen 6 iddi , gan adran 35
< roundbracket elementid="20050662w-rb63">
(  2 )
< /roundbracket>
o Ddeddf Budd-daliadau Atodol 1976
< roundbracket elementid="20050662w-rb64">
(  p.71 )
< /roundbracket>
a pharagraff 3
< roundbracket elementid="20050662w-rb65">
(  b )
< /roundbracket>
o Atodlen 7 iddi , gan adran 20 o Ddeddf Nawdd Cymdeithasol 1980
< roundbracket elementid="20050662w-rb66">
(  p.30 )
< /roundbracket>
a pharagraff 2 o Atodlen 4 iddi , a chan adran 86 o Ddeddf Nawdd Cymdeithasol 1986
< roundbracket elementid="20050662w-rb67">
(  p.50 )
< /roundbracket>
a pharagraff 32 o Atodlen 10 iddi .
< /sentence>
< sentence type="heading" elementid="20050662w-s81">
< a type="href" elementid="20050662w-a10">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20050662w-p26">
< sentence type="normal" elementid="20050662w-s82">
< squarebracket elementid="20050662w-sb8">
[  2 ]
< /squarebracket>
Trosglwyddwyd swyddogaethau 'r Ysgrifennydd Gwladol o dan adran 22
< roundbracket elementid="20050662w-rb68">
(  5 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 i Gynulliad Cenedlaethol Cymru gan Orchymyn Cynulliad Cenedlaethol Cymru
< roundbracket elementid="20050662w-rb69">
(  Trosglwyddo Swyddogaethau )
< /roundbracket>
1999
< roundbracket elementid="20050662w-rb70">
(  O.S. 1999/ 672 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20050662w-s83">
< a type="href" elementid="20050662w-a11">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20050662w-p27">
< sentence type="normal" elementid="20050662w-s84">
< squarebracket elementid="20050662w-sb9">
[  3 ]
< /squarebracket>
O.S. 1992/ 2977 ; fel y 'i diwygiwyd gan gyfres o offerynnau dilynol .
< /sentence>
< sentence type="heading" elementid="20050662w-s85">
< a type="href" elementid="20050662w-a12">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20050662w-p28">
< sentence type="normal" elementid="20050662w-s86">
< squarebracket elementid="20050662w-sb10">
[  4 ]
< /squarebracket>
O.S. 2003/ 931 .
< /sentence>
< sentence type="heading" elementid="20050662w-s87">
< a type="href" elementid="20050662w-a13">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20050662w-p29">
< sentence type="normal" elementid="20050662w-s88">
< squarebracket elementid="20050662w-sb11">
[  5 ]
< /squarebracket>
1989 p.41 .
< /sentence>
< sentence type="normal" elementid="20050662w-s89">
Caiff adran 14F o Ddeddf Plant 1989 ei mewnosod gan adran 115 of Ddeddf Mabwysiadu a Phlant 2002 p.38 .
< /sentence>
< sentence type="heading" elementid="20050662w-s90">
< a type="href" elementid="20050662w-a14">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20050662w-p30">
< sentence type="normal" elementid="20050662w-s91">
< squarebracket elementid="20050662w-sb12">
[  6 ]
< /squarebracket>
1998 p .
< /sentence>
< sentence type="heading" elementid="20050662w-s92">
38 
< a type="href" elementid="20050662w-a15">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20050662w-p31">
< sentence type="heading" elementid="20050662w-s93">
< a type="href" elementid="20050662w-a16">
< english>
English version
< /english>
< /a>
< /sentence>
< sentence type="heading" elementid="20050662w-s94">
ISBN 0 11 091088 5
< /sentence>
< /p>
< table elementid="20050662w-tb2">
< td elementid="20050662w-td5">
< p elementid="20050662w-p32">
< sentence type="heading" elementid="20050662w-s95">
< a type="href" elementid="20050662w-a17">
< english>
Other UK SIs
< /english>
< /a>
< a type="href" elementid="20050662w-a18">
< english>
Home
< /english>
< /a>
< a type="href" elementid="20050662w-a19">
< english>
National Assembly for Wales Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20050662w-a20">
< english>
Scottish Statutory Instrument
< /english>
< /a>
< a type="href" elementid="20050662w-a21">
< english>
Statutory Rules of Northern Ireland
< /english>
< /a>
< a type="href" elementid="20050662w-a22">
< english>
Her Majesty 's Stationery Office
< /english>
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20050662w-td6">
< p elementid="20050662w-p33">
< sentence type="heading" elementid="20050662w-s96">
< english>
< strong elementid="20050662w-st13">
We welcome your 
< a type="href" elementid="20050662w-a23">
comments
< /a>
on this site
< /strong>
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20050662w-td7">
< p elementid="20050662w-p34">
< sentence type="heading" elementid="20050662w-s97">
< english>
© Crown copyright 2005
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20050662w-td8">
< p elementid="20050662w-p35">
< sentence type="heading" elementid="20050662w-s98">
< english>
Prepared 17 March 2005
< /english>
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>1368< /wordcount>
< /file>
< file elementid="20050663w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Rheoliadau Cymorth Gwladol ( Symiau at Anghenion Personol ) ( Cymru ) 2005< /pagetitle>
< doc_class>Statutory Instrument - Wales< /doc_class>
< year>2005< /year>
< number>0663< /number>
< legislation_code>30102< /legislation_code>
< legislation_type>Statutory Instrument - Wales< /legislation_type>
< subject>GWASANAETHAU CYMORTH GWLADOL, CYMRU< /subject>
< laid_date>_EMPTY< /laid_date>
< made_date>20050308< /made_date>
< coming_into_force>20050411< /coming_into_force>
< isbn>0110910893< /isbn>
< title>Rheoliadau Cymorth Gwladol (Symiau at Anghenion Personol) (Cymru) 2005< /title>
< signatory>D. Elis-Thomas< /signatory>
< signatory_title>Llywydd y Cynulliad Cenedlaethol< /signatory_title>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20050663w-p1">
< sentence type="heading" elementid="20050663w-s1">
Offerynnau Statudol 2005 Rhif 663
< roundbracket elementid="20050663w-rb1">
(  Cy.53 )
< /roundbracket>
< /sentence>
< /p>
< p elementid="20050663w-p2">
< sentence type="heading" elementid="20050663w-s2">
< strong elementid="20050663w-st1">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20050663w-rb2">
(  Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20050663w-rb3">
(  Cymru )
< /roundbracket>
2005
< /strong>
< /sentence>
< /p>
< p elementid="20050663w-p3">
< sentence type="heading" elementid="20050663w-s3">
© Hawlfraint y Goron 2005
< /sentence>
< /p>
< p elementid="20050663w-p4">
< sentence type="normal" elementid="20050663w-s4">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20050663w-p5">
< sentence type="normal" elementid="20050663w-s5">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20050663w-s6">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20050663w-p6">
< sentence type="normal" elementid="20050663w-s7">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20050663w-s8">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20050663w-s9">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20050663w-p7">
< sentence type="normal" elementid="20050663w-s10">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20050663w-s11">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan
< english>
The Stationery Office Limited
< /english>
fel
< strong elementid="20050663w-st2">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20050663w-rb4">
(  Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20050663w-rb5">
(  Cymru )
< /roundbracket>
2005
< /strong>
, ISBN 0110910893 .
< /sentence>
< sentence type="normal" elementid="20050663w-s12">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20050663w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20050663w-s13">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20050663w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20050663w-p8">
< sentence type="normal" elementid="20050663w-s14">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20050663w-a3">
< english>
Frequently Asked Questions
< /english>
< /a> .
< /sentence>
< /p>
< p elementid="20050663w-p9">
< sentence type="normal" elementid="20050663w-s15">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20050663w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20050663w-s16">
Pan welwch fotwm
< doublequotation elementid="20050663w-dq2">
"
< english>
continue
< /continue>
"
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20050663w-p10">
< sentence type="heading" elementid="20050663w-s17">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="heading" elementid="20050663w-s18">
< strong elementid="20050663w-st3">
2005 Rhif 663
< roundbracket elementid="20050663w-rb6">
(  Cy.53 )
< /roundbracket>
< /strong>
< /sentence>
< sentence type="heading" elementid="20050663w-s19">
< strong elementid="20050663w-st4">
GWASANAETHAU CYMORTH GWLADOL , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20050663w-s20">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20050663w-rb7">
(  Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20050663w-rb8">
(  Cymru )
< /roundbracket>
2005
< /sentence>
< /p>
< table elementid="20050663w-tb1">
< td elementid="20050663w-td1">
< p elementid="20050663w-p11">
< sentence type="heading" elementid="20050663w-s21">
< em elementid="20050663w-em1">
Wedi 'u gwneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20050663w-td2">
< p elementid="20050663w-p12">
< sentence type="heading" elementid="20050663w-s22">
< em elementid="20050663w-em2">
8 Mawrth 2005
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20050663w-td3">
< p elementid="20050663w-p13">
< sentence type="heading" elementid="20050663w-s23">
< em elementid="20050663w-em3">
Yn dod i rym
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20050663w-td4">
< p elementid="20050663w-p14">
< sentence type="heading" elementid="20050663w-s24">
< em elementid="20050663w-em4">
11 Ebrill 2005
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20050663w-p15">
< sentence type="normal" elementid="20050663w-s25">
Mae Cynulliad Cenedlaethol Cymru yn gwneud y Rheoliadau a ganlyn drwy arfer y pwerau a roddwyd i 'r Ysgrifennydd Gwladol gan adran 22
< roundbracket elementid="20050663w-rb9">
(  4 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948
< squarebracket elementid="20050663w-sb1">
[ 
< a type="href" elementid="20050663w-a4">
1
< /a>
]
< /squarebracket>
ac sydd bellach wedi 'u breinio yng Nghynulliad Cenedlaethol Cymru
< squarebracket elementid="20050663w-sb2">
[ 
< a type="href" elementid="20050663w-a5">
2
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="heading" elementid="20050663w-s26">
< strong elementid="20050663w-st5">
Enwi , cychwyn a chymhwyso
< /strong>
< /sentence>
< sentence type="normal" elementid="20050663w-s27">
1 .
< /sentence>
< sentence type="normal" elementid="20050663w-s28">
 - 
< roundbracket elementid="20050663w-rb10">
(  1 )
< /roundbracket>
Enw 'r Rheoliadau hyn yw Rheoliadau Cymorth Gwladol
< roundbracket elementid="20050663w-rb11">
(  Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20050663w-rb12">
(  Cymru )
< /roundbracket>
2005 , a deuant i rym ar 11 Ebrill 2005 .
< /sentence>
< sentence type="normal" elementid="20050663w-s29">
< roundbracket elementid="20050663w-rb13">
(  2 )
< /roundbracket>
Mae 'r Rheoliadau hyn yn gymwys i Gymru yn unig .
< /sentence>
< sentence type="heading" elementid="20050663w-s30">
< strong elementid="20050663w-st6">
Y swm y mae ei angen at anghenion personol
< /strong>
< /sentence>
< sentence type="normal" elementid="20050663w-s31">
2 .
< /sentence>
< sentence type="normal" elementid="20050663w-s32">
Y swm y mae awdurdod lleol yn rhagdybio y bydd ei angen ar berson at ei anghenion personol o dan adran 22
< roundbracket elementid="20050663w-rb14">
(  4 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 yw £ 19.10 yr wythnos .
< /sentence>
< sentence type="heading" elementid="20050663w-s33">
< strong elementid="20050663w-st7">
Dirymu
< /strong>
< /sentence>
< sentence type="normal" elementid="20050663w-s34">
3 .
< /sentence>
< sentence type="normal" elementid="20050663w-s35">
Drwy hyn , dirymir Rheoliadau Cymorth Gwladol
< roundbracket elementid="20050663w-rb15">
(  Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20050663w-rb16">
(  Cymru )
< /roundbracket>
2004
< squarebracket elementid="20050663w-sb3">
[ 
< a type="href" elementid="20050663w-a6">
3
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="normal" elementid="20050663w-s36">
Llofnodwyd ar ran Cynulliad Cenedlaethol Cymru o dan adran 66
< roundbracket elementid="20050663w-rb17">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20050663w-sb4">
[ 
< a type="href" elementid="20050663w-a7">
4
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="heading" elementid="20050663w-s37">
< em elementid="20050663w-em5">
D. Elis-Thomas
< /em>
< /sentence>
< sentence type="heading" elementid="20050663w-s38">
Llywydd y Cynulliad Cenedlaethol
< /sentence>
< sentence type="heading" elementid="20050663w-s39">
8 Mawrth 2005
< /sentence>
< sentence type="heading" elementid="20050663w-s40">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20050663w-s41">
< em elementid="20050663w-em6">
< roundbracket elementid="20050663w-rb18">
(  Nid yw 'r nodyn hwn yn rhan o 'r Rheoliadau )
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20050663w-s42">
Mae 'r Rheoliadau hyn yn nodi 'r swm wythnosol y mae awdurdodau lleol yng Nghymru i ragdybio , pan nad oes amgylchiadau arbennig , y bydd ei angen ar breswylwyr sydd mewn llety a drefnwyd o dan Ran III o Ddeddf Cymorth Gwladol 1948 at eu hanghenion personol .
< /sentence>
< sentence type="normal" elementid="20050663w-s43">
0 11 Ebrill 2005 ymlaen , rhagdybir y bydd angen £ 19.10 yr wythnos ar bob preswylydd o 'r fath at eu hanghenion personol .
< /sentence>
< sentence type="normal" elementid="20050663w-s44">
Mae 'r Rheoliadau hyn yn disodli Rheoliadau Cymorth Gwladol
< roundbracket elementid="20050663w-rb19">
(  Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20050663w-rb20">
(  Cymru )
< /roundbracket>
2004 sy 'n cael eu dirymu ac a oedd yn rhagnodi swm wythnosol o £ 18.40 .
< /sentence>
< sentence type="heading" elementid="20050663w-s45">
< em elementid="20050663w-em7">
< english>
Notes
< /english>
:
< /em>
< /sentence>
< sentence type="normal" elementid="20050663w-s46">
< squarebracket elementid="20050663w-sb5">
[  1 ]
< /squarebracket>
1948 p.29 .
< /sentence>
< sentence type="normal" elementid="20050663w-s47">
< em elementid="20050663w-em8">
Gweler
< /em>
adrannau 35
< roundbracket elementid="20050663w-rb21">
(  1 )
< /roundbracket>
a 64
< roundbracket elementid="20050663w-rb22">
(  1 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 ar gyfer y diffiniadau o
< doublequotation elementid="20050663w-dq3">
"
< english>
the Minister
< /english>
"
< /doublequotation>
a
< doublequotation elementid="20050663w-dq4">
"
< english>
prescribed
< /english>
"
< /doublequotation>
yn ôl eu trefn , ac erthygl 2 o Orchymyn yr Ysgrifennydd Gwladol dros Wasanaethau Cymdeithasol 1968
< roundbracket elementid="20050663w-rb23">
(  O.S. 1968/ 1699 )
< /roundbracket>
a drosglwyddodd holl swyddogaethau 'r Gweinidog Iechyd i 'r Ysgrifennydd Gwladol .
< /sentence>
< sentence type="heading" elementid="20050663w-s48">
< a type="href" elementid="20050663w-a8">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20050663w-p16">
< sentence type="normal" elementid="20050663w-s49">
< squarebracket elementid="20050663w-sb6">
[  2 ]
< /squarebracket>
Trosglwyddwyd swyddogaethau 'r Ysgrifennydd Gwladol o dan adran 22
< roundbracket elementid="20050663w-rb24">
(  4 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 i Gynulliad Cenedlaethol Cymru gan Orchymyn Cynulliad Cenedlaethol Cymru
< roundbracket elementid="20050663w-rb25">
(  Trosglwyddo Swyddogaethau )
< /roundbracket>
1999
< roundbracket elementid="20050663w-rb26">
(  O.S. 1999/ 672 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20050663w-s50">
< a type="href" elementid="20050663w-a9">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20050663w-p17">
< sentence type="heading" elementid="20050663w-s51">
< squarebracket elementid="20050663w-sb7">
[  3 ]
< /squarebracket>
O.S. 2004/ 1024
< roundbracket elementid="20050663w-rb27">
(  Cy.121 )
< /roundbracket>

< a type="href" elementid="20050663w-a10">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20050663w-p18">
< sentence type="heading" elementid="20050663w-s52">
< squarebracket elementid="20050663w-sb8">
[  4 ]
< /squarebracket>
1998 p.38 
< a type="href" elementid="20050663w-a11">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20050663w-p19">
< sentence type="heading" elementid="20050663w-s53">
< a type="href" elementid="20050663w-a12">
< english>
English version
< /english>
< /a>
< /sentence>
< sentence type="heading" elementid="20050663w-s54">
ISBN 0 11 091089 3
< /sentence>
< /p>
< table elementid="20050663w-tb2">
< td elementid="20050663w-td5">
< p elementid="20050663w-p20">
< sentence type="heading" elementid="20050663w-s55">
< a type="href" elementid="20050663w-a13">
< english>
Other UK SIs
< /english>
< /a>
< a type="href" elementid="20050663w-a14">
< english>
Home
< /english>
< /a>
< a type="href" elementid="20050663w-a15">
< english>
National Assembly for Wales Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20050663w-a16">
< english>
Scottish Statutory Instrument
< /english>
< /a>
< a type="href" elementid="20050663w-a17">
< english>
Statutory Rules of Northern Ireland
< /english>
< /a>
< a type="href" elementid="20050663w-a18">
< english>
Her Majesty 's Stationery Office
< /english>
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20050663w-td6">
< p elementid="20050663w-p21">
< sentence type="heading" elementid="20050663w-s56">
< english>
< strong elementid="20050663w-st8">
We welcome your 
< a type="href" elementid="20050663w-a19">
comments
< /a>
on this site
< /strong>
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20050663w-td7">
< p elementid="20050663w-p22">
< sentence type="heading" elementid="20050663w-s57">
< english>
© Crown copyright 2005
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20050663w-td8">
< p elementid="20050663w-p23">
< sentence type="heading" elementid="20050663w-s58">
< english>
Prepared 17 March 2005
< /english>
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>748< /wordcount>
< /file>
< file elementid="20053288w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Rheoliadau Cymorth Gwladol ( Asesu Adnoddau ) ( Diwygio Rhif 2 ) ( Cymru ) 2005< /pagetitle>
< doc_class>Statutory Instrument - Wales< /doc_class>
< year>2005< /year>
< number>3288< /number>
< legislation_code>30102< /legislation_code>
< legislation_type>Statutory Instrument - Wales< /legislation_type>
< subject>GWASANAETHAU CYMORTH GWLADOL, CYMRU< /subject>
< laid_date>_EMPTY< /laid_date>
< made_date>20051129< /made_date>
< coming_into_force>20051206< /coming_into_force>
< isbn>0110912314< /isbn>
< title>Rheoliadau Cymorth Gwladol (Asesu Adnoddau) (Diwygio Rhif 2) (Cymru) 2005< /title>
< signatory>D. Elis-Thomas< /signatory>
< signatory_title>Llywydd y Cynulliad Cenedlaethol< /signatory_title>
< GENERATOR>MSHTML 6.00.2900.2995< /GENERATOR>
< /metadata>
< text>
< p elementid="20053288w-p1">
< sentence type="heading" elementid="20053288w-s1">
Offerynnau Statudol 2005 Rhif 3288
< roundbracket elementid="20053288w-rb1">
(  Cy.251 )
< /roundbracket>
< /sentence>
< /p>
< p elementid="20053288w-p2">
< sentence type="heading" elementid="20053288w-s2">
< strong elementid="20053288w-st1">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20053288w-rb2">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20053288w-rb3">
(  Diwygio Rhif 2 )
< /roundbracket>
< roundbracket elementid="20053288w-rb4">
(  Cymru )
< /roundbracket>
2005
< /strong>
< /sentence>
< /p>
< p elementid="20053288w-p3">
< sentence type="heading" elementid="20053288w-s3">
© Hawlfraint y Goron 2005
< /sentence>
< /p>
< p elementid="20053288w-p4">
< sentence type="normal" elementid="20053288w-s4">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20053288w-p5">
< sentence type="normal" elementid="20053288w-s5">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20053288w-s6">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20053288w-p6">
< sentence type="normal" elementid="20053288w-s7">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20053288w-s8">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20053288w-s9">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20053288w-p7">
< sentence type="normal" elementid="20053288w-s10">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20053288w-s11">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan
< english>
The Stationery Office Limited
< /english>
fel
< strong elementid="20053288w-st2">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20053288w-rb5">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20053288w-rb6">
(  Diwygio Rhif 2 )
< /roundbracket>
< roundbracket elementid="20053288w-rb7">
(  Cymru )
< /roundbracket>
2005
< /strong>
, ISBN 0110912314 .
< /sentence>
< sentence type="normal" elementid="20053288w-s12">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20053288w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20053288w-s13">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20053288w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20053288w-p8">
< sentence type="normal" elementid="20053288w-s14">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20053288w-a3">
< english>
Frequently Asked Questions
< /english>
< /a> .
< /sentence>
< /p>
< p elementid="20053288w-p9">
< sentence type="normal" elementid="20053288w-s15">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20053288w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20053288w-s16">
Pan welwch fotwm
< doublequotation elementid="20053288w-dq2">
"
< english>
continue
< /continue>
"
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20053288w-p10">
< sentence type="heading" elementid="20053288w-s17">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="heading" elementid="20053288w-s18">
< strong elementid="20053288w-st3">
2005 Rhif 3288
< roundbracket elementid="20053288w-rb8">
(  Cy.251 )
< /roundbracket>
< /strong>
< /sentence>
< sentence type="heading" elementid="20053288w-s19">
< strong elementid="20053288w-st4">
GWASANAETHAU CYMORTH GWLADOL , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20053288w-s20">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20053288w-rb9">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20053288w-rb10">
(  Diwygio Rhif 2 )
< /roundbracket>
< roundbracket elementid="20053288w-rb11">
(  Cymru )
< /roundbracket>
2005
< /sentence>
< /p>
< table elementid="20053288w-tb1">
< td elementid="20053288w-td1">
< p elementid="20053288w-p11">
< sentence type="heading" elementid="20053288w-s21">
< em elementid="20053288w-em1">
Wedi 'u gwneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20053288w-td2">
< p elementid="20053288w-p12">
< sentence type="heading" elementid="20053288w-s22">
< em elementid="20053288w-em2">
29 Tachwedd 2005
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20053288w-td3">
< p elementid="20053288w-p13">
< sentence type="heading" elementid="20053288w-s23">
< em elementid="20053288w-em3">
Yn dod i rym
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20053288w-td4">
< p elementid="20053288w-p14">
< sentence type="heading" elementid="20053288w-s24">
< em elementid="20053288w-em4">
6 Rhagfyr 2005
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20053288w-p15">
< sentence type="normal" elementid="20053288w-s25">
Mae Cynulliad Cenedlaethol Cymru yn gwneud y Rheoliadau a ganlyn drwy arfer y pwerau a roddwyd i 'r Ysgrifennydd Gwladol gan adran 22
< roundbracket elementid="20053288w-rb12">
(  5 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948
< squarebracket elementid="20053288w-sb1">
[ 
< a type="href" elementid="20053288w-a4">
1
< /a>
]
< /squarebracket>
ac sydd bellach wedi 'u breinio yng Nghynulliad Cenedlaethol Cymru
< squarebracket elementid="20053288w-sb2">
[ 
< a type="href" elementid="20053288w-a5">
2
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="heading" elementid="20053288w-s26">
< strong elementid="20053288w-st5">
Enwi , cychwyn , dehongli a chymhwyso
< /strong>
< /sentence>
< sentence type="normal" elementid="20053288w-s27">
1 .
< /sentence>
< sentence type="normal" elementid="20053288w-s28">
—
< roundbracket elementid="20053288w-rb13">
(  1 )
< /roundbracket>
Enw 'r Rheoliadau hyn yw Rheoliadau Cymorth Gwladol
< roundbracket elementid="20053288w-rb14">
(  Asesu Adnoddau )
< /roundbracket>
< roundbracket elementid="20053288w-rb15">
(  Diwygio Rhif 2 )
< /roundbracket>
< roundbracket elementid="20053288w-rb16">
(  Cymru )
< /roundbracket>
2005 .
< /sentence>
< sentence type="normal" elementid="20053288w-s29">
< roundbracket elementid="20053288w-rb17">
(  2 )
< /roundbracket>
Daw 'r Rheoliadau hyn i rym ar 6 Rhagfyr 2005 .
< /sentence>
< sentence type="normal" elementid="20053288w-s30">
< roundbracket elementid="20053288w-rb18">
(  3 )
< /roundbracket>
Yn y Rheoliadau hyn , ystyr
< doublequotation elementid="20053288w-dq3">
"  y Prif Reoliadau "
< /doublequotation>
< roundbracket elementid="20053288w-rb19">
(
< doublequotation elementid="20053288w-dq4">
"
< em elementid="20053288w-em5">
< english>
the Principal Regulations
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw Rheoliadau Cymorth Gwladol
< roundbracket elementid="20053288w-rb20">
(  Asesu Adnoddau )
< /roundbracket>
1992
< squarebracket elementid="20053288w-sb3">
[ 
< a type="href" elementid="20053288w-a6">
3
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="normal" elementid="20053288w-s31">
< roundbracket elementid="20053288w-rb21">
(  4 )
< /roundbracket>
Mae 'r Rheoliadau hyn yn gymwys i Gymru .
< /sentence>
< sentence type="heading" elementid="20053288w-s32">
Diwygio Atodlen 4 i 'r Prif Reoliadau
< roundbracket elementid="20053288w-rb22">
(  Cyfalaf sydd i 'w ddiystyru )
< /roundbracket>
< /sentence>
< sentence type="normal" elementid="20053288w-s33">
2 .
< /sentence>
< sentence type="heading" elementid="20053288w-s34">
Yn Atodlen 4 i 'r Prif Reoliadau , ar ôl paragraff 27 , ychwaneger y paragraff a ganlyn—
< /sentence>
< sentence type="normal" elementid="20053288w-s35">
28 .
< /sentence>
< sentence type="normal" elementid="20053288w-s36">
< english>
Any payment made to the resident under regulations made under section 7 of the Age-Related Payments Act 2004
< squarebracket elementid="20053288w-sb4">
[ 
< a type="href" elementid="20053288w-a7">
4
< /a>
]
< /squarebracket>
< roundbracket elementid="20053288w-rb23">
(  power to provide future payments )
< /roundbracket> .
< /english>
< /sentence>
< /p>
< p elementid="20053288w-p16">
< sentence type="heading" elementid="20053288w-s37">
Llofnodwyd ar ran Cynulliad Cenedlaethol Cymru o dan adran 66
< roundbracket elementid="20053288w-rb24">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20053288w-sb5">
[ 
< a type="href" elementid="20053288w-a8">
5
< /a>
]
< /squarebracket>
< /sentence>
< sentence type="heading" elementid="20053288w-s38">
< em elementid="20053288w-em6">
D. Elis-Thomas
< /em>
< /sentence>
< sentence type="heading" elementid="20053288w-s39">
Llywydd y Cynulliad Cenedlaethol
< /sentence>
< sentence type="heading" elementid="20053288w-s40">
29 Tachwedd 2005
< /sentence>
< /p>
< p elementid="20053288w-p17">
< sentence type="heading" elementid="20053288w-s41">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20053288w-s42">
< em elementid="20053288w-em7">
< roundbracket elementid="20053288w-rb25">
(  Nid yw 'r nodyn hwn yn rhan o 'r Rheoliadau )
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20053288w-s43">
Mae 'r Rheoliadau hyn yn gwneud diwygiadau pellach i Reoliadau Cymorth Gwladol
< roundbracket elementid="20053288w-rb26">
(  Asesu Adnoddau )
< /roundbracket>
1992
< roundbracket elementid="20053288w-rb27">
(
< doublequotation elementid="20053288w-dq5">
"  y Prif Reoliadau "
< /doublequotation>
)
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20053288w-s44">
Mae 'r Prif Reoliadau yn penderfynu 'r modd y mae awdurdodau lleol yn asesu gallu person i dalu am y llety y mae awdurdodau lleol yn ei drefnu o dan Ran III o Ddeddf Cymorth Gwladol 1948 .
< /sentence>
< sentence type="normal" elementid="20053288w-s45">
Mae rheoliad 2 yn diwygio 'r Prif Reoliadau i ganiatáu i daliadau penodol ar sail oedran a wneir o dan adran 7 o Ddeddf Taliadau ar Sail Oed 2004 gael eu diystyru fel cyfalaf gan awdurdodau lleol .
< /sentence>
< sentence type="heading" elementid="20053288w-s46">
< em elementid="20053288w-em8">
< english>
Notes
< /english>
:
< /em>
< /sentence>
< sentence type="normal" elementid="20053288w-s47">
< squarebracket elementid="20053288w-sb6">
[  1 ]
< /squarebracket>
1948 p .
< /sentence>
< sentence type="normal" elementid="20053288w-s48">
29 ; diwygiwyd adran 22
< roundbracket elementid="20053288w-rb28">
(  5 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 gan adran 39
< roundbracket elementid="20053288w-rb29">
(  1 )
< /roundbracket>
o Ddeddf y Weinyddiaeth Nawdd Cymdeithasol 1966
< roundbracket elementid="20053288w-rb30">
(  p.20 )
< /roundbracket>
a pharagraff 6 o Atodlen 6 iddi , gan adran 35
< roundbracket elementid="20053288w-rb31">
(  2 )
< /roundbracket>
o Ddeddf Budd-daliadau Atodol 1976
< roundbracket elementid="20053288w-rb32">
(  p.71 )
< /roundbracket>
a pharagraff 3
< roundbracket elementid="20053288w-rb33">
(  b )
< /roundbracket>
o Atodlen 7 iddi , gan adran 20 o Ddeddf Nawdd Cymdeithasol 1980
< roundbracket elementid="20053288w-rb34">
(  p.30 )
< /roundbracket>
a pharagraff 2 o Atodlen 4 iddi , a chan adran 86 o Ddeddf Nawdd Cymdeithasol 1986
< roundbracket elementid="20053288w-rb35">
(  p.50 )
< /roundbracket>
a pharagraff 32 o Atodlen 10 iddi .
< /sentence>
< sentence type="heading" elementid="20053288w-s49">
< a type="href" elementid="20053288w-a9">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20053288w-p18">
< sentence type="normal" elementid="20053288w-s50">
< squarebracket elementid="20053288w-sb7">
[  2 ]
< /squarebracket>
Trosglwyddwyd swyddogaethau 'r Ysgrifennydd Gwladol o dan adran 22
< roundbracket elementid="20053288w-rb36">
(  5 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 i Gynulliad Cenedlaethol Cymru gan Orchymyn Cynulliad Cenedlaethol Cymru
< roundbracket elementid="20053288w-rb37">
(  Trosglwyddo Swyddogaethau )
< /roundbracket>
1999
< roundbracket elementid="20053288w-rb38">
(  O.S. 1999/ 672 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20053288w-s51">
< a type="href" elementid="20053288w-a10">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20053288w-p19">
< sentence type="normal" elementid="20053288w-s52">
< squarebracket elementid="20053288w-sb8">
[  3 ]
< /squarebracket>
O.S. 1992/ 2977 ; ac O.S. 1996/ 602 , O.S. 2002/ 814
< roundbracket elementid="20053288w-rb39">
(  Cy.94 )
< /roundbracket>
ac O.S. 2003/ 897  Cy .
< /sentence>
< sentence type="normal" elementid="20053288w-s53">
117 ) yw 'r offerynnau diwygio perthnasol .
< /sentence>
< sentence type="heading" elementid="20053288w-s54">
< a type="href" elementid="20053288w-a11">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20053288w-p20">
< sentence type="normal" elementid="20053288w-s55">
< squarebracket elementid="20053288w-sb9">
[  4 ]
< /squarebracket>
2004 p.10 .
< /sentence>
< sentence type="heading" elementid="20053288w-s56">
< a type="href" elementid="20053288w-a12">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20053288w-p21">
< sentence type="normal" elementid="20053288w-s57">
< squarebracket elementid="20053288w-sb10">
[  5 ]
< /squarebracket>
1998 p.38 .
< /sentence>
< sentence type="heading" elementid="20053288w-s58">
< a type="href" elementid="20053288w-a13">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20053288w-p22">
< sentence type="heading" elementid="20053288w-s59">
< a type="href" elementid="20053288w-a14">
< english>
English version
< /english>
< /a>
< /sentence>
< sentence type="heading" elementid="20053288w-s60">
ISBN 0 11 091231 4
< /sentence>
< /p>
< table elementid="20053288w-tb2">
< td elementid="20053288w-td5">
< p elementid="20053288w-p23">
< sentence type="heading" elementid="20053288w-s61">
< a type="href" elementid="20053288w-a15">
< english>
Other UK SIs
< /english>
< /a>
< a type="href" elementid="20053288w-a16">
< english>
Home
< /english>
< /a>
< a type="href" elementid="20053288w-a17">
< english>
National Assembly for Wales Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20053288w-a18">
< english>
Scottish Statutory Instrument
< /english>
< /a>
< a type="href" elementid="20053288w-a19">
< english>
Statutory Rules of Northern Ireland
< /english>
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20053288w-td6">
< p elementid="20053288w-p24">
< sentence type="heading" elementid="20053288w-s62">
< english>
< strong elementid="20053288w-st6">
We welcome your 
< a type="href" elementid="20053288w-a20">
comments
< /a>
on this site
< /strong>
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20053288w-td7">
< p elementid="20053288w-p25">
< sentence type="heading" elementid="20053288w-s63">
< english>
© Crown copyright 2005
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20053288w-td8">
< p elementid="20053288w-p26">
< sentence type="heading" elementid="20053288w-s64">
< english>
Prepared 6 December 2005
< /english>
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>812< /wordcount>
< /file>
< file elementid="20061051w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Rheoliadau Cymorth Gwladol ( Asesu Adnoddau a Symiau at Anghenion Personol ) ( Diwygio ) ( Cymru ) 2006< /pagetitle>
< doc_class>Statutory Instrument - Wales< /doc_class>
< year>2006< /year>
< number>1051< /number>
< legislation_code>30102< /legislation_code>
< legislation_type>Statutory Instrument - Wales< /legislation_type>
< subject>GWASANAETHAU CYMORTH GWLADOL, CYMRU< /subject>
< laid_date>_EMPTY< /laid_date>
< made_date>20060404< /made_date>
< coming_into_force>20060410< /coming_into_force>
< isbn>0110913205< /isbn>
< title>Rheoliadau Cymorth Gwladol (Asesu Adnoddau a Symiau at Anghenion Personol) (Diwygio) (Cymru) 2006< /title>
< signatory>D. Elis—Thomas< /signatory>
< signatory_title>Llywydd y Cynulliad Cenedlaethol< /signatory_title>
< GENERATOR>MSHTML 6.00.2900.2995< /GENERATOR>
< /metadata>
< text>
< p elementid="20061051w-p1">
< sentence type="heading" elementid="20061051w-s1">
Offerynnau Statudol 2006 Rhif 1051
< roundbracket elementid="20061051w-rb1">
(  Cy.107 )
< /roundbracket>
< /sentence>
< /p>
< p elementid="20061051w-p2">
< sentence type="heading" elementid="20061051w-s2">
< strong elementid="20061051w-st1">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20061051w-rb2">
(  Asesu Adnoddau a Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20061051w-rb3">
(  Diwygio )
< /roundbracket>
< roundbracket elementid="20061051w-rb4">
(  Cymru )
< /roundbracket>
2006
< /strong>
< /sentence>
< /p>
< p elementid="20061051w-p3">
< sentence type="heading" elementid="20061051w-s3">
© Hawlfraint y Goron 2006
< /sentence>
< /p>
< p elementid="20061051w-p4">
< sentence type="normal" elementid="20061051w-s4">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20061051w-p5">
< sentence type="normal" elementid="20061051w-s5">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20061051w-s6">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20061051w-p6">
< sentence type="normal" elementid="20061051w-s7">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20061051w-s8">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20061051w-s9">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20061051w-p7">
< sentence type="normal" elementid="20061051w-s10">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20061051w-s11">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan
< english>
The Stationery Office Limited
< /english>
fel
< strong elementid="20061051w-st2">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20061051w-rb5">
(  Asesu Adnoddau a Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20061051w-rb6">
(  Diwygio )
< /roundbracket>
< roundbracket elementid="20061051w-rb7">
(  Cymru )
< /roundbracket>
2006
< /strong>
, ISBN 0110913205 .
< /sentence>
< sentence type="normal" elementid="20061051w-s12">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20061051w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20061051w-s13">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20061051w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20061051w-p8">
< sentence type="normal" elementid="20061051w-s14">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20061051w-a3">
< english>
Frequently Asked Questions
< /english>
< /a> .
< /sentence>
< /p>
< p elementid="20061051w-p9">
< sentence type="normal" elementid="20061051w-s15">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20061051w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20061051w-s16">
Pan welwch fotwm
< doublequotation elementid="20061051w-dq2">
"
< english>
continue
< /continue>
"
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20061051w-p10">
< sentence type="heading" elementid="20061051w-s17">
STATUTORYOFFERYNNAU STATUDOL
< /sentence>
< sentence type="heading" elementid="20061051w-s18">
< strong elementid="20061051w-st3">
2006 Rhif 1051
< roundbracket elementid="20061051w-rb8">
(  Cy.107 )
< /roundbracket>
< /strong>
< /sentence>
< sentence type="heading" elementid="20061051w-s19">
< strong elementid="20061051w-st4">
GWASANAETHAU CYMORTH GWLADOL , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20061051w-s20">
Rheoliadau Cymorth Gwladol
< roundbracket elementid="20061051w-rb9">
(  Asesu Adnoddau a Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20061051w-rb10">
(  Diwygio )
< /roundbracket>
< roundbracket elementid="20061051w-rb11">
(  Cymru )
< /roundbracket>
2006
< /sentence>
< /p>
< table elementid="20061051w-tb1">
< td elementid="20061051w-td1">
< p elementid="20061051w-p11">
< sentence type="heading" elementid="20061051w-s21">
< em elementid="20061051w-em1">
Wedi 'u gwneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20061051w-td2">
< p elementid="20061051w-p12">
< sentence type="heading" elementid="20061051w-s22">
< em elementid="20061051w-em2">
4 Ebrill 2006
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20061051w-td3">
< p elementid="20061051w-p13">
< sentence type="heading" elementid="20061051w-s23">
< em elementid="20061051w-em3">
Yn dod i rym
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20061051w-td4">
< p elementid="20061051w-p14">
< sentence type="heading" elementid="20061051w-s24">
< em elementid="20061051w-em4">
10 Ebrill 2006
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20061051w-p15">
< sentence type="normal" elementid="20061051w-s25">
Mae Cynulliad Cenedlaethol Cymru , drwy arfer y pwerau a roddwyd i 'r Ysgrifennydd Gwladol gan adrannau 22
< roundbracket elementid="20061051w-rb12">
(  4 )
< /roundbracket>
a
< roundbracket elementid="20061051w-rb13">
(  5 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948
< squarebracket elementid="20061051w-sb1">
[ 
< a type="href" elementid="20061051w-a4">
1
< /a>
]
< /squarebracket>
ac sydd bellach wedi 'u breinio yng Nghynulliad Cenedlaethol Cymru
< squarebracket elementid="20061051w-sb2">
[ 
< a type="href" elementid="20061051w-a5">
2
< /a>
]
< /squarebracket>
drwy hyn yn gwneud y Rheoliadau a ganlyn .
< /sentence>
< sentence type="heading" elementid="20061051w-s26">
< strong elementid="20061051w-st5">
Enwi , cychwyn , dehongli a chymhwyso
< /strong>
< /sentence>
< sentence type="normal" elementid="20061051w-s27">
1 .
< /sentence>
< sentence type="normal" elementid="20061051w-s28">
—
< roundbracket elementid="20061051w-rb14">
(  1 )
< /roundbracket>
Enw 'r Rheoliadau hyn yw Rheoliadau Cymorth Gwladol
< roundbracket elementid="20061051w-rb15">
(  Asesu Adnoddau a Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20061051w-rb16">
(  Diwygio )
< /roundbracket>
< roundbracket elementid="20061051w-rb17">
(  Cymru )
< /roundbracket>
2006 .
< /sentence>
< sentence type="normal" elementid="20061051w-s29">
< roundbracket elementid="20061051w-rb18">
(  2 )
< /roundbracket>
Daw 'r Rheoliadau hyn i rym ar 10 Ebrill 2006 .
< /sentence>
< sentence type="normal" elementid="20061051w-s30">
< roundbracket elementid="20061051w-rb19">
(  3 )
< /roundbracket>
Yn y Rheoliadau hyn , ystyr
< doublequotation elementid="20061051w-dq3">
"  y Prif Reoliadau "
< /doublequotation>
< roundbracket elementid="20061051w-rb20">
(
< em elementid="20061051w-em5">
< doublequotation elementid="20061051w-dq4">
"
< english>
the Principal Regulations
< /english>
"
< /doublequotation>
< /em>
)
< /roundbracket>
yw Rheoliadau Cymorth Gwladol
< roundbracket elementid="20061051w-rb21">
(  Asesu Adnoddau )
< /roundbracket>
1992
< squarebracket elementid="20061051w-sb3">
[ 
< a type="href" elementid="20061051w-a6">
3
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="normal" elementid="20061051w-s31">
< roundbracket elementid="20061051w-rb22">
(  4 )
< /roundbracket>
Mae 'r Rheoliadau hyn yn gymwys o ran Cymru .
< /sentence>
< sentence type="heading" elementid="20061051w-s32">
Symiau y mae eu hangen at anghenion personol
< /sentence>
< sentence type="normal" elementid="20061051w-s33">
2 .
< /sentence>
< sentence type="normal" elementid="20061051w-s34">
Y swm y mae awdurdod lleol yn rhagdybio y bydd ei angen ar berson at ei anghenion personol o dan adran 22
< roundbracket elementid="20061051w-rb23">
(  4 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948
< squarebracket elementid="20061051w-sb4">
[ 
< a type="href" elementid="20061051w-a7">
4
< /a>
]
< /squarebracket>
yw £ 20.00 yr wythnos .
< /sentence>
< sentence type="heading" elementid="20061051w-s35">
Dirymu
< /sentence>
< sentence type="normal" elementid="20061051w-s36">
3 .
< /sentence>
< sentence type="normal" elementid="20061051w-s37">
Mae Rheoliadau Cymorth Gwladol
< roundbracket elementid="20061051w-rb24">
(  Symiau at Anghenion Personol )
< /roundbracket>
< roundbracket elementid="20061051w-rb25">
(  Cymru )
< /roundbracket>
2005
< squarebracket elementid="20061051w-sb5">
[ 
< a type="href" elementid="20061051w-a8">
5
< /a>
]
< /squarebracket>
drwy hyn wedi 'u dirymu .
< /sentence>
< sentence type="heading" elementid="20061051w-s38">
Diwygio rheoliad 2 o 'r Prif Reoliadau
< /sentence>
< sentence type="normal" elementid="20061051w-s39">
4 .
< /sentence>
< sentence type="normal" elementid="20061051w-s40">
Ym mharagraff
< roundbracket elementid="20061051w-rb26">
(  1 )
< /roundbracket>
o reoliad 2 o 'r Prif Reoliadau
< roundbracket elementid="20061051w-rb27">
(  dehongli )
< /roundbracket>
, yn y diffiniad o
< doublequotation elementid="20061051w-dq5">
"
< english>
liable relative
< /english>
"
< /doublequotation>
, yn lle
< doublequotation elementid="20061051w-dq6">
"
< english>
or former spouse
< /english>
"
< /doublequotation>
, rhodder
< doublequotation elementid="20061051w-dq7">
"
< english>
, former spouse , civil partner or former civil partner
< /english>
"
< /doublequotation> .
< /sentence>
< sentence type="heading" elementid="20061051w-s41">
< strong elementid="20061051w-st6">
Diwygio rheoliad 20A o 'r Prif Reoliadau
< /strong>
< /sentence>
< sentence type="normal" elementid="20061051w-s42">
5 .
< /sentence>
< sentence type="normal" elementid="20061051w-s43">
Ym mharagraff
< roundbracket elementid="20061051w-rb28">
(  2 )
< /roundbracket>
o reoliad 20A o 'r Prif Reoliadau yn lle 'r ffigur
< doublequotation elementid="20061051w-dq8">
"  £ 21,000 "
< /doublequotation>
rhodder y ffigur
< doublequotation elementid="20061051w-dq9">
"  £ 21,500 "
< /doublequotation> .
< /sentence>
< sentence type="heading" elementid="20061051w-s44">
< strong elementid="20061051w-st7">
Diwygio rheoliad 28A o 'r Prif Reoliadau
< /strong>
< /sentence>
< sentence type="normal" elementid="20061051w-s45">
6 .
< /sentence>
< sentence type="heading" elementid="20061051w-s46">
Ym mharagraff
< roundbracket elementid="20061051w-rb29">
(  2 )
< /roundbracket>
o reoliad 28A o 'r Prif Reoliadau
< roundbracket elementid="20061051w-rb30">
(  cyfrifo incwm tariff o gyfalaf - Cymru )
< /roundbracket>
—
< /sentence>
< sentence type="heading" elementid="20061051w-s47">
< roundbracket elementid="20061051w-rb31">
(  a )
< /roundbracket>
yn lle 'r ffigur
< doublequotation elementid="20061051w-dq10">
"  £ 14,750 "
< /doublequotation>
rhodder y ffigur
< doublequotation elementid="20061051w-dq11">
"  £ 16,000 "
< /doublequotation>
yn y ddau le y mae 'n ymddangos ; a
< /sentence>
< sentence type="normal" elementid="20061051w-s48">
< roundbracket elementid="20061051w-rb32">
(  b )
< /roundbracket>
yn lle 'r ffigur
< doublequotation elementid="20061051w-dq12">
"  £ 21,000 "
< /doublequotation>
rhodder y ffigur
< doublequotation elementid="20061051w-dq13">
"  £ 21,500 "
< /doublequotation> .
< /sentence>
< /p>
< p elementid="20061051w-p16">
< sentence type="heading" elementid="20061051w-s49">
< strong elementid="20061051w-st8">
Diwygio Atodlen 3 i 'r Prif Reoliadau
< /strong>
< /sentence>
< sentence type="normal" elementid="20061051w-s50">
7 .
< /sentence>
< sentence type="heading" elementid="20061051w-s51">
Yn Atodlen 3 i 'r Prif Reoliadau
< roundbracket elementid="20061051w-rb33">
(  symiau i 'w diystyru wrth gyfrifo incwm nad yw 'n enillion )
< /roundbracket>
—
< /sentence>
< /p>
< p elementid="20061051w-p17">
< sentence type="heading" elementid="20061051w-s52">
< roundbracket elementid="20061051w-rb34">
(  a )
< /roundbracket>
Ym mharagraff 10A
< squarebracket elementid="20061051w-sb6">
[ 
< a type="href" elementid="20061051w-a9">
6
< /a>
]
< /squarebracket>
—
< /sentence>
< sentence type="heading" elementid="20061051w-s53">
< roundbracket elementid="20061051w-rb35">
(  i )
< /roundbracket>
yn is-baragraffau
< roundbracket elementid="20061051w-rb36">
(  1 )
< /roundbracket>
< roundbracket elementid="20061051w-rb37">
(  a )
< /roundbracket>
a
< roundbracket elementid="20061051w-rb38">
(  3 )
< /roundbracket>
, ar
< roundbracket elementid="20061051w-rb39">
(  tm )
< /roundbracket>
l
< doublequotation elementid="20061051w-dq14">
"
< english>
spouse
< /english>
"
< /doublequotation>
mewnosoder
< doublequotation elementid="20061051w-dq15">
"
< english>
or civil partner
< /english>
"
< /doublequotation>
, a
< /sentence>
< sentence type="heading" elementid="20061051w-s54">
< roundbracket elementid="20061051w-rb40">
(  ii )
< /roundbracket>
yn is-baragraff
< roundbracket elementid="20061051w-rb41">
(  1 )
< /roundbracket>
< roundbracket elementid="20061051w-rb42">
(  b )
< /roundbracket>
, ar
< roundbracket elementid="20061051w-rb43">
(  tm )
< /roundbracket>
l
< doublequotation elementid="20061051w-dq16">
"
< english>
spouse 's maintenance
< /english>
"
< /doublequotation>
mewnosoder
< doublequotation elementid="20061051w-dq17">
"
< english>
or his civil partner for that civil partner 's maintenance
< /english>
"
< /doublequotation>
;
< /sentence>
< /p>
< p elementid="20061051w-p18">
< sentence type="heading" elementid="20061051w-s55">
< roundbracket elementid="20061051w-rb44">
(  b )
< /roundbracket>
ym mharagraff 17
< squarebracket elementid="20061051w-sb7">
[ 
< a type="href" elementid="20061051w-a10">
7
< /a>
]
< /squarebracket>
—
< /sentence>
< /p>
< p elementid="20061051w-p19">
< sentence type="heading" elementid="20061051w-s56">
< roundbracket elementid="20061051w-rb45">
(  i )
< /roundbracket>
yn lle is-baragraff
< roundbracket elementid="20061051w-rb46">
(  1 )
< /roundbracket>
< roundbracket elementid="20061051w-rb47">
(  a )
< /roundbracket>
rhodder—
< /sentence>
< sentence type="heading" elementid="20061051w-s57">
< doublequotation elementid="20061051w-dq18">
"
< english>
< roundbracket elementid="20061051w-rb48">
(  a )
< /roundbracket>
pursuant to regulations made under section 2
< roundbracket elementid="20061051w-rb49">
(  6 )
< /roundbracket>
< roundbracket elementid="20061051w-rb50">
(  b )
< /roundbracket>
or 3 of the Adoption and Children Act 2002
< squarebracket elementid="20061051w-sb8">
[ 
< a type="href" elementid="20061051w-a11">
8
< /a>
]
< /squarebracket>
< /english>
"
< /doublequotation>
; a
< /sentence>
< sentence type="heading" elementid="20061051w-s58">
< roundbracket elementid="20061051w-rb51">
(  ii )
< /roundbracket>
yn lle is-baragraff
< roundbracket elementid="20061051w-rb52">
(  2 )
< /roundbracket>
rhodder—
< /sentence>
< sentence type="normal" elementid="20061051w-s59">
< english>
< roundbracket elementid="20061051w-rb53">
(  2 )
< /roundbracket>
Any payment other than a payment to which sub-paragraph
< roundbracket elementid="20061051w-rb54">
(  1 )
< /roundbracket>
< roundbracket elementid="20061051w-rb55">
(  a )
< /roundbracket>
applies , made to the resident pursuant to regulations made under section 2
< roundbracket elementid="20061051w-rb56">
(  6 )
< /roundbracket>
< roundbracket elementid="20061051w-rb57">
(  b )
< /roundbracket>
or 3 of the Adoption and Children Act 2002 .
< /english>
< /sentence>
< sentence type="heading" elementid="20061051w-s60">
;
< /sentence>
< sentence type="heading" elementid="20061051w-s61">
< roundbracket elementid="20061051w-rb58">
(  c )
< /roundbracket>
ym mharagraff 28H
< /sentence>
< sentence type="heading" elementid="20061051w-s62">
< roundbracket elementid="20061051w-rb59">
(  i )
< /roundbracket>
yn is-baragraffau
< roundbracket elementid="20061051w-rb60">
(  1 )
< /roundbracket>
a
< roundbracket elementid="20061051w-rb61">
(  2 )
< /roundbracket>
yn lle 'r ffigur
< doublequotation elementid="20061051w-dq19">
"  £ 4.85 "
< /doublequotation>
rhodder y ffigur
< doublequotation elementid="20061051w-dq20">
"  £ 5.05 "
< /doublequotation>
ym mhob man y mae 'n ymddangos ; a
< /sentence>
< sentence type="normal" elementid="20061051w-s63">
< roundbracket elementid="20061051w-rb62">
(  ii )
< /roundbracket>
yn is-baragraffau
< roundbracket elementid="20061051w-rb63">
(  3 )
< /roundbracket>
a
< roundbracket elementid="20061051w-rb64">
(  4 )
< /roundbracket>
yn lle 'r ffigur
< doublequotation elementid="20061051w-dq21">
"  £ 7.20 "
< /doublequotation>
rhodder y ffigur
< doublequotation elementid="20061051w-dq22">
"  £ 7.50 "
< /doublequotation>
ym mhob man y mae 'n ymddangos .
< /sentence>
< /p>
< p elementid="20061051w-p20">
< sentence type="heading" elementid="20061051w-s64">
< strong elementid="20061051w-st9">
Diwygio Atodlen 4 i 'r Prif Reoliadau
< /strong>
< /sentence>
< sentence type="normal" elementid="20061051w-s65">
8 .
< /sentence>
< sentence type="heading" elementid="20061051w-s66">
Yn Atodlen 4 i 'r Prif Reoliadau
< roundbracket elementid="20061051w-rb65">
(  cyfalaf i 'w ddiystyru )
< /roundbracket>
yn lle paragraff 24 rhodder—
< /sentence>
< /p>
< p elementid="20061051w-p21">
< sentence type="normal" elementid="20061051w-s67">
24 .
< /sentence>
< sentence type="normal" elementid="20061051w-s68">
< english>
Any payment made to the resident pursuant to regulations made under section 2
< roundbracket elementid="20061051w-rb66">
(  6 )
< /roundbracket>
< roundbracket elementid="20061051w-rb67">
(  b )
< /roundbracket>
or 3 of the Adoption and Children Act 2002 .
< /english>
< /sentence>
< sentence type="normal" elementid="20061051w-s69">
.
< /sentence>
< /p>
< p elementid="20061051w-p22">
< sentence type="heading" elementid="20061051w-s70">
Llofnodwyd ar ran Cynulliad Cenedlaethol Cymru o dan adran 66
< roundbracket elementid="20061051w-rb68">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20061051w-sb9">
[ 
< a type="href" elementid="20061051w-a12">
9
< /a>
]
< /squarebracket>
< /sentence>
< sentence type="heading" elementid="20061051w-s71">
< em elementid="20061051w-em6">
D. Elis—Thomas
< /em>
< /sentence>
< sentence type="heading" elementid="20061051w-s72">
Llywydd y Cynulliad Cenedlaethol
< /sentence>
< sentence type="heading" elementid="20061051w-s73">
4 Ebrill 2006
< /sentence>
< /p>
< p elementid="20061051w-p23">
< sentence type="heading" elementid="20061051w-s74">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20061051w-s75">
< em elementid="20061051w-em7">
< roundbracket elementid="20061051w-rb69">
(  Nid yw 'r nodyn hwn yn rhan o 'r Rheoliadau )
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20061051w-s76">
Mae 'r Rheoliadau hyn yn pennu 'r swm wythnosol y mae awdurdodau lleol i ragdybio , yn niffyg amgylchiadau arbennig , y bydd ar breswylwyr , sydd mewn llety a drefnwyd o dan Ran 3 o Ddeddf Cymorth Gwladol 1948 , ei angen at eu hanghenion personol .
< /sentence>
< sentence type="normal" elementid="20061051w-s77">
O 10 Ebrill 2006 ymlaen , rhagdybir y bydd ar bob preswylydd o 'r fath angen £ 20.00 yr wythnos .
< /sentence>
< sentence type="normal" elementid="20061051w-s78">
Yn ail , mae 'r Rheoliadau hyn yn gwneud diwygiadau pellach i Reoliadau Cymorth Gwladol
< roundbracket elementid="20061051w-rb70">
(  Asesu Adnoddau )
< /roundbracket>
1992
< roundbracket elementid="20061051w-rb71">
(
< doublequotation elementid="20061051w-dq23">
"  y Prif Reoliadau "
< /doublequotation>
)
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20061051w-s79">
Mae 'r Prif Reoliadau 'n penderfynu 'r ffordd y mae awdurdodau lleol yn asesu gallu person i dalu am y llety a drefnwyd ar ei gyfer o dan Ran 3 o Ddeddf Cymorth Gwladol 1948 .
< /sentence>
< sentence type="normal" elementid="20061051w-s80">
Mae 'r diwygiadau yn darparu i bartneriaid sifil gael eu trin yn yr un modd ‰ phobl briod ac i daliadau cymorth mabwysiadu o dan Ddeddf Mabwysiadu a Phlant 2002 gael eu diystyru fel cyfalaf ac fel incwm .
< /sentence>
< sentence type="normal" elementid="20061051w-s81">
Mae codiadau blynyddol hefyd yn y terfynau cyfalaf a 'r diystyriad ar gyfer y rhai sy 'n cael credyd pensiwn .
< /sentence>
< sentence type="heading" elementid="20061051w-s82">
< em elementid="20061051w-em8">
< english>
Notes
< /english>
:
< /em>
< /sentence>
< sentence type="normal" elementid="20061051w-s83">
< squarebracket elementid="20061051w-sb10">
[  1 ]
< /squarebracket>
1948 p.29 .
< /sentence>
< sentence type="normal" elementid="20061051w-s84">
Gweler adrannau 35
< roundbracket elementid="20061051w-rb72">
(  1 )
< /roundbracket>
a 64
< roundbracket elementid="20061051w-rb73">
(  1 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 i gael y diffiniadau o
< doublequotation elementid="20061051w-dq24">
"
< english>
the minister
< /english>
"
< /doublequotation>
a
< doublequotation elementid="20061051w-dq25">
"
< english>
prescribed
< /english>
"
< /doublequotation>
yn y drefn honno ac erthygl 2 o Orchymyn yr Ysgrifennydd Gwladol dros Wasanaethau Cymdeithasol 1968
< roundbracket elementid="20061051w-rb74">
(  O.S.1968/ 1699 )
< /roundbracket>
a drosglwyddodd holl swyddogaethau 'r Gweinidog Iechyd i 'r Ysgrifennydd Gwladol .
< /sentence>
< sentence type="heading" elementid="20061051w-s85">
< a type="href" elementid="20061051w-a13">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20061051w-p24">
< sentence type="normal" elementid="20061051w-s86">
< squarebracket elementid="20061051w-sb11">
[  2 ]
< /squarebracket>
Trosglwyddwyd swyddogaethau 'r Ysgrifennydd Gwladol o dan adran 22
< roundbracket elementid="20061051w-rb75">
(  4 )
< /roundbracket>
a
< roundbracket elementid="20061051w-rb76">
(  5 )
< /roundbracket>
o Ddeddf Cymorth Gwladol 1948 i Gynulliad Cenedlaethol Cymru gan Orchymyn Cynulliad Cenedlaethol Cymru
< roundbracket elementid="20061051w-rb77">
(  Trosglwyddo Swyddogaethau )
< /roundbracket>
1999
< roundbracket elementid="20061051w-rb78">
(  O.S. 1999/ 672 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20061051w-s87">
< a type="href" elementid="20061051w-a14">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20061051w-p25">
< sentence type="normal" elementid="20061051w-s88">
< squarebracket elementid="20061051w-sb12">
[  3 ]
< /squarebracket>
O.S. 1992/ 2977 fel y 'i diwygiwyd gan gyfres o offerynnau dilynol .
< /sentence>
< sentence type="heading" elementid="20061051w-s89">
< a type="href" elementid="20061051w-a15">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20061051w-p26">
< sentence type="normal" elementid="20061051w-s90">
< squarebracket elementid="20061051w-sb13">
[  4 ]
< /squarebracket>
1948 p.29 .
< /sentence>
< sentence type="heading" elementid="20061051w-s91">
< a type="href" elementid="20061051w-a16">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20061051w-p27">
< sentence type="normal" elementid="20061051w-s92">
< squarebracket elementid="20061051w-sb14">
[  5 ]
< /squarebracket>
O.S. 2005/ 663
< roundbracket elementid="20061051w-rb79">
(  Cy.53 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20061051w-s93">
< a type="href" elementid="20061051w-a17">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20061051w-p28">
< sentence type="normal" elementid="20061051w-s94">
< squarebracket elementid="20061051w-sb15">
[  6 ]
< /squarebracket>
Wedi 'i fewnosod gan O.S. 1996/ 602 a 'i ddiwygio gan O.S. 1997/ 485 .
< /sentence>
< sentence type="heading" elementid="20061051w-s95">
< a type="href" elementid="20061051w-a18">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20061051w-p29">
< sentence type="normal" elementid="20061051w-s96">
< squarebracket elementid="20061051w-sb16">
[  7 ]
< /squarebracket>
Wedi 'i ddiwygio gan O.S. 2003/ 2530 .
< /sentence>
< sentence type="heading" elementid="20061051w-s97">
< a type="href" elementid="20061051w-a19">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20061051w-p30">
< sentence type="normal" elementid="20061051w-s98">
< squarebracket elementid="20061051w-sb17">
[  8 ]
< /squarebracket>
2002 c.38 .
< /sentence>
< sentence type="heading" elementid="20061051w-s99">
< a type="href" elementid="20061051w-a20">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20061051w-p31">
< sentence type="normal" elementid="20061051w-s100">
< squarebracket elementid="20061051w-sb18">
[  9 ]
< /squarebracket>
1998 p .
< /sentence>
< sentence type="normal" elementid="20061051w-s101">
38 .
< /sentence>
< sentence type="heading" elementid="20061051w-s102">
< a type="href" elementid="20061051w-a21">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20061051w-p32">
< sentence type="heading" elementid="20061051w-s103">
< a type="href" elementid="20061051w-a22">
< english>
English version
< /english>
< /a>
< /sentence>
< sentence type="heading" elementid="20061051w-s104">
ISBN 0 11 091320 5
< /sentence>
< /p>
< table elementid="20061051w-tb2">
< td elementid="20061051w-td5">
< p elementid="20061051w-p33">
< sentence type="heading" elementid="20061051w-s105">
< a type="href" elementid="20061051w-a23">
< english>
Other UK SIs
< /english>
< /a>
< a type="href" elementid="20061051w-a24">
< english>
Home
< /english>
< /a>
< a type="href" elementid="20061051w-a25">
< english>
National Assembly for Wales Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20061051w-a26">
< english>
Scottish Statutory Instrument
< /english>
< /a>
< a type="href" elementid="20061051w-a27">
< english>
Statutory Rules of Northern Ireland
< /english>
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20061051w-td6">
< p elementid="20061051w-p34">
< sentence type="heading" elementid="20061051w-s106">
< english>
< strong elementid="20061051w-st10">
We welcome your 
< a type="href" elementid="20061051w-a28">
comments
< /a>
on this site
< /strong>
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20061051w-td7">
< p elementid="20061051w-p35">
< sentence type="heading" elementid="20061051w-s107">
< english>
© Crown copyright 2006
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20061051w-td8">
< p elementid="20061051w-p36">
< sentence type="heading" elementid="20061051w-s108">
< english>
Prepared 12 April 2006
< /english>
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>1274< /wordcount>
< /file>
< /corpus>

< ?xml version = "1.0" encoding="UTF-8"?>
< corpus topic="children's-commissioner" language="Welsh">
< file elementid="20002992w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Offerynnau Statudol 2000 Rhif 2992 ( Cy . 192 ) ( C. 93 )< /pagetitle>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20002992w-p1">
< sentence type="normal" elementid="20002992w-s1">
Offerynnau Statudol 2000 Rhif 2992  Cy .
< /sentence>
< sentence type="heading" elementid="20002992w-s2">
192 )
< roundbracket elementid="20002992w-rb1">
(  C. 93 )
< /roundbracket>
< /sentence>
< /p>
< p elementid="20002992w-p2">
< sentence type="heading" elementid="20002992w-s3">
< strong elementid="20002992w-st1">
Gorchymyn Deddf Safonau Gofal 2000
< roundbracket elementid="20002992w-rb2">
(  Cychwyn Rhif 1 )
< /roundbracket>
< roundbracket elementid="20002992w-rb3">
(  Cymru )
< /roundbracket>
2000
< /strong>
< /sentence>
< /p>
< p elementid="20002992w-p3">
< sentence type="heading" elementid="20002992w-s4">
< english>
© Crown Copyright 2000
< /english>
< /sentence>
< /p>
< p elementid="20002992w-p4">
< sentence type="normal" elementid="20002992w-s5">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20002992w-p5">
< sentence type="normal" elementid="20002992w-s6">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20002992w-s7">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20002992w-p6">
< sentence type="normal" elementid="20002992w-s8">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20002992w-s9">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20002992w-s10">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20002992w-p7">
< sentence type="normal" elementid="20002992w-s11">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20002992w-s12">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan 
< english>
The Stationery Office Limited
< /english>
 fel
< strong elementid="20002992w-st2">
Gorchymyn Deddf Safonau Gofal 2000
< roundbracket elementid="20002992w-rb4">
(  Cychwyn Rhif 1 )
< /roundbracket>
< roundbracket elementid="20002992w-rb5">
(  Cymru )
< /roundbracket>
2000
< /strong>
, ISBN 0 11 090135 5 .
< /sentence>
< sentence type="normal" elementid="20002992w-s13">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20002992w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20002992w-s14">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20002992w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20002992w-p8">
< sentence type="normal" elementid="20002992w-s15">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20002992w-a3">
< english>
Frequently Asked Questions
< /english>
< /a> .
< /sentence>
< /p>
< p elementid="20002992w-p9">
< sentence type="normal" elementid="20002992w-s16">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20002992w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20002992w-s17">
Pan welwch fotwm
< doublequotation elementid="20002992w-dq2">
"  continue "
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20002992w-p10">
< sentence type="heading" elementid="20002992w-s18">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="normal" elementid="20002992w-s19">
2000 Rhif 2992  Cy .
< /sentence>
< sentence type="heading" elementid="20002992w-s20">
192 )
< roundbracket elementid="20002992w-rb6">
(  C. 93 )
< /roundbracket>
< /sentence>
< sentence type="heading" elementid="20002992w-s21">
< strong elementid="20002992w-st3">
Y COMISIYNYDD PLANT , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20002992w-s22">
< strong elementid="20002992w-st4">
GOFAL CYMDEITHASOL , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20002992w-s23">
Gorchymyn Deddf Safonau Gofal 2000
< roundbracket elementid="20002992w-rb7">
(  Cychwyn Rhif 1 )
< /roundbracket>
< roundbracket elementid="20002992w-rb8">
(  Cymru )
< /roundbracket>
2000
< /sentence>
< /p>
< table elementid="20002992w-tb1">
< td elementid="20002992w-td1">
< p elementid="20002992w-p11">
< sentence type="heading" elementid="20002992w-s24">
< em elementid="20002992w-em1">
Wedi 'i wneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20002992w-td2">
< p elementid="20002992w-p12">
< sentence type="heading" elementid="20002992w-s25">
< em elementid="20002992w-em2">
7 Tachwedd 2000
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20002992w-p13">
< sentence type="heading" elementid="20002992w-s26">
Mae Cynulliad Cenedlaethol Cymru yn gwneud y Gorchymyn canlynol drwy arfer y pwerau a roddwyd iddo gan adrannau 122 o Ddeddf Safonau Gofal 2000
< squarebracket elementid="20002992w-sb1">
[ 
< a type="href" elementid="20002992w-a4">
1
< /a>
]
< /squarebracket>
:
< /sentence>
< sentence type="heading" elementid="20002992w-s27">
< strong elementid="20002992w-st5">
Enwi , dehongli a chymhwyso
< /strong>
< /sentence>
< sentence type="normal" elementid="20002992w-s28">
1 .
< /sentence>
< sentence type="normal" elementid="20002992w-s29">
 - 
< roundbracket elementid="20002992w-rb9">
(  1 )
< /roundbracket>
Enw 'r Gorchymyn hwn yw Gorchymyn Deddf Safonau Gofal 2000
< roundbracket elementid="20002992w-rb10">
(  Cychwyn Rhif 1 )
< /roundbracket>
< roundbracket elementid="20002992w-rb11">
(  Cymru )
< /roundbracket>
2000 .
< /sentence>
< sentence type="heading" elementid="20002992w-s30">
< roundbracket elementid="20002992w-rb12">
(  2 )
< /roundbracket>
Yn y Gorchymyn hwn - 
< /sentence>
< sentence type="normal" elementid="20002992w-s31">
ystyr
< doublequotation elementid="20002992w-dq3">
"  y Cyngor "
< /doublequotation>
< roundbracket elementid="20002992w-rb13">
(
< doublequotation elementid="20002992w-dq4">
"
< em elementid="20002992w-em3">
< english>
the Council
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw Cyngor Gofal Cymru a sefydlir yn rhinwedd erthygl 2
< roundbracket elementid="20002992w-rb14">
(  2 )
< /roundbracket>
o 'r Gorchymyn hwn ; ystyr
< doublequotation elementid="20002992w-dq5">
"  y Ddeddf "
< /doublequotation>
< roundbracket elementid="20002992w-rb15">
(
< doublequotation elementid="20002992w-dq6">
"
< em elementid="20002992w-em4">
< english>
the Act
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw Deddf Safonau Gofal 2000 .
< /sentence>
< /p>
< p elementid="20002992w-p14">
< sentence type="normal" elementid="20002992w-s32">
< roundbracket elementid="20002992w-rb16">
(  3 )
< /roundbracket>
Mae 'r Gorchymyn hwn yn gymwys i Gymru .
< /sentence>
< sentence type="heading" elementid="20002992w-s33">
< strong elementid="20002992w-st6">
Dyddiau penodedig
< /strong>
< /sentence>
< sentence type="normal" elementid="20002992w-s34">
2 .
< /sentence>
< sentence type="normal" elementid="20002992w-s35">
 - 
< roundbracket elementid="20002992w-rb17">
(  1 )
< /roundbracket>
13 Tachwedd 2000 yw 'r dydd a benodir i bob un o ddarpariaethau 'r Ddeddf a bennir yn Atodlen 1 i 'r Gorchymyn hwn ddod i rym .
< /sentence>
< sentence type="normal" elementid="20002992w-s36">
< roundbracket elementid="20002992w-rb18">
(  2 )
< /roundbracket>
1 Ebrill 2001 yw 'r dydd a benodir i bob un o ddarpariaethau 'r Ddeddf a bennir yn Atodlen 2 i 'r Gorchymyn hwn ddod i rym , i 'r graddau a bennir yno .
< /sentence>
< sentence type="heading" elementid="20002992w-s37">
Llofnodwyd ar ran Cynulliad Cenedlaethol Cymru o dan adran 66
< roundbracket elementid="20002992w-rb19">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20002992w-sb2">
[ 
< a type="href" elementid="20002992w-a5">
2
< /a>
]
< /squarebracket>
< /sentence>
< sentence type="heading" elementid="20002992w-s38">
< em elementid="20002992w-em5">
D Elis Thomas
< /em>
< /sentence>
< sentence type="heading" elementid="20002992w-s39">
Llywydd y Cynulliad Cenedlaethol
< /sentence>
< sentence type="heading" elementid="20002992w-s40">
7 Tachwedd 2000
< /sentence>
< /p>
< p elementid="20002992w-p15">
< sentence type="heading" elementid="20002992w-s41">
< strong elementid="20002992w-st8">
< strong elementid="20002992w-st7">
ATODLEN 1
< /strong>
< /strong>
Erthygl 2
< roundbracket elementid="20002992w-rb20">
(  1 )
< /roundbracket>
< /sentence>
< sentence type="heading" elementid="20002992w-s42">
DARPARIAETHAU 'R DDEDDF SY 'N DOD I RYM AR 13 TACHWEDD 2000
< /sentence>
< sentence type="heading" elementid="20002992w-s43">
Adran 72
< roundbracket elementid="20002992w-rb21">
(  Comisiynydd Plant Cymru )
< /roundbracket>
< /sentence>
< sentence type="heading" elementid="20002992w-s44">
Atodlen 2
< /sentence>
< sentence type="heading" elementid="20002992w-s45">
< strong elementid="20002992w-st9">
ATODLEN 2
< /strong>
Erthygl 2
< roundbracket elementid="20002992w-rb22">
(  2 )
< /roundbracket>
< /sentence>
< sentence type="heading" elementid="20002992w-s46">
DARPARIAETHAU 'R DDEDDF SY 'N DOD I RYM AR 1 EBRILL 2001
< /sentence>
< sentence type="heading" elementid="20002992w-s47">
Adran 54  1 ),( 3 )-
< roundbracket elementid="20002992w-rb23">
(  7 )
< /roundbracket>
< roundbracket elementid="20002992w-rb24">
(  Cynghorau Gofal )
< /roundbracket>
i 'r graddau y mae 'n ymwneud â 'r Cyngor
< /sentence>
< sentence type="heading" elementid="20002992w-s48">
Adran 55
< roundbracket elementid="20002992w-rb25">
(  Dehongli )
< /roundbracket>
< /sentence>
< sentence type="heading" elementid="20002992w-s49">
Adran 113  2 )-
< roundbracket elementid="20002992w-rb26">
(  4 )
< /roundbracket>
< roundbracket elementid="20002992w-rb27">
(  Pwerau diofyn y Gweinidog priodol )
< /roundbracket>
i 'r graddau y mae 'n ymwneud â 'r Cyngor
< /sentence>
< sentence type="heading" elementid="20002992w-s50">
Adran 114
< roundbracket elementid="20002992w-rb28">
(  Cynlluniau ar gyfer trosglwyddo staff )
< /roundbracket>
i 'r graddau y mae 'n ymwneud â Gorchymyn gan y Cyfrin Gyngor , neu argymhelliad i 'w Mawrhydi i wneud Gorchymyn yn y Cyfrin Gyngor , o dan Adran 70 o 'r Ddeddf
< roundbracket elementid="20002992w-rb29">
(  Dileu y Cyngor Canolog Addysg a Hyfforddiant mewn Gwaith Cymdeithasol )
< /roundbracket>
yn cynnwys cynllun ar gyfer trosglwyddo staff o 'r Cyngor Canolog Addysg a Hyfforddiant mewn Gwaith Cymdeithasol i 'r Cyngor
< /sentence>
< sentence type="heading" elementid="20002992w-s51">
Atodlen 1 i 'r graddau y mae 'n ymwneud â 'r Cyngor
< /sentence>
< sentence type="heading" elementid="20002992w-s52">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20002992w-s53">
< em elementid="20002992w-em6">
< roundbracket elementid="20002992w-rb30">
(  Nid yw 'r nodyn hwn yn rhan o 'r Gorchymyn )
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20002992w-s54">
Mae 'r Gorchymyn hwn yn pennu dydd i rai o ddarpariaethau penodol Deddf Safonau Gofal 2000
< roundbracket elementid="20002992w-rb31">
(
< doublequotation elementid="20002992w-dq7">
"  y Ddeddf "
< /doublequotation>
)
< /roundbracket>
ddod i rym .
< /sentence>
< sentence type="normal" elementid="20002992w-s55">
Mae 'r Gorchymyn yn gymwys i Gymru .
< /sentence>
< sentence type="heading" elementid="20002992w-s56">
Mae 'r Gorchymyn yn darparu y daw 'r darpariaethau canlynol yn Rhan V o 'r Ddeddf i rym ar 13 Tachwedd 2000 :
< /sentence>
< sentence type="heading" elementid="20002992w-s57">
< roundbracket elementid="20002992w-rb32">
(  a )
< /roundbracket>
adran 72 , sy 'n sefydlu swydd Comisiynydd Plant Cymru ;
< /sentence>
< sentence type="normal" elementid="20002992w-s58">
< roundbracket elementid="20002992w-rb33">
(  b )
< /roundbracket>
Atodlen 2 , sy 'n gwneud darpariaeth ynghylch rhai o swyddogaethau gweithredol penodol y swydd megis ei statws , penodi iddi a 'i thâl , ei phwerau cyffredinol , ei chyfrifon , ei hatebolrwydd a 'i hadroddiadau .
< /sentence>
< /p>
< p elementid="20002992w-p16">
< sentence type="heading" elementid="20002992w-s59">
Mae 'r Gorchymyn yn darparu y daw 'r darpariaethau canlynol yn Rhan IV o 'r Ddeddf i rym ar 1 Ebrill 2001 i 'r graddau y maent yn ymwneud â Chyngor Gofal Cymru
< roundbracket elementid="20002992w-rb34">
(
< doublequotation elementid="20002992w-dq8">
"  y Cyngor "
< /doublequotation>
)
< /roundbracket>
:
< /sentence>
< /p>
< p elementid="20002992w-p17">
< sentence type="heading" elementid="20002992w-s60">
< roundbracket elementid="20002992w-rb35">
(  a )
< /roundbracket>
adran 54
< roundbracket elementid="20002992w-rb36">
(  1 )
< /roundbracket>
,  3 )-
< roundbracket elementid="20002992w-rb37">
(  7 )
< /roundbracket>
, sy 'n sefydlu corff corfforaethol sydd i 'w alw 'n Gyngor Gofal Cymru ; yn darparu ei ddyletswydd gyffredinol ; ac yn darparu y bydd , wrth arfer ei swyddogaethau , yn gweithredu yn unol ag unrhyw gyfarwyddiadau , neu o dan unrhyw ganllawiau cyffredinol , a roddir gan Gynulliad Cenedlaethol Cymru ;
< /sentence>
< sentence type="heading" elementid="20002992w-s61">
< roundbracket elementid="20002992w-rb38">
(  b )
< /roundbracket>
adran 55 sy 'n gwneud darpariaeth ynghylch dehongli Rhan IV o 'r Ddeddf ;
< /sentence>
< sentence type="heading" elementid="20002992w-s62">
< roundbracket elementid="20002992w-rb39">
(  c )
< /roundbracket>
adran 113  2 )-
< roundbracket elementid="20002992w-rb40">
(  4 )
< /roundbracket>
sy 'n darparu bod gan y Cynulliad bwerau diofyn mewn perthynas â 'r Cyngor ;
< /sentence>
< sentence type="heading" elementid="20002992w-s63">
< roundbracket elementid="20002992w-rb41">
(  ch )
< /roundbracket>
adran 114 sy 'n gwneud darpariaeth ynghylch Gorchmynion y Cyfrin Gyngor a wneir o dan adran 70 o 'r Ddeddf sy 'n trosglwyddo staff o 'r Cyngor Canolog Addysg a Hyfforddiant mewn Gwaith Cymdeithasol i 'r Cyngor ;
< /sentence>
< sentence type="normal" elementid="20002992w-s64">
< roundbracket elementid="20002992w-rb42">
(  d )
< /roundbracket>
Atodlen 1 sy 'n gwneud darpariaeth ynghylch rhai o swyddogaethau gweithredol penodol y Cyngor megis ei statws , ei bwerau cyffredinol , ei aelodaeth a phenodi iddo , ei weithdrefnau , ei staff , ei gyfrifon a 'i adroddiadau .
< /sentence>
< sentence type="heading" elementid="20002992w-s65">
< strong elementid="20002992w-st10">
NODYN AM ORCHMYNION CYCHWYN BLAENOROL
< /strong>
< /sentence>
< sentence type="heading" elementid="20002992w-s66">
< em elementid="20002992w-em7">
< roundbracket elementid="20002992w-rb43">
(  Nid yw 'r nodyn hwn yn rhan o 'r Gorchymyn )
< /roundbracket>
< /em>
< /sentence>
< /p>
< p elementid="20002992w-p18">
< sentence type="normal" elementid="20002992w-s67">
Daethpwyd â darpariaethau canlynol y Ddeddf i rym mewn perthynas â Chymru , yn ogystal â Lloegr , drwy O.S. 2000/ 2544
< roundbracket elementid="20002992w-rb44">
(  C.72 )
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20002992w-s68">
Yn ogystal â 'r darpariaethau a restrir isod , daethpwyd ag amryw ddarpariaethau eraill y Ddeddf i rym mewn perthynas â Lloegr drwy O.S. 2000/ 2795
< roundbracket elementid="20002992w-rb45">
(  C. 79 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20002992w-s69">
< em elementid="20002992w-em8">
Darpariaeth/ provision
< /em>
< /sentence>
< sentence type="heading" elementid="20002992w-s70">
Adran/ section 96
< roundbracket elementid="20002992w-rb46">
(  yn rhannol )
< /roundbracket>
< roundbracket elementid="20002992w-rb47">
(  
< english>
partially
< /english>
)
< /roundbracket>
< /sentence>
< sentence type="heading" elementid="20002992w-s71">
Adran/ section 99
< /sentence>
< sentence type="heading" elementid="20002992w-s72">
Adran/ section 80
< roundbracket elementid="20002992w-rb48">
(  8 )
< /roundbracket>
< roundbracket elementid="20002992w-rb49">
(  yn rhannol )
< /roundbracket>
< roundbracket elementid="20002992w-rb50">
(  
< english>
partially
< /english>
)
< /roundbracket>
< /sentence>
< sentence type="heading" elementid="20002992w-s73">
Adran/ section 94
< /sentence>
< sentence type="heading" elementid="20002992w-s74">
Adran/ section 96
< roundbracket elementid="20002992w-rb51">
(  gweddill )
< /roundbracket>
< roundbracket elementid="20002992w-rb52">
(  
< english>
remainder
< /english>
)
< /roundbracket>
< /sentence>
< sentence type="heading" elementid="20002992w-s75">
Adran/ section 100
< /sentence>
< sentence type="heading" elementid="20002992w-s76">
Adran/ section 101
< /sentence>
< sentence type="heading" elementid="20002992w-s77">
Adran/ section 103
< /sentence>
< sentence type="heading" elementid="20002992w-s78">
Adran/ section 116
< /sentence>
< sentence type="heading" elementid="20002992w-s79">
Adran/ section 117
< roundbracket elementid="20002992w-rb53">
(  2 )
< /roundbracket>
< /sentence>
< sentence type="heading" elementid="20002992w-s80">
Atodlen/ Schedule 4
< roundbracket elementid="20002992w-rb54">
(  yn rhannol )
< /roundbracket>
< roundbracket elementid="20002992w-rb55">
(  
< english>
partially
< /english>
)
< /roundbracket>
< /sentence>
< sentence type="heading" elementid="20002992w-s81">
Atodlen/ Schedule 6
< roundbracket elementid="20002992w-rb56">
(  yn rhannol )
< /roundbracket>
< roundbracket elementid="20002992w-rb57">
(  
< english>
partially
< /english>
)
< /roundbracket>
< /sentence>
< /p>
< p elementid="20002992w-p19">
< sentence type="heading" elementid="20002992w-s82">
< em elementid="20002992w-em9">
Notes :
< /em>
< /sentence>
< sentence type="normal" elementid="20002992w-s83">
< squarebracket elementid="20002992w-sb3">
[  1 ]
< /squarebracket>
2000 p.14 .
< /sentence>
< sentence type="normal" elementid="20002992w-s84">
Mae 'r p er yn aferadwy gan y Gweinidog priodol .
< /sentence>
< sentence type="normal" elementid="20002992w-s85">
Yn unol â 'r diffiniad o
< doublequotation elementid="20002992w-dq9">
"
< english>
the appropriate Minister
< /english>
"
< /doublequotation>
< roundbracket elementid="20002992w-rb58">
(
< doublequotation elementid="20002992w-dq10">
"  y Gweinidog priodol "
< /doublequotation>
)
< /roundbracket>
yn adran 121
< roundbracket elementid="20002992w-rb59">
(  1 )
< /roundbracket>
ystyr
< doublequotation elementid="20002992w-dq11">
"  y Gweinidog priodol "
< /doublequotation>
mewn perthynas â Chymru yw Cynulliad Cenedlaethol Cymru ; mewn perthynas â 'r Alban , Gog ; edd Iwerddon neu Loegr , ystyr
< doublequotation elementid="20002992w-dq12">
"  y Gweinidog priodol "
< /doublequotation>
yw 'r Ysgrifennydd Gwladol .
< /sentence>
< sentence type="heading" elementid="20002992w-s86">
< a type="href" elementid="20002992w-a6">
back
< /a>
< /sentence>
< /p>
< p elementid="20002992w-p20">
< sentence type="normal" elementid="20002992w-s87">
< squarebracket elementid="20002992w-sb4">
[  2 ]
< /squarebracket>
1998 p .
< /sentence>
< sentence type="normal" elementid="20002992w-s88">
38 .
< /sentence>
< sentence type="heading" elementid="20002992w-s89">
< a type="href" elementid="20002992w-a7">
back
< /a>
< /sentence>
< /p>
< p elementid="20002992w-p21">
< sentence type="heading" elementid="20002992w-s90">
< a type="href" elementid="20002992w-a8">
< english>
English version
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20002992w-p22">
< sentence type="heading" elementid="20002992w-s91">
ISBN 0 11 090135 5
< /sentence>
< /p>
< table elementid="20002992w-tb2">
< td elementid="20002992w-td3">
< p elementid="20002992w-p23">
< sentence type="heading" elementid="20002992w-s92">
< a type="href" elementid="20002992w-a9">
< english>
Other UK SIs
< /english>
< /a>
< a type="href" elementid="20002992w-a10">
< english>
Home
< /english>
< /a>
< a type="href" elementid="20002992w-a11">
< english>
National Assembly for Wales Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20002992w-a12">
< english>
Scottish Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20002992w-a13">
< english>
Statutory Rules of Northern Ireland
< /english>
< /a>
< a type="href" elementid="20002992w-a14">
< english>
Her Majesty 's Stationery Office
< /english>
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20002992w-td4">
< p elementid="20002992w-p24">
< sentence type="heading" elementid="20002992w-s93">
< english>
< strong elementid="20002992w-st11">
We welcome your 
< a type="href" elementid="20002992w-a15">
comments
< /a>
on this site
< /strong>
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20002992w-td5">
< p elementid="20002992w-p25">
< sentence type="heading" elementid="20002992w-s94">
< english>
© Crown copyright 2000
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20002992w-td6">
< p elementid="20002992w-p26">
< sentence type="heading" elementid="20002992w-s95">
< english>
Prepared 29 November 2000
< /english>
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>1209< /wordcount>
< /file>
< file elementid="20003121w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Offerynnau Statudol 2000 Rhif 3121 ( Cy . 199 )< /pagetitle>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20003121w-p1">
< sentence type="normal" elementid="20003121w-s1">
Offerynnau Statudol 2000 Rhif 3121  Cy .
< /sentence>
< sentence type="heading" elementid="20003121w-s2">
199 )
< /sentence>
< /p>
< p elementid="20003121w-p2">
< sentence type="heading" elementid="20003121w-s3">
< strong elementid="20003121w-st1">
Rheoliadau Comisiynydd Plant Cymru
< roundbracket elementid="20003121w-rb1">
(  Penodi )
< /roundbracket>
2000
< /strong>
< /sentence>
< /p>
< p elementid="20003121w-p3">
< sentence type="heading" elementid="20003121w-s4">
< english>
© Crown Copyright 2000
< /english>
< /sentence>
< /p>
< p elementid="20003121w-p4">
< sentence type="normal" elementid="20003121w-s5">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20003121w-p5">
< sentence type="normal" elementid="20003121w-s6">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20003121w-s7">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20003121w-p6">
< sentence type="normal" elementid="20003121w-s8">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20003121w-s9">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20003121w-s10">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20003121w-p7">
< sentence type="normal" elementid="20003121w-s11">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20003121w-s12">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan 
< english>
The Stationery Office Limited
< /english>
 fel
< strong elementid="20003121w-st2">
Rheoliadau Comisiynydd Plant Cymru
< roundbracket elementid="20003121w-rb2">
(  Penodi )
< /roundbracket>
2000
< /strong>
, ISBN 0-11-090150-9 .
< /sentence>
< sentence type="normal" elementid="20003121w-s13">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20003121w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20003121w-s14">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20003121w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20003121w-p8">
< sentence type="normal" elementid="20003121w-s15">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20003121w-a3">
< english>
Frequently Asked Questions
< /english>
< /a> .
< /sentence>
< /p>
< p elementid="20003121w-p9">
< sentence type="normal" elementid="20003121w-s16">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20003121w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20003121w-s17">
Pan welwch fotwm
< doublequotation elementid="20003121w-dq2">
"
< english>
continue
< /english>
"
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20003121w-p10">
< sentence type="heading" elementid="20003121w-s18">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="normal" elementid="20003121w-s19">
2000 Rhif 3121  Cy .
< /sentence>
< sentence type="heading" elementid="20003121w-s20">
199 )
< /sentence>
< sentence type="heading" elementid="20003121w-s21">
< strong elementid="20003121w-st3">
COMISIYNYDD PLANT , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20003121w-s22">
Rheoliadau Comisiynydd Plant Cymru
< roundbracket elementid="20003121w-rb3">
(  Penodi )
< /roundbracket>
2000
< /sentence>
< /p>
< table elementid="20003121w-tb1">
< td elementid="20003121w-td1">
< p elementid="20003121w-p11">
< sentence type="heading" elementid="20003121w-s23">
< em elementid="20003121w-em1">
Wedi 'u gwneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20003121w-td2">
< p elementid="20003121w-p12">
< sentence type="heading" elementid="20003121w-s24">
< em elementid="20003121w-em2">
23 Tachwedd 2000
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20003121w-td3">
< p elementid="20003121w-p13">
< sentence type="heading" elementid="20003121w-s25">
< em elementid="20003121w-em3">
Yn dod i rym
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20003121w-td4">
< p elementid="20003121w-p14">
< sentence type="heading" elementid="20003121w-s26">
< em elementid="20003121w-em4">
8 Rhagfyr 2000
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20003121w-p15">
< sentence type="heading" elementid="20003121w-s27">
Mae Cynulliad Cenedlaethol Cymru yn gwneud y Rheoliadau canlynol drwy arfer y pwerau a roddwyd iddo gan adran 118
< roundbracket elementid="20003121w-rb4">
(  7 )
< /roundbracket>
o Ddeddf Safonau Gofal 2000
< squarebracket elementid="20003121w-sb1">
[ 
< a type="href" elementid="20003121w-a4">
1
< /a>
]
< /squarebracket>
a pharagraff 2 o Atodlen 2 iddi :
< /sentence>
< sentence type="heading" elementid="20003121w-s28">
< strong elementid="20003121w-st4">
Enwi , cychwyn , dehongli a chymhwyso
< /strong>
< /sentence>
< sentence type="normal" elementid="20003121w-s29">
1 .
< /sentence>
< sentence type="normal" elementid="20003121w-s30">
 - 
< roundbracket elementid="20003121w-rb5">
(  1 )
< /roundbracket>
Enw 'r Rheoliadau hyn yw Rheoliadau Comisiynydd Plant Cymru
< roundbracket elementid="20003121w-rb6">
(  Penodi )
< /roundbracket>
2000 a deuant i rym ar 8 Rhagfyr 2000 .
< /sentence>
< sentence type="heading" elementid="20003121w-s31">
< roundbracket elementid="20003121w-rb7">
(  2 )
< /roundbracket>
Yn y Rheoliadau hyn  - 
< /sentence>
< sentence type="heading" elementid="20003121w-s32">
ystyr
< doublequotation elementid="20003121w-dq3">
"  y Comisiynydd "
< /doublequotation>
< roundbracket elementid="20003121w-rb8">
(
< doublequotation elementid="20003121w-dq4">
"
< em elementid="20003121w-em5">
< english>
the Commissioner
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw Comisiynydd Plant Cymru
< squarebracket elementid="20003121w-sb2">
[ 
< a type="href" elementid="20003121w-a5">
2
< /a>
]
< /squarebracket>
; ystyr
< doublequotation elementid="20003121w-dq5">
"  y Cynulliad Cenedlaethol "
< /doublequotation>
< roundbracket elementid="20003121w-rb9">
(
< doublequotation elementid="20003121w-dq6">
"
< em elementid="20003121w-em6">
< english>
the National Assembly
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw Cynulliad Cenedlaethol Cymru ; ystyr
< doublequotation elementid="20003121w-dq7">
"  plant perthnasol "
< /doublequotation>
< roundbracket elementid="20003121w-rb10">
(
< doublequotation elementid="20003121w-dq8">
"
< em elementid="20003121w-em7">
< english>
relevant children
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw unrhyw blant sy 'n preswylio yng Nghymru sy 'n cael eu dethol at ddibenion penodiad penodol yn y fath fodd  - 
< roundbracket elementid="20003121w-rb11">
(  a )
< /roundbracket>
ag y gellid ei benderfynu gan y pwyllgor perthnasol yn unol â chylch gorchwyl y pwyllgor , neu
< /sentence>
< sentence type="heading" elementid="20003121w-s33">
< roundbracket elementid="20003121w-rb12">
(  b )
< /roundbracket>
yn absenoldeb penderfyniad o 'r fath , ag y mae 'n rhaid i 'r Prif Ysgrifennydd ei benderfynu ;
< /sentence>
< sentence type="normal" elementid="20003121w-s34">
ystyr
< doublequotation elementid="20003121w-dq9">
"  y Prif Ysgrifennydd "
< /doublequotation>
< roundbracket elementid="20003121w-rb13">
(
< doublequotation elementid="20003121w-dq10">
"
< english>
the First Secretary
< /english>
< /doublequotation>
)
< /roundbracket>
yw 'r person a etholir o dro i dro yn Brif Ysgrifennydd y Cynulliad yn unol ag adran 53
< roundbracket elementid="20003121w-rb14">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20003121w-sb3">
[ 
< a type="href" elementid="20003121w-a6">
3
< /a>
]
< /squarebracket>
; ystyr
< doublequotation elementid="20003121w-dq11">
"  pwyllgor perthnasol "
< /doublequotation>
< roundbracket elementid="20003121w-rb15">
(
< doublequotation elementid="20003121w-dq12">
"
< em elementid="20003121w-em8">
< english>
relevant committee
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw unrhyw bwyllgor y gellid ei sefydlu gan y Cynulliad Cenedlaethol o dro i dro o dan adran 54
< roundbracket elementid="20003121w-rb16">
(  1 )
< /roundbracket>
< roundbracket elementid="20003121w-rb17">
(  b )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998 er mwyn darparu cyngor a phenderfynu materion sy 'n berthnasol i benodi 'r Comisiynydd .
< /sentence>
< /p>
< p elementid="20003121w-p16">
< sentence type="normal" elementid="20003121w-s35">
< roundbracket elementid="20003121w-rb18">
(  3 )
< /roundbracket>
Mae 'r Rheoliadau hyn yn gymwys i Gymru yn unig .
< /sentence>
< sentence type="heading" elementid="20003121w-s36">
< strong elementid="20003121w-st5">
Penodi 'r Comisiynydd
< /strong>
< /sentence>
< sentence type="normal" elementid="20003121w-s37">
2 .
< /sentence>
< sentence type="normal" elementid="20003121w-s38">
 - 
< roundbracket elementid="20003121w-rb19">
(  1 )
< /roundbracket>
Rhaid i 'r Comisiynydd gael ei benodi gan y Prif Ysgrifennydd .
< /sentence>
< sentence type="heading" elementid="20003121w-s39">
< roundbracket elementid="20003121w-rb20">
(  2 )
< /roundbracket>
Dim ond ar ôl cymryd y canlynol i ystyriaeth y bydd y Comisynydd yn cael ei benodi - 
< /sentence>
< /p>
< p elementid="20003121w-p17">
< sentence type="heading" elementid="20003121w-s40">
< roundbracket elementid="20003121w-rb21">
(  a )
< /roundbracket>
cyngor pwyllgor perthnasol,
< /sentence>
< sentence type="heading" elementid="20003121w-s41">
< roundbracket elementid="20003121w-rb22">
(  b )
< /roundbracket>
barn plant perthnasol ynghylch unrhyw ymgeiswyr a gafodd eu cyfweld ar gyfer y penodiad , ac
< /sentence>
< sentence type="normal" elementid="20003121w-s42">
< roundbracket elementid="20003121w-rb23">
(  c )
< /roundbracket>
cyngor unrhyw banel dewis , a sefydlwyd er mwyn cyfweld ymgeiswyr , ynghylch eu priodoldeb ar gyfer y penodiad .
< /sentence>
< /p>
< p elementid="20003121w-p18">
< sentence type="normal" elementid="20003121w-s43">
< roundbracket elementid="20003121w-rb24">
(  3 )
< /roundbracket>
Yn ddarostyngedig i reoliad 3 , saith mlynedd fydd cyfnod swydd y Comisynydd .
< /sentence>
< sentence type="normal" elementid="20003121w-s44">
< roundbracket elementid="20003121w-rb25">
(  4 )
< /roundbracket>
Ni fydd y Comisiynydd yn gymwys i gael ei ailbenodi pan ddaw cyfnod y swydd i ben neu os terfynir hi cyn hynny .
< /sentence>
< sentence type="normal" elementid="20003121w-s45">
3 .
< /sentence>
< sentence type="heading" elementid="20003121w-s46">
Gall y Prif Ysgrifennydd ryddhau 'r Comisiynydd o 'i swydd cyn i gyfnod y swydd ddod i ben - 
< /sentence>
< /p>
< p elementid="20003121w-p19">
< sentence type="heading" elementid="20003121w-s47">
< roundbracket elementid="20003121w-rb26">
(  a )
< /roundbracket>
ar gais y Comisiynydd,
< /sentence>
< sentence type="heading" elementid="20003121w-s48">
< roundbracket elementid="20003121w-rb27">
(  b )
< /roundbracket>
ar sail camymddwyn , neu
< /sentence>
< sentence type="normal" elementid="20003121w-s49">
< roundbracket elementid="20003121w-rb28">
(  c )
< /roundbracket>
os bydd wedi 'i fodloni nad yw 'r Comisynydd yn alluog oherwydd gwendid meddyliol neu gorfforol i gyflawni swyddogaethau 'r Comisiynydd .
< /sentence>
< /p>
< p elementid="20003121w-p20">
< sentence type="normal" elementid="20003121w-s50">
4 .
< /sentence>
< sentence type="normal" elementid="20003121w-s51">
Os bydd y Prif Ysgrifennydd yn arfer swyddogaethau a roddir gan y rheoliadau hyn , bydd arfer y swyddogaethau hynny 'n cael ei drin fel pe bai 'n arfer gan y Cynulliad Cenedlaethol .
< /sentence>
< sentence type="heading" elementid="20003121w-s52">
Llofnodwyd ar ran Cynulliad Cenedlaethol Cymru o dan adran 66
< roundbracket elementid="20003121w-rb29">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< /sentence>
< sentence type="heading" elementid="20003121w-s53">
< em elementid="20003121w-em9">
John Marek
< /em>
< /sentence>
< sentence type="heading" elementid="20003121w-s54">
Dirpwy Lywydd y Cynulliad Cenedlaethol
< /sentence>
< /p>
< p elementid="20003121w-p21">
< sentence type="heading" elementid="20003121w-s55">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20003121w-s56">
< em elementid="20003121w-em10">
< roundbracket elementid="20003121w-rb30">
(  Nid yw 'r nodyn hwn yn rhan o 'r Rheoliadau )
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20003121w-s57">
Mae 'r Rheoliadau hyn yn gwneud darpariaeth ar gyfer penodi Comisiynydd Plant Cymru sef swydd a sefydlwyd o dan Ran V o Ddeddf Safonau Gofal 2000 .
< /sentence>
< sentence type="normal" elementid="20003121w-s58">
Yn benodol , mae 'r Rheoliadau 'n gwneud darpariaeth i 'r penodiad gael ei wneud gan Brif Ysgrifennydd y Cynulliad yn dilyn cyngor gan unrhyw bwyllgor o 'r Cynulliad a sefydlwyd er mwyn cynghori ynghylch y penodiad a chyngor , ar ôl i ymgeiswyr gael eu cyfweld gan banel dewis , ynghylch eu priodoldeb i gael eu penodi .
< /sentence>
< sentence type="normal" elementid="20003121w-s59">
Mae 'r Prif Ysgrifennydd o dan ddyletswydd hefyd i gymryd i ystyriaeth farn plant sy 'n byw yng Nghymru ynghylch y penodiad arfaethedig .
< /sentence>
< sentence type="normal" elementid="20003121w-s60">
Mae 'r Rheoliadau hefyd yn gwneud darpariaeth ynghylch cyfnod y swydd , ac ynghylch yr amgylchiadau y gellir diswyddo 'r Comisiynydd o danynt .
< /sentence>
< sentence type="normal" elementid="20003121w-s61">
O ran eglurder ynghylch cyfrifoldeb cyfreithiol ar gyfer arfer swyddogaethau gan y Prif Ysgrifennydd , mae 'r rheoliadau 'n cynnwys darpariaeth atodol i ymdrin â swyddogaethau sy 'n cael eu harfer felly fel petaent yn swyddogaethau sy 'n cael eu harfer gan y Cynulliad .
< /sentence>
< sentence type="heading" elementid="20003121w-s62">
< em elementid="20003121w-em11">
Notes :
< /em>
< /sentence>
< sentence type="normal" elementid="20003121w-s63">
< squarebracket elementid="20003121w-sb4">
[  1 ]
< /squarebracket>
2000 p.14 .
< /sentence>
< sentence type="heading" elementid="20003121w-s64">
< a type="href" elementid="20003121w-a7">
back
< /a>
< /sentence>
< /p>
< p elementid="20003121w-p22">
< sentence type="normal" elementid="20003121w-s65">
< squarebracket elementid="20003121w-sb5">
[  2 ]
< /squarebracket>
Sefydlir swydd Comisiynydd Plant Cymru gan adran 72
< roundbracket elementid="20003121w-rb31">
(  1 )
< /roundbracket>
o Ddeddf Safonau Gofal 2000 .
< /sentence>
< sentence type="heading" elementid="20003121w-s66">
< a type="href" elementid="20003121w-a8">
back
< /a>
< /sentence>
< /p>
< p elementid="20003121w-p23">
< sentence type="normal" elementid="20003121w-s67">
< squarebracket elementid="20003121w-sb6">
[  3 ]
< /squarebracket>
1998 p.38 .
< /sentence>
< sentence type="heading" elementid="20003121w-s68">
< a type="href" elementid="20003121w-a9">
back
< /a>
< /sentence>
< /p>
< p elementid="20003121w-p24">
< sentence type="heading" elementid="20003121w-s69">
< a type="href" elementid="20003121w-a10">
< english>
English version
< /english>
< /a>
< /sentence>
< sentence type="heading" elementid="20003121w-s70">
ISBN 0-11-090150-9
< /sentence>
< /p>
< table elementid="20003121w-tb2">
< td elementid="20003121w-td5">
< p elementid="20003121w-p25">
< sentence type="heading" elementid="20003121w-s71">
< a type="href" elementid="20003121w-a11">
< english>
Other UK SIs
< /english>
< /a>
< a type="href" elementid="20003121w-a12">
< english>
Home
< /english>
< /a>
< a type="href" elementid="20003121w-a13">
< english>
National Assembly for Wales Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20003121w-a14">
< english>
Scottish Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20003121w-a15">
< english>
Statutory Rules of Northern Ireland
< /english>
< /a>
< a type="href" elementid="20003121w-a16">
< english>
Her Majesty 's Stationery Office
< /english>
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20003121w-td6">
< p elementid="20003121w-p26">
< sentence type="heading" elementid="20003121w-s72">
< english>
< strong elementid="20003121w-st6">
We welcome your 
< a type="href" elementid="20003121w-a17">
comments
< /a>
on this site
< /strong>
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20003121w-td7">
< p elementid="20003121w-p27">
< sentence type="heading" elementid="20003121w-s73">
< english>
© Crown copyright 2000
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20003121w-td8">
< p elementid="20003121w-p28">
< sentence type="heading" elementid="20003121w-s74">
< english>
Prepared 16 January 2001
< /english>
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>1024< /wordcount>
< /file>
< file elementid="20012782w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Gorchymyn Deddf Safonau Gofal 2000 ( Cychwyn Rhif 7 ) ( Cymru ) 2001< /pagetitle>
< doc_class>Statutory Instrument - Wales< /doc_class>
< year>2001< /year>
< number>2782< /number>
< legislation_code>30102< /legislation_code>
< legislation_type>Statutory Instrument - Wales< /legislation_type>
< subject>COMISIYNYDD PLANT, CYMRU< /subject>
< laid_date>_EMPTY< /laid_date>
< made_date>20011225< /made_date>
< coming_into_force>_EMPTY< /coming_into_force>
< isbn>0110903269< /isbn>
< title>Gorchymyn Deddf Safonau Gofal 2000 (Cychwyn Rhif 7) (Cymru) 2001< /title>
< signatory>Rhodri Morgan< /signatory>
< signatory_title>Prif Weinidog Cynulliad Cenedlaethol Cymru< /signatory_title>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20012782w-p1">
< sentence type="heading" elementid="20012782w-s1">
Offerynnau Statudol 2001 Rhif 2782
< roundbracket elementid="20012782w-rb1">
(  Cy.235 )
< /roundbracket>
< roundbracket elementid="20012782w-rb2">
(  C.92 )
< /roundbracket>
< /sentence>
< /p>
< p elementid="20012782w-p2">
< sentence type="heading" elementid="20012782w-s2">
< strong elementid="20012782w-st1">
Gorchymyn Deddf Safonau Gofal 2000
< roundbracket elementid="20012782w-rb3">
(  Cychwyn Rhif 7 )
< /roundbracket>
< roundbracket elementid="20012782w-rb4">
(  Cymru )
< /roundbracket>
2001
< /strong>
< /sentence>
< /p>
< p elementid="20012782w-p3">
< sentence type="heading" elementid="20012782w-s3">
< english>
© Crown Copyright 2001
< /english>
< /sentence>
< /p>
< p elementid="20012782w-p4">
< sentence type="normal" elementid="20012782w-s4">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20012782w-p5">
< sentence type="normal" elementid="20012782w-s5">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20012782w-s6">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20012782w-p6">
< sentence type="normal" elementid="20012782w-s7">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20012782w-s8">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20012782w-s9">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20012782w-p7">
< sentence type="normal" elementid="20012782w-s10">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20012782w-s11">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan 
< english>
The Stationery Office Limited
< /english>
 fel
< strong elementid="20012782w-st2">
Gorchymyn Deddf Safonau Gofal 2000
< roundbracket elementid="20012782w-rb5">
(  Cychwyn Rhif 7 )
< /roundbracket>
< roundbracket elementid="20012782w-rb6">
(  Cymru )
< /roundbracket>
2001
< /strong>
, ISBN 0 11090326 9 .
< /sentence>
< sentence type="normal" elementid="20012782w-s12">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20012782w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20012782w-s13">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20012782w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20012782w-p8">
< sentence type="normal" elementid="20012782w-s14">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20012782w-a3">
< english>
Frequently Asked Questions
< /english>
< /a> .
< /sentence>
< /p>
< p elementid="20012782w-p9">
< sentence type="normal" elementid="20012782w-s15">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20012782w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20012782w-s16">
Pan welwch fotwm
< doublequotation elementid="20012782w-dq2">
"
< english>
continue
< /english>
"
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20012782w-p10">
< sentence type="heading" elementid="20012782w-s17">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="heading" elementid="20012782w-s18">
< strong elementid="20012782w-st3">
2001 Rhif 2782
< roundbracket elementid="20012782w-rb7">
(  Cy.235 )
< /roundbracket>
< roundbracket elementid="20012782w-rb8">
(  C.92 )
< /roundbracket>
< /strong>
< /sentence>
< sentence type="heading" elementid="20012782w-s19">
< strong elementid="20012782w-st4">
COMISIYNYDD PLANT , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20012782w-s20">
Gorchymyn Deddf Safonau Gofal 2000
< roundbracket elementid="20012782w-rb9">
(  Cychwyn Rhif 7 )
< /roundbracket>
< roundbracket elementid="20012782w-rb10">
(  Cymru )
< /roundbracket>
2001
< /sentence>
< /p>
< table elementid="20012782w-tb1">
< td elementid="20012782w-td1">
< p elementid="20012782w-p11">
< sentence type="heading" elementid="20012782w-s21">
< em elementid="20012782w-em1">
Wedi 'i wneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td2">
< p elementid="20012782w-p12">
< sentence type="heading" elementid="20012782w-s22">
< em elementid="20012782w-em2">
25 Gorffennaf 2001
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20012782w-p13">
< sentence type="heading" elementid="20012782w-s23">
Mae Cynulliad Cenedlaethol Cymru yn gwneud y Gorchymyn canlynol drwy arfer y pwerau a roddir gan adran 122 o Ddeddf Safonau Gofal 2000
< squarebracket elementid="20012782w-sb1">
[ 
< a type="href" elementid="20012782w-a4">
1
< /a>
]
< /squarebracket>
:
< /sentence>
< sentence type="heading" elementid="20012782w-s24">
< strong elementid="20012782w-st5">
Enwi a dehongli
< /strong>
< /sentence>
< sentence type="normal" elementid="20012782w-s25">
1 .
< /sentence>
< sentence type="normal" elementid="20012782w-s26">
 - 
< roundbracket elementid="20012782w-rb11">
(  1 )
< /roundbracket>
Enw 'r Gorchymyn hwn yw Gorchymyn Deddf Safonau Gofal 2000
< roundbracket elementid="20012782w-rb12">
(  Cychwyn Rhif 7 )
< /roundbracket>
< roundbracket elementid="20012782w-rb13">
(  Cymru )
< /roundbracket>
2001 .
< /sentence>
< sentence type="normal" elementid="20012782w-s27">
< roundbracket elementid="20012782w-rb14">
(  2 )
< /roundbracket>
Yn y Gorchymyn hwn - ystyr
< doublequotation elementid="20012782w-dq3">
"  y Ddeddf "
< /doublequotation>
< roundbracket elementid="20012782w-rb15">
(
< doublequotation elementid="20012782w-dq4">
"
< em elementid="20012782w-em3">
< english>
the Act
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw Deddf Safonau Gofal 2000 .
< /sentence>
< sentence type="heading" elementid="20012782w-s28">
< strong elementid="20012782w-st6">
Y diwrnod penodedig
< /strong>
< /sentence>
< sentence type="normal" elementid="20012782w-s29">
2 .
< /sentence>
< sentence type="normal" elementid="20012782w-s30">
 - 
< roundbracket elementid="20012782w-rb16">
(  1 )
< /roundbracket>
26 Awst 2001 yw 'r diwrnod penodedig ar gyfer dwyn darpariaethau 'r Ddeddf a bennir ym mharagraff
< roundbracket elementid="20012782w-rb17">
(  2 )
< /roundbracket>
i rym mewn perthynas â Chymru .
< /sentence>
< sentence type="heading" elementid="20012782w-s31">
< roundbracket elementid="20012782w-rb18">
(  2 )
< /roundbracket>
Dyma 'r darpariaethau - 
< /sentence>
< sentence type="heading" elementid="20012782w-s32">
< roundbracket elementid="20012782w-rb19">
(  a )
< /roundbracket>
Adran 72A
< roundbracket elementid="20012782w-rb20">
(  prif nod y Comisiynydd )
< /roundbracket>
;
< /sentence>
< sentence type="heading" elementid="20012782w-s33">
< roundbracket elementid="20012782w-rb21">
(  b )
< /roundbracket>
Adran 72B
< roundbracket elementid="20012782w-rb22">
(  adolygu arfer swyddogaethau gan y Cynulliad a chan bersonau eraill )
< /roundbracket>
;
< /sentence>
< sentence type="heading" elementid="20012782w-s34">
< roundbracket elementid="20012782w-rb23">
(  c )
< /roundbracket>
Adran 73
< roundbracket elementid="20012782w-rb24">
(  adolygu a monitro trefniadau )
< /roundbracket>
;
< /sentence>
< sentence type="heading" elementid="20012782w-s35">
< roundbracket elementid="20012782w-rb25">
(  ch )
< /roundbracket>
Adran 74
< roundbracket elementid="20012782w-rb26">
(  archwilio achosion )
< /roundbracket>
;
< /sentence>
< sentence type="heading" elementid="20012782w-s36">
< roundbracket elementid="20012782w-rb27">
(  d )
< /roundbracket>
Adran 75  rhwystro etc.) ;
< /sentence>
< sentence type="heading" elementid="20012782w-s37">
< roundbracket elementid="20012782w-rb28">
(  dd )
< /roundbracket>
Adran 75A
< roundbracket elementid="20012782w-rb29">
(  p er ychwanegol i ystyried a chynrychioli )
< /roundbracket>
;
< /sentence>
< sentence type="heading" elementid="20012782w-s38">
< roundbracket elementid="20012782w-rb30">
(  e )
< /roundbracket>
Adran 76
< roundbracket elementid="20012782w-rb31">
(  swyddogaethau pellach )
< /roundbracket>
;
< /sentence>
< sentence type="heading" elementid="20012782w-s39">
< roundbracket elementid="20012782w-rb32">
(  f )
< /roundbracket>
Adran 77
< roundbracket elementid="20012782w-rb33">
(  cyfyngiadau )
< /roundbracket>
;
< /sentence>
< sentence type="heading" elementid="20012782w-s40">
< roundbracket elementid="20012782w-rb34">
(  ff )
< /roundbracket>
Adran 78
< roundbracket elementid="20012782w-rb35">
(  dehongli )
< /roundbracket>
;
< /sentence>
< sentence type="heading" elementid="20012782w-s41">
< roundbracket elementid="20012782w-rb36">
(  g )
< /roundbracket>
Atodlen 2A
< roundbracket elementid="20012782w-rb37">
(  personau sy 'n destun adolygiad gan y Comisiynydd o dan Adran 72B )
< /roundbracket>
;
< /sentence>
< sentence type="heading" elementid="20012782w-s42">
< roundbracket elementid="20012782w-rb38">
(  ng )
< /roundbracket>
Atodlen 2B
< roundbracket elementid="20012782w-rb39">
(  personau y mae eu trefniadau 'n destun adolygiad gan y Comisiynydd o dan Adran 73 )
< /roundbracket>
< /sentence>
< sentence type="normal" elementid="20012782w-s43">
< roundbracket elementid="20012782w-rb40">
(  h )
< /roundbracket>
Paragraff 3 o Atodlen 5
< roundbracket elementid="20012782w-rb41">
(  darpariaethau trosiannol ac eithriadau )
< /roundbracket>
ac adran 117
< roundbracket elementid="20012782w-rb42">
(  1 )
< /roundbracket>
i 'r graddau y mae 'n ymwneud ag ef .
< /sentence>
< /p>
< p elementid="20012782w-p14">
< sentence type="heading" elementid="20012782w-s44">
Llofnodwyd ar ran Cynulliad Cenedlaethol Cymru o dan adran 66
< roundbracket elementid="20012782w-rb43">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20012782w-sb2">
[ 
< a type="href" elementid="20012782w-a5">
2
< /a>
]
< /squarebracket>
< /sentence>
< sentence type="heading" elementid="20012782w-s45">
< em elementid="20012782w-em4">
Rhodri Morgan
< /em>
< /sentence>
< sentence type="heading" elementid="20012782w-s46">
Prif Weinidog Cynulliad Cenedlaethol Cymru
< /sentence>
< sentence type="heading" elementid="20012782w-s47">
25 Gorffennaf 2001
< /sentence>
< /p>
< p elementid="20012782w-p15">
< sentence type="heading" elementid="20012782w-s48">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20012782w-s49">
< em elementid="20012782w-em5">
< roundbracket elementid="20012782w-rb44">
(  Nid yw 'r nodyn hwn yn rhan o 'r Gorchymyn )
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20012782w-s50">
Mae 'r Gorchymyn hwn yn penodi diwrnod i ddarpariaethau penodol yn Neddf Safonau Gofal 2000
< roundbracket elementid="20012782w-rb45">
(
< doublequotation elementid="20012782w-dq5">
"  Deddf 2000 "
< /doublequotation>
)
< /roundbracket>
ddod i rym ynghylch Comisiynydd Plant Cymru
< roundbracket elementid="20012782w-rb46">
(
< doublequotation elementid="20012782w-dq6">
"  y Comisiynydd "
< /doublequotation>
)
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20012782w-s51">
Mewnosodir rhai o 'r darpariaethau gan Ddeddf Comisiynydd Plant Cymru 2001
< roundbracket elementid="20012782w-rb47">
(
< doublequotation elementid="20012782w-dq7">
"  Deddf 2001 "
< /doublequotation>
)
< /roundbracket>
y mae Gorchymyn Deddf Comisiynydd Plant Cymru 2001
< roundbracket elementid="20012782w-rb48">
(  Cychwyn )
< /roundbracket>
yn gymwys iddi .
< /sentence>
< sentence type="heading" elementid="20012782w-s52">
26 Awst 2001 yw 'r diwrnod penodedig i ddarpariaethau canlynol y Ddeddf ddod i rym :
< /sentence>
< sentence type="heading" elementid="20012782w-s53">
< roundbracket elementid="20012782w-rb49">
(  a )
< /roundbracket>
Adran 72A , a fewnosodwyd gan Ddeddf 2001 , sy 'n darparu mai diogelu a hybu hawliau a lles plant y mae Rhan V o Ddeddf 2000 yn gymwys iddynt yw prif nod y Comisiynydd wrth arfer ei swyddogaethau ;
< /sentence>
< sentence type="heading" elementid="20012782w-s54">
< roundbracket elementid="20012782w-rb50">
(  b )
< /roundbracket>
Adran 72B , a fewnosodwyd gan Ddeddf 2001 , sy 'n galluogi 'r Comisiynydd i adolygu yr effaith ar blant a gâi arfer unrhyw swyddogaeth sydd gan Gynulliad Cenedlaethol Cymru
< roundbracket elementid="20012782w-rb51">
(
< doublequotation elementid="20012782w-dq8">
"  y Cynulliad "
< /doublequotation>
)
< /roundbracket>
neu sydd gan unrhyw berson a grybwyllir yn Atodlen 2A i Ddeddf 2000 , fel y 'i mewnosodwyd gan Ddeddf 2001 ;
< /sentence>
< sentence type="heading" elementid="20012782w-s55">
< roundbracket elementid="20012782w-rb52">
(  c )
< /roundbracket>
Adran 73 , fel y 'i diwygir gan Ddeddf 2001 , sy 'n galluogi 'r Comisiynydd i adolygu a monitro sut mae trefniadau ar gyfer c ynion , eiriolaeth a chwythu 'r chwiban yn cael eu gweithredu gan ddarparwyr gwasanaethau plant a reoleiddir o dan Ddeddf 2000 , y Cynulliad , personau a grybwyllir yn Atodlen 2B i Ddeddf 2000 , fel y 'i mewnosodwyd gan Ddeddf 2001 , a phersonau penodol eraill , ac i asesu effaith methiant i wneud trefniadau o 'r fath ;
< /sentence>
< sentence type="heading" elementid="20012782w-s56">
< roundbracket elementid="20012782w-rb53">
(  ch )
< /roundbracket>
Adran 74 , fel y 'i diwygiwyd gan Ddeddf 2001 , sy 'n galluogi 'r Cynulliad i wneud rheoliadau ac sy 'n gwneud darpariaeth arall am archwilio achosion penodol gan y Comisiynydd ;
< /sentence>
< sentence type="heading" elementid="20012782w-s57">
< roundbracket elementid="20012782w-rb54">
(  d )
< /roundbracket>
Adran 75 , sy 'n galluogi 'r Comisiynydd i ardystio tramgwydd i 'r Uchel Lys yn achos ymddygiad rhwystrol penodol ;
< /sentence>
< sentence type="heading" elementid="20012782w-s58">
< roundbracket elementid="20012782w-rb55">
(  dd )
< /roundbracket>
Adran 75A , fel y 'i mewnosodwyd gan Ddeddf 2001 , sy 'n ymwneud â ph er y Comisiynydd i ystyried unrhyw fater sy 'n effeithio ar hawliau neu les plant yng Nghymru , a chyflwyno sylw i 'r Cynulliad yn eu cylch ;
< /sentence>
< sentence type="normal" elementid="20012782w-s59">
< roundbracket elementid="20012782w-rb56">
(  e )
< /roundbracket>
Adran 76 , fel y 'i diwygiwyd gan Ddeddf 2001 , sy 'n galluogi 'r Cynulliad i wneud rheoliadau ynghylch cymorth ariannol a chymorth arall y gall y Comisiynydd ei roi mewn achosion penodol , ac ynghylch adroddiadau a phwerau a dyletswyddau eraill y gall y Cynulliad eu rhoi o dan Ran V o Ddeddf 2000 .
< /sentence>
< sentence type="heading" elementid="20012782w-s60">
Mae adran 76 hefyd yn galluogi 'r Comisiynydd i roi cyngor a gwybodaeth i unrhyw un mewn cysylltiad â 'i swyddogaethau , ac yn ymdrin ag enwi unigolion mewn adroddiadau ac yn rhoi braint absoliwt ar gynnwys adroddiadau 'r Comisiynydd ;
< /sentence>
< sentence type="heading" elementid="20012782w-s61">
< roundbracket elementid="20012782w-rb57">
(  f )
< /roundbracket>
Adran 77 , sy 'n darparu ar gyfer cyfyngu ar swyddogaethau 'r Comisiynydd mewn perthynas ag achosion penodol ac mewn perthynas â swyddogaethau personau a ragnodir gan reoliadau a wneir gan y Cynulliad ;
< /sentence>
< sentence type="heading" elementid="20012782w-s62">
< roundbracket elementid="20012782w-rb58">
(  ff )
< /roundbracket>
Adran 78 , fel y 'i diwygiwyd gan Ddeddf 2001 , sy 'n cynnwys darpariaethau dehongli ac sy 'n galluogi 'r Cynulliad i wneud rheoliadau sy 'n estyn ychydig ar awdurdodaeth y Comisiynydd mewn perthynas â phlant penodol ;
< /sentence>
< sentence type="heading" elementid="20012782w-s63">
< roundbracket elementid="20012782w-rb59">
(  g )
< /roundbracket>
Atodlenni 2A a 2B a grybwyllir uchod , fel y 'u mewnosodwyd gan Ddeddf 2001 ; ac
< /sentence>
< sentence type="normal" elementid="20012782w-s64">
< roundbracket elementid="20012782w-rb60">
(  h )
< /roundbracket>
Paragraff 3 o Atodlen 5 sy 'n gwneud darpariaeth drosiannol sy 'n ymdrin â chyfeiriadau at ddarparwyr a gwasanaethau fel cyfeiriadau at ddarparwyr a gwasanaethau a fyddai 'n cael eu rheoleiddio yn unol â Deddf 2000 , pe bai rhai o ddarpariaethau eraill Deddf 2000 mewn grym .
< /sentence>
< sentence type="heading" elementid="20012782w-s65">
< strong elementid="20012782w-st7">
NODYN AM ORCHMYNION CYCHWYN BLAENOROL
< /strong>
< /sentence>
< sentence type="heading" elementid="20012782w-s66">
< em elementid="20012782w-em6">
< roundbracket elementid="20012782w-rb61">
(  Nid yw 'r nodyn hwn yn rhan o 'r Gorchymyn )
< /roundbracket>
< /em>
< /sentence>
< /p>
< p elementid="20012782w-p16">
< sentence type="normal" elementid="20012782w-s67">
Mae darpariaethau Deddf Safonau Gofal 2000  'y Ddeddf ') y gwneir cofnod ar eu cyfer yn y Tabl isod wedi 'u dwyn i rym neu i 'w dwyn i rym mewn perthynas â Chymru ar y dyddiad a bennir ochr yn ochr â 'u cofnod drwy orchmynion cychwyn sydd wedi 'u gwneud cyn dyddiad y Gorchymyn hwn .
< /sentence>
< sentence type="normal" elementid="20012782w-s68">
Cafodd y darpariaethau hynny y dilynir eu cofnod ag '(
< strong elementid="20012782w-st8">
a
< /strong>
)
< singlequotation elementid="20012782w-sq1">
'  eu dwyn i rym gan O.S. 2000/ 2992
< roundbracket elementid="20012782w-rb62">
(  Cy.192 )
< /roundbracket>
< roundbracket elementid="20012782w-rb63">
(  C.93 )
< /roundbracket>
; cafodd y rhai a ddilynir â '(
< strong elementid="20012782w-st9">
b
< /strong>
) '
< /singlequotation>
eu dwyn i rym gan O.S. 2001/ 139
< roundbracket elementid="20012782w-rb64">
(  Cy.5 )
< /roundbracket>
< roundbracket elementid="20012782w-rb65">
(  C.7 )
< /roundbracket>
; cafodd y rhai a ddilynir ag '(
< strong elementid="20012782w-st10">
c
< /strong>
)
< singlequotation elementid="20012782w-sq2">
'  eu dwyn i rym gan O.S. 2001/ 2190
< roundbracket elementid="20012782w-rb66">
(  Cy.152 )
< /roundbracket>
< roundbracket elementid="20012782w-rb67">
(  C.70 )
< /roundbracket>
; cafodd y rhai a ddilynir ag '(
< strong elementid="20012782w-st11">
ch
< /strong>
) '
< /singlequotation>
eu dwyn i rym gan O.S. 2001/ 2354
< roundbracket elementid="20012782w-rb68">
(  Cy.192 )
< /roundbracket>
< roundbracket elementid="20012782w-rb69">
(  C.80 )
< /roundbracket>
; cafodd y rhai a ddilynir â '(
< strong elementid="20012782w-st12">
d
< /strong>
)
< singlequotation elementid="20012782w-sq3">
'  eu dwyn i rym gan O.S. 2001/ 2504
< roundbracket elementid="20012782w-rb70">
(  Cy.205 )
< /roundbracket>
< roundbracket elementid="20012782w-rb71">
(  C.82 )
< /roundbracket>
; a chafodd y rhai a ddilynir â '( dd ) '
< /singlequotation>
eu dwyn i rym gan O.S. 2001/ 2538
< roundbracket elementid="20012782w-rb72">
(  Cy.213 )
< /roundbracket>
< roundbracket elementid="20012782w-rb73">
(  C.83 )
< /roundbracket> .
< /sentence>
< /p>
< table elementid="20012782w-tb2">
< td elementid="20012782w-td3">
< p elementid="20012782w-p17">
< sentence type="heading" elementid="20012782w-s69">
< em elementid="20012782w-em7">
Y ddarpariaeth
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td4">
< p elementid="20012782w-p18">
< sentence type="heading" elementid="20012782w-s70">
< em elementid="20012782w-em8">
Y dyddiad cychwyn
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td5">
< p elementid="20012782w-p19">
< sentence type="heading" elementid="20012782w-s71">
Adrannau 1 - 5
< roundbracket elementid="20012782w-rb74">
(
< strong elementid="20012782w-st13">
c
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td6">
< p elementid="20012782w-p20">
< sentence type="heading" elementid="20012782w-s72">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td7">
< p elementid="20012782w-p21">
< sentence type="heading" elementid="20012782w-s73">
Adran 7
< roundbracket elementid="20012782w-rb75">
(  7 )
< /roundbracket>
< roundbracket elementid="20012782w-rb76">
(
< strong elementid="20012782w-st14">
c
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td8">
< p elementid="20012782w-p22">
< sentence type="heading" elementid="20012782w-s74">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td9">
< p elementid="20012782w-p23">
< sentence type="heading" elementid="20012782w-s75">
Adran 8
< roundbracket elementid="20012782w-rb77">
(  yn rhannol )
< /roundbracket>
< roundbracket elementid="20012782w-rb78">
(
< strong elementid="20012782w-st15">
c
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td10">
< p elementid="20012782w-p24">
< sentence type="heading" elementid="20012782w-s76">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td11">
< p elementid="20012782w-p25">
< sentence type="heading" elementid="20012782w-s77">
Adran 9
< roundbracket elementid="20012782w-rb79">
(  3 )
< /roundbracket>
 - 
< roundbracket elementid="20012782w-rb80">
(  5 )
< /roundbracket>
< roundbracket elementid="20012782w-rb81">
(
< strong elementid="20012782w-st16">
c
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td12">
< p elementid="20012782w-p26">
< sentence type="heading" elementid="20012782w-s78">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td13">
< p elementid="20012782w-p27">
< sentence type="heading" elementid="20012782w-s79">
Adrannau 11 - 12
< roundbracket elementid="20012782w-rb82">
(  yn rhannol )
< /roundbracket>
< roundbracket elementid="20012782w-rb83">
(
< strong elementid="20012782w-st17">
c
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td14">
< p elementid="20012782w-p28">
< sentence type="heading" elementid="20012782w-s80">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td15">
< p elementid="20012782w-p29">
< sentence type="heading" elementid="20012782w-s81">
Adrannau 14 - 15
< roundbracket elementid="20012782w-rb84">
(  yn rhannol )
< /roundbracket>
< roundbracket elementid="20012782w-rb85">
(
< strong elementid="20012782w-st18">
c
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td16">
< p elementid="20012782w-p30">
< sentence type="heading" elementid="20012782w-s82">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td17">
< p elementid="20012782w-p31">
< sentence type="heading" elementid="20012782w-s83">
Adran 16
< roundbracket elementid="20012782w-rb86">
(
< strong elementid="20012782w-st19">
c
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td18">
< p elementid="20012782w-p32">
< sentence type="heading" elementid="20012782w-s84">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td19">
< p elementid="20012782w-p33">
< sentence type="heading" elementid="20012782w-s85">
Adrannau 22 - 23
< roundbracket elementid="20012782w-rb87">
(
< strong elementid="20012782w-st20">
c
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td20">
< p elementid="20012782w-p34">
< sentence type="heading" elementid="20012782w-s86">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td21">
< p elementid="20012782w-p35">
< sentence type="heading" elementid="20012782w-s87">
Adran 25
< roundbracket elementid="20012782w-rb88">
(
< strong elementid="20012782w-st21">
c
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td22">
< p elementid="20012782w-p36">
< sentence type="heading" elementid="20012782w-s88">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td23">
< p elementid="20012782w-p37">
< sentence type="heading" elementid="20012782w-s89">
Adrannau 33 - 35
< roundbracket elementid="20012782w-rb89">
(
< strong elementid="20012782w-st22">
c
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td24">
< p elementid="20012782w-p38">
< sentence type="heading" elementid="20012782w-s90">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td25">
< p elementid="20012782w-p39">
< sentence type="heading" elementid="20012782w-s91">
Adran 36
< roundbracket elementid="20012782w-rb90">
(  yn rhannol )
< /roundbracket>
< roundbracket elementid="20012782w-rb91">
(
< strong elementid="20012782w-st23">
c
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td26">
< p elementid="20012782w-p40">
< sentence type="heading" elementid="20012782w-s92">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td27">
< p elementid="20012782w-p41">
< sentence type="heading" elementid="20012782w-s93">
Adran 38
< roundbracket elementid="20012782w-rb92">
(
< strong elementid="20012782w-st24">
c
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td28">
< p elementid="20012782w-p42">
< sentence type="heading" elementid="20012782w-s94">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td29">
< p elementid="20012782w-p43">
< sentence type="heading" elementid="20012782w-s95">
Adran 39
< roundbracket elementid="20012782w-rb93">
(  yn rhannol )
< /roundbracket>
< roundbracket elementid="20012782w-rb94">
(
< strong elementid="20012782w-st25">
d
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td30">
< p elementid="20012782w-p44">
< sentence type="heading" elementid="20012782w-s96">
31 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td31">
< p elementid="20012782w-p45">
< sentence type="heading" elementid="20012782w-s97">
Adran 39
< roundbracket elementid="20012782w-rb95">
(  y gweddill )
< /roundbracket>
< roundbracket elementid="20012782w-rb96">
(
< strong elementid="20012782w-st26">
d
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td32">
< p elementid="20012782w-p46">
< sentence type="heading" elementid="20012782w-s98">
31 Awst 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td33">
< p elementid="20012782w-p47">
< sentence type="heading" elementid="20012782w-s99">
Adran 40
< roundbracket elementid="20012782w-rb97">
(  yn rhannol )
< /roundbracket>
< roundbracket elementid="20012782w-rb98">
(
< strong elementid="20012782w-st27">
b
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td34">
< p elementid="20012782w-p48">
< sentence type="heading" elementid="20012782w-s100">
1 Chwefror 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td35">
< p elementid="20012782w-p49">
< sentence type="heading" elementid="20012782w-s101">
Adran 40
< roundbracket elementid="20012782w-rb99">
(  y gweddill )
< /roundbracket>
< roundbracket elementid="20012782w-rb100">
(
< strong elementid="20012782w-st28">
b
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td36">
< p elementid="20012782w-p50">
< sentence type="heading" elementid="20012782w-s102">
28 Chwefror 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td37">
< p elementid="20012782w-p51">
< sentence type="heading" elementid="20012782w-s103">
Adran 41
< roundbracket elementid="20012782w-rb101">
(
< strong elementid="20012782w-st29">
b
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td38">
< p elementid="20012782w-p52">
< sentence type="heading" elementid="20012782w-s104">
28 Chwefror 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td39">
< p elementid="20012782w-p53">
< sentence type="heading" elementid="20012782w-s105">
Adrannau 42 - 43
< roundbracket elementid="20012782w-rb102">
(
< strong elementid="20012782w-st30">
c
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td40">
< p elementid="20012782w-p54">
< sentence type="heading" elementid="20012782w-s106">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td41">
< p elementid="20012782w-p55">
< sentence type="heading" elementid="20012782w-s107">
Adrannau 48 - 52
< roundbracket elementid="20012782w-rb103">
(
< strong elementid="20012782w-st31">
c
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td42">
< p elementid="20012782w-p56">
< sentence type="heading" elementid="20012782w-s108">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td43">
< p elementid="20012782w-p57">
< sentence type="heading" elementid="20012782w-s109">
Adran 54
< roundbracket elementid="20012782w-rb104">
(  1 )
< /roundbracket>
,
< roundbracket elementid="20012782w-rb105">
(  3 )
< /roundbracket>
 - 
< roundbracket elementid="20012782w-rb106">
(  7 )
< /roundbracket>
< roundbracket elementid="20012782w-rb107">
(
< strong elementid="20012782w-st32">
a
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td44">
< p elementid="20012782w-p58">
< sentence type="heading" elementid="20012782w-s110">
1 Ebrill 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td45">
< p elementid="20012782w-p59">
< sentence type="heading" elementid="20012782w-s111">
Adran 55 ac Atodlen 1
< roundbracket elementid="20012782w-rb108">
(
< strong elementid="20012782w-st33">
a
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td46">
< p elementid="20012782w-p60">
< sentence type="heading" elementid="20012782w-s112">
13 Tachwedd 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td47">
< p elementid="20012782w-p61">
< sentence type="heading" elementid="20012782w-s113">
Adran 63
< roundbracket elementid="20012782w-rb109">
(
< strong elementid="20012782w-st34">
dd
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td48">
< p elementid="20012782w-p62">
< sentence type="heading" elementid="20012782w-s114">
31 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td49">
< p elementid="20012782w-p63">
< sentence type="heading" elementid="20012782w-s115">
Adran 66
< roundbracket elementid="20012782w-rb110">
(
< strong elementid="20012782w-st35">
dd
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td50">
< p elementid="20012782w-p64">
< sentence type="heading" elementid="20012782w-s116">
31 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td51">
< p elementid="20012782w-p65">
< sentence type="heading" elementid="20012782w-s117">
Adran 67
< roundbracket elementid="20012782w-rb111">
(
< strong elementid="20012782w-st36">
dd
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td52">
< p elementid="20012782w-p66">
< sentence type="heading" elementid="20012782w-s118">
1 Hydref 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td53">
< p elementid="20012782w-p67">
< sentence type="heading" elementid="20012782w-s119">
Adran 70
< roundbracket elementid="20012782w-rb112">
(  1 )
< /roundbracket>
< roundbracket elementid="20012782w-rb113">
(
< strong elementid="20012782w-st37">
dd
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td54">
< p elementid="20012782w-p68">
< sentence type="heading" elementid="20012782w-s120">
1 Hydref 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td55">
< p elementid="20012782w-p69">
< sentence type="heading" elementid="20012782w-s121">
Adran 71
< roundbracket elementid="20012782w-rb114">
(
< strong elementid="20012782w-st38">
dd
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td56">
< p elementid="20012782w-p70">
< sentence type="heading" elementid="20012782w-s122">
31 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td57">
< p elementid="20012782w-p71">
< sentence type="heading" elementid="20012782w-s123">
Adran 72 ac Atodlen 2
< roundbracket elementid="20012782w-rb115">
(
< strong elementid="20012782w-st39">
a
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td58">
< p elementid="20012782w-p72">
< sentence type="heading" elementid="20012782w-s124">
1 Ebrill 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td59">
< p elementid="20012782w-p73">
< sentence type="heading" elementid="20012782w-s125">
Adran 79
< roundbracket elementid="20012782w-rb116">
(  1 )
< /roundbracket>
< roundbracket elementid="20012782w-rb117">
(  yn rhannol )
< /roundbracket>
< roundbracket elementid="20012782w-rb118">
(
< strong elementid="20012782w-st40">
c
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td60">
< p elementid="20012782w-p74">
< sentence type="heading" elementid="20012782w-s126">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td61">
< p elementid="20012782w-p75">
< sentence type="heading" elementid="20012782w-s127">
Adran 79
< roundbracket elementid="20012782w-rb119">
(  2 )
< /roundbracket>
ac Atodlen 3
< roundbracket elementid="20012782w-rb120">
(  yn rhannol )
< /roundbracket>
< roundbracket elementid="20012782w-rb121">
(
< strong elementid="20012782w-st41">
c
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td62">
< p elementid="20012782w-p76">
< sentence type="heading" elementid="20012782w-s128">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td63">
< p elementid="20012782w-p77">
< sentence type="heading" elementid="20012782w-s129">
Adran 79
< roundbracket elementid="20012782w-rb122">
(  3 ),( 4 )
< /roundbracket>
< roundbracket elementid="20012782w-rb123">
(
< strong elementid="20012782w-st42">
c
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td64">
< p elementid="20012782w-p78">
< sentence type="heading" elementid="20012782w-s130">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td65">
< p elementid="20012782w-p79">
< sentence type="heading" elementid="20012782w-s131">
Adran 98
< roundbracket elementid="20012782w-rb124">
(
< strong elementid="20012782w-st43">
ch
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td66">
< p elementid="20012782w-p80">
< sentence type="heading" elementid="20012782w-s132">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td67">
< p elementid="20012782w-p81">
< sentence type="heading" elementid="20012782w-s133">
Adrannau 107 - 108
< roundbracket elementid="20012782w-rb125">
(
< strong elementid="20012782w-st44">
c
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td68">
< p elementid="20012782w-p82">
< sentence type="heading" elementid="20012782w-s134">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td69">
< p elementid="20012782w-p83">
< sentence type="heading" elementid="20012782w-s135">
Adran 112
< roundbracket elementid="20012782w-rb126">
(
< strong elementid="20012782w-st45">
c
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td70">
< p elementid="20012782w-p84">
< sentence type="heading" elementid="20012782w-s136">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td71">
< p elementid="20012782w-p85">
< sentence type="heading" elementid="20012782w-s137">
Adran 113
< roundbracket elementid="20012782w-rb127">
(  2 )
< /roundbracket>
 - 
< roundbracket elementid="20012782w-rb128">
(  4 )
< /roundbracket>
< roundbracket elementid="20012782w-rb129">
(
< strong elementid="20012782w-st46">
a
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td72">
< p elementid="20012782w-p86">
< sentence type="heading" elementid="20012782w-s138">
1 Ebrill 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td73">
< p elementid="20012782w-p87">
< sentence type="heading" elementid="20012782w-s139">
Adran 114
< roundbracket elementid="20012782w-rb130">
(  yn rhannol )
< /roundbracket>
< roundbracket elementid="20012782w-rb131">
(
< strong elementid="20012782w-st47">
a
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td74">
< p elementid="20012782w-p88">
< sentence type="heading" elementid="20012782w-s140">
1 Ebrill 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td75">
< p elementid="20012782w-p89">
< sentence type="heading" elementid="20012782w-s141">
Adran 114
< roundbracket elementid="20012782w-rb132">
(  y gweddill )
< /roundbracket>
< roundbracket elementid="20012782w-rb133">
(
< strong elementid="20012782w-st48">
c
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td76">
< p elementid="20012782w-p90">
< sentence type="heading" elementid="20012782w-s142">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td77">
< p elementid="20012782w-p91">
< sentence type="heading" elementid="20012782w-s143">
Adran 115
< roundbracket elementid="20012782w-rb134">
(
< strong elementid="20012782w-st49">
c
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td78">
< p elementid="20012782w-p92">
< sentence type="heading" elementid="20012782w-s144">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td79">
< p elementid="20012782w-p93">
< sentence type="heading" elementid="20012782w-s145">
Adran 116 ac Atodlen 4
< roundbracket elementid="20012782w-rb135">
(  yn rhannol )
< /roundbracket>
< roundbracket elementid="20012782w-rb136">
(
< strong elementid="20012782w-st50">
b
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td80">
< p elementid="20012782w-p94">
< sentence type="heading" elementid="20012782w-s146">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td81">
< p elementid="20012782w-p95">
< sentence type="heading" elementid="20012782w-s147">
Adran 116 ac Atodlen 4
< roundbracket elementid="20012782w-rb137">
(  yn rhannol )
< /roundbracket>
< roundbracket elementid="20012782w-rb138">
(
< strong elementid="20012782w-st51">
b
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td82">
< p elementid="20012782w-p96">
< sentence type="heading" elementid="20012782w-s148">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td83">
< p elementid="20012782w-p97">
< sentence type="heading" elementid="20012782w-s149">
Adran 117
< roundbracket elementid="20012782w-rb139">
(  1 )
< /roundbracket>
ac Atodlen 5
< roundbracket elementid="20012782w-rb140">
(  yn rhannol )
< /roundbracket>
< roundbracket elementid="20012782w-rb141">
(
< strong elementid="20012782w-st52">
c
< /strong>
)
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td84">
< p elementid="20012782w-p98">
< sentence type="heading" elementid="20012782w-s150">
1 Gorffennaf 2001
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20012782w-p99">
< sentence type="normal" elementid="20012782w-s151">
Mae darpariaethau 'r Ddeddf y gwneir cofnod ar eu cyfer yn y Tabl isod wedi 'u dwyn i rym gan O.S. 2000/ 2544
< roundbracket elementid="20012782w-rb142">
(  C.72 )
< /roundbracket>
mewn perthynas â Chymru , yn ogystal ag mewn perthynas â Lloegr , ar y dyddiad a bennir ochr yn ochr â 'u cofnod .
< /sentence>
< /p>
< table elementid="20012782w-tb3">
< td elementid="20012782w-td85">
< p elementid="20012782w-p100">
< sentence type="heading" elementid="20012782w-s152">
< em elementid="20012782w-em9">
Y ddarpariaeth
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td86">
< p elementid="20012782w-p101">
< sentence type="heading" elementid="20012782w-s153">
< em elementid="20012782w-em10">
Y dyddiad cychwyn
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td87">
< p elementid="20012782w-p102">
< sentence type="heading" elementid="20012782w-s154">
Adran 80
< roundbracket elementid="20012782w-rb143">
(  8 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td88">
< p elementid="20012782w-p103">
< sentence type="heading" elementid="20012782w-s155">
2 Hydref 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td89">
< p elementid="20012782w-p104">
< sentence type="heading" elementid="20012782w-s156">
Adran 94
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td90">
< p elementid="20012782w-p105">
< sentence type="heading" elementid="20012782w-s157">
2 Hydref 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td91">
< p elementid="20012782w-p106">
< sentence type="heading" elementid="20012782w-s158">
Adran 96
< roundbracket elementid="20012782w-rb144">
(  yn rhannol )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td92">
< p elementid="20012782w-p107">
< sentence type="heading" elementid="20012782w-s159">
15 Medi 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td93">
< p elementid="20012782w-p108">
< sentence type="heading" elementid="20012782w-s160">
Adran 96
< roundbracket elementid="20012782w-rb145">
(  y gweddill )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td94">
< p elementid="20012782w-p109">
< sentence type="heading" elementid="20012782w-s161">
2 Hydref 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td95">
< p elementid="20012782w-p110">
< sentence type="heading" elementid="20012782w-s162">
Adran 99
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td96">
< p elementid="20012782w-p111">
< sentence type="heading" elementid="20012782w-s163">
15 Medi 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td97">
< p elementid="20012782w-p112">
< sentence type="heading" elementid="20012782w-s164">
Adran 100
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td98">
< p elementid="20012782w-p113">
< sentence type="heading" elementid="20012782w-s165">
2 Hydref 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td99">
< p elementid="20012782w-p114">
< sentence type="heading" elementid="20012782w-s166">
Adran 101
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td100">
< p elementid="20012782w-p115">
< sentence type="heading" elementid="20012782w-s167">
2 Hydref 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td101">
< p elementid="20012782w-p116">
< sentence type="heading" elementid="20012782w-s168">
Adran 103
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td102">
< p elementid="20012782w-p117">
< sentence type="heading" elementid="20012782w-s169">
2 Hydref 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td103">
< p elementid="20012782w-p118">
< sentence type="heading" elementid="20012782w-s170">
Adran 116 ac Atodlen 4
< roundbracket elementid="20012782w-rb146">
(  yn rhannol )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td104">
< p elementid="20012782w-p119">
< sentence type="heading" elementid="20012782w-s171">
2 Hydref 2001
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td105">
< p elementid="20012782w-p120">
< sentence type="heading" elementid="20012782w-s172">
Adran 117
< roundbracket elementid="20012782w-rb147">
(  2 )
< /roundbracket>
ac Atodlen 6
< roundbracket elementid="20012782w-rb148">
(  yn rhannol )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td106">
< p elementid="20012782w-p121">
< sentence type="heading" elementid="20012782w-s173">
2 Hydref 2001
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20012782w-p122">
< sentence type="normal" elementid="20012782w-s174">
Yn ychwanegol , mae amryw o ddarpariaethau eraill y Ddeddf wedi 'u dwyn i rym mewn perthynas â Lloegr gan yr Offerynnau Statudol canlynol : O.S. 2000/ 2795
< roundbracket elementid="20012782w-rb149">
(  C.79 )
< /roundbracket>
; O.S. 2001/ 290
< roundbracket elementid="20012782w-rb150">
(  C.17 )
< /roundbracket>
; O.S. 2001/ 1210
< roundbracket elementid="20012782w-rb151">
(  C.41 )
< /roundbracket>
; O.S. 2001/ 1536
< roundbracket elementid="20012782w-rb152">
(  C.55 )
< /roundbracket>
; a O.S 2001/ 2041
< roundbracket elementid="20012782w-rb153">
(  C.68 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20012782w-s175">
< em elementid="20012782w-em11">
Notes :
< /em>
< /sentence>
< sentence type="normal" elementid="20012782w-s176">
< squarebracket elementid="20012782w-sb3">
[  1 ]
< /squarebracket>
2000 p.14 .
< /sentence>
< sentence type="normal" elementid="20012782w-s177">
Mae 'r p er yn arferadwy gan y Gweinidog priodol .
< /sentence>
< sentence type="normal" elementid="20012782w-s178">
Diffinnir y Gweinidog priodol yn adran 121
< roundbracket elementid="20012782w-rb154">
(  1 )
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20012782w-s179">
Mewn perthynas â Chymru mae 'n golygu Cynulliad Cenedlaethol Cymru .
< /sentence>
< sentence type="normal" elementid="20012782w-s180">
Mewnosodwyd adrannau 72A , 72B , 75A ac Atodlenni 2A a 2B a diwygiwyd adrannau 73 , 74 , 76 a 78 gan Ddeddf Comisiynydd Plant Cymru 2001
< roundbracket elementid="20012782w-rb155">
(  p.18 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20012782w-s181">
< a type="href" elementid="20012782w-a6">
back
< /a>
< /sentence>
< /p>
< p elementid="20012782w-p123">
< sentence type="heading" elementid="20012782w-s182">
< squarebracket elementid="20012782w-sb4">
[  2 ]
< /squarebracket>
1998 p.38 
< a type="href" elementid="20012782w-a7">
back
< /a>
< /sentence>
< /p>
< p elementid="20012782w-p124">
< sentence type="heading" elementid="20012782w-s183">
< a type="href" elementid="20012782w-a8">
< english>
English version
< /english>
< /a>
< /sentence>
< sentence type="heading" elementid="20012782w-s184">
ISBN 0 11090326 9
< /sentence>
< /p>
< table elementid="20012782w-tb4">
< td elementid="20012782w-td107">
< p elementid="20012782w-p125">
< sentence type="heading" elementid="20012782w-s185">
< a type="href" elementid="20012782w-a9">
< english>
Other UK SIs
< /english>
< /a>
< a type="href" elementid="20012782w-a10">
< english>
Home
< /english>
< /a>
< a type="href" elementid="20012782w-a11">
< english>
National Assembly for Wales Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20012782w-a12">
< english>
Scottish Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20012782w-a13">
< english>
Statutory Rules of Northern Ireland
< /english>
< /a>
< a type="href" elementid="20012782w-a14">
< english>
Her Majesty 's Stationery Office
< /english>
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td108">
< p elementid="20012782w-p126">
< sentence type="heading" elementid="20012782w-s186">
< english>
< strong elementid="20012782w-st53">
We welcome your 
< a type="href" elementid="20012782w-a15">
comments
< /a>
on this site
< /strong>
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td109">
< p elementid="20012782w-p127">
< sentence type="heading" elementid="20012782w-s187">
< english>
© Crown copyright 2001
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20012782w-td110">
< p elementid="20012782w-p128">
< sentence type="heading" elementid="20012782w-s188">
< english>
Prepared 24 August 2001
< /english>
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>1880< /wordcount>
< /file>
< file elementid="20012783w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Gorchymyn Deddf Comisiynydd Plant Cymru 2001 ( Cychwyn ) 2001< /pagetitle>
< doc_class>Statutory Instrument - Wales< /doc_class>
< year>2001< /year>
< number>2783< /number>
< legislation_code>30102< /legislation_code>
< legislation_type>Statutory Instrument - Wales< /legislation_type>
< subject>COMISIYNYDD PLANT, CYMRU< /subject>
< laid_date>_EMPTY< /laid_date>
< made_date>20011225< /made_date>
< coming_into_force>_EMPTY< /coming_into_force>
< isbn>0110903277< /isbn>
< title>Gorchymyn Deddf Comisiynydd Plant Cymru 2001(Cychwyn) 2001< /title>
< signatory>Rhodri Morgan< /signatory>
< signatory_title>Prif weinidog y Cynulliad Cenedlaethol Cymru< /signatory_title>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20012783w-p1">
< sentence type="heading" elementid="20012783w-s1">
Offerynnau Statudol 2001 Rhif 2783
< roundbracket elementid="20012783w-rb1">
(  Cy.236 )
< /roundbracket>
< roundbracket elementid="20012783w-rb2">
(  C.93 )
< /roundbracket>
< /sentence>
< /p>
< p elementid="20012783w-p2">
< sentence type="heading" elementid="20012783w-s2">
< strong elementid="20012783w-st1">
Gorchymyn Deddf Comisiynydd Plant Cymru 2001
< roundbracket elementid="20012783w-rb3">
(  Cychwyn )
< /roundbracket>
2001
< /strong>
< /sentence>
< /p>
< p elementid="20012783w-p3">
< sentence type="heading" elementid="20012783w-s3">
< english>
© Crown Copyright 2001
< /english>
< /sentence>
< /p>
< p elementid="20012783w-p4">
< sentence type="normal" elementid="20012783w-s4">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20012783w-p5">
< sentence type="normal" elementid="20012783w-s5">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20012783w-s6">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20012783w-p6">
< sentence type="normal" elementid="20012783w-s7">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20012783w-s8">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20012783w-s9">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20012783w-p7">
< sentence type="normal" elementid="20012783w-s10">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20012783w-s11">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan 
< english>
The Stationery Office Limited
< /english>
 fel
< strong elementid="20012783w-st2">
Gorchymyn Deddf Comisiynydd Plant Cymru 2001
< roundbracket elementid="20012783w-rb4">
(  Cychwyn )
< /roundbracket>
2001
< /strong>
, ISBN 0 11090327 7 .
< /sentence>
< sentence type="normal" elementid="20012783w-s12">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20012783w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20012783w-s13">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20012783w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20012783w-p8">
< sentence type="normal" elementid="20012783w-s14">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20012783w-a3">
< english>
Frequently Asked Questions
< /english>
< /a> .
< /sentence>
< /p>
< p elementid="20012783w-p9">
< sentence type="normal" elementid="20012783w-s15">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20012783w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20012783w-s16">
Pan welwch fotwm
< doublequotation elementid="20012783w-dq2">
"
< english>
continue
< /english>
"
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20012783w-p10">
< sentence type="heading" elementid="20012783w-s17">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="heading" elementid="20012783w-s18">
< strong elementid="20012783w-st3">
2001 Rhif 2783
< roundbracket elementid="20012783w-rb5">
(  Cy.236 )
< /roundbracket>
< roundbracket elementid="20012783w-rb6">
(  C.93 )
< /roundbracket>
< /strong>
< /sentence>
< sentence type="heading" elementid="20012783w-s19">
< strong elementid="20012783w-st4">
COMISIYNYDD PLANT , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20012783w-s20">
Gorchymyn Deddf Comisiynydd Plant Cymru 2001
< roundbracket elementid="20012783w-rb7">
(  Cychwyn )
< /roundbracket>
2001
< /sentence>
< /p>
< table elementid="20012783w-tb1">
< td elementid="20012783w-td1">
< p elementid="20012783w-p11">
< sentence type="heading" elementid="20012783w-s21">
< em elementid="20012783w-em1">
Wedi 'i wneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20012783w-td2">
< p elementid="20012783w-p12">
< sentence type="heading" elementid="20012783w-s22">
< em elementid="20012783w-em2">
25 Gorffennaf 2001
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20012783w-p13">
< sentence type="heading" elementid="20012783w-s23">
Mae Cynulliad Cenedlaethol Cymru 'n gwneud y Gorchymyn canlynol drwy arfer y pwerau a roddir gan adran 9
< roundbracket elementid="20012783w-rb8">
(  1 )
< /roundbracket>
o Ddeddf Comisiynydd Plant Cymru 2001
< squarebracket elementid="20012783w-sb1">
[ 
< a type="href" elementid="20012783w-a4">
1
< /a>
]
< /squarebracket>
:
< /sentence>
< sentence type="heading" elementid="20012783w-s24">
< strong elementid="20012783w-st5">
Enwi a dehongli
< /strong>
< /sentence>
< sentence type="normal" elementid="20012783w-s25">
1 .
< /sentence>
< sentence type="normal" elementid="20012783w-s26">
 - 
< roundbracket elementid="20012783w-rb9">
(  1 )
< /roundbracket>
Enw 'r Gorchymyn hwn yw Gorchymyn Deddf Comisiynydd Plant Cymru 2001
< roundbracket elementid="20012783w-rb10">
(  Cychwyn )
< /roundbracket>
2001 .
< /sentence>
< sentence type="normal" elementid="20012783w-s27">
< roundbracket elementid="20012783w-rb11">
(  2 )
< /roundbracket>
Yn y Gorchymyn hwn , ystyr
< doublequotation elementid="20012783w-dq3">
"  y Ddeddf "
< /doublequotation>
< roundbracket elementid="20012783w-rb12">
(
< doublequotation elementid="20012783w-dq4">
"
< em elementid="20012783w-em3">
< english>
the Act
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw Deddf Comisiynydd Plant Cymru 2001 .
< /sentence>
< sentence type="heading" elementid="20012783w-s28">
< strong elementid="20012783w-st6">
Y Diwrnod Penodedig
< /strong>
< /sentence>
< sentence type="normal" elementid="20012783w-s29">
2 .
< /sentence>
< sentence type="normal" elementid="20012783w-s30">
 - 
< roundbracket elementid="20012783w-rb13">
(  1 )
< /roundbracket>
Y diwrnod penodedig ar gyfer dod â darpariaethau 'r Ddeddf a bennir ym mharagraff
< roundbracket elementid="20012783w-rb14">
(  2 )
< /roundbracket>
i rym yw 26 Awst 2001 .
< /sentence>
< sentence type="heading" elementid="20012783w-s31">
< roundbracket elementid="20012783w-rb15">
(  2 )
< /roundbracket>
Dyma 'r darpariaethau - 
< /sentence>
< sentence type="heading" elementid="20012783w-s32">
< roundbracket elementid="20012783w-rb16">
(  a )
< /roundbracket>
Adran 1
< roundbracket elementid="20012783w-rb17">
(  cymhwyso Rhan 5 o Ddeddf Safonau Gofal 2000 )
< /roundbracket>
;
< /sentence>
< sentence type="heading" elementid="20012783w-s33">
< roundbracket elementid="20012783w-rb18">
(  b )
< /roundbracket>
Adran 2
< roundbracket elementid="20012783w-rb19">
(  prif nod y Comisiynydd )
< /roundbracket>
;
< /sentence>
< sentence type="heading" elementid="20012783w-s34">
< roundbracket elementid="20012783w-rb20">
(  c )
< /roundbracket>
Adran 3
< roundbracket elementid="20012783w-rb21">
(  adolygu arfer swyddogaethau 'r Cynulliad a swyddogaethau personau eraill )
< /roundbracket>
;
< /sentence>
< sentence type="heading" elementid="20012783w-s35">
< roundbracket elementid="20012783w-rb22">
(  ch )
< /roundbracket>
Adran 4
< roundbracket elementid="20012783w-rb23">
(  adolygu a monitro trefniadau )
< /roundbracket>
;
< /sentence>
< sentence type="heading" elementid="20012783w-s36">
< roundbracket elementid="20012783w-rb24">
(  d )
< /roundbracket>
Adran 5
< roundbracket elementid="20012783w-rb25">
(  p er ychwanegol i ystyried a gwneud sylwadau )
< /roundbracket>
;
< /sentence>
< sentence type="heading" elementid="20012783w-s37">
< roundbracket elementid="20012783w-rb26">
(  dd )
< /roundbracket>
Adran 5
< roundbracket elementid="20012783w-rb27">
(  swyddogaethau pellach y Comisiynydd )
< /roundbracket>
;
< /sentence>
< sentence type="heading" elementid="20012783w-s38">
< roundbracket elementid="20012783w-rb28">
(  e )
< /roundbracket>
Adran 6  darpariaethau canlyniadol etc.) ;
< /sentence>
< sentence type="heading" elementid="20012783w-s39">
< roundbracket elementid="20012783w-rb29">
(  f )
< /roundbracket>
Adran 7
< roundbracket elementid="20012783w-rb30">
(  darpariaethau ariannol )
< /roundbracket>
; ac
< /sentence>
< sentence type="normal" elementid="20012783w-s40">
< roundbracket elementid="20012783w-rb31">
(  ff )
< /roundbracket>
Yr Atodlen
< roundbracket elementid="20012783w-rb32">
(  personau a threfniadau sy 'n destun adolygiad )
< /roundbracket> .
< /sentence>
< /p>
< p elementid="20012783w-p14">
< sentence type="heading" elementid="20012783w-s41">
Llofnodwyd ar ran Cynulliad Cenedlaethol Cymru o dan adran 66
< roundbracket elementid="20012783w-rb33">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20012783w-sb2">
[ 
< a type="href" elementid="20012783w-a5">
2
< /a>
]
< /squarebracket>
< /sentence>
< sentence type="heading" elementid="20012783w-s42">
< em elementid="20012783w-em4">
Rhodri Morgan
< /em>
< /sentence>
< sentence type="heading" elementid="20012783w-s43">
Prif weinidog y Cynulliad Cenedlaethol Cymru
< /sentence>
< sentence type="heading" elementid="20012783w-s44">
25 Gorffennaf 2001
< /sentence>
< /p>
< p elementid="20012783w-p15">
< sentence type="heading" elementid="20012783w-s45">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20012783w-s46">
< em elementid="20012783w-em5">
< roundbracket elementid="20012783w-rb34">
(  Nid yw 'r nodyn hwn yn rhan o 'r Gorchymyn )
< /roundbracket>
< /em>
< /sentence>
< sentence type="heading" elementid="20012783w-s47">
Mae 'r Gorchymyn hwn yn penodi 26 Awst 2001 i 'r darpariaethau canlynol yn Neddf Comisiynydd Plant Cymru 2001
< roundbracket elementid="20012783w-rb35">
(
< doublequotation elementid="20012783w-dq5">
"  Deddf 2001 "
< /doublequotation>
)
< /roundbracket>
ddod i rym :  - 
< /sentence>
< sentence type="heading" elementid="20012783w-s48">
< roundbracket elementid="20012783w-rb36">
(  a )
< /roundbracket>
Adran 1 , sy 'n diwygio adran 78
< roundbracket elementid="20012783w-rb37">
(  dehongli )
< /roundbracket>
o Ddeddf Safonau Gofal 2000
< roundbracket elementid="20012783w-rb38">
(
< doublequotation elementid="20012783w-dq6">
"  Ddeddf 2000 "
< /doublequotation>
)
< /roundbracket>
;
< /sentence>
< sentence type="heading" elementid="20012783w-s49">
< roundbracket elementid="20012783w-rb39">
(  b )
< /roundbracket>
Adran 2 , sy 'n mewnosod adran 72A yn Neddf 2000 sy 'n darparu mai diogelu a hybu hawliau a lles plant y mae Rhan V o Ddeddf 2000 yn gymwys iddynt yw prif nod y Comisiynydd wrth arfer ei swyddogaethau ;
< /sentence>
< sentence type="normal" elementid="20012783w-s50">
< roundbracket elementid="20012783w-rb40">
(  c )
< /roundbracket>
Adran 3 , sy 'n mewnosod adran 72B yn Neddf 2000 sy 'n galluogi 'r Comisiynydd i adolygu effaith arfer swyddogaethau Cynulliad Cenedlaethol Cymru
< roundbracket elementid="20012783w-rb41">
(
< doublequotation elementid="20012783w-dq7">
"  y Cynulliad "
< /doublequotation>
)
< /roundbracket>
a swyddogaethau personau eraill a bennir yn Atodlen 2A i Ddeddf 2000 , sydd hefyd yn cael ei mewnosod gan adran 3 .
< /sentence>
< sentence type="heading" elementid="20012783w-s51">
Mae adran 72B yn darparu y gall y Cynulliad ddiwygio Atodlen 2A drwy orchymyn o dan amgylchiadau penodol ;
< /sentence>
< sentence type="normal" elementid="20012783w-s52">
< roundbracket elementid="20012783w-rb42">
(  ch )
< /roundbracket>
Adran 4 , sy 'n diwygio adran 73 o Ddeddf 2000 sy 'n ymdrin ag adolygu a monitro trefniadau penodol ar gyfer c ynion , eiriolaeth a chwythu 'r chwiban ac sy 'n mewnosod Atodlen 2B yn Neddf 2000 .
< /sentence>
< sentence type="heading" elementid="20012783w-s53">
Yn benodol mae 'r diwygiadau yn darparu y gall y Cynulliad ddiwygio Atodlen 2B drwy orchymyn o dan amgylchiadau penodol ;
< /sentence>
< sentence type="normal" elementid="20012783w-s54">
< roundbracket elementid="20012783w-rb43">
(  d )
< /roundbracket>
Adran 5 , sy 'n mewnosod adran 75A yn Neddf 2000 ynghyd a diwigiadau canlyniadol i adran 74 .
< /sentence>
< sentence type="heading" elementid="20012783w-s55">
Mae adran 75A yn galluogi 'r Comisiynydd i ystyried unrhyw fater sy 'n effeithio ar hawliau neu les plant yng Nghymru ac i wneud sylwadau i 'r cynulliad yng Nghylch hynny ;
< /sentence>
< sentence type="heading" elementid="20012783w-s56">
< roundbracket elementid="20012783w-rb44">
(  dd )
< /roundbracket>
Adran 6 , sy 'n gwneud mân ddiwygiadau canlyniadol i adran 76 o Ddeddf 2000 ;
< /sentence>
< sentence type="heading" elementid="20012783w-s57">
< roundbracket elementid="20012783w-rb45">
(  e )
< /roundbracket>
Adran 7 , sy 'n gwneud mân ddiwygiadau canlyniadol i adran 118
< roundbracket elementid="20012783w-rb46">
(  8 )
< /roundbracket>
o Ddeddf 2000 ;
< /sentence>
< sentence type="heading" elementid="20012783w-s58">
< roundbracket elementid="20012783w-rb47">
(  f )
< /roundbracket>
Adran 8 , sy 'n cynnwys darpariaethau ariannol canlyniadol ; ac
< /sentence>
< sentence type="normal" elementid="20012783w-s59">
< roundbracket elementid="20012783w-rb48">
(  g )
< /roundbracket>
yr Atodlen , sy 'n mewnosod Atodlen 2A a 2B yn Neddf 2000 .
< /sentence>
< sentence type="heading" elementid="20012783w-s60">
< em elementid="20012783w-em6">
Notes :
< /em>
< /sentence>
< sentence type="normal" elementid="20012783w-s61">
< squarebracket elementid="20012783w-sb3">
[  1 ]
< /squarebracket>
2001 p.18 .
< /sentence>
< sentence type="heading" elementid="20012783w-s62">
< a type="href" elementid="20012783w-a6">
back
< /a>
< /sentence>
< /p>
< p elementid="20012783w-p16">
< sentence type="normal" elementid="20012783w-s63">
< squarebracket elementid="20012783w-sb4">
[  2 ]
< /squarebracket>
1998 p.38 .
< /sentence>
< sentence type="heading" elementid="20012783w-s64">
< a type="href" elementid="20012783w-a7">
back
< /a>
< /sentence>
< /p>
< p elementid="20012783w-p17">
< sentence type="heading" elementid="20012783w-s65">
< a type="href" elementid="20012783w-a8">
< english>
English version
< /english>
< /a>
< /sentence>
< sentence type="heading" elementid="20012783w-s66">
ISBN 0 11090327 7
< /sentence>
< /p>
< table elementid="20012783w-tb2">
< td elementid="20012783w-td3">
< p elementid="20012783w-p18">
< sentence type="heading" elementid="20012783w-s67">
< a type="href" elementid="20012783w-a9">
< english>
Other UK SIs
< /english>
< /a>
< a type="href" elementid="20012783w-a10">
< english>
Home
< /english>
< /a>
< a type="href" elementid="20012783w-a11">
< english>
National Assembly for Wales Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20012783w-a12">
< english>
Scottish Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20012783w-a13">
< english>
Statutory Rules of Northern Ireland
< /english>
< /a>
< a type="href" elementid="20012783w-a14">
< english>
Her Majesty 's Stationery Office
< /english>
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20012783w-td4">
< p elementid="20012783w-p19">
< sentence type="heading" elementid="20012783w-s68">
< strong elementid="20012783w-st7">
We welcome your 
< a type="href" elementid="20012783w-a15">
comments
< /a>
on this site
< /strong>
< /sentence>
< /p>
< /td>
< td elementid="20012783w-td5">
< p elementid="20012783w-p20">
< sentence type="heading" elementid="20012783w-s69">
< english>
© Crown copyright 2001
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20012783w-td6">
< p elementid="20012783w-p21">
< sentence type="heading" elementid="20012783w-s70">
Prepared 24 August 2001
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>892< /wordcount>
< /file>
< file elementid="20012787w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Rheoliadau Comisiynydd Plant Cymru 2001< /pagetitle>
< doc_class>Statutory Instrument - Wales< /doc_class>
< year>2001< /year>
< number>2787< /number>
< legislation_code>30102< /legislation_code>
< legislation_type>Statutory Instrument - Wales< /legislation_type>
< subject>COMISIYNYDD PLANT, CYMRU< /subject>
< laid_date>_EMPTY< /laid_date>
< made_date>20011225< /made_date>
< coming_into_force>20011201< /coming_into_force>
< isbn>0110903293< /isbn>
< title>Rheoliadau Comisiynydd Plant Cymru 2001< /title>
< signatory>Rhodri Morgan< /signatory>
< signatory_title>Prif weinidog Cynulliad Cenedlaethol Cymru< /signatory_title>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20012787w-p1">
< sentence type="heading" elementid="20012787w-s1">
Offerynnau Statudol 2001 Rhif 2787
< roundbracket elementid="20012787w-rb1">
(  Cy.237 )
< /roundbracket>
< /sentence>
< /p>
< p elementid="20012787w-p2">
< sentence type="heading" elementid="20012787w-s2">
< strong elementid="20012787w-st1">
Rheoliadau Comisiynydd Plant Cymru 2001
< /strong>
< /sentence>
< /p>
< p elementid="20012787w-p3">
< sentence type="heading" elementid="20012787w-s3">
< english>
© Crown Copyright 2001
< /english>
< /sentence>
< /p>
< p elementid="20012787w-p4">
< sentence type="normal" elementid="20012787w-s4">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20012787w-p5">
< sentence type="normal" elementid="20012787w-s5">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20012787w-s6">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20012787w-p6">
< sentence type="normal" elementid="20012787w-s7">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20012787w-s8">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20012787w-s9">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20012787w-p7">
< sentence type="normal" elementid="20012787w-s10">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20012787w-s11">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan 
< english>
The Stationery Office Limited
< /english>
 fel
< strong elementid="20012787w-st2">
Rheoliadau Comisiynydd Plant Cymru 2001
< /strong>
, ISBN 0 11090329 3 .
< /sentence>
< sentence type="normal" elementid="20012787w-s12">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20012787w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20012787w-s13">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20012787w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20012787w-p8">
< sentence type="normal" elementid="20012787w-s14">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20012787w-a3">
< english>
Frequently Asked Questions
< /english>
< /a> .
< /sentence>
< /p>
< p elementid="20012787w-p9">
< sentence type="normal" elementid="20012787w-s15">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20012787w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20012787w-s16">
Pan welwch fotwm
< doublequotation elementid="20012787w-dq2">
"
< english>
continue
< /english>
"
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20012787w-p10">
< sentence type="heading" elementid="20012787w-s17">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="heading" elementid="20012787w-s18">
< strong elementid="20012787w-st3">
2001 Rhif 2787
< roundbracket elementid="20012787w-rb2">
(  Cy.237 )
< /roundbracket>
< /strong>
< /sentence>
< sentence type="heading" elementid="20012787w-s19">
< strong elementid="20012787w-st4">
COMISIYNYDD PLANT , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20012787w-s20">
Rheoliadau Comisiynydd Plant Cymru 2001
< /sentence>
< /p>
< table elementid="20012787w-tb1">
< td elementid="20012787w-td1">
< p elementid="20012787w-p11">
< sentence type="heading" elementid="20012787w-s21">
< em elementid="20012787w-em1">
Wedi 'i wneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20012787w-td2">
< p elementid="20012787w-p12">
< sentence type="heading" elementid="20012787w-s22">
< em elementid="20012787w-em2">
25 Gorffennaf 2001
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20012787w-td3">
< p elementid="20012787w-p13">
< sentence type="heading" elementid="20012787w-s23">
< em elementid="20012787w-em3">
Yn dod i rym
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20012787w-td4">
< p elementid="20012787w-p14">
< sentence type="heading" elementid="20012787w-s24">
< em elementid="20012787w-em4">
At bob diben heblaw Rheoliad 21
< roundbracket elementid="20012787w-rb3">
(  1 )
< /roundbracket>
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20012787w-td5">
< p elementid="20012787w-p15">
< sentence type="heading" elementid="20012787w-s25">
< em elementid="20012787w-em5">
26 Awst 2001
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20012787w-td6">
< p elementid="20012787w-p16">
< sentence type="heading" elementid="20012787w-s26">
< em elementid="20012787w-em6">
At ddibenion Rheoliad 21
< roundbracket elementid="20012787w-rb4">
(  1 )
< /roundbracket>
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20012787w-td7">
< p elementid="20012787w-p17">
< sentence type="heading" elementid="20012787w-s27">
< em elementid="20012787w-em7">
1 Hydref 2001
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20012787w-p18">
< sentence type="heading" elementid="20012787w-s28">
Mae Cynulliad Cenedlaethol Cymru yn gwneud y Rheoliadau canlynol drwy arfer y p er a roddwyd iddo gan adrannau 73
< roundbracket elementid="20012787w-rb5">
(  4 )
< /roundbracket>
< roundbracket elementid="20012787w-rb6">
(  b )
< /roundbracket>
a
< roundbracket elementid="20012787w-rb7">
(  5 )
< /roundbracket>
, 74
< roundbracket elementid="20012787w-rb8">
(  1 )
< /roundbracket>
i
< roundbracket elementid="20012787w-rb9">
(  3 )
< /roundbracket>
a
< roundbracket elementid="20012787w-rb10">
(  6 )
< /roundbracket>
, 76
< roundbracket elementid="20012787w-rb11">
(  1 )
< /roundbracket>
,
< roundbracket elementid="20012787w-rb12">
(  1A )
< /roundbracket>
,
< roundbracket elementid="20012787w-rb13">
(  2 )
< /roundbracket>
,
< roundbracket elementid="20012787w-rb14">
(  4 )
< /roundbracket>
a
< roundbracket elementid="20012787w-rb15">
(  5 )
< /roundbracket>
, 77
< roundbracket elementid="20012787w-rb16">
(  2 )
< /roundbracket>
, 78
< roundbracket elementid="20012787w-rb17">
(  1A )
< /roundbracket>
a
< roundbracket elementid="20012787w-rb18">
(  6 )
< /roundbracket>
, 118
< roundbracket elementid="20012787w-rb19">
(  4 )
< /roundbracket>
a
< roundbracket elementid="20012787w-rb20">
(  7 )
< /roundbracket>
o Ddeddf Safonau Gofal 2000
< squarebracket elementid="20012787w-sb1">
[ 
< a type="href" elementid="20012787w-a4">
1
< /a>
]
< /squarebracket>
a pharagraffau 6
< roundbracket elementid="20012787w-rb21">
(  4 )
< /roundbracket>
ac 8 o Atodlen 2 iddi :
< /sentence>
< sentence type="heading" elementid="20012787w-s29">
< strong elementid="20012787w-st5">
RHAN I
< /strong>
< /sentence>
< sentence type="heading" elementid="20012787w-s30">
< strong elementid="20012787w-st6">
CYFFREDINOL
< /strong>
< /sentence>
< sentence type="heading" elementid="20012787w-s31">
< strong elementid="20012787w-st7">
Enwi , cychwyn a dehongli
< /strong>
< /sentence>
< sentence type="normal" elementid="20012787w-s32">
1 .
< /sentence>
< sentence type="normal" elementid="20012787w-s33">
 - 
< roundbracket elementid="20012787w-rb22">
(  1 )
< /roundbracket>
Enw 'r Rheoliadau hyn yw Rheoliadau Comisiynydd Plant Cymru 2001 a deuant i rym at ddibenion paragraff
< roundbracket elementid="20012787w-rb23">
(  1 )
< /roundbracket>
o reoliad 21 ar 1 Hydref 2001 ac at bob diben arall ar 26 Awst 2001 .
< /sentence>
< sentence type="heading" elementid="20012787w-s34">
< roundbracket elementid="20012787w-rb24">
(  2 )
< /roundbracket>
Yn y Rheoliadau hyn - 
< /sentence>
< sentence type="normal" elementid="20012787w-s35">
ystyr
< doublequotation elementid="20012787w-dq3">
"  adolygiad swyddogaethau "
< /doublequotation>
< roundbracket elementid="20012787w-rb25">
(
< doublequotation elementid="20012787w-dq4">
"
< em elementid="20012787w-em8">
< english>
a functions review
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw adolygiad o 'r effaith a gâi arfer swyddogaethau , neu 'r bwriad i arfer swyddogaethau , ar blant yn unol ag adran 72B
< roundbracket elementid="20012787w-rb26">
(  1 )
< /roundbracket>
o 'r Ddeddf ; ystyr
< doublequotation elementid="20012787w-dq5">
"  adolygiad trefniadau "
< /doublequotation>
< roundbracket elementid="20012787w-rb27">
(
< doublequotation elementid="20012787w-dq6">
"
< em elementid="20012787w-em9">
< english>
arrangements review
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw adolygiad o 'r trefniadau mewn perthynas â ch ynion , chwythu 'r chwiban neu eiriolaeth yn unol ag adran 73
< roundbracket elementid="20012787w-rb28">
(  1 )
< /roundbracket>
o 'r Ddeddf ; ystyr
< doublequotation elementid="20012787w-dq7">
"  y Comisiynydd "
< /doublequotation>
< roundbracket elementid="20012787w-rb29">
(
< doublequotation elementid="20012787w-dq8">
"
< em elementid="20012787w-em10">
< english>
the Commissioner
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw Comisiynydd Plant Cymru ; ystyr
< doublequotation elementid="20012787w-dq9">
"  y Cynulliad "
< /doublequotation>
< roundbracket elementid="20012787w-rb30">
(
< doublequotation elementid="20012787w-dq10">
"
< em elementid="20012787w-em11">
< english>
the Assembly
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw Cynulliad Cenedlaethol Cymru ; ystyr
< doublequotation elementid="20012787w-dq11">
"  y Ddeddf "
< /doublequotation>
< roundbracket elementid="20012787w-rb31">
(
< doublequotation elementid="20012787w-dq12">
"
< em elementid="20012787w-em12">
< english>
the Act
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw Deddf Safonau Gofal 2000 ; dehonglir
< doublequotation elementid="20012787w-dq13">
"  gwasanaethau rheoleiddiedig i blant yng Nghymru "
< /doublequotation>
< roundbracket elementid="20012787w-rb32">
(
< doublequotation elementid="20012787w-dq14">
"
< em elementid="20012787w-em13">
< english>
regulated children 's services in Wales
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
a
< doublequotation elementid="20012787w-dq15">
"  darparydd "
< /doublequotation>
< roundbracket elementid="20012787w-rb33">
(
< doublequotation elementid="20012787w-dq16">
"
< em elementid="20012787w-em14">
< english>
provider
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
, mewn perthynas â gwasanaethau o 'r fath , yn unol ag adran 78 o 'r Ddeddf ac eithrio bod cyfeiriadau at wasanaethau o 'r fath ac at ddarparwyr gwasanaethau o 'r fath hefyd yn cael eu dehongli yn unol â pharagraff 3 o Atodlen 5 i 'r Ddeddf i 'r perwyl eu bod yn cynnwys cyfeiriadau at wasanaethau y mae 'n bosibl nad ydynt ar unrhyw adeg yn cael eu rheoli o dan y Ddeddf neu at ddarparwyr y gwasanaethau hynny ar adeg felly , wrth ddisgwyl i ddarpariaethau perthnasol yn y Ddeddf ddod i rym ; ystyr
< doublequotation elementid="20012787w-dq17">
"  plant perthnasol "
< /doublequotation>
< roundbracket elementid="20012787w-rb34">
(
< doublequotation elementid="20012787w-dq18">
"
< em elementid="20012787w-em15">
< english>
relevant children
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw plant y mae Rhan V o 'r Ddeddf yn gymwys iddynt ; ystyr
< doublequotation elementid="20012787w-dq19">
"  y Prif Weinidog "
< /doublequotation>
< roundbracket elementid="20012787w-rb35">
(
< doublequotation elementid="20012787w-dq20">
"
< em elementid="20012787w-em16">
< english>
the First Minister
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw 'r person a etholir o dro i dro yn Brif Ysgrifennydd y Cynulliad yn unol ag adran 53
< roundbracket elementid="20012787w-rb36">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998 ; ystyr
< doublequotation elementid="20012787w-dq21">
"  trefniadau mewn perthynas â ch ynion , chwythu 'r chwiban neu eiriolaeth "
< /doublequotation>
< roundbracket elementid="20012787w-rb37">
(
< doublequotation elementid="20012787w-dq22">
"
< em elementid="20012787w-em17">
< english>
arrangements in relation to complaints , whistle-blowing or advocacy
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw trefniadau sy 'n dod o fewn is-adran
< roundbracket elementid="20012787w-rb38">
(  2 )
< /roundbracket>
,
< roundbracket elementid="20012787w-rb39">
(  2A )
< /roundbracket>
,
< roundbracket elementid="20012787w-rb40">
(  2B )
< /roundbracket>
,
< roundbracket elementid="20012787w-rb41">
(  2C )
< /roundbracket>
,
< roundbracket elementid="20012787w-rb42">
(  3 )
< /roundbracket>
, neu
< roundbracket elementid="20012787w-rb43">
(  4 )
< /roundbracket>
o adran 73 o 'r Ddeddf yn ol fel y digwydd .
< /sentence>
< /p>
< p elementid="20012787w-p19">
< sentence type="heading" elementid="20012787w-s36">
< roundbracket elementid="20012787w-rb44">
(  3 )
< /roundbracket>
Yn y Rheoliadau hyn mae cyfeiriad - 
< /sentence>
< /p>
< p elementid="20012787w-p20">
< sentence type="heading" elementid="20012787w-s37">
< roundbracket elementid="20012787w-rb45">
(  a )
< /roundbracket>
at reoliad â rhif yn gyfeiriad at y rheoliad yn y Rheoliadau hyn sy 'n dwyn y rhif hwnnw ;
< /sentence>
< sentence type="heading" elementid="20012787w-s38">
< roundbracket elementid="20012787w-rb46">
(  b )
< /roundbracket>
mewn rheoliad at baragraff â rhif , yn gyfeiriad at y paragraff yn y rheoliad hwnnw sy 'n dwyn y rhif hwnnw ;
< /sentence>
< sentence type="normal" elementid="20012787w-s39">
< roundbracket elementid="20012787w-rb47">
(  c )
< /roundbracket>
mewn paragraff at is-baragraff â llythyren neu rif , yn gyfeiriad at yr is-baragraff yn y paragraff hwnnw sy 'n dwyn y llythyren honno neu 'r rhif hwnnw .
< /sentence>
< /p>
< p elementid="20012787w-p21">
< sentence type="heading" elementid="20012787w-s40">
< strong elementid="20012787w-st8">
RHAN II
< /strong>
< /sentence>
< sentence type="heading" elementid="20012787w-s41">
< strong elementid="20012787w-st9">
ADOLYGU A MONITRO TREFNIADAU
< /strong>
< /sentence>
< sentence type="heading" elementid="20012787w-s42">
< strong elementid="20012787w-st10">
Cyngor a chymorth a ragnodir
< /strong>
< /sentence>
< sentence type="normal" elementid="20012787w-s43">
2 .
< /sentence>
< sentence type="heading" elementid="20012787w-s44">
Dyma 'r math o gyngor a chymorth a ragnodir at ddibenion adran 73
< roundbracket elementid="20012787w-rb48">
(  4 )
< /roundbracket>
< roundbracket elementid="20012787w-rb49">
(  b )
< /roundbracket>
o 'r Ddeddf - 
< /sentence>
< sentence type="heading" elementid="20012787w-s45">
< roundbracket elementid="20012787w-rb50">
(  a )
< /roundbracket>
rhoi cyngor a chymorth i blant perthnasol y bwriedir iddynt eu galluogi neu eu helpu i fynegi eu barn a 'u dymuniadau ar lafar neu drwy ddefnyddio unrhyw ddull cyfathrebu arall , a
< /sentence>
< sentence type="normal" elementid="20012787w-s46">
< roundbracket elementid="20012787w-rb51">
(  b )
< /roundbracket>
rhoi cyngor
< roundbracket elementid="20012787w-rb52">
(  gan gynnwys gwybodaeth )
< /roundbracket>
i blant o 'r fath ynghylch eu hawliau a 'u lles .
< /sentence>
< /p>
< p elementid="20012787w-p22">
< sentence type="heading" elementid="20012787w-s47">
< strong elementid="20012787w-st11">
Rhoi gwybodaeth gan bersonau rhagnodedig
< /strong>
< /sentence>
< sentence type="normal" elementid="20012787w-s48">
3 .
< /sentence>
< sentence type="heading" elementid="20012787w-s49">
 - 
< roundbracket elementid="20012787w-rb53">
(  1 )
< /roundbracket>
Caiff y Comisiynydd ei gwneud yn ofynnol i unrhyw berson y mae paragraff
< roundbracket elementid="20012787w-rb54">
(  2 )
< /roundbracket>
yn gymwys iddo roi gwybodaeth i 'r Comisiynydd , wedi 'i chofnodi ar unrhyw ffurf , y mae 'r Comisiynydd o 'r farn ei bod yn angenrheidiol neu 'n hwylus ei chael at ddibenion
< /sentence>
< /p>
< p elementid="20012787w-p23">
< sentence type="heading" elementid="20012787w-s50">
< roundbracket elementid="20012787w-rb55">
(  a )
< /roundbracket>
adolygu a monitro trefniadau mewn perthynas â ch ynion , chwythu 'r chwiban neu eiriolaeth , a
< /sentence>
< sentence type="normal" elementid="20012787w-s51">
< roundbracket elementid="20012787w-rb56">
(  b )
< /roundbracket>
asesu effaith methiant unrhyw berson i wneud trefniadau o 'r fath yn unol ag adran 73
< roundbracket elementid="20012787w-rb57">
(  1A )
< /roundbracket>
o 'r Ddeddf .
< /sentence>
< /p>
< p elementid="20012787w-p24">
< sentence type="heading" elementid="20012787w-s52">
< roundbracket elementid="20012787w-rb58">
(  2 )
< /roundbracket>
Dyma 'r personau y cyfeirir atynt ym mharagraff
< roundbracket elementid="20012787w-rb59">
(  1 )
< /roundbracket>
 - 
< /sentence>
< /p>
< p elementid="20012787w-p25">
< sentence type="heading" elementid="20012787w-s53">
< roundbracket elementid="20012787w-rb60">
(  a )
< /roundbracket>
Mewn perthynas â darparu gwasanaethau rheoleiddiedig i blant yng Nghymru , darparwyr neu gyn-ddarparwyr gwasanaethau o 'r fath , cyflogeion neu gyn-gyflogeion darparwyr neu gyn-ddarparwyr o 'r fath , personau sy 'n gweithio neu a fu 'n gweithio i ddarparwyr neu gyn-ddarparwyr o 'r fath yn wirfoddol , ac aelodau a chyflogeion a chyn-aelodau a chyn-gyflogeion y Cynulliad ;
< /sentence>
< sentence type="heading" elementid="20012787w-s54">
< roundbracket elementid="20012787w-rb61">
(  b )
< /roundbracket>
< /sentence>
< sentence type="heading" elementid="20012787w-s55">
< roundbracket elementid="20012787w-rb62">
(  i )
< /roundbracket>
Aelodau
< roundbracket elementid="20012787w-rb63">
(  gan gynnwys aelodau etholedig )
< /roundbracket>
, cyfarwyddwyr , gweithredwyr , swyddogion a chyflogeion person perthnasol , cyn-aelodau , cyn-gyfarwyddwyr , cyn-weithredwyr , cyn-swyddogion a chyn-gyflogeion person perthnasol a phersonau sy 'n gweithio neu a fu 'n gweithio i berson perthnasol yn wirfoddol ;
< /sentence>
< sentence type="normal" elementid="20012787w-s56">
< roundbracket elementid="20012787w-rb64">
(  ii )
< /roundbracket>
At ddibenion is-baragraff
< roundbracket elementid="20012787w-rb65">
(  i )
< /roundbracket>
ystyr
< doublequotation elementid="20012787w-dq23">
"  person perthnasol "
< /doublequotation>
yw 'r Cynulliad , unrhyw berson a grybwyllir yn Atodlen 2B i 'r Ddeddf
< roundbracket elementid="20012787w-rb66">
(  heblaw 'r Cynulliad )
< /roundbracket>
sy 'n darparu gwasanaethau i blant neu mewn perthynas â phlant yng Nghymru neu berson sy 'n darparu gwasanaethau o 'r fath ar ran y person hwnnw neu o dan drefniadau ag ef .
< /sentence>
< /p>
< p elementid="20012787w-p26">
< sentence type="normal" elementid="20012787w-s57">
< roundbracket elementid="20012787w-rb67">
(  c )
< /roundbracket>
Derbynnydd neu reolwr eiddo person sy 'n darparu neu a fu 'n darparu 'r gwasanaethau a ddisgrifir ym mharagraffau
< roundbracket elementid="20012787w-rb68">
(  a )
< /roundbracket>
neu
< roundbracket elementid="20012787w-rb69">
(  b )
< /roundbracket>
, ei ddatodwr neu ei ddatodwr dros dro neu ei ymddiriedolwr mewn methdaliad , yn ôl fel y digwydd .
< /sentence>
< /p>
< p elementid="20012787w-p27">
< sentence type="heading" elementid="20012787w-s58">
< strong elementid="20012787w-st12">
RHAN III
< /strong>
< /sentence>
< sentence type="heading" elementid="20012787w-s59">
< strong elementid="20012787w-st13">
ARCHWILIO ACHOSION
< /strong>
< /sentence>
< sentence type="heading" elementid="20012787w-s60">
< strong elementid="20012787w-st14">
Archwiliadau
< /strong>
< /sentence>
< sentence type="normal" elementid="20012787w-s61">
4 .
< /sentence>
< sentence type="normal" elementid="20012787w-s62">
Yn ddarostyngedig i 'r paragraffau canlynol yn y Rhan hon caiff y Comisiynydd archwilio achosion plant penodol y mae Rhan V o 'r Ddeddf yn gymwys iddynt .
< /sentence>
< sentence type="heading" elementid="20012787w-s63">
< strong elementid="20012787w-st15">
Achosion sy 'n agored i 'w harchwilio
< /strong>
< /sentence>
< sentence type="normal" elementid="20012787w-s64">
5 .
< /sentence>
< sentence type="heading" elementid="20012787w-s65">
Yn ddarostyngedig i reoliad 6 , caiff y Comisiynydd archwilio achosion plant penodol - 
< /sentence>
< sentence type="heading" elementid="20012787w-s66">
< roundbracket elementid="20012787w-rb70">
(  a )
< /roundbracket>
y mae gwasanaethau rheoleiddiedig i blant yng Nghymru yn cael neu wedi cael eu darparu iddynt neu mewn perthynas â hwy ;
< /sentence>
< sentence type="heading" elementid="20012787w-s67">
< roundbracket elementid="20012787w-rb71">
(  b )
< /roundbracket>
y mae gwasanaethau yn cael neu wedi cael eu darparu iddynt neu mewn perthynas â hwy gan unrhyw un o 'r personau a grybwyllir yn Atodlen 2B i 'r Ddeddf neu bersonau sy 'n darparu gwasanaethau o 'r fath ar ran unrhyw un o 'r personau hynny neu o dan drefniadau â hwy ; neu
< /sentence>
< sentence type="heading" elementid="20012787w-s68">
< roundbracket elementid="20012787w-rb72">
(  c )
< /roundbracket>
sydd fel arfer yn preswylio yng Nghymru ac y mae arfer , neu 'r bwriad i arfer , unrhyw swyddogaeth sydd gan y Cynulliad neu sydd gan unrhyw berson a grybwyllir yn Atodlen 2A i 'r Ddeddf yn effeithio arnynt neu wedi effeithio arnynt,
< /sentence>
< /p>
< p elementid="20012787w-p28">
< sentence type="normal" elementid="20012787w-s69">
os yw 'r achosion yn ymwneud â materion sy 'n ymwneud â darparu gwasanaethau o 'r fath neu ag effaith arfer y swyddogaethau hynny ar y plant a enwyd .
< /sentence>
< sentence type="heading" elementid="20012787w-s70">
< strong elementid="20012787w-st16">
Yr amgylchiadau y gellir archwilio odanynt
< /strong>
< /sentence>
< sentence type="normal" elementid="20012787w-s71">
6 .
< /sentence>
< sentence type="heading" elementid="20012787w-s72">
Dim ond o dan yr amgylchiadau canlynol y caiff y Comisiynydd archwilio achos plentyn penodol :
< /sentence>
< /p>
< p elementid="20012787w-p29">
< sentence type="heading" elementid="20012787w-s73">
< roundbracket elementid="20012787w-rb73">
(  a )
< /roundbracket>
os cyflwynir sylwadau i 'r Comisiynydd gan y plentyn o dan sylw neu , os nad yw 'r plentyn am unrhyw reswm yn gallu cyflwyno sylwadau o 'r fath , os cyflwynir sylwadau i 'r Comisiynydd ar ran y plentyn gan berson sydd , ym marn resymol y Comisiynydd , yn addas i gyflwyno sylwadau o 'r fath ;
< /sentence>
< sentence type="heading" elementid="20012787w-s74">
< roundbracket elementid="20012787w-rb74">
(  b )
< /roundbracket>
os yw 'r Comisiynydd o 'r farn bod y sylwadau 'n codi cwestiwn o egwyddor sy 'n gymwys neu 'n berthnasol yn fwy cyffredinol i hawliau neu les plant perthnasol nag yn yr achos penodol o dan sylw ; ac
< /sentence>
< sentence type="normal" elementid="20012787w-s75">
< roundbracket elementid="20012787w-rb75">
(  c )
< /roundbracket>
os yw 'r Comisiynydd wedi cymryd i ystyriaeth a yw 'r materion sydd o dan sylw yn yr achos wedi cael neu yn cael eu hystyried yn ffurfiol mewn unrhyw fodd gan bersonau eraill ac os nad ydynt , a ydynt , ym marn y Comisiynydd , yn fwy addas i gael eu hystyried gan bersonau eraill .
< /sentence>
< /p>
< p elementid="20012787w-p30">
< sentence type="heading" elementid="20012787w-s76">
< strong elementid="20012787w-st17">
Y weithdrefn ar gyfer cynnal archwiliad
< /strong>
< /sentence>
< sentence type="normal" elementid="20012787w-s77">
7 .
< /sentence>
< sentence type="heading" elementid="20012787w-s78">
 - 
< roundbracket elementid="20012787w-rb76">
(  1 )
< /roundbracket>
Os yw 'r Comisiynydd yn penderfynu cynnal archwiliad rhaid iddo - 
< /sentence>
< /p>
< p elementid="20012787w-p31">
< sentence type="heading" elementid="20012787w-s79">
< roundbracket elementid="20012787w-rb77">
(  a )
< /roundbracket>
llunio cylch gwaith yr archwiliad ;
< /sentence>
< sentence type="heading" elementid="20012787w-s80">
< roundbracket elementid="20012787w-rb78">
(  b )
< /roundbracket>
anfon y cylch gwaith at y person a gyflwynodd sylwadau mewn perthynas â 'r achos yn unol â pharagraff
< roundbracket elementid="20012787w-rb79">
(  a )
< /roundbracket>
o reoliad 6 ;
< /sentence>
< sentence type="heading" elementid="20012787w-s81">
< roundbracket elementid="20012787w-rb80">
(  c )
< /roundbracket>
anfon hysbysiad ysgrifenedig o 'r archwiliad arfaethedig a chopïau o 'r cylch gwaith at y person
< roundbracket elementid="20012787w-rb81">
(
< doublequotation elementid="20012787w-dq24">
"  y person sy 'n cael ei archwilio "
< /doublequotation>
)
< /roundbracket>
y mae ei waith wrth ddarparu gwasanaethau neu wrth arfer swyddogaethau i gael ei archwilio ;
< /sentence>
< sentence type="normal" elementid="20012787w-s82">
< roundbracket elementid="20012787w-rb82">
(  ch )
< /roundbracket>
rhoi cyfle i 'r person sy 'n cael ei archwilio , ac os yw ef yn dymuno hynny , i 'w gynrychiolydd , gyflwyno sylwadau mewn ysgrifen neu yn bersonol mewn perthynas â 'r materion sy 'n cael eu harchwilio .
< /sentence>
< /p>
< p elementid="20012787w-p32">
< sentence type="heading" elementid="20012787w-s83">
< roundbracket elementid="20012787w-rb83">
(  2 )
< /roundbracket>
Os yw 'r Comisiynydd yn penderfynu peidio â chynnal archwiliad rhaid iddo baratoi datganiad o 'r rhesymau am y penderfyniad hwnnw ac anfon copïau ohono - 
< /sentence>
< /p>
< p elementid="20012787w-p33">
< sentence type="heading" elementid="20012787w-s84">
< roundbracket elementid="20012787w-rb84">
(  a )
< /roundbracket>
at y person a gyflwynodd sylwadau mewn perthynas â 'r achos yn unol â pharagraff
< roundbracket elementid="20012787w-rb85">
(  a )
< /roundbracket>
o reoliad 6 , a
< /sentence>
< sentence type="normal" elementid="20012787w-s85">
< roundbracket elementid="20012787w-rb86">
(  b )
< /roundbracket>
at unrhyw bersonau eraill y mae 'r Comisiynydd o 'r farn ei bod yn briodol eu hanfon atynt .
< /sentence>
< /p>
< p elementid="20012787w-p34">
< sentence type="heading" elementid="20012787w-s86">
< strong elementid="20012787w-st18">
Rhoi gwybodaeth mewn cysylltiad ag archwiliad
< /strong>
< /sentence>
< sentence type="normal" elementid="20012787w-s87">
8 .
< /sentence>
< sentence type="heading" elementid="20012787w-s88">
 - 
< roundbracket elementid="20012787w-rb87">
(  1 )
< /roundbracket>
Wrth gynnal archwiliad caiff y Comisiynydd - 
< /sentence>
< /p>
< p elementid="20012787w-p35">
< sentence type="heading" elementid="20012787w-s89">
< roundbracket elementid="20012787w-rb88">
(  a )
< /roundbracket>
ei gwneud yn ofynnol i berson y mae paragraff
< roundbracket elementid="20012787w-rb89">
(  2 )
< /roundbracket>
yn gymwys iddo roi unrhyw wybodaeth y mae 'n ymddangos i 'r Comisiynydd y mae angen amdani at ddibenion yr archwiliad o dan sylw ;
< /sentence>
< sentence type="heading" elementid="20012787w-s90">
< roundbracket elementid="20012787w-rb90">
(  b )
< /roundbracket>
ei gwneud yn ofynnol i berson o 'r fath neu berson arall o 'r fath a all fod yn atebol am yr wybodaeth honno , roi esboniad neu gymorth i 'r Comisiynydd - 
< /sentence>
< sentence type="heading" elementid="20012787w-s91">
< roundbracket elementid="20012787w-rb91">
(  i )
< /roundbracket>
mewn perthynas ag unrhyw faterion sy 'n destun archwiliad , neu
< /sentence>
< sentence type="normal" elementid="20012787w-s92">
< roundbracket elementid="20012787w-rb92">
(  ii )
< /roundbracket>
mewn perthynas ag unrhyw wybodaeth a ddarperir o dan is-baragraff
< roundbracket elementid="20012787w-rb93">
(  a )
< /roundbracket> .
< /sentence>
< /p>
< p elementid="20012787w-p36">
< sentence type="heading" elementid="20012787w-s93">
< roundbracket elementid="20012787w-rb94">
(  2 )
< /roundbracket>
Dyma 'r personau y mae 'r paragraff hwn yn gymwys iddynt - 
< /sentence>
< /p>
< p elementid="20012787w-p37">
< sentence type="heading" elementid="20012787w-s94">
< roundbracket elementid="20012787w-rb95">
(  a )
< /roundbracket>
Mewn perthynas â darparu gwasanaethau rheoleiddiedig i blant yng Nghymru , darparwyr neu gyn-ddarparwyr gwasanaethau o 'r fath , cyflogeion neu gyn-gyflogeion darparwyr neu gyn-ddarparwyr o 'r fath , personau sy 'n gweithio neu sydd wedi gweithio i ddarparwyr neu gyn-ddarparwyr o 'r fath yn wirfoddol , ac aelodau a chyflogeion a chyn-aelodau a chyn-gyflogeion y Cynulliad ;
< /sentence>
< sentence type="heading" elementid="20012787w-s95">
< roundbracket elementid="20012787w-rb96">
(  b )
< /roundbracket>
< /sentence>
< sentence type="heading" elementid="20012787w-s96">
< roundbracket elementid="20012787w-rb97">
(  i )
< /roundbracket>
Aelodau
< roundbracket elementid="20012787w-rb98">
(  gan gynnwys aelodau etholedig )
< /roundbracket>
, cyfarwyddwyr , gweithredwyr , swyddogion a chyflogeion person perthnasol , cyn-aelodau , cyn-gyfarwyddwyr , cyn-weithredwyr , cyn-swyddogion a chyn-gyflogeion person perthnasol a phersonau sy 'n gweithio neu sydd wedi gweithio i berson perthnasol yn wirfoddol ;
< /sentence>
< sentence type="normal" elementid="20012787w-s97">
< roundbracket elementid="20012787w-rb99">
(  ii )
< /roundbracket>
At ddibenion is-baragraff
< roundbracket elementid="20012787w-rb100">
(  i )
< /roundbracket>
ystyr
< doublequotation elementid="20012787w-dq25">
"  person perthnasol "
< /doublequotation>
< roundbracket elementid="20012787w-rb101">
(
< doublequotation elementid="20012787w-dq26">
"
< em elementid="20012787w-em18">
< english>
relevant person
< /english>
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw 'r Cynulliad , unrhyw berson a grybwyllir yn Atodlen 2A i 'r Ddeddf , unrhyw berson arall sy 'n arfer swyddogaeth gan y Cynulliad neu unrhyw berson a grybwyllir yn y cyfryw Atodlen 2A , neu unrhyw berson sy 'n darparu gwasanaethau i blant neu ar gyfer plant yng Nghymru ar ran unrhyw berson a grybwyllir yn Atodlen 2B i 'r Ddeddf neu o dan drefniant ag ef .
< /sentence>
< /p>
< p elementid="20012787w-p38">
< sentence type="normal" elementid="20012787w-s98">
< roundbracket elementid="20012787w-rb102">
(  c )
< /roundbracket>
Derbynnydd neu reolwr eiddo person sy 'n darparu neu sydd wedi darparu 'r gwasanaethau a ddisgrifir ym mharagraffau
< roundbracket elementid="20012787w-rb103">
(  a )
< /roundbracket>
neu
< roundbracket elementid="20012787w-rb104">
(  b )
< /roundbracket>
, ei ddatodwr neu ei ddatodwr dros dro neu ei ymddiriedolwr mewn methdaliad , yn ôl fel y digwydd .
< /sentence>
< /p>
< p elementid="20012787w-p39">
< sentence type="heading" elementid="20012787w-s99">
< strong elementid="20012787w-st19">
Presenoldeb tystion
< /strong>
< /sentence>
< sentence type="normal" elementid="20012787w-s100">
9 .
< /sentence>
< sentence type="normal" elementid="20012787w-s101">
 - 
< roundbracket elementid="20012787w-rb105">
(  1 )
< /roundbracket>
Caiff y Comisiynydd , os bernir ei bod yn angenrheidiol at ddibenion archwiliad , ei gwneud yn ofynnol i berson y mae paragraff
< roundbracket elementid="20012787w-rb106">
(  2 )
< /roundbracket>
yn gymwys iddo fod yn bresennol yn bersonol gerbron y Comisiynydd i roi gwybodaeth , esboniadau neu gymorth .
< /sentence>
< sentence type="heading" elementid="20012787w-s102">
< roundbracket elementid="20012787w-rb107">
(  2 )
< /roundbracket>
Y personau y mae 'r paragraff hwn yn berthnasol iddynt yw personau y mae 'n ofynnol iddynt - 
< /sentence>
< /p>
< p elementid="20012787w-p40">
< sentence type="heading" elementid="20012787w-s103">
< roundbracket elementid="20012787w-rb108">
(  a )
< /roundbracket>
roi gwybodaeth o dan baragraff
< roundbracket elementid="20012787w-rb109">
(  1 )
< /roundbracket>
< roundbracket elementid="20012787w-rb110">
(  a )
< /roundbracket>
o reoliad 8 , neu
< /sentence>
< sentence type="normal" elementid="20012787w-s104">
< roundbracket elementid="20012787w-rb111">
(  b )
< /roundbracket>
roi esboniad o dan baragraff
< roundbracket elementid="20012787w-rb112">
(  1 )
< /roundbracket>
< roundbracket elementid="20012787w-rb113">
(  b )
< /roundbracket>
o reoliad 8 .
< /sentence>
< /p>
< p elementid="20012787w-p41">
< sentence type="normal" elementid="20012787w-s105">
< roundbracket elementid="20012787w-rb114">
(  3 )
< /roundbracket>
Ni chaiff y Comisiynydd ei gwneud yn ofynnol i berson fod yn bresennol yn bersonol mewn unrhyw le yn unol â pharagraff
< roundbracket elementid="20012787w-rb115">
(  1 )
< /roundbracket>
ond os rhoddwyd i 'r person hwnnw hysbysiad ysgrifenedig rhesymol o ddyddiad arfaethedig ei bresenoldeb a 'r wybodaeth , yr esboniadau neu 'r cymorth y mae ar y Comisiynydd eu hangen .
< /sentence>
< sentence type="normal" elementid="20012787w-s106">
< roundbracket elementid="20012787w-rb116">
(  4 )
< /roundbracket>
Mewn cysylltiad â phresenoldeb personol o 'r fath , caiff y Comisiynydd , yn ddarostyngedig i adran 74
< roundbracket elementid="20012787w-rb117">
(  4 )
< /roundbracket>
o 'r Ddeddf , roi gwysion tystion a gweinyddu llwon neu gadarnhadau a chaiff ganiatáu i berson gael ei gynrychioli gerbron y Comisiynydd .
< /sentence>
< /p>
< p elementid="20012787w-p42">
< sentence type="heading" elementid="20012787w-s107">
< strong elementid="20012787w-st20">
RHAN IV
< /strong>
< /sentence>
< sentence type="heading" elementid="20012787w-s108">
< strong elementid="20012787w-st21">
RHOI CYMORTH
< /strong>
< /sentence>
< sentence type="heading" elementid="20012787w-s109">
< strong elementid="20012787w-st22">
Rhoi cymorth mewn achosion
< /strong>
< /sentence>
< sentence type="normal" elementid="20012787w-s110">
10 .
< /sentence>
< sentence type="heading" elementid="20012787w-s111">
 - 
< roundbracket elementid="20012787w-rb118">
(  1 )
< /roundbracket>
Caiff y Comisiynydd , yn ddarostyngedig i baragraffau
< roundbracket elementid="20012787w-rb119">
(  3 )
< /roundbracket>
a
< roundbracket elementid="20012787w-rb120">
(  5 )
< /roundbracket>
, roi cymorth perthnasol i blentyn perthnasol - 
< /sentence>
< sentence type="heading" elementid="20012787w-s112">
< roundbracket elementid="20012787w-rb121">
(  a )
< /roundbracket>
mewn perthynas ag achosion a ragnodir ym mharagraff
< roundbracket elementid="20012787w-rb122">
(  2 )
< /roundbracket>
pan fydd yr achosion , ym marn resymol y Comisiynydd , yn ymwneud â materion sy 'n gymwys neu 'n berthnasol yn fwy cyffredinol i hawliau neu les plant perthnasol nag yn yr achos penodol o dan sylw , a
< /sentence>
< sentence type="heading" elementid="20012787w-s113">
< roundbracket elementid="20012787w-rb123">
(  b )
< /roundbracket>
wrth wneud C yn neu gyflwyno sylwadau i ddarparydd gwasanaethau rheoleiddiedig i blant yng Nghymru neu mewn perthynas ag ef , ac
< /sentence>
< sentence type="normal" elementid="20012787w-s114">
< roundbracket elementid="20012787w-rb124">
(  c )
< /roundbracket>
wrth wneud C yn neu gyflwyno sylwadau i berson a grybwyllir yn adran 73
< roundbracket elementid="20012787w-rb125">
(  2B )
< /roundbracket>
o 'r Ddeddf neu yn Atodlen 2B iddi neu mewn perthynas ag ef .
< /sentence>
< /p>
< p elementid="20012787w-p43">
< sentence type="heading" elementid="20012787w-s115">
< roundbracket elementid="20012787w-rb126">
(  2 )
< /roundbracket>
Mae 'r achosion a ragnodir at ddibenion adran 76
< roundbracket elementid="20012787w-rb127">
(  1 )
< /roundbracket>
< roundbracket elementid="20012787w-rb128">
(  b )
< /roundbracket>
o 'r Ddeddf yn achosion sy 'n ymwneud - 
< /sentence>
< /p>
< p elementid="20012787w-p44">
< sentence type="heading" elementid="20012787w-s116">
< roundbracket elementid="20012787w-rb129">
(  a )
< /roundbracket>
â darparu gwasanaethau rheoleiddiedig i blant yng Nghymru i blentyn perthnasol neu mewn perthynas ag ef ;
< /sentence>
< sentence type="heading" elementid="20012787w-s117">
< roundbracket elementid="20012787w-rb130">
(  b )
< /roundbracket>
â darparu gwasanaethau i blentyn o 'r fath neu mewn perthynas ag ef gan unrhyw berson a grybwyllir yn Atodlen 2B i 'r Ddeddf neu gan unrhyw berson sy 'n darparu gwasanaethau ar ran y person hwnnw neu o dan drefniadau ag ef ; neu
< /sentence>
< sentence type="normal" elementid="20012787w-s118">
< roundbracket elementid="20012787w-rb131">
(  c )
< /roundbracket>
â 'r effaith a gâi arfer , neu 'r bwriad i arfer , unrhyw un o swyddogaethau 'r Cynulliad neu unrhyw un o swyddogaethau person a grybwyllir yn Atodlen 2A i 'r Ddeddf ar blentyn o 'r fath .
< /sentence>
< /p>
< p elementid="20012787w-p45">
< sentence type="normal" elementid="20012787w-s119">
< roundbracket elementid="20012787w-rb132">
(  3 )
< /roundbracket>
Wrth benderfynu a ddylid rhoi cymorth perthnasol , caiff y Comisiynydd gymryd i ystyriaeth y cymorth ariannol a 'r cymorth arall sydd ar gael i 'r plentyn perthnasol mewn perthynas â 'r achosion , y g yn neu 'r sylwadau o dan sylw , gan gynnwys cymorth o dan Ddeddf Cyfle i Gael Cyfiawnder 1999 .
< /sentence>
< sentence type="heading" elementid="20012787w-s120">
< roundbracket elementid="20012787w-rb133">
(  4 )
< /roundbracket>
At ddibenion y rheoliad hwn , ystyr
< doublequotation elementid="20012787w-dq27">
"  cymorth perthnasol "
< /doublequotation>
yw - 
< /sentence>
< /p>
< p elementid="20012787w-p46">
< sentence type="heading" elementid="20012787w-s121">
< roundbracket elementid="20012787w-rb134">
(  a )
< /roundbracket>
rhoi , neu drefnu rhoi , cyngor i 'r plentyn a 'i gynrychioli ; a
< /sentence>
< sentence type="normal" elementid="20012787w-s122">
< roundbracket elementid="20012787w-rb135">
(  b )
< /roundbracket>
rhoi , neu drefnu rhoi , unrhyw gymorth arall y mae 'n credu ei fod yn briodol .
< /sentence>
< /p>
< p elementid="20012787w-p47">
< sentence type="normal" elementid="20012787w-s123">
< roundbracket elementid="20012787w-rb136">
(  5 )
< /roundbracket>
Nid yw paragraff
< roundbracket elementid="20012787w-rb137">
(  4 )
< /roundbracket>
yn effeithio ar y gyfraith a 'r arferion ynghylch pwy gaiff gynrychioli person mewn perthynas ag unrhyw achos .
< /sentence>
< sentence type="heading" elementid="20012787w-s124">
< strong elementid="20012787w-st23">
Amodau
< /strong>
< /sentence>
< sentence type="normal" elementid="20012787w-s125">
11 .
< /sentence>
< sentence type="normal" elementid="20012787w-s126">
 - 
< roundbracket elementid="20012787w-rb138">
(  1 )
< /roundbracket>
Os yw 'r Comisiynydd yn penderfynu rhoi cymorth ariannol i blentyn perthnasol yn unol â rheoliad 10 gall y cymorth gael ei roi o dan y naill neu 'r llall neu o dan y ddau o 'r amodau a bennir ym mharagraff
< roundbracket elementid="20012787w-rb139">
(  2 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20012787w-s127">
< roundbracket elementid="20012787w-rb140">
(  2 )
< /roundbracket>
Dyma 'r amodau - 
< /sentence>
< /p>
< p elementid="20012787w-p48">
< sentence type="heading" elementid="20012787w-s128">
< roundbracket elementid="20012787w-rb141">
(  a )
< /roundbracket>
y caiff y Comisiynydd adennill cost resymol rhoi 'r cymorth o unrhyw symiau o delir gan bartïon at y diben hwnnw yn yr achos o dan sylw ;
< /sentence>
< sentence type="heading" elementid="20012787w-s129">
< roundbracket elementid="20012787w-rb142">
(  b )
< /roundbracket>
nad yw 'r cymorth a roddir yn dyblygu cymorth sydd wedi 'i roi neu a all gael ei roi o dan unrhyw ddeddfiad ;
< /sentence>
< /p>
< p elementid="20012787w-p49">
< sentence type="normal" elementid="20012787w-s130">
< roundbracket elementid="20012787w-rb143">
(  3 )
< /roundbracket>
At ddibenion paragraff
< roundbracket elementid="20012787w-rb144">
(  2 )
< /roundbracket>
< roundbracket elementid="20012787w-rb145">
(  a )
< /roundbracket>
nid yw 'n berthnasol a yw 'r symiau a delir gan bartïon eraill yn daladwy yn rhinwedd penderfyniad gan lys neu dribiwnlys , cytundeb a wnaed er mwyn osgoi achos neu er mwyn dod ag achos i ben , neu fel arall .
< /sentence>
< /p>
< p elementid="20012787w-p50">
< sentence type="heading" elementid="20012787w-s131">
< strong elementid="20012787w-st24">
RHAN V
< /strong>
< /sentence>
< sentence type="heading" elementid="20012787w-s132">
< strong elementid="20012787w-st25">
SWYDDOGAETHAU PELLACH
< /strong>
< /sentence>
< sentence type="heading" elementid="20012787w-s133">
< strong elementid="20012787w-st26">
Y berthynas â phlant
< /strong>
< /sentence>
< sentence type="normal" elementid="20012787w-s134">
12 .
< /sentence>
< sentence type="heading" elementid="20012787w-s135">
 - 
< roundbracket elementid="20012787w-rb146">
(  1 )
< /roundbracket>
Rhaid i 'r Comisiynydd gymryd camau rhesymol i sicrhau - 
< /sentence>
< sentence type="heading" elementid="20012787w-s136">
< roundbracket elementid="20012787w-rb147">
(  a )
< /roundbracket>
bod plant yng Nghymru yn cael gwybod am leoliad swyddfa neu swyddfeydd y Comisiynydd ac ym mha ffyrdd y gallant gyfathrebu â 'r Comisiynydd a 'i staff ;
< /sentence>
< sentence type="heading" elementid="20012787w-s137">
< roundbracket elementid="20012787w-rb148">
(  b )
< /roundbracket>
bod plant o 'r fath yn cael eu hannog i gyfathrebu â 'r Comisiynydd a 'i staff ;
< /sentence>
< sentence type="heading" elementid="20012787w-s138">
< roundbracket elementid="20012787w-rb149">
(  c )
< /roundbracket>
bod cynnwys unrhyw ddeunydd a gyhoeddir gan y Comisiynydd neu ei staff , p 'un a fydd wedi 'i argraffu neu ar ffurf electronig , y bwriedir iddo gael ei ddarllen gan unrhyw un neu ragor o blant o 'r fath , yn cymryd i ystyriaeth , cyn belled ag y bo 'n ymarferol , oedran , lefel dealltwriaeth ac iaith arferol y sawl y bwriedir iddo ei dderbyn ;
< /sentence>
< sentence type="heading" elementid="20012787w-s139">
< roundbracket elementid="20012787w-rb150">
(  ch )
< /roundbracket>
bod barn plant o 'r fath ynghylch sut y dylai 'r Comisiynydd arfer ei swyddogaethau ac ynghylch cynnwys rhaglen waith flynyddol y Comisiynydd yn cael ei cheisio ; a
< /sentence>
< sentence type="normal" elementid="20012787w-s140">
< roundbracket elementid="20012787w-rb151">
(  d )
< /roundbracket>
bod y Comisiynydd a 'i staff yn trefnu eu bod ar gael i blant o 'r fath yn ardal y plant .
< /sentence>
< /p>
< p elementid="20012787w-p51">
< sentence type="normal" elementid="20012787w-s141">
< roundbracket elementid="20012787w-rb152">
(  2 )
< /roundbracket>
Wrth arfer y swyddogaethau a nodir ym mharagraff
< roundbracket elementid="20012787w-rb153">
(  1 )
< /roundbracket>
rhaid i 'r Comisiynydd roi sylw i beth yw anghenion ac amgylchiadau plant o 'r fath yn ei farn resymol ef .
< /sentence>
< /p>
< p elementid="20012787w-p52">
< sentence type="heading" elementid="20012787w-s142">
< strong elementid="20012787w-st27">
RHAN VI
< /strong>
< /sentence>
< sentence type="heading" elementid="20012787w-s143">
< strong elementid="20012787w-st28">
ADRODDIADAU
< /strong>
< /sentence>
< sentence type="heading" elementid="20012787w-s144">
< strong elementid="20012787w-st29">
Adroddiadau
< /strong>
< /sentence>
< sentence type="normal" elementid="20012787w-s145">
13 .
< /sentence>
< sentence type="normal" elementid="20012787w-s146">
 - 
< roundbracket elementid="20012787w-rb154">
(  1 )
< /roundbracket>
Pan ddaw archwiliad a gynhelir yn unol â Rhan III o 'r rheoliadau hyn i ben , rhaid i 'r Comisiynydd gyflwyno adroddiad .
< /sentence>
< sentence type="normal" elementid="20012787w-s147">
< roundbracket elementid="20012787w-rb155">
(  2 )
< /roundbracket>
Pan ddaw adolygiad swyddogaethau , adolygiad trefniadau , monitro yn unol ag adran 73
< roundbracket elementid="20012787w-rb156">
(  1 )
< /roundbracket>
o 'r Ddeddf neu asesiad yn unol ag adran 73
< roundbracket elementid="20012787w-rb157">
(  1A )
< /roundbracket>
o 'r Ddeddf i ben , fe gaiff y Comisiynydd gyflwyno adroddiad .
< /sentence>
< sentence type="heading" elementid="20012787w-s148">
< roundbracket elementid="20012787w-rb158">
(  3 )
< /roundbracket>
Rhaid i adroddiad a gyflwynir o dan baragraff
< roundbracket elementid="20012787w-rb159">
(  1 )
< /roundbracket>
neu
< roundbracket elementid="20012787w-rb160">
(  2 )
< /roundbracket>
nodi - 
< /sentence>
< sentence type="heading" elementid="20012787w-s149">
< roundbracket elementid="20012787w-rb161">
(  a )
< /roundbracket>
canfyddiadau a chasgliadau 'r Comisiynydd ; a
< /sentence>
< sentence type="normal" elementid="20012787w-s150">
< roundbracket elementid="20012787w-rb162">
(  b )
< /roundbracket>
unrhyw argymhellion a wneir gan y Comisiynydd .
< /sentence>
< /p>
< p elementid="20012787w-p53">
< sentence type="heading" elementid="20012787w-s151">
< roundbracket elementid="20012787w-rb163">
(  4 )
< /roundbracket>
Rhaid i 'r Comisiynydd anfon copi o adroddiad o 'r fath - 
< /sentence>
< /p>
< p elementid="20012787w-p54">
< sentence type="heading" elementid="20012787w-s152">
< roundbracket elementid="20012787w-rb164">
(  a )
< /roundbracket>
at y Prif Weinidog ; a
< /sentence>
< sentence type="normal" elementid="20012787w-s153">
< roundbracket elementid="20012787w-rb165">
(  b )
< /roundbracket>
i lyfrgelloedd y Cynulliad a dau D 'r Senedd .
< /sentence>
< /p>
< p elementid="20012787w-p55">
< sentence type="heading" elementid="20012787w-s154">
< roundbracket elementid="20012787w-rb166">
(  5 )
< /roundbracket>
Rhaid i 'r Comisiynydd anfon copi
< /sentence>
< /p>
< p elementid="20012787w-p56">
< sentence type="heading" elementid="20012787w-s155">
< roundbracket elementid="20012787w-rb167">
(  a )
< /roundbracket>
yn achos adroddiad a gyflwynir o dan baragraff
< roundbracket elementid="20012787w-rb168">
(  1 )
< /roundbracket>
, at y plentyn neu 'r person , yn ôl fel y digwydd , a gyflwynodd sylwadau i 'r Comisiynydd yn unol â rheoliad 6 ; a
< /sentence>
< sentence type="heading" elementid="20012787w-s156">
< roundbracket elementid="20012787w-rb169">
(  b )
< /roundbracket>
yn achos adroddiad a gyflwynir o dan baragraff
< roundbracket elementid="20012787w-rb170">
(  1 )
< /roundbracket>
neu
< roundbracket elementid="20012787w-rb171">
(  2 )
< /roundbracket>
, at y person neu 'r personau
< /sentence>
< sentence type="heading" elementid="20012787w-s157">
< roundbracket elementid="20012787w-rb172">
(  i )
< /roundbracket>
y mae ei waith wrth ddarparu gwasanaethau neu ei waith wrth arfer swyddogaethau wedi 'i archwilio,
< /sentence>
< sentence type="heading" elementid="20012787w-s158">
< roundbracket elementid="20012787w-rb173">
(  ii )
< /roundbracket>
y mae ei drefniadau mewn perthynas â ch ynion , chwythu 'r chwiban neu eiriolaeth wedi 'u hadolygu neu wedi 'u monitro,
< /sentence>
< sentence type="heading" elementid="20012787w-s159">
< roundbracket elementid="20012787w-rb174">
(  iii )
< /roundbracket>
y mae asesiad yn unol ag adran 73
< roundbracket elementid="20012787w-rb175">
(  1A )
< /roundbracket>
o 'r Ddeddf wedi 'i gynnal mewn perthynas ag ef , neu
< /sentence>
< sentence type="normal" elementid="20012787w-s160">
< roundbracket elementid="20012787w-rb176">
(  iv )
< /roundbracket>
a grybwyllir yn yr adroddiad .
< /sentence>
< /p>
< p elementid="20012787w-p57">
< sentence type="heading" elementid="20012787w-s161">
< strong elementid="20012787w-st30">
Camau pellach yn sgil adroddiad
< /strong>
< /sentence>
< sentence type="normal" elementid="20012787w-s162">
14 .
< /sentence>
< sentence type="normal" elementid="20012787w-s163">
 - 
< roundbracket elementid="20012787w-rb177">
(  1 )
< /roundbracket>
Os yw 'r Comisiynydd wedi cyflwyno adroddiad o dan baragraff
< roundbracket elementid="20012787w-rb178">
(  1 )
< /roundbracket>
o reoliad 13 sy 'n cynnwys argymhelliad mewn perthynas â darparydd gwasanaethau rheoleiddiedig i blant yng Nghymru , y Cynulliad neu berson a grybwyllir yn Atodlen 2A i 'r Ddeddf , caiff y Comisiynydd ei gwneud yn ofynnol mewn ysgrifen i 'r person y gwneir yr argymhelliad mewn perthynas ag ef roi 'r wybodaeth berthnasol i 'r Comisiynydd o fewn tri mis o ddyddiad anfon copi o 'r adroddiad at y person hwnnw .
< /sentence>
< sentence type="normal" elementid="20012787w-s164">
< roundbracket elementid="20012787w-rb179">
(  2 )
< /roundbracket>
Os yw 'r Comisiynydd wedi cyflwyno adroddiad o dan baragraff
< roundbracket elementid="20012787w-rb180">
(  2 )
< /roundbracket>
o reoliad 13 sy 'n cynnwys argymhelliad mewn perthynas â pherson a grybwyllir ym mharagraff
< roundbracket elementid="20012787w-rb181">
(  1 )
< /roundbracket>
o 'r rheoliad hwn , caiff y Comisiynydd ofyn mewn ysgrifen i 'r person y gwneir yr argymhelliad mewn perthynas ag ef roi 'r wybodaeth berthnasol i 'r Comisiynydd o fewn tri mis o ddyddiad anfon copi o 'r adroddiad at y person hwnnw .
< /sentence>
< sentence type="normal" elementid="20012787w-s165">
< roundbracket elementid="20012787w-rb182">
(  3 )
< /roundbracket>
At ddibenion paragraffau
< roundbracket elementid="20012787w-rb183">
(  1 )
< /roundbracket>
a
< roundbracket elementid="20012787w-rb184">
(  2 )
< /roundbracket>
ystyr
< doublequotation elementid="20012787w-dq28">
"  yr wybodaeth berthnasol "
< /doublequotation>
yw unrhyw wybodaeth , esboniadau neu gymorth i alluogi 'r Comisiynydd i benderfynu a yw 'r person o dan sylw wedi cydymffurfio â 'r argymhelliad neu a fydd yn cydymffurfio ag ef , neu esboniad ar y rheswm dros beidio â chymryd camau o 'r fath neu dros beidio â bwriadu eu cymryd .
< /sentence>
< sentence type="normal" elementid="20012787w-s166">
< roundbracket elementid="20012787w-rb185">
(  4 )
< /roundbracket>
Pan wneir gofyniad o dan baragraff
< roundbracket elementid="20012787w-rb186">
(  1 )
< /roundbracket>
neu gais o dan baragraff
< roundbracket elementid="20012787w-rb187">
(  2 )
< /roundbracket>
rhaid iddo gynnwys datganiad y gall methiant i ymateb o fewn y tri mis gael ei gyhoeddi mewn unrhyw fodd y mae 'r Comisiynydd yn credu ei fod yn briodol .
< /sentence>
< sentence type="normal" elementid="20012787w-s167">
< roundbracket elementid="20012787w-rb188">
(  5 )
< /roundbracket>
Os yw 'r Comisiynydd yn credu 'n rhesymol , pan gaiff yr wybodaeth berthnasol , nad yw 'r camau a gymerwyd neu y bwriedir eu cymryd er mwyn cydymffurfio â 'r argymhelliad neu nad yw 'r rheswm am beidio â chymryd camau o 'r fath neu am beidio â bwriadu eu cymryd , yn ddigonol , caiff y Comisiynydd anfon hysbysiad ysgrifenedig at y person o dan sylw yn nodi 'r diffygion , sef hysbysiad y mae angen ymateb iddo o fewn un mis o ddyddiad ei anfon .
< /sentence>
< sentence type="normal" elementid="20012787w-s168">
< roundbracket elementid="20012787w-rb189">
(  6 )
< /roundbracket>
Os na chaiff y Comisiynydd ymateb yn unol â 'r hysbysiad ysgrifenedig o dan baragraff
< roundbracket elementid="20012787w-rb190">
(  5 )
< /roundbracket>
o fewn un mis neu os yw 'n anfodlon ar yr ymateb , caiff y Comisiynydd anfon hysbysiad atodol sy 'n mynnu ymateb atodol o fewn un mis o ddyddiad ei anfon .
< /sentence>
< sentence type="normal" elementid="20012787w-s169">
< roundbracket elementid="20012787w-rb191">
(  7 )
< /roundbracket>
Rhaid i 'r hysbysiad atodol gynnwys datganiad y gall methiant i roi 'r hyn sy 'n ymateb atodol boddhaol ym marn y Comisiynydd , neu fethiant i ymateb o gwbl , gael ei gyhoeddi mewn unrhyw fodd y mae 'r Comisiynydd yn credu ei fod yn briodol .
< /sentence>
< sentence type="heading" elementid="20012787w-s170">
< roundbracket elementid="20012787w-rb192">
(  8 )
< /roundbracket>
Rhaid i 'r Comisiynydd gadw cofrestr yn cynnwys manylion - 
< /sentence>
< /p>
< p elementid="20012787w-p58">
< sentence type="heading" elementid="20012787w-s171">
< roundbracket elementid="20012787w-rb193">
(  a )
< /roundbracket>
argymhellion a geir mewn adroddiadau a gyflwynir o dan baragraffau
< roundbracket elementid="20012787w-rb194">
(  1 )
< /roundbracket>
neu
< roundbracket elementid="20012787w-rb195">
(  2 )
< /roundbracket>
o reoliad 13 , a
< /sentence>
< sentence type="normal" elementid="20012787w-s172">
< roundbracket elementid="20012787w-rb196">
(  b )
< /roundbracket>
canlyniadau camau pellach a gymerir yn unol â pharagraffau
< roundbracket elementid="20012787w-rb197">
(  1 )
< /roundbracket>
,
< roundbracket elementid="20012787w-rb198">
(  2 )
< /roundbracket>
,
< roundbracket elementid="20012787w-rb199">
(  5 )
< /roundbracket>
a
< roundbracket elementid="20012787w-rb200">
(  6 )
< /roundbracket>
o 'r rheoliad hwn .
< /sentence>
< /p>
< p elementid="20012787w-p59">
< sentence type="normal" elementid="20012787w-s173">
< roundbracket elementid="20012787w-rb201">
(  9 )
< /roundbracket>
Rhaid i unrhyw gofrestr a gedwir o dan baragraff
< roundbracket elementid="20012787w-rb202">
(  8 )
< /roundbracket>
fod yn agored i 'w harchwilio gan unrhyw un ar bob adeg resymol yn swyddfeydd y Comisiynydd a chaiff y Comisiynydd wneud trefniadau i gopïau o 'r gofrestr fod ar gael i 'w harchwilio mewn unrhyw fan arall neu fannau eraill neu drwy unrhyw fodd arall y mae 'n credu eu bod yn briodol .
< /sentence>
< sentence type="normal" elementid="20012787w-s174">
< roundbracket elementid="20012787w-rb203">
(  10 )
< /roundbracket>
Rhaid i 'r Comisiynydd gyhoeddi 'r trefniadau a enwyd mewn ffordd a fydd yn dod â hwy i sylw personau y mae 'n debyg , ym marn resymol y Comisiynydd , y bydd ganddynt ddiddordeb .
< /sentence>
< sentence type="heading" elementid="20012787w-s175">
< strong elementid="20012787w-st31">
Adroddiadau Blynyddol
< /strong>
< /sentence>
< sentence type="normal" elementid="20012787w-s176">
15 .
< /sentence>
< sentence type="heading" elementid="20012787w-s177">
 - 
< roundbracket elementid="20012787w-rb204">
(  1 )
< /roundbracket>
Yn ddarostyngedig i baragraff
< roundbracket elementid="20012787w-rb205">
(  3 )
< /roundbracket>
, rhaid i 'r Comisiynydd gyflwyno adroddiad blynyddol i 'r Prif Weinidog , y mae 'n rhaid iddo gynnwys - 
< /sentence>
< /p>
< p elementid="20012787w-p60">
< sentence type="heading" elementid="20012787w-s178">
< roundbracket elementid="20012787w-rb206">
(  a )
< /roundbracket>
crynodeb o 'r camau a gymerwyd wrth arfer swyddogaethau 'r Comisiynydd o dan y Ddeddf yn ystod y flwyddyn ariannol flaenorol , gan gynnwys crynodeb o 'r adroddiadau a gyhoeddwyd yn ystod y cyfnod hwnnw ac o unrhyw sylwadau y gall y Comisiynydd fod wedi 'u cyflwyno yn ystod y cyfnod hwnnw yn unol ag adran 75A o 'r Ddeddf , gan gynnwys unrhyw sylwadau a gyflwynwyd ynghylch amrediad neu effeithiolrwydd pwerau 'r Comisiynydd ;
< /sentence>
< sentence type="heading" elementid="20012787w-s179">
< roundbracket elementid="20012787w-rb207">
(  b )
< /roundbracket>
adolygiad o faterion sy 'n berthnasol i hawliau a lles plant yng Nghymru ; ac
< /sentence>
< sentence type="normal" elementid="20012787w-s180">
< roundbracket elementid="20012787w-rb208">
(  c )
< /roundbracket>
crynodeb o raglen waith y Comisiynydd ar gyfer y flwyddyn ariannol y cyflwynir yr adroddiad ynddi ac o gynigion y Comisiynydd ar gyfer rhaglen waith ar gyfer y flwyddyn ariannol yn dilyn y flwyddyn honno .
< /sentence>
< /p>
< p elementid="20012787w-p61">
< sentence type="normal" elementid="20012787w-s181">
< roundbracket elementid="20012787w-rb209">
(  2 )
< /roundbracket>
Rhaid i 'r Comisiynydd hefyd gynhyrchu fersiwn o 'r adroddiad blynyddol sydd , cyn belled ag y bo 'n rhesymol ymarferol , yn addas ar gyfer plant .
< /sentence>
< sentence type="normal" elementid="20012787w-s182">
< roundbracket elementid="20012787w-rb210">
(  3 )
< /roundbracket>
Rhaid i 'r adroddiadau cyntaf y cyfeirir atynt ym mharagraffau
< roundbracket elementid="20012787w-rb211">
(  1 )
< /roundbracket>
a
< roundbracket elementid="20012787w-rb212">
(  2 )
< /roundbracket>
gael eu cyflwyno yn 2002 .
< /sentence>
< sentence type="heading" elementid="20012787w-s183">
< roundbracket elementid="20012787w-rb213">
(  4 )
< /roundbracket>
Yn ddarostyngedig i baragraff
< roundbracket elementid="20012787w-rb214">
(  3 )
< /roundbracket>
, rhaid i 'r Comisiynydd , erbyn 1 Hydref bob blwyddyn , anfon copi o 'r adroddiadau y cyfeirir atynt ym mharagraffau
< roundbracket elementid="20012787w-rb215">
(  1 )
< /roundbracket>
a
< roundbracket elementid="20012787w-rb216">
(  2 )
< /roundbracket>
 - 
< /sentence>
< /p>
< p elementid="20012787w-p62">
< sentence type="heading" elementid="20012787w-s184">
< roundbracket elementid="20012787w-rb217">
(  a )
< /roundbracket>
at y Prif Weinidog ; a
< /sentence>
< sentence type="normal" elementid="20012787w-s185">
< roundbracket elementid="20012787w-rb218">
(  b )
< /roundbracket>
i lyfrgelloedd y Cynulliad a dau D 'r Senedd .
< /sentence>
< /p>
< p elementid="20012787w-p63">
< sentence type="heading" elementid="20012787w-s186">
< strong elementid="20012787w-st32">
Cyhoeddi adroddiadau
< /strong>
< /sentence>
< sentence type="normal" elementid="20012787w-s187">
16 .
< /sentence>
< sentence type="normal" elementid="20012787w-s188">
 - 
< roundbracket elementid="20012787w-rb219">
(  1 )
< /roundbracket>
Rhaid i 'r Comisiynydd drefnu bod copïau o adroddiadau a gyflwynir o dan baragraffau
< roundbracket elementid="20012787w-rb220">
(  1 )
< /roundbracket>
neu
< roundbracket elementid="20012787w-rb221">
(  2 )
< /roundbracket>
o reoliad 13 ac o dan reoliad 15 ar gael i 'w harchwilio yn swyddfa 'r Comisiynydd ar bob adeg resymol ac mewn unrhyw fannau eraill neu drwy unrhyw gyfrwng arall , gan gynnwys drwy gyfrwng electronig , y mae 'r Comisiynydd yn credu eu bod yn briodol .
< /sentence>
< sentence type="normal" elementid="20012787w-s189">
< roundbracket elementid="20012787w-rb222">
(  2 )
< /roundbracket>
Rhaid i 'r Comisiynydd gyhoeddi 'r trefniadau a enwyd mewn ffordd a fydd yn dod â hwy i sylw personau y mae 'n debyg , ym marn resymol y Comisiynydd , y bydd ganddynt ddiddordeb .
< /sentence>
< /p>
< p elementid="20012787w-p64">
< sentence type="heading" elementid="20012787w-s190">
< strong elementid="20012787w-st33">
RHAN VII
< /strong>
< /sentence>
< sentence type="heading" elementid="20012787w-s191">
< strong elementid="20012787w-st34">
AMRYWIOL
< /strong>
< /sentence>
< sentence type="heading" elementid="20012787w-s192">
< strong elementid="20012787w-st35">
Cyfyngiadau ar arfer swyddogaethau sy 'n arferadwy gan bersonau rhagnodedig
< /strong>
< /sentence>
< sentence type="normal" elementid="20012787w-s193">
17 .
< /sentence>
< sentence type="normal" elementid="20012787w-s194">
At ddibenion adran 77
< roundbracket elementid="20012787w-rb223">
(  2 )
< /roundbracket>
o 'r Ddeddf , rhagnodir Gwasanaeth Cynghori a Chynorthwyo Llys i Blant a Theuluoedd
< squarebracket elementid="20012787w-sb2">
[ 
< a type="href" elementid="20012787w-a5">
2
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="heading" elementid="20012787w-s195">
Blynyddoedd ariannol
< /sentence>
< sentence type="normal" elementid="20012787w-s196">
18 .
< /sentence>
< sentence type="heading" elementid="20012787w-s197">
At ddibenion paragraff 6
< roundbracket elementid="20012787w-rb224">
(  4 )
< /roundbracket>
o Atodlen 2 i 'r Ddeddf pennir y cyfnodau canlynol - 
< /sentence>
< sentence type="heading" elementid="20012787w-s198">
< roundbracket elementid="20012787w-rb225">
(  a )
< /roundbracket>
mewn perthynas â 'r flwyddyn ariannol gyntaf , y cyfnod o 1 Mawrth 2001 tan 31 Mawrth 2002 ;
< /sentence>
< sentence type="normal" elementid="20012787w-s199">
< roundbracket elementid="20012787w-rb226">
(  b )
< /roundbracket>
mewn perthynas â phob blwyddyn ariannol ddilynol y cyfnod o 1 Ebrill tan 31 Mawrth .
< /sentence>
< /p>
< p elementid="20012787w-p65">
< sentence type="heading" elementid="20012787w-s200">
< strong elementid="20012787w-st36">
Gwybodaeth
< /strong>
< /sentence>
< sentence type="normal" elementid="20012787w-s201">
19 .
< /sentence>
< sentence type="normal" elementid="20012787w-s202">
Os yw gwybodaeth y gofynnir iddi gael ei rhoi o dan baragraff
< roundbracket elementid="20012787w-rb227">
(  1 )
< /roundbracket>
o reoliad 3 , paragraff
< roundbracket elementid="20012787w-rb228">
(  1 )
< /roundbracket>
< roundbracket elementid="20012787w-rb229">
(  a )
< /roundbracket>
o reoliad 8 neu baragraff
< roundbracket elementid="20012787w-rb230">
(  1 )
< /roundbracket>
o reoliad 14
< roundbracket elementid="20012787w-rb231">
(
< doublequotation elementid="20012787w-dq29">
"  y darpariaethau perthnasol "
< /doublequotation>
)
< /roundbracket>
yn wybodaeth sy 'n cael ei chadw drwy gyfrwng cyfrifiadur neu ar unrhyw ffurf arall , caiff y Comisiynydd ei gwneud yn ofynnol i unrhyw berson sydd â gofal y cyfrifiadur neu 'r ddyfais arall sy 'n cadw 'r wybodaeth honno , neu sydd fel arall yn ymwneud â hwy , drefnu bod yr wybodaeth ar gael , neu gyflwyno 'r wybodaeth , ar ffurf weladwy a darllenadwy .
< /sentence>
< sentence type="normal" elementid="20012787w-s203">
20 .
< /sentence>
< sentence type="heading" elementid="20012787w-s204">
Os yw person yn rhoi gwybodaeth i 'r Comisiynydd yn unol â pharagraff
< roundbracket elementid="20012787w-rb232">
(  1 )
< /roundbracket>
< roundbracket elementid="20012787w-rb233">
(  a )
< /roundbracket>
o reoliad 8 neu 'n bresennol gerbron y Comisiynydd yn unol â rheoliad 9 , caiff y Comisiynydd dalu i 'r person hwnnw , os yw 'n credu bod hynny 'n briodol - 
< /sentence>
< /p>
< p elementid="20012787w-p66">
< sentence type="heading" elementid="20012787w-s205">
< roundbracket elementid="20012787w-rb234">
(  a )
< /roundbracket>
Symiau mewn perthynas â chostau a dynnwyd yn briodol gan y person , a
< /sentence>
< sentence type="heading" elementid="20012787w-s206">
< roundbracket elementid="20012787w-rb235">
(  b )
< /roundbracket>
Lwfansau yn iawndal am golli eu hamser,
< /sentence>
< /p>
< p elementid="20012787w-p67">
< sentence type="normal" elementid="20012787w-s207">
yn unol ag unrhyw raddfeydd , ac o dan unrhyw amodau y gall y Comisiynydd eu pennu .
< /sentence>
< sentence type="heading" elementid="20012787w-s208">
< strong elementid="20012787w-st37">
Cymhwyso cyfeiriadau at blant
< /strong>
< /sentence>
< sentence type="normal" elementid="20012787w-s209">
21 .
< /sentence>
< sentence type="normal" elementid="20012787w-s210">
 - 
< roundbracket elementid="20012787w-rb236">
(  1 )
< /roundbracket>
At ddibenion Rhan V o 'r Ddeddf mae
< doublequotation elementid="20012787w-dq30">
"
< english>
child
< /english>
"
< /doublequotation>
yn cynnwys person 18 oed neu drosodd sy 'n dod o fewn is-adran
< roundbracket elementid="20012787w-rb237">
(  1B )
< /roundbracket>
o adran 78 o 'r Ddeddf a dehonglir cyfeiriadau at
< doublequotation elementid="20012787w-dq31">
"
< english>
child
< /english>"
< /doublequotation>
neu
< doublequotation elementid="20012787w-dq32">
"
< english>
children
< /english>
"
< /doublequotation>
yn Rhan V o 'r Ddeddf ac at
< doublequotation elementid="20012787w-dq33">
"  plentyn "
< /doublequotation>
neu
< doublequotation elementid="20012787w-dq34">
"  plant "
< /doublequotation>
yn y Rheoliadau hyn yn unol â hynny .
< /sentence>
< sentence type="heading" elementid="20012787w-s211">
< roundbracket elementid="20012787w-rb238">
(  2 )
< /roundbracket>
Yn ddarostyngedig i baragraff
< roundbracket elementid="20012787w-rb239">
(  3 )
< /roundbracket>
, bydd cyfeiriadau at blentyn yn is-adran
< roundbracket elementid="20012787w-rb240">
(  1 )
< /roundbracket>
o adran 78 o 'r Ddeddf yn cynnwys cyfeiriadau at berson
< roundbracket elementid="20012787w-rb241">
(  gan gynnwys plentyn )
< /roundbracket>
a oedd ar unrhyw adeg
< roundbracket elementid="20012787w-rb242">
(  gan gynnwys adeg cyn i 'r paragraff hwn ddod i rym )
< /roundbracket>
 - 
< /sentence>
< /p>
< p elementid="20012787w-p68">
< sentence type="heading" elementid="20012787w-s212">
< roundbracket elementid="20012787w-rb243">
(  a )
< /roundbracket>
yn blentyn a oedd fel arfer yn preswylio yng Nghymru ;
< /sentence>
< sentence type="heading" elementid="20012787w-s213">
< roundbracket elementid="20012787w-rb244">
(  b )
< /roundbracket>
yn blentyn y cafodd gwasanaethau eu darparu iddo neu mewn perthynas ag ef yng Nghymru gan berson a grybwyllir yn Atodlen 2B i 'r Ddeddf , ar ei ran neu o dan drefniadau ag ef ; neu
< /sentence>
< sentence type="heading" elementid="20012787w-s214">
< roundbracket elementid="20012787w-rb245">
(  c )
< /roundbracket>
yn blentyn y cafodd gwasanaethau rheoleiddiedig i blant eu darparu iddo neu mewn perthynas ag ef,
< /sentence>
< /p>
< p elementid="20012787w-p69">
< sentence type="normal" elementid="20012787w-s215">
a dehonglir cyfeiriadau at
< doublequotation elementid="20012787w-dq35">
"
< english>
child
< /english>
"
< /doublequotation>
neu
< doublequotation elementid="20012787w-dq36">
"
< english>
children
< /english>
""  children "
< /doublequotation>
yn Rhan V o 'r Ddeddf ac at
< doublequotation elementid="20012787w-dq37">
"  plentyn "
< /doublequotation>
neu
< doublequotation elementid="20012787w-dq38">
"  plant "
< /doublequotation>
yn y Rheoliadau hyn yn unol â hynny .
< /sentence>
< sentence type="normal" elementid="20012787w-s216">
< roundbracket elementid="20012787w-rb246">
(  3 )
< /roundbracket>
Rhaid peidio â dehongli cyfeiriadau at
< doublequotation elementid="20012787w-dq39">
"  plentyn "
< /doublequotation>
neu
< doublequotation elementid="20012787w-dq40">
"  plant "
< /doublequotation>
a ddehonglir yn unol â pharagraff
< roundbracket elementid="20012787w-rb247">
(  1 )
< /roundbracket>
yn unol â pharagraff
< roundbracket elementid="20012787w-rb248">
(  2 )
< /roundbracket>
hefyd mewn perthynas ag unrhyw amser cyn i baragraff
< roundbracket elementid="20012787w-rb249">
(  1 )
< /roundbracket>
ddod i rym .
< /sentence>
< sentence type="heading" elementid="20012787w-s217">
< strong elementid="20012787w-st38">
Confensiwn y Cenhedloedd Unedig ar Hawliau 'r Plentyn
< /strong>
< /sentence>
< sentence type="normal" elementid="20012787w-s218">
22 .
< /sentence>
< sentence type="normal" elementid="20012787w-s219">
Wrth arfer ei swyddogaethau rhaid i 'r Comisiynydd roi sylw i Gonfensiwn y Cenhedloedd Unedig ar Hawliau 'r Plentyn
< squarebracket elementid="20012787w-sb3">
[ 
< a type="href" elementid="20012787w-a6">
3
< /a>
]
< /squarebracket>
fel y cafodd ei gadarnhau gan y Deyrnas Unedig ac yn ddarostynedig i 'r cyfryw gymalau cadw a wnaed gan y Deyrnas Unedig sy 'n gymwys ar ddyddiad gwneud y rheoliad hyn .
< /sentence>
< sentence type="heading" elementid="20012787w-s220">
Llofnodwyd ar ran Cynulliad Cenedlaethol Cymru o dan adran 66
< roundbracket elementid="20012787w-rb250">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20012787w-sb4">
[ 
< a type="href" elementid="20012787w-a7">
4
< /a>
]
< /squarebracket>
< /sentence>
< sentence type="heading" elementid="20012787w-s221">
< em elementid="20012787w-em19">
Rhodri Morgan
< /em>
< /sentence>
< sentence type="heading" elementid="20012787w-s222">
Prif weinidog Cynulliad Cenedlaethol Cymru
< /sentence>
< sentence type="heading" elementid="20012787w-s223">
25 Gorffennaf 2001
< /sentence>
< /p>
< p elementid="20012787w-p70">
< sentence type="heading" elementid="20012787w-s224">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20012787w-s225">
< em elementid="20012787w-em20">
< roundbracket elementid="20012787w-rb251">
(  Nid yw 'r nodyn hwn yn rhan o 'r Rheoliadau )
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20012787w-s226">
Mae 'r Rheoliadau hyn yn gwneud darpariaeth mewn perthynas â swyddogaethau Comisiynydd Plant Cymru
< roundbracket elementid="20012787w-rb252">
(
< doublequotation elementid="20012787w-dq41">
"  y Comisiynydd "
< /doublequotation>
)
< /roundbracket>
a sefydlwyd o dan Ran V o Ddeddf Safonau Gofal 2000
< roundbracket elementid="20012787w-rb253">
(
< doublequotation elementid="20012787w-dq42">
"  y Ddeddf "
< /doublequotation>
)
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20012787w-s227">
Mae Rhan I o 'r Rheoliadau 'n cynnwys darpariaethau ynghylch dehongli .
< /sentence>
< sentence type="normal" elementid="20012787w-s228">
Mae Rhan II , mewn perthynas â rôl y Comisiynydd wrth adolygu a monitro trefniadau ar gyfer c ynion , chwythu 'r chwiban ac eiriolaeth , yn rhagnodi 'r math o gyngor a chymorth i blant y mae 'r trefniadau ar eu cyfer i ddod o dan awdurdodaeth y Comisiynydd
< roundbracket elementid="20012787w-rb254">
(  rheoliad 2 )
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20012787w-s229">
Mae hefyd yn rhoi p er i 'r Comisiynydd fynnu cael gwybodaeth oddi wrth bersonau rhagnodedig
< roundbracket elementid="20012787w-rb255">
(  rheoliad 3 )
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20012787w-s230">
Mae Rhan III yn rhoi swyddogaethau i 'r Comisiynydd ynghylch archwilio achosion plant penodol y mae Rhan V o 'r Ddeddf yn gymwys iddynt
< roundbracket elementid="20012787w-rb256">
(  rheoliad 4 )
< /roundbracket>
; yn pennu 'r mathau o achos a all gael eu harchwilio
< roundbracket elementid="20012787w-rb257">
(  rheoliad 5 )
< /roundbracket>
ac o dan ba amgylchiadau y gall archwiliad gael ei wneud
< roundbracket elementid="20012787w-rb258">
(  rheoliad 6 )
< /roundbracket>
; yn gwneud darpariaeth ar gyfer cynnal archwiliad
< roundbracket elementid="20012787w-rb259">
(  rheoliad 7 )
< /roundbracket>
, ynghylch rhoi gwybodaeth i 'r Comisiynydd mewn cysylltiad ag archwiliad
< roundbracket elementid="20012787w-rb260">
(  rheoliad 8 )
< /roundbracket>
, ac ynghylch bod yn bresennol gerbron y Comisiynydd yn bersonol
< roundbracket elementid="20012787w-rb261">
(  rheoliad 9 )
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20012787w-s231">
Mae Rhan IV yn rhoi p er i 'r Comisiynydd roi cymorth ariannol a chymorth arall i blant y mae Rhan V o 'r Ddeddf yn gymwys iddynt , yn rhagnodi 'r achosion a 'r gweithdrefnau y gall cymorth o 'r fath gael ei roi mewn perthynas â hwy
< roundbracket elementid="20012787w-rb262">
(  rheoliad 10 )
< /roundbracket>
ac yn darparu ar gyfer amodau a all gael eu gosod mewn cysylltiad â rhoi cymorth
< roundbracket elementid="20012787w-rb263">
(  rheoliad 11 )
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20012787w-s232">
Mae Rhan V yn gwneud darpariaeth bellach ar gyfer y trefniadau ynghylch perthynas y Comisiynydd â phlant
< roundbracket elementid="20012787w-rb264">
(  rheoliad 12 )
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20012787w-s233">
Mae Rhan VI yn gwneud darpariaeth ar gyfer adroddiadau penodol a chamau i 'w dilyn
< roundbracket elementid="20012787w-rb265">
(  rheoliadau 13 a 14 )
< /roundbracket>
, ynghylch adroddiadau blynyddol
< roundbracket elementid="20012787w-rb266">
(  rheoliad 15 )
< /roundbracket>
ac ynghylch cyhoeddi adroddiadau
< roundbracket elementid="20012787w-rb267">
(  rheoliad 16 )
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20012787w-s234">
Mae Rhan VII yn cynnwys darpariaethau amrywiol ynghylch y cyfyngiadau ar arfer swyddogaethau penodol lle maent yn gorgyffwrdd â swyddogaethau cyrff eraill sydd wedi 'u rhagnodi
< roundbracket elementid="20012787w-rb268">
(  rheoliad 17 )
< /roundbracket>
; i bennu cyfnod y flwyddyn ariannol gychwynnol a 'r blynyddoedd ariannol canlynol
< roundbracket elementid="20012787w-rb269">
(  rheoliad 18 )
< /roundbracket>
; ynghylch y modd y rhoddir gwybodaeth
< roundbracket elementid="20012787w-rb270">
(  rheoliad 19 )
< /roundbracket>
; ynghylch talu treuliau a lwfansau mewn perthynas â rhoi gwybodaeth
< roundbracket elementid="20012787w-rb271">
(  rheoliad 20 )
< /roundbracket>
ac ynghylch dehongli cyfeiriadau penodol at blant
< roundbracket elementid="20012787w-rb272">
(  rheoliad 21 )
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20012787w-s235">
Yn olaf , mae 'r Rheoliadau yn gosod dyletswydd ar y Comisiynydd i roi sylw i Gonfensiwn y Cenhedloedd Unedig ar Hawliau 'r Plentyn wrth arfer ei swyddogaethau
< roundbracket elementid="20012787w-rb273">
(  rheoliad 22 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20012787w-s236">
< em elementid="20012787w-em21">
< english>
Notes
< /english>
:
< /em>
< /sentence>
< sentence type="normal" elementid="20012787w-s237">
< squarebracket elementid="20012787w-sb5">
[  1 ]
< /squarebracket>
2000 p.14 .
< /sentence>
< sentence type="normal" elementid="20012787w-s238">
Gweler adran 78
< roundbracket elementid="20012787w-rb274">
(  7 )
< /roundbracket>
i gael y diffiniad o
< doublequotation elementid="20012787w-dq43">
"
< english>
regulations
< /english>
"
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20012787w-s239">
Cafodd diwygiadau perthnasol eu gwneud i adrannau 74 , 76 a 78 gan Ddeddf Comisiynydd Plant Cymru 2001
< roundbracket elementid="20012787w-rb275">
(  p.18 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20012787w-s240">
< a type="href" elementid="20012787w-a8">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20012787w-p71">
< sentence type="normal" elementid="20012787w-s241">
< squarebracket elementid="20012787w-sb6">
[  2 ]
< /squarebracket>
Cafodd Gwasanaeth Cynghori a Chynorthwyo Llys i Blant a Theuluoedd
< roundbracket elementid="20012787w-rb276">
(
< doublequotation elementid="20012787w-dq44">
"  CAFCASS "
< /doublequotation>
)
< /roundbracket>
ei sefydlu ar 1 Ebrill 2001 o dan Ddeddf Cyfiawnder Troseddol a Gwasanaethau Llys 2000  p .
< /sentence>
< sentence type="normal" elementid="20012787w-s242">
43 ) .
< /sentence>
< sentence type="heading" elementid="20012787w-s243">
< a type="href" elementid="20012787w-a9">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20012787w-p72">
< sentence type="heading" elementid="20012787w-s244">
< squarebracket elementid="20012787w-sb7">
[  3 ]
< /squarebracket>
Gweler Papur Gorchymyn 1668 
< a type="href" elementid="20012787w-a10">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20012787w-p73">
< sentence type="normal" elementid="20012787w-s245">
< squarebracket elementid="20012787w-sb8">
[  4 ]
< /squarebracket>
1998 p.38 .
< /sentence>
< sentence type="heading" elementid="20012787w-s246">
< a type="href" elementid="20012787w-a11">
< english>
back
< /english>
< /a>
< /sentence>
< /p>
< p elementid="20012787w-p74">
< sentence type="heading" elementid="20012787w-s247">
< a type="href" elementid="20012787w-a12">
< english>
English version
< /english>
< /a>
< /sentence>
< sentence type="heading" elementid="20012787w-s248">
ISBN 0 11090329 3
< /sentence>
< /p>
< table elementid="20012787w-tb2">
< td elementid="20012787w-td8">
< p elementid="20012787w-p75">
< sentence type="heading" elementid="20012787w-s249">
< a type="href" elementid="20012787w-a13">
< english>
Other UK SIs
< /english>
< /a>
< a type="href" elementid="20012787w-a14">
< english>
Home
< /english>
< /a>
< a type="href" elementid="20012787w-a15">
< english>
National Assembly for Wales Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20012787w-a16">
< english>
Scottish Statutory Instruments
< /english>
< /a>
< a type="href" elementid="20012787w-a17">
< english>
Statutory Rules of Northern Ireland
< /english>
< /a>
< a type="href" elementid="20012787w-a18">
< english>
Her Majesty 's Stationery Office
< /english>
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20012787w-td9">
< p elementid="20012787w-p76">
< sentence type="heading" elementid="20012787w-s250">
< english>
< strong elementid="20012787w-st39">
We welcome your 
< a type="href" elementid="20012787w-a19">
comments
< /a>
on this site
< /strong>
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20012787w-td10">
< p elementid="20012787w-p77">
< sentence type="heading" elementid="20012787w-s251">
< english>
© Crown copyright 2001
< /english>
< /sentence>
< /p>
< /td>
< td elementid="20012787w-td11">
< p elementid="20012787w-p78">
< sentence type="heading" elementid="20012787w-s252">
< english>
Prepared 24 August 2001
< /english>
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>5262< /wordcount>
< /file>
< /corpus>

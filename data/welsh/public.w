< ?xml version = "1.0" encoding="UTF-8"?>
< corpus topic="public-audit" language="Welsh">
< file elementid="20050071w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Gorchymyn Deddf Archwilio Cyhoeddus ( Cymru ) 2004 ( Cychwyn Rhif 1 ) 2005< /pagetitle>
< doc_class>Statutory Instrument - Wales< /doc_class>
< year>2005< /year>
< number>0071< /number>
< legislation_code>30102< /legislation_code>
< legislation_type>Statutory Instrument - Wales< /legislation_type>
< subject>ARCHWILIO CYHOEDDUS, CYMRU A LLOEGR< /subject>
< laid_date>_EMPTY< /laid_date>
< made_date>20050118< /made_date>
< coming_into_force>_EMPTY< /coming_into_force>
< isbn>0110910575< /isbn>
< title>Gorchymyn Deddf Archwilio Cyhoeddus (Cymru) 2004 (Cychwyn Rhif 1) 2005< /title>
< signatory>D. Elis-Thomas< /signatory>
< signatory_title>Llywydd y Cynulliad Cenedlaethol< /signatory_title>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20050071w-p1">
< sentence type="heading" elementid="20050071w-s1">
Offerynnau Statudol 2005 Rhif 71
< roundbracket elementid="20050071w-rb1">
(  Cy.9 )
< /roundbracket>
< roundbracket elementid="20050071w-rb2">
(  C.3 )
< /roundbracket>
< /sentence>
< /p>
< p elementid="20050071w-p2">
< sentence type="heading" elementid="20050071w-s2">
< strong elementid="20050071w-st1">
Gorchymyn Deddf Archwilio Cyhoeddus
< roundbracket elementid="20050071w-rb3">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20050071w-rb4">
(  Cychwyn Rhif 1 )
< /roundbracket>
2005
< /strong>
< /sentence>
< /p>
< p elementid="20050071w-p3">
< sentence type="heading" elementid="20050071w-s3">
© Hawlfraint y Goron 2005
< /sentence>
< /p>
< p elementid="20050071w-p4">
< sentence type="normal" elementid="20050071w-s4">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20050071w-p5">
< sentence type="normal" elementid="20050071w-s5">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20050071w-s6">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20050071w-p6">
< sentence type="normal" elementid="20050071w-s7">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20050071w-s8">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20050071w-s9">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20050071w-p7">
< sentence type="normal" elementid="20050071w-s10">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20050071w-s11">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan The Stationery Office Limited fel
< strong elementid="20050071w-st2">
Gorchymyn Deddf Archwilio Cyhoeddus
< roundbracket elementid="20050071w-rb5">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20050071w-rb6">
(  Cychwyn Rhif 1 )
< /roundbracket>
2005
< /strong>
, ISBN 0110910575 .
< /sentence>
< sentence type="normal" elementid="20050071w-s12">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20050071w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20050071w-s13">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20050071w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20050071w-p8">
< sentence type="normal" elementid="20050071w-s14">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20050071w-a3">
Frequently Asked Questions
< /a> .
< /sentence>
< /p>
< p elementid="20050071w-p9">
< sentence type="normal" elementid="20050071w-s15">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20050071w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20050071w-s16">
Pan welwch fotwm
< doublequotation elementid="20050071w-dq2">
"  continue "
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20050071w-p10">
< sentence type="heading" elementid="20050071w-s17">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="heading" elementid="20050071w-s18">
< strong elementid="20050071w-st3">
2005 Rhif 71
< roundbracket elementid="20050071w-rb7">
(  Cy.9 )
< /roundbracket>
< roundbracket elementid="20050071w-rb8">
(  C.3 )
< /roundbracket>
< /strong>
< /sentence>
< sentence type="heading" elementid="20050071w-s19">
< strong elementid="20050071w-st4">
ARCHWILIO CYHOEDDUS , CYMRU A LLOEGR
< /strong>
< /sentence>
< sentence type="heading" elementid="20050071w-s20">
Gorchymyn Deddf Archwilio Cyhoeddus
< roundbracket elementid="20050071w-rb9">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20050071w-rb10">
(  Cychwyn Rhif 1 )
< /roundbracket>
2005
< /sentence>
< /p>
< table elementid="20050071w-tb1">
< td elementid="20050071w-td1">
< p elementid="20050071w-p11">
< sentence type="heading" elementid="20050071w-s21">
< em elementid="20050071w-em1">
Wedi 'i wneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20050071w-td2">
< p elementid="20050071w-p12">
< sentence type="heading" elementid="20050071w-s22">
< em elementid="20050071w-em2">
18 Ionawr 2005
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20050071w-p13">
< sentence type="heading" elementid="20050071w-s23">
Mae Cynulliad Cenedlaethol Cymru , drwy arfer y pwerau a roddwyd iddo gan adran 73 o Ddeddf Archwilio Cyhoeddus
< roundbracket elementid="20050071w-rb11">
(  Cymru )
< /roundbracket>
2004
< squarebracket elementid="20050071w-sb1">
[ 
< a type="href" elementid="20050071w-a4">
1
< /a>
]
< /squarebracket>
, drwy hyn yn gwneud y Gorchymyn a ganlyn :
< /sentence>
< sentence type="heading" elementid="20050071w-s24">
< strong elementid="20050071w-st5">
Enwi a dehongli
< /strong>
< /sentence>
< sentence type="normal" elementid="20050071w-s25">
1 .
< /sentence>
< sentence type="normal" elementid="20050071w-s26">
 - 
< roundbracket elementid="20050071w-rb12">
(  1 )
< /roundbracket>
Enw 'r Gorchymyn hwn yw Gorchymyn Deddf Archwilio Cyhoeddus
< roundbracket elementid="20050071w-rb13">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20050071w-rb14">
(  Cychwyn Rhif 1 )
< /roundbracket>
2005 .
< /sentence>
< sentence type="normal" elementid="20050071w-s27">
< roundbracket elementid="20050071w-rb15">
(  2 )
< /roundbracket>
Yn y Gorchymyn hwn ystyr
< doublequotation elementid="20050071w-dq3">
"  y Ddeddf "
< /doublequotation>
< roundbracket elementid="20050071w-rb16">
(
< doublequotation elementid="20050071w-dq4">
"
< em elementid="20050071w-em3">
the Act
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw Deddf Archwilio Cyhoeddus
< roundbracket elementid="20050071w-rb17">
(  Cymru )
< /roundbracket>
2004 .
< /sentence>
< sentence type="normal" elementid="20050071w-s28">
< roundbracket elementid="20050071w-rb18">
(  3 )
< /roundbracket>
Mae cyfeiriadau at adrannau ac Atodlenni , oni nodir yn wahanol , yn gyfeiriadau at adrannau o 'r Ddeddf ac Atodlenni iddi .
< /sentence>
< sentence type="heading" elementid="20050071w-s29">
< strong elementid="20050071w-st6">
Darpariaethau sy 'n dod i rym ar 31 Ionawr 2005
< /strong>
< /sentence>
< sentence type="normal" elementid="20050071w-s30">
2 .
< /sentence>
< sentence type="normal" elementid="20050071w-s31">
 - 
< roundbracket elementid="20050071w-rb19">
(  1 )
< /roundbracket>
Daw darpariaethau 'r Ddeddf a bennir yng ngholofn gyntaf yr Atodlen i 'r Gorchymyn hwn i rym ar 31 Ionawr 2005 .
< /sentence>
< sentence type="normal" elementid="20050071w-s32">
< roundbracket elementid="20050071w-rb20">
(  2 )
< /roundbracket>
Daw 'r darpariaethau y cyfeirir atynt ym mharagraff
< roundbracket elementid="20050071w-rb21">
(  1 )
< /roundbracket>
uchod i rym at y dibenion a bennir yn ail golofn yr Atodlen i 'r Gorchymyn hwn .
< /sentence>
< sentence type="normal" elementid="20050071w-s33">
Llofnodwyd ar ran Cynulliad Cenedlaethol Cymru o dan adran 66
< roundbracket elementid="20050071w-rb22">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20050071w-sb2">
[ 
< a type="href" elementid="20050071w-a5">
2
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="heading" elementid="20050071w-s34">
< em elementid="20050071w-em4">
D. Elis-Thomas
< /em>
< /sentence>
< sentence type="heading" elementid="20050071w-s35">
Llywydd y Cynulliad Cenedlaethol
< /sentence>
< sentence type="heading" elementid="20050071w-s36">
18 Ionawr 2005
< /sentence>
< sentence type="heading" elementid="20050071w-s37">
< strong elementid="20050071w-st7">
YR ATODLEN
< /strong>
Erthygl 2
< /sentence>
< sentence type="heading" elementid="20050071w-s38">
< strong elementid="20050071w-st8">
Darpariaethau sy 'n dod i rym ar 31 Ionawr 2005
< /strong>
< /sentence>
< /p>
< table elementid="20050071w-tb2">
< td elementid="20050071w-td3">
< p elementid="20050071w-p14">
< sentence type="heading" elementid="20050071w-s39">
Adran 12
< /sentence>
< /p>
< /td>
< td elementid="20050071w-td4">
< p elementid="20050071w-p15">
< sentence type="heading" elementid="20050071w-s40">
Pob diben
< /sentence>
< /p>
< /td>
< td elementid="20050071w-td5">
< p elementid="20050071w-p16">
< sentence type="heading" elementid="20050071w-s41">
Adran 16
< /sentence>
< /p>
< /td>
< td elementid="20050071w-td6">
< p elementid="20050071w-p17">
< sentence type="heading" elementid="20050071w-s42">
Pob diben
< /sentence>
< /p>
< /td>
< td elementid="20050071w-td7">
< p elementid="20050071w-p18">
< sentence type="heading" elementid="20050071w-s43">
Is-adrannau
< roundbracket elementid="20050071w-rb23">
(  1 )
< /roundbracket>
i
< roundbracket elementid="20050071w-rb24">
(  3 )
< /roundbracket>
o adran 20
< /sentence>
< /p>
< /td>
< td elementid="20050071w-td8">
< p elementid="20050071w-p19">
< sentence type="normal" elementid="20050071w-s44">
At ddibenion rhagnodi graddfeydd ffioedd am archwilio 'r cyfrifon a baratowyd mewn perthynas â blynyddoedd ariannol sy 'n dechrau ar neu ar ôl 1 Ebrill 2005 .
< /sentence>
< /p>
< /td>
< td elementid="20050071w-td9">
< p elementid="20050071w-p20">
< sentence type="heading" elementid="20050071w-s45">
Is-adrannau
< roundbracket elementid="20050071w-rb25">
(  1 )
< /roundbracket>
,
< roundbracket elementid="20050071w-rb26">
(  2 )
< /roundbracket>
a
< roundbracket elementid="20050071w-rb27">
(  5 )
< /roundbracket>
o adran 21
< /sentence>
< /p>
< /td>
< td elementid="20050071w-td10">
< p elementid="20050071w-p21">
< sentence type="heading" elementid="20050071w-s46">
Pob diben
< /sentence>
< /p>
< /td>
< td elementid="20050071w-td11">
< p elementid="20050071w-p22">
< sentence type="heading" elementid="20050071w-s47">
Adran 39
< /sentence>
< /p>
< /td>
< td elementid="20050071w-td12">
< p elementid="20050071w-p23">
< sentence type="normal" elementid="20050071w-s48">
At ddibenion ymgynghori a gwneud rheoliadau mewn perthynas â chyfrifon neu ddatganiadau o gyfrifon a baratowyd mewn perthynas â blynyddoedd ariannol sy 'n dechrau ar neu ar ôl 1 Ebrill 2005 .
< /sentence>
< /p>
< /td>
< td elementid="20050071w-td13">
< p elementid="20050071w-p24">
< sentence type="heading" elementid="20050071w-s49">
Adran 50 a pharagraffau 1 a 7 o Atodlen 1
< /sentence>
< /p>
< /td>
< td elementid="20050071w-td14">
< p elementid="20050071w-p25">
< sentence type="normal" elementid="20050071w-s50">
At ddibenion dwyn i effaith :
< roundbracket elementid="20050071w-rb28">
(  a )
< /roundbracket>
paragraff 1 o Atodlen 1 , i 'r graddau y mae 'n angenrheidiol at ddibenion
< roundbracket elementid="20050071w-rb29">
(  b )
< /roundbracket>
isod , a
< roundbracket elementid="20050071w-rb30">
(  b )
< /roundbracket>
paragraff 7 o Atodlen 1 , i 'r graddau y mae 'n darparu i adran 8A newydd gael ei mewnosod ar ôl adran 8 o Ddeddf Llywodraeth Leol 1999
< squarebracket elementid="20050071w-sb3">
[ 
< a type="href" elementid="20050071w-a6">
3
< /a>
]
< /squarebracket> .
< /sentence>
< /p>
< /td>
< td elementid="20050071w-td15">
< p elementid="20050071w-p26">
< sentence type="heading" elementid="20050071w-s51">
Is-adrannau
< roundbracket elementid="20050071w-rb31">
(  6 )
< /roundbracket>
i
< roundbracket elementid="20050071w-rb32">
(  8 )
< /roundbracket>
o adran 54
< /sentence>
< /p>
< /td>
< td elementid="20050071w-td16">
< p elementid="20050071w-p27">
< sentence type="heading" elementid="20050071w-s52">
Pob diben
< /sentence>
< /p>
< /td>
< td elementid="20050071w-td17">
< p elementid="20050071w-p28">
< sentence type="heading" elementid="20050071w-s53">
Adran 58
< /sentence>
< /p>
< /td>
< td elementid="20050071w-td18">
< p elementid="20050071w-p29">
< sentence type="heading" elementid="20050071w-s54">
Pob diben
< /sentence>
< /p>
< /td>
< td elementid="20050071w-td19">
< p elementid="20050071w-p30">
< sentence type="heading" elementid="20050071w-s55">
Adran 59
< /sentence>
< /p>
< /td>
< td elementid="20050071w-td20">
< p elementid="20050071w-p31">
< sentence type="heading" elementid="20050071w-s56">
Pob diben
< /sentence>
< /p>
< /td>
< td elementid="20050071w-td21">
< p elementid="20050071w-p32">
< sentence type="heading" elementid="20050071w-s57">
Adran 68 ac Atodlen 3
< /sentence>
< /p>
< /td>
< td elementid="20050071w-td22">
< p elementid="20050071w-p33">
< sentence type="heading" elementid="20050071w-s58">
Pob diben
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20050071w-p34">
< sentence type="heading" elementid="20050071w-s59">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20050071w-s60">
< em elementid="20050071w-em5">
< roundbracket elementid="20050071w-rb33">
(  Nid yw 'r nodyn hwn yn rhan o 'r Gorchymyn )
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20050071w-s61">
Mae 'r Gorchymyn hwn yn rhoi grym i ddapariaethau amrywiol Deddf Archwilio Cyhoeddus
< roundbracket elementid="20050071w-rb34">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20050071w-rb35">
(
< doublequotation elementid="20050071w-dq5">
"  y Ddeddf "
< /doublequotation>
)
< /roundbracket>
yng Nghymru a Lloegr .
< /sentence>
< sentence type="normal" elementid="20050071w-s62">
Mae cyfeiriadau at adrannau neu Atodlenni
< roundbracket elementid="20050071w-rb36">
(  oni nodir fel arall )
< /roundbracket>
yn gyfeiriadau at adrannau o 'r Ddeddf neu 'r Atodlenni iddi .
< /sentence>
< sentence type="normal" elementid="20050071w-s63">
Daw darpariaethau 'r Ddeddf a restrir yng ngholofn gyntaf yr Atodlen i 'r Gorchymyn hwn i rym ar 31 Ionawr 2005 .
< /sentence>
< sentence type="normal" elementid="20050071w-s64">
Oni phennir fel arall yn ail golofn yr Atodlen , daw 'r darpariaethau hynny i rym ar y dyddiad hwnnw at bob diben .
< /sentence>
< sentence type="normal" elementid="20050071w-s65">
Effaith y darpariaethau fydd fel a ganlyn .
< /sentence>
< sentence type="normal" elementid="20050071w-s66">
Mae adran 12 yn diffinio 'r term
< doublequotation elementid="20050071w-dq6">
"  corff llywodraeth leol yng Nghymru "
< /doublequotation>
at ddibenion y Ddeddf .
< /sentence>
< sentence type="normal" elementid="20050071w-s67">
Mae hefyd yn rhoi i Gynulliad Cenedlaethol Cymru
< roundbracket elementid="20050071w-rb37">
(
< doublequotation elementid="20050071w-dq7">
"  y Cynulliad "
< /doublequotation>
)
< /roundbracket>
bŵer i newid y rhestr o gyrff y 'u diffinnir fel cyrff llywodraeth leol yng Nghymru .
< /sentence>
< sentence type="normal" elementid="20050071w-s68">
Mae adran 16 yn rhoi pŵer i Archwilydd Cyffredinol Cymru i ddyroddi côd arferion archwilio sy 'n rhagnodi 'r ffordd y mae archwilwyr i gyflawni eu swyddogaethau o dan Bennod 1 o Ran 2 o 'r Ddeddf .
< /sentence>
< sentence type="normal" elementid="20050071w-s69">
Mae is-adrannau  1 )-
< roundbracket elementid="20050071w-rb38">
(  3 )
< /roundbracket>
o adran 20 yn rhoi pŵer i Archwilydd Cyffredinol Cymru i ragnodi graddfa neu raddfeydd o ffioedd sy 'n daladwy am archwilio cyfrifon cyrff llywodraeth leol yng Nghymru o dan y Ddeddf .
< /sentence>
< sentence type="normal" elementid="20050071w-s70">
Dygir y darpariaethau hyn i rym mewn perthynas yn unig â ffioedd sy 'n daladwy am archwilio cyfrifon a baratowyd mewn perthynas â blynyddoedd ariannol sy 'n dechrau ar neu ar ôl 1 Ebrill 2005 , sef y dyddiad y bwriedir dwyn gweddill darpariaethau 'r Ddeddf i effaith .
< /sentence>
< sentence type="normal" elementid="20050071w-s71">
Mae is-adrannau
< roundbracket elementid="20050071w-rb39">
(  1 )
< /roundbracket>
,
< roundbracket elementid="20050071w-rb40">
(  2 )
< /roundbracket>
a
< roundbracket elementid="20050071w-rb41">
(  5 )
< /roundbracket>
o adran 21 yn rhoi pŵer i 'r Cynulliad i ddisodli 'r raddfa neu 'r graddfeydd a osodir gan Archwilydd Cyffredinol Cymru gan ei raddfa neu ei raddfedydd ei hun , os yw o 'r farn bod hynny 'n angenrheidiol neu 'n ddymunol .
< /sentence>
< sentence type="normal" elementid="20050071w-s72">
Mae Adran 39 yn rhoi pŵer i 'r Cynulliad i wneud rheoliadau sy 'n rheoli cadw cyfrifon gan gyrff llywodraeth leol yng Nghymru , eu ffurf , a 'u hardystio etc , ac yn rheoli arfer hawliau gan aelodau o 'r cyhoedd o dan adrannau 29  -  31 o 'r Ddeddf i gael mynediad i ddogfennau , ac i godi cwestiynnau a gwrthwynebiadau .
< /sentence>
< sentence type="normal" elementid="20050071w-s73">
Dygir y ddarpariaeth i rym yn unig er mwyn galluogi i reoliadau gael eu gwneud i reoli cyfrifon a datganiadau o gyfrifon a baratoir mewn perthynas â blynyddoedd ariannol sy 'n dechrau ar neu ar ôl 1 Ebrill 2005 .
< /sentence>
< sentence type="normal" elementid="20050071w-s74">
Mae adran 50 yn darparu bod Atodlen 1 yn effeithiol .
< /sentence>
< sentence type="normal" elementid="20050071w-s75">
Dygir hi i rym gan y Gorchymyn hwn ond yn unig at ddibenion dwyn i effaith ran o baragraff 7 o Atodlen 1 , sy 'n darparu ar gyfer adran 8A newydd i 'w mewnosod ar ôl adran 8 o Ddeddf Llywodraeth Leol 1999
< roundbracket elementid="20050071w-rb42">
(
< doublequotation elementid="20050071w-dq8">
"  Deddf 1999 "
< /doublequotation>
)
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20050071w-s76">
Mae 'r adran 8A newydd o Ddeddf 1999 yn rhoi pŵer i Archwilydd Cyffredinol Cymru i ddyroddi côd ymarfer sy 'n rhagnodi 'r ffordd y mae archwilwyr a benodwyd ganddo yn cyflawni eu swyddogaethau o dan adran 7 o Ddeddf 1999 .
< /sentence>
< sentence type="normal" elementid="20050071w-s77">
Mae hefyd yn rhoi pŵer i Archwilydd Cyffredinol Cymru i ragnodi graddfa neu raddfeydd ffioedd mewn perthynas ag archwilio cynlluniau perfformiad , gan archwilwyr a benodwyd ganddo , o dan Ran 1 o Ddeddf 1999 .
< /sentence>
< sentence type="normal" elementid="20050071w-s78">
Yn rhinwedd yr adran 8A
< roundbracket elementid="20050071w-rb43">
(  4 )
< /roundbracket>
newydd o Ddeddf 1999 , mae gan y Cynulliad y pŵer i ragnodi graddfa neu raddfeydd ffioedd amgen .
< /sentence>
< sentence type="normal" elementid="20050071w-s79">
Mae is-adrannau
< roundbracket elementid="20050071w-rb44">
(  6 )
< /roundbracket>
i
< roundbracket elementid="20050071w-rb45">
(  8 )
< /roundbracket>
o adran 54 yn rhoi pŵer i 'r Ysgrifennydd Gwladol i ddiddymu neu ddiwygio darpariaethau blaenorol adran 54 , drwy orchymyn a wnaed gan offeryn statudol .
< /sentence>
< sentence type="normal" elementid="20050071w-s80">
Mae adran 58 yn cynnwys darpariaethau o ran ffurf a chynnwys gorchmynion a rheoliadau y caiff y Cynulliad eu gwneud o dan Ran 2 o 'r Ddeddf , ac o ran cynnwys gorchmynion neu reoliadau y caiff yr Ysgrifennydd Gwladol eu gwneud o dan y Rhan honno .
< /sentence>
< sentence type="normal" elementid="20050071w-s81">
Mae adran 59 yn gosod y dehongliad o 'r termau amrywiol at ddibenion Rhan 2 o 'r Ddeddf .
< /sentence>
< sentence type="normal" elementid="20050071w-s82">
Mae adran 68 yn darparu ar gyfer gwneud cynlluniau trosglwyddo i drosglwyddo eiddo , hawliau a rhwymedigaethau i Archwilydd Cyffredinol Cymru , ac yn gwneud Atodlen 3 yn effeithiol , sy 'n gwneud darpariaeth bellach am y cynlluniau trosglwyddo hynny .
< /sentence>
< sentence type="heading" elementid="20050071w-s83">
< em elementid="20050071w-em6">
Notes :
< /em>
< /sentence>
< sentence type="normal" elementid="20050071w-s84">
< squarebracket elementid="20050071w-sb4">
[  1 ]
< /squarebracket>
2004 p .
< /sentence>
< sentence type="normal" elementid="20050071w-s85">
23 .
< /sentence>
< sentence type="heading" elementid="20050071w-s86">
< a type="href" elementid="20050071w-a7">
back
< /a>
< /sentence>
< /p>
< p elementid="20050071w-p35">
< sentence type="normal" elementid="20050071w-s87">
< squarebracket elementid="20050071w-sb5">
[  2 ]
< /squarebracket>
1998 p.38 .
< /sentence>
< sentence type="heading" elementid="20050071w-s88">
< a type="href" elementid="20050071w-a8">
back
< /a>
< /sentence>
< /p>
< p elementid="20050071w-p36">
< sentence type="normal" elementid="20050071w-s89">
< squarebracket elementid="20050071w-sb6">
[  3 ]
< /squarebracket>
1999 p.27 .
< /sentence>
< sentence type="heading" elementid="20050071w-s90">
< a type="href" elementid="20050071w-a9">
back
< /a>
< /sentence>
< /p>
< p elementid="20050071w-p37">
< sentence type="heading" elementid="20050071w-s91">
< a type="href" elementid="20050071w-a10">
English version
< /a>
< /sentence>
< sentence type="heading" elementid="20050071w-s92">
ISBN 0 11 091057 5
< /sentence>
< /p>
< table elementid="20050071w-tb3">
< td elementid="20050071w-td23">
< p elementid="20050071w-p38">
< sentence type="heading" elementid="20050071w-s93">
< a type="href" elementid="20050071w-a11">
Other UK SIs
< /a>
< a type="href" elementid="20050071w-a12">
Home
< /a>
< a type="href" elementid="20050071w-a13">
National Assembly for Wales Statutory Instruments
< /a>
< a type="href" elementid="20050071w-a14">
Scottish Statutory Instruments
< /a>
< a type="href" elementid="20050071w-a15">
Statutory Rules of Northern Ireland
< /a>
< a type="href" elementid="20050071w-a16">
Her Majesty 's Stationery Office
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20050071w-td24">
< p elementid="20050071w-p39">
< sentence type="heading" elementid="20050071w-s94">
< strong elementid="20050071w-st9">
We welcome your 
< a type="href" elementid="20050071w-a17">
comments
< /a>
on this site
< /strong>
< /sentence>
< /p>
< /td>
< td elementid="20050071w-td25">
< p elementid="20050071w-p40">
< sentence type="heading" elementid="20050071w-s95">
© Crown copyright 2005
< /sentence>
< /p>
< /td>
< td elementid="20050071w-td26">
< p elementid="20050071w-p41">
< sentence type="heading" elementid="20050071w-s96">
Prepared 25 January 2005
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>1442< /wordcount>
< /file>
< file elementid="20050558w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Gorchymyn Deddf Archwilio Cyhoeddus ( Cymru ) 2004 ( Cychwyn Rhif 2 a Darpariaethau Trosiannol ac Arbedion ) 2005< /pagetitle>
< doc_class>Statutory Instrument - Wales< /doc_class>
< year>2005< /year>
< number>0558< /number>
< legislation_code>30102< /legislation_code>
< legislation_type>Statutory Instrument - Wales< /legislation_type>
< subject>ARCHWILIO CYHOEDDUS, CYMRU A LLOEGR< /subject>
< laid_date>_EMPTY< /laid_date>
< made_date>20050308< /made_date>
< coming_into_force>_EMPTY< /coming_into_force>
< isbn>0110910877< /isbn>
< title>Gorchymyn Deddf Archwilio Cyhoeddus (Cymru) 2004 (Cychwyn Rhif 2 a Darpariaethau Trosiannol ac Arbedion) 2005< /title>
< signatory>D. Elis-Thomas< /signatory>
< signatory_title>Llywydd y Cynulliad Cenedlaethol< /signatory_title>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20050558w-p1">
< sentence type="heading" elementid="20050558w-s1">
Offerynnau Statudol 2005 Rhif 558
< roundbracket elementid="20050558w-rb1">
(  Cy.48 )
< /roundbracket>
< roundbracket elementid="20050558w-rb2">
(  C.24 )
< /roundbracket>
< /sentence>
< /p>
< p elementid="20050558w-p2">
< sentence type="heading" elementid="20050558w-s2">
< strong elementid="20050558w-st1">
Gorchymyn Deddf Archwilio Cyhoeddus
< roundbracket elementid="20050558w-rb3">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20050558w-rb4">
(  Cychwyn Rhif 2 a Darpariaethau Trosiannol ac Arbedion )
< /roundbracket>
2005
< /strong>
< /sentence>
< /p>
< p elementid="20050558w-p3">
< sentence type="heading" elementid="20050558w-s3">
© Hawlfraint y Goron 2005
< /sentence>
< /p>
< p elementid="20050558w-p4">
< sentence type="normal" elementid="20050558w-s4">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20050558w-p5">
< sentence type="normal" elementid="20050558w-s5">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20050558w-s6">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20050558w-p6">
< sentence type="normal" elementid="20050558w-s7">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20050558w-s8">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20050558w-s9">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20050558w-p7">
< sentence type="normal" elementid="20050558w-s10">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20050558w-s11">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan The Stationery Office Limited fel
< strong elementid="20050558w-st2">
Gorchymyn Deddf Archwilio Cyhoeddus
< roundbracket elementid="20050558w-rb5">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20050558w-rb6">
(  Cychwyn Rhif 2 a Darpariaethau Trosiannol ac Arbedion )
< /roundbracket>
2005
< /strong>
, ISBN 0110910877 .
< /sentence>
< sentence type="normal" elementid="20050558w-s12">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20050558w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20050558w-s13">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20050558w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20050558w-p8">
< sentence type="normal" elementid="20050558w-s14">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20050558w-a3">
Frequently Asked Questions
< /a> .
< /sentence>
< /p>
< p elementid="20050558w-p9">
< sentence type="normal" elementid="20050558w-s15">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20050558w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20050558w-s16">
Pan welwch fotwm
< doublequotation elementid="20050558w-dq2">
"  continue "
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20050558w-p10">
< sentence type="heading" elementid="20050558w-s17">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="heading" elementid="20050558w-s18">
< strong elementid="20050558w-st3">
2005 Rhif 558
< roundbracket elementid="20050558w-rb7">
(  Cy.48 )
< /roundbracket>
< roundbracket elementid="20050558w-rb8">
(  C.24 )
< /roundbracket>
< /strong>
< /sentence>
< sentence type="heading" elementid="20050558w-s19">
< strong elementid="20050558w-st4">
ARCHWILIO CYHOEDDUS , CYMRU A LLOEGR
< /strong>
< /sentence>
< sentence type="heading" elementid="20050558w-s20">
Gorchymyn Deddf Archwilio Cyhoeddus
< roundbracket elementid="20050558w-rb9">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20050558w-rb10">
(  Cychwyn Rhif 2 a Darpariaethau Trosiannol ac Arbedion )
< /roundbracket>
2005
< /sentence>
< /p>
< table elementid="20050558w-tb1">
< td elementid="20050558w-td1">
< p elementid="20050558w-p11">
< sentence type="heading" elementid="20050558w-s21">
< em elementid="20050558w-em1">
Wedi 'i wneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td2">
< p elementid="20050558w-p12">
< sentence type="heading" elementid="20050558w-s22">
< em elementid="20050558w-em2">
8 Mawrth 2005
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20050558w-p13">
< sentence type="heading" elementid="20050558w-s23">
Mae Cynulliad Cenedlaethol Cymru , drwy arfer y pwerau a roddwyd iddo gan adran 73 o Ddeddf Archwilio Cyhoeddus
< roundbracket elementid="20050558w-rb11">
(  Cymru )
< /roundbracket>
2004
< squarebracket elementid="20050558w-sb1">
[ 
< a type="href" elementid="20050558w-a4">
1
< /a>
]
< /squarebracket>
, drwy hyn yn gwneud y Gorchymyn a ganlyn :
< /sentence>
< sentence type="heading" elementid="20050558w-s24">
< strong elementid="20050558w-st5">
Enwi a dehongli
< /strong>
< /sentence>
< sentence type="normal" elementid="20050558w-s25">
1 .
< /sentence>
< sentence type="normal" elementid="20050558w-s26">
 - 
< roundbracket elementid="20050558w-rb12">
(  1 )
< /roundbracket>
Enw 'r Gorchymyn hwn yw Gorchymyn Deddf Archwilio Cyhoeddus
< roundbracket elementid="20050558w-rb13">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20050558w-rb14">
(  Cychwyn Rhif 2 a Darpariaethau Trosiannol ac Arbedion )
< /roundbracket>
2005 .
< /sentence>
< sentence type="normal" elementid="20050558w-s27">
< roundbracket elementid="20050558w-rb15">
(  2 )
< /roundbracket>
Yn y Gorchymyn hwn ystyr
< doublequotation elementid="20050558w-dq3">
"  y Ddeddf "
< /doublequotation>
< roundbracket elementid="20050558w-rb16">
(
< doublequotation elementid="20050558w-dq4">
"
< em elementid="20050558w-em3">
the Act
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw Deddf Archwilio Cyhoeddus
< roundbracket elementid="20050558w-rb17">
(  Cymru )
< /roundbracket>
2004 .
< /sentence>
< sentence type="normal" elementid="20050558w-s28">
< roundbracket elementid="20050558w-rb18">
(  3 )
< /roundbracket>
Mae cyfeiriadau at adrannau ac Atodlenni , oni nodir yn wahanol , yn gyfeiriadau at adrannau o 'r Ddeddf ac Atodlenni iddi .
< /sentence>
< sentence type="heading" elementid="20050558w-s29">
< strong elementid="20050558w-st6">
Darpariaethau sy 'n dod i rym
< /strong>
< /sentence>
< sentence type="normal" elementid="20050558w-s30">
2 .
< /sentence>
< sentence type="normal" elementid="20050558w-s31">
 - 
< roundbracket elementid="20050558w-rb19">
(  1 )
< /roundbracket>
Daw darpariaethau 'r Ddeddf a bennir yng ngholofn gyntaf Atodlen 1 i 'r Gorchymyn hwn i rym ar 1 Ebrill 2005 .
< /sentence>
< sentence type="normal" elementid="20050558w-s32">
< roundbracket elementid="20050558w-rb20">
(  2 )
< /roundbracket>
Daw 'r darpariaethau y cyfeirir atynt ym mharagraff
< roundbracket elementid="20050558w-rb21">
(  1 )
< /roundbracket>
uchod i rym at y dibenion a bennir yn ail golofn yr Atodlen i 'r Gorchymyn hwn .
< /sentence>
< sentence type="heading" elementid="20050558w-s33">
< strong elementid="20050558w-st7">
Darpariaethau trosiannol ac arbedion
< /strong>
< /sentence>
< sentence type="normal" elementid="20050558w-s34">
3 .
< /sentence>
< sentence type="normal" elementid="20050558w-s35">
Mae Atodlen 2 i 'r Gorchymyn hwn yn effeithiol at ddibenion gwneud darpariaethau trosiannol ac arbedion o ran y darpariaethau y maent yn cyfeirio atynt .
< /sentence>
< sentence type="normal" elementid="20050558w-s36">
Llofnodwyd ar ran Cynulliad Cenedlaethol Cymru o dan adran 66
< roundbracket elementid="20050558w-rb22">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20050558w-sb2">
[ 
< a type="href" elementid="20050558w-a5">
2
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="heading" elementid="20050558w-s37">
< em elementid="20050558w-em4">
D. Elis-Thomas
< /em>
< /sentence>
< sentence type="heading" elementid="20050558w-s38">
Llywydd y Cynulliad Cenedlaethol
< /sentence>
< sentence type="heading" elementid="20050558w-s39">
8 Mawrth 2005
< /sentence>
< sentence type="heading" elementid="20050558w-s40">
< strong elementid="20050558w-st8">
ATODLEN 1
< /strong>
Erthygl 2
< /sentence>
< /p>
< table elementid="20050558w-tb2">
< td elementid="20050558w-td3">
< p elementid="20050558w-p14">
< sentence type="heading" elementid="20050558w-s41">
Adrannau 1 i 6
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td4">
< p elementid="20050558w-p15">
< sentence type="normal" elementid="20050558w-s42">
At bob diben .
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td5">
< p elementid="20050558w-p16">
< sentence type="heading" elementid="20050558w-s43">
Adran 7
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td6">
< p elementid="20050558w-p17">
< sentence type="normal" elementid="20050558w-s44">
Yn ddarostyngedig i ddarpariaethau paragraff 1 o Atodlen 2 i 'r Gorchymyn hwn , at bob diben .
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td7">
< p elementid="20050558w-p18">
< sentence type="heading" elementid="20050558w-s45">
Adrannau 8 i 11
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td8">
< p elementid="20050558w-p19">
< sentence type="normal" elementid="20050558w-s46">
At bob diben .
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td9">
< p elementid="20050558w-p20">
< sentence type="heading" elementid="20050558w-s47">
Adrannau 13 i 15
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td10">
< p elementid="20050558w-p21">
< sentence type="normal" elementid="20050558w-s48">
At bob diben .
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td11">
< p elementid="20050558w-p22">
< sentence type="heading" elementid="20050558w-s49">
Adrannau 17 i 19
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td12">
< p elementid="20050558w-p23">
< sentence type="normal" elementid="20050558w-s50">
At bob diben .
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td13">
< p elementid="20050558w-p24">
< sentence type="heading" elementid="20050558w-s51">
Is-adrannau
< roundbracket elementid="20050558w-rb23">
(  4 )
< /roundbracket>
i
< roundbracket elementid="20050558w-rb24">
(  6 )
< /roundbracket>
o adran 20
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td14">
< p elementid="20050558w-p25">
< sentence type="normal" elementid="20050558w-s52">
At bob diben .
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td15">
< p elementid="20050558w-p26">
< sentence type="heading" elementid="20050558w-s53">
Is-adrannau
< roundbracket elementid="20050558w-rb25">
(  3 )
< /roundbracket>
a
< roundbracket elementid="20050558w-rb26">
(  4 )
< /roundbracket>
o adran 21
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td16">
< p elementid="20050558w-p27">
< sentence type="normal" elementid="20050558w-s54">
At bob diben .
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td17">
< p elementid="20050558w-p28">
< sentence type="heading" elementid="20050558w-s55">
Adrannau 22 i 38
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td18">
< p elementid="20050558w-p29">
< sentence type="normal" elementid="20050558w-s56">
At bob diben .
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td19">
< p elementid="20050558w-p30">
< sentence type="heading" elementid="20050558w-s57">
Adran 39
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td20">
< p elementid="20050558w-p31">
< sentence type="normal" elementid="20050558w-s58">
At bob diben nad yw eisoes wedi 'i chychwyn , ond dim ond o ran y cyfrifon neu 'r datganiadau o gyfrifon a baratowyd o ran blwyddyn ariannol sy 'n dechrau ar 1 Ebrill 2005 neu ar ôl hynny .
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td21">
< p elementid="20050558w-p32">
< sentence type="heading" elementid="20050558w-s59">
Adrannau 40 i 49
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td22">
< p elementid="20050558w-p33">
< sentence type="normal" elementid="20050558w-s60">
At bob diben .
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td23">
< p elementid="20050558w-p34">
< sentence type="heading" elementid="20050558w-s61">
Adran 50 ac Atodlen 1
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td24">
< p elementid="20050558w-p35">
< sentence type="normal" elementid="20050558w-s62">
At bob diben nad ydynt eisoes wedi 'u cychwyn .
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td25">
< p elementid="20050558w-p36">
< sentence type="heading" elementid="20050558w-s63">
Adrannau 51 i 53
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td26">
< p elementid="20050558w-p37">
< sentence type="normal" elementid="20050558w-s64">
At bob diben .
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td27">
< p elementid="20050558w-p38">
< sentence type="heading" elementid="20050558w-s65">
Adrannau 55 i 57
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td28">
< p elementid="20050558w-p39">
< sentence type="normal" elementid="20050558w-s66">
At bob diben .
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td29">
< p elementid="20050558w-p40">
< sentence type="heading" elementid="20050558w-s67">
Rhan 3
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td30">
< p elementid="20050558w-p41">
< sentence type="normal" elementid="20050558w-s68">
At bob diben .
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td31">
< p elementid="20050558w-p42">
< sentence type="heading" elementid="20050558w-s69">
Adran 65
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td32">
< p elementid="20050558w-p43">
< sentence type="normal" elementid="20050558w-s70">
At bob diben .
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td33">
< p elementid="20050558w-p44">
< sentence type="heading" elementid="20050558w-s71">
Adran 66 ac Atodlen 2
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td34">
< p elementid="20050558w-p45">
< sentence type="normal" elementid="20050558w-s72">
Yn ddarostyngedig i ddarpariaethau Atodlen 2 i 'r Gorchymyn hwn , at bob diben .
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td35">
< p elementid="20050558w-p46">
< sentence type="heading" elementid="20050558w-s73">
Adran 67
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td36">
< p elementid="20050558w-p47">
< sentence type="normal" elementid="20050558w-s74">
At bob diben .
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td37">
< p elementid="20050558w-p48">
< sentence type="heading" elementid="20050558w-s75">
Adrannau 69 i 70
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td38">
< p elementid="20050558w-p49">
< sentence type="normal" elementid="20050558w-s76">
At bob diben .
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td39">
< p elementid="20050558w-p50">
< sentence type="heading" elementid="20050558w-s77">
Adran 72 ac Atodlen 4
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td40">
< p elementid="20050558w-p51">
< sentence type="normal" elementid="20050558w-s78">
Yn ddarostyngedig i ddarpariaethau Atodlen 2 i 'r Gorchymyn hwn , at bob diben .
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20050558w-p52">
< sentence type="heading" elementid="20050558w-s79">
ATODLEN 2 Erthygl 3
< /sentence>
< sentence type="heading" elementid="20050558w-s80">
< strong elementid="20050558w-st9">
Darpariaethau Trosiannol ac Arbedion
< /strong>
< /sentence>
< sentence type="normal" elementid="20050558w-s81">
Pŵer yr Archwilydd Cyffredinol i godi ffi am archwilio cyfrifon .
< /sentence>
< sentence type="normal" elementid="20050558w-s82">
1 .
< /sentence>
< sentence type="normal" elementid="20050558w-s83">
Er i adran 7 ac Atodlen 4 ddod i rym , mae adran 93
< roundbracket elementid="20050558w-rb27">
(  3 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20050558w-sb3">
[ 
< a type="href" elementid="20050558w-a6">
3
< /a>
]
< /squarebracket>
yn parhau i fod yn effeithiol , fel pe na bai 'r Ddeddf wedi 'i diddymu , i 'r graddau y mae 'n ymwneud â ffioedd ar gyfer archwilio cyfrifon y Cynulliad a baratowyd o ran unrhyw flwyddyn ariannol sy 'n dechrau cyn 1 Ebrill 2005 .
< /sentence>
< sentence type="heading" elementid="20050558w-s84">
< strong elementid="20050558w-st10">
Rheoliadau cyfrifon ac archwilio
< /strong>
< /sentence>
< sentence type="normal" elementid="20050558w-s85">
2 .
< /sentence>
< sentence type="heading" elementid="20050558w-s86">
 - 
< roundbracket elementid="20050558w-rb28">
(  1 )
< /roundbracket>
Mae is-baragraff
< roundbracket elementid="20050558w-rb29">
(  2 )
< /roundbracket>
yn gymwys er - 
< /sentence>
< sentence type="heading" elementid="20050558w-s87">
< roundbracket elementid="20050558w-rb30">
(  a )
< /roundbracket>
i adran 67 a pharagraffau 35 , 36 a 38
< roundbracket elementid="20050558w-rb31">
(  3 )
< /roundbracket>
o Atodlen 2 , a
< /sentence>
< sentence type="normal" elementid="20050558w-s88">
< roundbracket elementid="20050558w-rb32">
(  b )
< /roundbracket>
i 'r geiriau
< doublequotation elementid="20050558w-dq5">
"  or the National Assmebly for Wales "
< /doublequotation>
yn adran 52
< roundbracket elementid="20050558w-rb33">
(  1 )
< /roundbracket>
o Ddeddf y Comisiwn Archwilio 1998 gael eu diddymu
< squarebracket elementid="20050558w-sb4">
[ 
< a type="href" elementid="20050558w-a7">
4
< /a>
]
< /squarebracket> .
< /sentence>
< /p>
< p elementid="20050558w-p53">
< sentence type="normal" elementid="20050558w-s89">
< roundbracket elementid="20050558w-rb34">
(  2 )
< /roundbracket>
Mae pŵer y Cynulliad i wneud rheoliadau o dan adrannau 27 a 52
< roundbracket elementid="20050558w-rb35">
(  1 )
< /roundbracket>
o Ddeddf y Comisiwn Archwilio 1998 i aros mewn grym , o ran cyfrifon neu ddatganiadau o gyfrifon a baratowyd o ran unrhyw flwyddyn ariannol sy 'n dechrau cyn 1 Ebrill 2005 .
< /sentence>
< sentence type="heading" elementid="20050558w-s90">
< strong elementid="20050558w-st11">
Deddf y Comisiwn Archwilio 1998 : darpariaethau ynghylch adennill symiau nad oes cyfrif amdanynt
< /strong>
< /sentence>
< sentence type="normal" elementid="20050558w-s91">
3 .
< /sentence>
< sentence type="heading" elementid="20050558w-s92">
Er i is-adrannau
< roundbracket elementid="20050558w-rb36">
(  1 )
< /roundbracket>
a
< roundbracket elementid="20050558w-rb37">
(  2 )
< /roundbracket>
o adran 69 a pharagraff 38
< roundbracket elementid="20050558w-rb38">
(  3 )
< /roundbracket>
o Atodlen 2 ddod i rym - 
< /sentence>
< /p>
< p elementid="20050558w-p54">
< sentence type="heading" elementid="20050558w-s93">
< roundbracket elementid="20050558w-rb39">
(  a )
< /roundbracket>
mae adran 2 o Ddeddf y Comisiwn Archwilio 1998 i barhau i fod yn gymwys i gyfrifon corff llywodraeth leol yng Nghymru
< roundbracket elementid="20050558w-rb40">
(  heblaw awdurdod heddlu ar gyfer ardal heddlu yng Nghymru )
< /roundbracket>
at ddibenion  - 
< /sentence>
< sentence type="heading" elementid="20050558w-s94">
< roundbracket elementid="20050558w-rb41">
(  i )
< /roundbracket>
adran 16
< roundbracket elementid="20050558w-rb42">
(  1 )
< /roundbracket>
< roundbracket elementid="20050558w-rb43">
(  a )
< /roundbracket>
o 'r Ddeddf honno , i 'r graddau bod yn ddarpariaeth honno 'n cael ei harbed gan is-baragraff
< roundbracket elementid="20050558w-rb44">
(  c )
< /roundbracket>
; a
< /sentence>
< sentence type="heading" elementid="20050558w-s95">
< roundbracket elementid="20050558w-rb45">
(  ii )
< /roundbracket>
adran 18 o 'r Ddeddf honno , i 'r graddau mae 'r adran honno yn cael ei harbed gan is-baragraff
< roundbracket elementid="20050558w-rb46">
(  b )
< /roundbracket>
;
< /sentence>
< /p>
< p elementid="20050558w-p55">
< sentence type="heading" elementid="20050558w-s96">
< roundbracket elementid="20050558w-rb47">
(  b )
< /roundbracket>
mae adran 18 o Ddeddf y Comisiwn Archwilio 1998 i aros mewn grym - 
< /sentence>
< /p>
< p elementid="20050558w-p56">
< sentence type="heading" elementid="20050558w-s97">
< roundbracket elementid="20050558w-rb48">
(  i )
< /roundbracket>
i 'r graddau y mae 'n ymwneud â chyfrifon corff llywodraeth leol yng Nghymru
< roundbracket elementid="20050558w-rb49">
(  heblaw awdurdod heddlu ar gyfer ardal heddlu yng Nghymru )
< /roundbracket>
a baratowyd o ran blwyddyn ariannol sy 'n dechrau cyn 1 Ebrill 2005 ; a
< /sentence>
< sentence type="heading" elementid="20050558w-s98">
< roundbracket elementid="20050558w-rb50">
(  ii )
< /roundbracket>
er mwyn i swyddogaeth archwilydd o dan is-adran
< roundbracket elementid="20050558w-rb51">
(  1 )
< /roundbracket>
o 'r adran honno fod yn arferadwy yn unig o ran mater y mae etholwr llywodraeth leol wedi cyflwyno gwrthwynebiad yn ei gylch o dan adran 16
< roundbracket elementid="20050558w-rb52">
(  1 )
< /roundbracket>
< roundbracket elementid="20050558w-rb53">
(  a )
< /roundbracket>
o 'r Ddeddf honno ;
< /sentence>
< /p>
< p elementid="20050558w-p57">
< sentence type="heading" elementid="20050558w-s99">
< roundbracket elementid="20050558w-rb54">
(  c )
< /roundbracket>
mae hawliau etholwr llywodraeth leol o dan adran 16
< roundbracket elementid="20050558w-rb55">
(  1 )
< /roundbracket>
< roundbracket elementid="20050558w-rb56">
(  a )
< /roundbracket>
o Ddeddf y Comisiwn Archwilio 1998 i ddod gerbron archwilydd ac i gyflwyno gwrthwynebiadau i aros mewn grym i 'r graddau  - 
< /sentence>
< /p>
< p elementid="20050558w-p58">
< sentence type="heading" elementid="20050558w-s100">
< roundbracket elementid="20050558w-rb57">
(  i )
< /roundbracket>
y mae 'r gwrthwynebiad yn ymwneud ag unrhyw fater y gallai archwilydd gymryd camau yn ei gylch o dan adran 18
< roundbracket elementid="20050558w-rb58">
(  1 )
< /roundbracket>
o 'r Ddeddf honno fel y mae wedi 'i harbed gan is-baragraff
< roundbracket elementid="20050558w-rb59">
(  b )
< /roundbracket>
;
< /sentence>
< sentence type="heading" elementid="20050558w-s101">
< roundbracket elementid="20050558w-rb60">
(  ii )
< /roundbracket>
mai cyfrifon corff llywodraeth leol yng Nghymru
< roundbracket elementid="20050558w-rb61">
(  heblaw awdurdod heddlu ar gyfer ardal heddlu yng Nghymru )
< /roundbracket>
yw 'r cyfrifon dan sylw ; a
< /sentence>
< sentence type="normal" elementid="20050558w-s102">
< roundbracket elementid="20050558w-rb62">
(  iii )
< /roundbracket>
yr oedd y cyfrifon dan sylw wedi 'u paratoi o ran blwyddyn ariannol a oedd yn dechrau cyn 1 Ebrill 2005 .
< /sentence>
< /p>
< p elementid="20050558w-p59">
< sentence type="heading" elementid="20050558w-s103">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20050558w-s104">
< em elementid="20050558w-em5">
< roundbracket elementid="20050558w-rb63">
(  Nid yw 'r nodyn hwn yn rhan o 'r Gorchymyn )
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20050558w-s105">
Yn bennaf , mae 'r Gorchymyn hwn yn cwblhau 'r broses o ddod â darpariaethau Deddf Archwilio Cyhoeddus
< roundbracket elementid="20050558w-rb64">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20050558w-rb65">
(
< doublequotation elementid="20050558w-dq6">
"  y Ddeddf "
< /doublequotation>
)
< /roundbracket>
i rym yng Nghymru a Lloegr .
< /sentence>
< sentence type="normal" elementid="20050558w-s106">
Mae 'r cyfeiriadau isod at adrannau neu Atodlenni , oni nodir fel arall , yn gyfeiriadau at adrannau o 'r Ddeddf neu Atodlenni iddi .
< /sentence>
< sentence type="normal" elementid="20050558w-s107">
Mae erthygl 2 o 'r Gorchymyn hwn yn dod â darpariaethau 'r Ddeddf a restrir yng ngholofn gyntaf Atodlen 1 i 'r Gorchymyn hwn i rym ar 1 Ebrill 2005 .
< /sentence>
< sentence type="normal" elementid="20050558w-s108">
Oni phennir fel arall yn ail golofn Atodlen 1 i 'r Gorchymyn hwn , neu yn Atodlen 2 iddo , daw 'r darpariaethau hynny i rym ar y dyddiad hwnnw at bob diben .
< /sentence>
< sentence type="normal" elementid="20050558w-s109">
Effaith gyffredinol darpariaethau 'r Ddeddf y mae 'r Gorchymyn hwn yn dod â hwy i rym yw rhoi nifer o swyddogaethau newydd i Archwilydd Cyffredinol Cymru .
< /sentence>
< sentence type="normal" elementid="20050558w-s110">
Effaith fwyaf arwyddocaol y swyddogaethau newydd yw y bydd yr Archwilydd Cyffredinol , o 'r dyddiad y daw 'r Gorchymyn hwn i rym , yn arfer y rhan fwyaf o 'r swyddogaethau a arferir ar hyn o bryd gan y Comisiwn Archwilio ar gyfer Awdurdodau Lleol a 'r Gwasanaeth Iechyd Gwladol yng Nghymru a Lloegr
< roundbracket elementid="20050558w-rb66">
(
< doublequotation elementid="20050558w-dq7">
"  y Comisiwn Archwilio "
< /doublequotation>
)
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20050558w-s111">
Mae Rhan 1 o 'r Ddeddf
< roundbracket elementid="20050558w-rb67">
(  adrannau 1 i 11 )
< /roundbracket>
yn ymdrin â darpariaethau sy 'n ymwneud â swyddogaethau Archwilydd Cyffredinol Cymru .
< /sentence>
< sentence type="normal" elementid="20050558w-s112">
Mae Rhan 1 yn ymdrin â sut y cyllidir y swyddfa honno ac mae hefyd yn ymdrin â materion staffio a materion gweinyddol ac mae 'n darparu ar gyfer swyddogaethau ychwanegol yr Archwilydd Cyffredinol .
< /sentence>
< sentence type="normal" elementid="20050558w-s113">
Mae Rhan 2 o 'r Ddeddf
< roundbracket elementid="20050558w-rb68">
(  adrannau 12 i 59 )
< /roundbracket>
yn ymdrin â chyrff llywodraeth leol yng Nghymru
< roundbracket elementid="20050558w-rb69">
(  fel y 'u diffinnir yn adran 12 )
< /roundbracket>
yng nghyd-destun y trefniadau newydd a gynigir gan y Ddeddf .
< /sentence>
< sentence type="normal" elementid="20050558w-s114">
O dan ddarpariaethau Deddf y Comisiwn Archwilio 1998
< roundbracket elementid="20050558w-rb70">
(
< doublequotation elementid="20050558w-dq8">
"  Deddf 1998 "
< /doublequotation>
)
< /roundbracket>
, mae 'r Comisiwn Archwilio ar hyn o bryd yn gyfrifol am benodi archwilwyr cyfrifon y cyrff hyn .
< /sentence>
< sentence type="normal" elementid="20050558w-s115">
O ran cyrff llywodraeth leol yng Nghymru , mae 'r Ddeddf yn trosglwyddo 'r cyfrifoldeb hwnnw i Archwilydd Cyffredinol Cymru .
< /sentence>
< sentence type="normal" elementid="20050558w-s116">
Mae Rhan 3 o 'r Ddeddf
< roundbracket elementid="20050558w-rb71">
(  adrannau 60 i 64 )
< /roundbracket>
yn ychwanegu cyfrifoldeb am archwilio cyfrifon cyrff GIG Cymru
< roundbracket elementid="20050558w-rb72">
(  fel y 'u diffinnir yn adran 60 )
< /roundbracket>
at gylch gwaith Archwilydd Cyffredinol Cymru .
< /sentence>
< sentence type="normal" elementid="20050558w-s117">
Mae Rhan 4 o 'r Ddeddf
< roundbracket elementid="20050558w-rb73">
(  adrannau 65 i 75 )
< /roundbracket>
yn cynnwys darpariaethau amrywiol , trosiannol a chyffredinol .
< /sentence>
< sentence type="normal" elementid="20050558w-s118">
Mae Atodlen 1 yn ymdrin â diwygiadau i 'r broses o archwilio trefniadau Gwerth Gorau o dan Ddeddf Llywodraeth Leol 1999 .
< /sentence>
< sentence type="normal" elementid="20050558w-s119">
Mae Atodlen 2 yn ymdrin â mân ddiwygiadau a diwygiadau canlyniadol sy 'n angenrheidiol oherwydd y Ddeddf .
< /sentence>
< sentence type="normal" elementid="20050558w-s120">
Mae Atodlen 3 , a gychwynnwyd eisoes , yn nodi 'r trefniadau ar gyfer cynlluniau trosglwyddo statudol manwl sy 'n ofynnol o dan y Ddeddf ac mae Atodlen 4 yn nodi 'r diddymiadau perthnasol .
< /sentence>
< sentence type="normal" elementid="20050558w-s121">
Mae erthygl 3 ac Atodlen 2 i 'r Gorchymyn hwn yn gwneud darpariaethau trosiannol ac arbedion .
< /sentence>
< sentence type="normal" elementid="20050558w-s122">
Mae paragraff 1 o Atodlen 2 i 'r Gorchymyn hwn yn sicrhau na all Archwilydd Cyffredinol Cymru godi ar Gynulliad Cenedlaethol Cymru am archwilio 'i gyfrifon am y flwyddyn ariannol 2004 - 05 .
< /sentence>
< sentence type="normal" elementid="20050558w-s123">
Mae 'r ddarpariaeth hon yn angenrheidiol gan y bydd y gwaith archwilio yn cael ei wneud ar ôl i 'r Ddeddf ddod i rym , ond mae 'n ymwneud â chyfnod pan oedd adran 93
< roundbracket elementid="20050558w-rb74">
(  3 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998 yn gwahardd Archwilydd Cyffredinol Cymru rhag codi am y gwaith hwnnw .
< /sentence>
< sentence type="normal" elementid="20050558w-s124">
Mae paragraff 2 o Atodlen 2 i 'r Gorchymyn hwn yn cadw pŵer y Cynulliad i wneud rheoliadau cyfrifon ac archwilio o dan Ddeddf 1998 o ran cyfrifon am y blynyddoedd ariannol sy 'n dechrau cyn 1 Ebrill 2005 .
< /sentence>
< sentence type="normal" elementid="20050558w-s125">
Am y blynyddoedd ariannol ar ôl hynny , bydd y Cynulliad yn gwneud rheoliadau o 'r fath o dan adran 39 o 'r Ddeddf .
< /sentence>
< sentence type="normal" elementid="20050558w-s126">
Mae paragraff 3 o Atodlen 2 i 'r Gorchymyn hwn yn arbed , o ran cyrff llywodraeth leol yng Nghymru , ddarpariaethau adran 18 o Ddeddf 1998 , sy 'n darparu pwerau i adennill symiau nad oes cyfrif amdanynt , a darpariaethau perthnasol eraill y Ddeddf honno .
< /sentence>
< sentence type="normal" elementid="20050558w-s127">
< roundbracket elementid="20050558w-rb75">
(  Nid yw 'r arbediad yn gymwys i un dosbarth ar gyrff llywodraeth leol yng Nghymru , sef awdurdodau heddlu , gan fod darpariaethau perthnasol Deddf 1998 eisoes wedi cael eu diddymu , o 'u rhan , gan Ddeddf Llywodraeth Leol 2000 )
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20050558w-s128">
Nid yw 'r arbediad ond yn gymwys i gyfrifon y cyrff dan sylw am y blynyddoedd ariannol sy 'n dechrau cyn 1 Ebrill 2005 .
< /sentence>
< sentence type="normal" elementid="20050558w-s129">
Mae hefyd yn gyfyngedig i sefyllfa pan fo etholwr llywodraeth leol ar gyfer ardal corff perthnasol wedi cyflwyno gwrthwynebiad o dan adran 16
< roundbracket elementid="20050558w-rb76">
(  1 )
< /roundbracket>
< roundbracket elementid="20050558w-rb77">
(  a )
< /roundbracket>
o Ddeddf 1998 .
< /sentence>
< sentence type="normal" elementid="20050558w-s130">
Is-adrannau
< roundbracket elementid="20050558w-rb78">
(  1 )
< /roundbracket>
i
< roundbracket elementid="20050558w-rb79">
(  5 )
< /roundbracket>
o adran 54 , sy 'n gosod cyfyngiadau ar yr amgylchiadau pan fydd modd datgelu gwybodaeth benodol a gafwyd gan Archwilydd Cyffredinol Cymru neu archwilydd , yw unig ddarpariaethau 'r Ddeddf na fyddant yn effeithiol pan ddaw 'r Gorchymyn hwn i rym .
< /sentence>
< sentence type="heading" elementid="20050558w-s131">
< strong elementid="20050558w-st12">
NODYN AM ORCHMYNION CYCHWYN BLAENOROL
< /strong>
< /sentence>
< sentence type="heading" elementid="20050558w-s132">
< em elementid="20050558w-em6">
< roundbracket elementid="20050558w-rb80">
(  Nid yw 'r nodyn hwn yn rhan o 'r Gorchymyn )
< /roundbracket>
< /em>
< /sentence>
< sentence type="heading" elementid="20050558w-s133">
Mae darpariaethau canlynol y Ddeddf wedi 'u dwyn i rym drwy orchymyn cychwyn a wnaed cyn dyddiad y Gorchymyn hwn :
< /sentence>
< /p>
< table elementid="20050558w-tb3">
< td elementid="20050558w-td41">
< p elementid="20050558w-p60">
< sentence type="heading" elementid="20050558w-s134">
< em elementid="20050558w-em7">
Y ddarpariaeth
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td42">
< p elementid="20050558w-p61">
< sentence type="heading" elementid="20050558w-s135">
< em elementid="20050558w-em8">
Y Dyddiad Cychwyn
< roundbracket elementid="20050558w-rb81">
(  ym mhob achos )
< /roundbracket>
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td43">
< p elementid="20050558w-p62">
< sentence type="heading" elementid="20050558w-s136">
< em elementid="20050558w-em9">
O.S. Rhif
< roundbracket elementid="20050558w-rb82">
(  ym mhob achos )
< /roundbracket>
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td44">
< p elementid="20050558w-p63">
< sentence type="heading" elementid="20050558w-s137">
Adran 12
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td45">
< p elementid="20050558w-p64">
< sentence type="heading" elementid="20050558w-s138">
31 Ionawr 2005
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td46">
< p elementid="20050558w-p65">
< sentence type="heading" elementid="20050558w-s139">
2005 Rhif 71
< roundbracket elementid="20050558w-rb83">
(  Cy.9 )
< /roundbracket>
< roundbracket elementid="20050558w-rb84">
(  C.3 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td47">
< p elementid="20050558w-p66">
< sentence type="heading" elementid="20050558w-s140">
Adran 16
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td48">
< p elementid="20050558w-p67">
< sentence type="normal" elementid="20050558w-s141">
Is-adrannau
< roundbracket elementid="20050558w-rb85">
(  1 )
< /roundbracket>
i
< roundbracket elementid="20050558w-rb86">
(  3 )
< /roundbracket>
o adran 20 , at ddibenion rhagnodi graddfeydd ffioedd ar gyfer archwilio cyfrifon a baratowyd o ran blynyddoedd ariannol sy 'n dechrau ar 1 Ebrill 2005 neu ar ôl hynny .
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td49">
< p elementid="20050558w-p68">
< sentence type="heading" elementid="20050558w-s142">
Is-adrannau
< roundbracket elementid="20050558w-rb87">
(  1 )
< /roundbracket>
,
< roundbracket elementid="20050558w-rb88">
(  2 )
< /roundbracket>
a
< roundbracket elementid="20050558w-rb89">
(  5 )
< /roundbracket>
o adran 21
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td50">
< p elementid="20050558w-p69">
< sentence type="heading" elementid="20050558w-s143">
Adran 39 , at ddibenion ymgynghori ynghylch rheoliadau a gwneud rheoliadau o ran cyfrifon neu ddatganiadau o gyfrifon a baratowyd o ran blynyddoedd ariannol sy 'n dechrau ar 1 Ebrill 2005 neu ar ôl hynny
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td51">
< p elementid="20050558w-p70">
< sentence type="heading" elementid="20050558w-s144">
Adran 50 a pharagraffau 1 a 7 o Atodlen 1 , at ddibenion gwneud y canlynol yn effeithiol :
< /sentence>
< sentence type="heading" elementid="20050558w-s145">
< roundbracket elementid="20050558w-rb90">
(  a )
< /roundbracket>
paragraff 1 o Atodlen 1 , i 'r graddau y mae 'n angenrheidiol at ddibenion
< roundbracket elementid="20050558w-rb91">
(  b )
< /roundbracket>
isod , a
< /sentence>
< sentence type="normal" elementid="20050558w-s146">
< roundbracket elementid="20050558w-rb92">
(  b )
< /roundbracket>
paragraff 7 o Atodlen 1 , i 'r graddau y mae 'n darparu i adran 8A newydd gael ei mewnosod ar ôl adran 8 o Ddeddf Llywodraeth Leol 1999 .
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td52">
< p elementid="20050558w-p71">
< sentence type="heading" elementid="20050558w-s147">
Is-adrannau
< roundbracket elementid="20050558w-rb93">
(  6 )
< /roundbracket>
i
< roundbracket elementid="20050558w-rb94">
(  8 )
< /roundbracket>
o adran 54
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td53">
< p elementid="20050558w-p72">
< sentence type="heading" elementid="20050558w-s148">
Adran 58
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td54">
< p elementid="20050558w-p73">
< sentence type="heading" elementid="20050558w-s149">
Adran 59
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td55">
< p elementid="20050558w-p74">
< sentence type="heading" elementid="20050558w-s150">
Adran 68 ac Atodlen 3
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20050558w-p75">
< sentence type="heading" elementid="20050558w-s151">
< em elementid="20050558w-em10">
Notes :
< /em>
< /sentence>
< sentence type="normal" elementid="20050558w-s152">
< squarebracket elementid="20050558w-sb5">
[  1 ]
< /squarebracket>
2004 p .
< /sentence>
< sentence type="normal" elementid="20050558w-s153">
23 .
< /sentence>
< sentence type="heading" elementid="20050558w-s154">
< a type="href" elementid="20050558w-a8">
back
< /a>
< /sentence>
< /p>
< p elementid="20050558w-p76">
< sentence type="normal" elementid="20050558w-s155">
< squarebracket elementid="20050558w-sb6">
[  2 ]
< /squarebracket>
1998 p.38 .
< /sentence>
< sentence type="heading" elementid="20050558w-s156">
< a type="href" elementid="20050558w-a9">
back
< /a>
< /sentence>
< /p>
< p elementid="20050558w-p77">
< sentence type="heading" elementid="20050558w-s157">
< squarebracket elementid="20050558w-sb7">
[  3 ]
< /squarebracket>
1998 p.38 
< a type="href" elementid="20050558w-a10">
back
< /a>
< /sentence>
< /p>
< p elementid="20050558w-p78">
< sentence type="heading" elementid="20050558w-s158">
< squarebracket elementid="20050558w-sb8">
[  4 ]
< /squarebracket>
1998 p.18 
< a type="href" elementid="20050558w-a11">
back
< /a>
< /sentence>
< /p>
< p elementid="20050558w-p79">
< sentence type="heading" elementid="20050558w-s159">
< a type="href" elementid="20050558w-a12">
English version
< /a>
< /sentence>
< sentence type="heading" elementid="20050558w-s160">
ISBN 0 11 091087 7
< /sentence>
< /p>
< table elementid="20050558w-tb4">
< td elementid="20050558w-td56">
< p elementid="20050558w-p80">
< sentence type="heading" elementid="20050558w-s161">
< a type="href" elementid="20050558w-a13">
Other UK SIs
< /a>
< a type="href" elementid="20050558w-a14">
Home
< /a>
< a type="href" elementid="20050558w-a15">
National Assembly for Wales Statutory Instruments
< /a>
< a type="href" elementid="20050558w-a16">
Scottish Statutory Instruments
< /a>
< a type="href" elementid="20050558w-a17">
Statutory Rules of Northern Ireland
< /a>
< a type="href" elementid="20050558w-a18">
Her Majesty 's Stationery Office
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td57">
< p elementid="20050558w-p81">
< sentence type="heading" elementid="20050558w-s162">
< strong elementid="20050558w-st13">
We welcome your 
< a type="href" elementid="20050558w-a19">
comments
< /a>
on this site
< /strong>
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td58">
< p elementid="20050558w-p82">
< sentence type="heading" elementid="20050558w-s163">
© Crown copyright 2005
< /sentence>
< /p>
< /td>
< td elementid="20050558w-td59">
< p elementid="20050558w-p83">
< sentence type="heading" elementid="20050558w-s164">
Prepared 17 March 2005
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>2335< /wordcount>
< /file>
< file elementid="20050757w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Gorchymyn Deddf Archwilio Cyhoeddus ( Cymru ) 2004 ( Diwygiadau Canlyniadol ) ( Cymru ) 2005< /pagetitle>
< doc_class>Statutory Instrument - Wales< /doc_class>
< year>2005< /year>
< number>0757< /number>
< legislation_code>30102< /legislation_code>
< legislation_type>Statutory Instrument - Wales< /legislation_type>
< subject>ARCHWILIO CYHOEDDUS, CYMRU< /subject>
< laid_date>_EMPTY< /laid_date>
< made_date>20050315< /made_date>
< coming_into_force>20050401< /coming_into_force>
< isbn>0110910974< /isbn>
< title>Gorchymyn Deddf Archwilio Cyhoeddus (Cymru) 2004 (Diwygiadau Canlyniadol) (Cymru) 2005< /title>
< signatory>D. Elis-Thomas< /signatory>
< signatory_title>Llywydd y Cynulliad Cenedlaethol< /signatory_title>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20050757w-p1">
< sentence type="heading" elementid="20050757w-s1">
Offerynnau Statudol 2005 Rhif 757
< roundbracket elementid="20050757w-rb1">
(  Cy.62 )
< /roundbracket>
< /sentence>
< /p>
< p elementid="20050757w-p2">
< sentence type="heading" elementid="20050757w-s2">
< strong elementid="20050757w-st1">
Gorchymyn Deddf Archwilio Cyhoeddus
< roundbracket elementid="20050757w-rb2">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20050757w-rb3">
(  Diwygiadau Canlyniadol )
< /roundbracket>
< roundbracket elementid="20050757w-rb4">
(  Cymru )
< /roundbracket>
2005
< /strong>
< /sentence>
< /p>
< p elementid="20050757w-p3">
< sentence type="heading" elementid="20050757w-s3">
© Hawlfraint y Goron 2005
< /sentence>
< /p>
< p elementid="20050757w-p4">
< sentence type="normal" elementid="20050757w-s4">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20050757w-p5">
< sentence type="normal" elementid="20050757w-s5">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20050757w-s6">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20050757w-p6">
< sentence type="normal" elementid="20050757w-s7">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20050757w-s8">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20050757w-s9">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20050757w-p7">
< sentence type="normal" elementid="20050757w-s10">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20050757w-s11">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan The Stationery Office Limited fel
< strong elementid="20050757w-st2">
Gorchymyn Deddf Archwilio Cyhoeddus
< roundbracket elementid="20050757w-rb5">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20050757w-rb6">
(  Diwygiadau Canlyniadol )
< /roundbracket>
< roundbracket elementid="20050757w-rb7">
(  Cymru )
< /roundbracket>
2005
< /strong>
, ISBN 0110910974 .
< /sentence>
< sentence type="normal" elementid="20050757w-s12">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20050757w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20050757w-s13">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20050757w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20050757w-p8">
< sentence type="normal" elementid="20050757w-s14">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20050757w-a3">
Frequently Asked Questions
< /a> .
< /sentence>
< /p>
< p elementid="20050757w-p9">
< sentence type="normal" elementid="20050757w-s15">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20050757w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20050757w-s16">
Pan welwch fotwm
< doublequotation elementid="20050757w-dq2">
"  continue "
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20050757w-p10">
< sentence type="heading" elementid="20050757w-s17">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="heading" elementid="20050757w-s18">
< strong elementid="20050757w-st3">
2005 Rhif 757
< roundbracket elementid="20050757w-rb8">
(  Cy.62 )
< /roundbracket>
< /strong>
< /sentence>
< sentence type="heading" elementid="20050757w-s19">
< strong elementid="20050757w-st4">
ARCHWILIO CYHOEDDUS , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20050757w-s20">
Gorchymyn Deddf Archwilio Cyhoeddus
< roundbracket elementid="20050757w-rb9">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20050757w-rb10">
(  Diwygiadau Canlyniadol )
< /roundbracket>
< roundbracket elementid="20050757w-rb11">
(  Cymru )
< /roundbracket>
2005
< /sentence>
< /p>
< table elementid="20050757w-tb1">
< td elementid="20050757w-td1">
< p elementid="20050757w-p11">
< sentence type="heading" elementid="20050757w-s21">
< em elementid="20050757w-em1">
Wedi 'i wneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20050757w-td2">
< p elementid="20050757w-p12">
< sentence type="heading" elementid="20050757w-s22">
< em elementid="20050757w-em2">
15 Mawrth 2005
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20050757w-td3">
< p elementid="20050757w-p13">
< sentence type="heading" elementid="20050757w-s23">
< em elementid="20050757w-em3">
Yn dod i rym
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20050757w-td4">
< p elementid="20050757w-p14">
< sentence type="heading" elementid="20050757w-s24">
< em elementid="20050757w-em4">
1 Ebrill 2005
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20050757w-p15">
< sentence type="heading" elementid="20050757w-s25">
Mae Cynulliad Cenedlaethol Cymru , drwy arfer y pwerau a roddwyd gan y deddfiadau a bennir yn yr Atodlen i 'r offeryn hwn , drwy hyn yn gwneud y Gorchymyn canlynol :
< /sentence>
< sentence type="heading" elementid="20050757w-s26">
< strong elementid="20050757w-st5">
Enwi , cychwyn a chymhwyso
< /strong>
< /sentence>
< sentence type="normal" elementid="20050757w-s27">
1 .
< /sentence>
< sentence type="normal" elementid="20050757w-s28">
 - 
< roundbracket elementid="20050757w-rb12">
(  1 )
< /roundbracket>
Enw 'r gorchymyn hwn yw Gorchymyn Deddf Archwilio Cyhoeddus 2004
< roundbracket elementid="20050757w-rb13">
(  Cymru )
< /roundbracket>
< roundbracket elementid="20050757w-rb14">
(  Diwygiadau Canlyniadol )
< /roundbracket>
< roundbracket elementid="20050757w-rb15">
(  Cymru )
< /roundbracket>
2005 a daw i rym ar 1 Ebrill 2005 .
< /sentence>
< sentence type="normal" elementid="20050757w-s29">
< roundbracket elementid="20050757w-rb16">
(  2 )
< /roundbracket>
Mae 'r Gorchymyn hwn yn gymwys o ran Cymru yn unig .
< /sentence>
< sentence type="heading" elementid="20050757w-s30">
< strong elementid="20050757w-st6">
Diwygio is-ddeddfwriaeth
< /strong>
< /sentence>
< sentence type="normal" elementid="20050757w-s31">
2 .
< /sentence>
< sentence type="heading" elementid="20050757w-s32">
 - 
< roundbracket elementid="20050757w-rb17">
(  1 )
< /roundbracket>
Diwygir Gorchymyn Awdurdodau Lleol
< roundbracket elementid="20050757w-rb18">
(  Cwmnïau )
< /roundbracket>
1995
< squarebracket elementid="20050757w-sb1">
[ 
< a type="href" elementid="20050757w-a4">
1
< /a>
]
< /squarebracket>
fel a ganlyn :
< /sentence>
< sentence type="heading" elementid="20050757w-s33">
< roundbracket elementid="20050757w-rb19">
(  2 )
< /roundbracket>
Yn erthygl 9 , hepgorer y geiriau o
< doublequotation elementid="20050757w-dq3">
"  the Audit Commission 's consent "
< /doublequotation>
hyd at y diwedd , ac yn eu lle rhodder  the consent of - 
< /sentence>
< sentence type="heading" elementid="20050757w-s34">
< roundbracket elementid="20050757w-rb20">
(  a )
< /roundbracket>
where the company is under the control of one or more local authorities , all of which are local government bodies in Wales within the meaning of section 12 of the Public Audit
< roundbracket elementid="20050757w-rb21">
(  Wales )
< /roundbracket>
Act 2004 , the Auditor General for Wales ;
< /sentence>
< sentence type="heading" elementid="20050757w-s35">
< roundbracket elementid="20050757w-rb22">
(  b )
< /roundbracket>
in any other case , the Audit Commission,
< /sentence>
< /p>
< p elementid="20050757w-p16">
< sentence type="normal" elementid="20050757w-s36">
to the appointment of that person .
< /sentence>
< /p>
< p elementid="20050757w-p17">
< sentence type="normal" elementid="20050757w-s37">
3 .
< /sentence>
< sentence type="heading" elementid="20050757w-s38">
 - 
< roundbracket elementid="20050757w-rb23">
(  1 )
< /roundbracket>
Diwygir Gorchymyn Eithrio Contractau Adeiladu
< roundbracket elementid="20050757w-rb24">
(  Cymru a Lloegr )
< /roundbracket>
1998
< squarebracket elementid="20050757w-sb2">
[ 
< a type="href" elementid="20050757w-a5">
2
< /a>
]
< /squarebracket>
fel a ganlyn :
< /sentence>
< sentence type="normal" elementid="20050757w-s39">
< roundbracket elementid="20050757w-rb25">
(  2 )
< /roundbracket>
Yn erthygl 4
< roundbracket elementid="20050757w-rb26">
(  2 )
< /roundbracket>
< roundbracket elementid="20050757w-rb27">
(  c )
< /roundbracket>
< roundbracket elementid="20050757w-rb28">
(  v )
< /roundbracket>
, ar y diwedd , mewnosoder y geiriau
< doublequotation elementid="20050757w-dq4">
"  or the Auditor General for Wales "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20050757w-s40">
Llofnodwyd ar ran y Cynulliad Cenedlaethol Cymru o dan adran 66
< roundbracket elementid="20050757w-rb29">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20050757w-sb3">
[ 
< a type="href" elementid="20050757w-a6">
3
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="heading" elementid="20050757w-s41">
< em elementid="20050757w-em5">
D. Elis-Thomas
< /em>
< /sentence>
< sentence type="heading" elementid="20050757w-s42">
Llywydd y Cynulliad Cenedlaethol
< /sentence>
< sentence type="heading" elementid="20050757w-s43">
15 Mawrth 2005
< /sentence>
< /p>
< p elementid="20050757w-p18">
< sentence type="heading" elementid="20050757w-s44">
< strong elementid="20050757w-st7">
YR ATODLEN
< /strong>
Rhaglith
< /sentence>
< sentence type="heading" elementid="20050757w-s45">
< strong elementid="20050757w-st8">
Deddfiadau sy 'n rhoi pwerau a freiniwyd bellach yng Nghynulliad Cenedlaethol Cymru gan Orchymyn Trosglwyddo Swyddogaethau
< roundbracket elementid="20050757w-rb30">
(  Cynulliad Cenedlaethol Cymru )
< /roundbracket>
1999
< squarebracket elementid="20050757w-sb4">
[ 
< a type="href" elementid="20050757w-a7">
4
< /a>
]
< /squarebracket>
< /strong>
< /sentence>
< /p>
< table elementid="20050757w-tb2">
< td elementid="20050757w-td5">
< p elementid="20050757w-p19">
< sentence type="heading" elementid="20050757w-s46">
Deddf Llywodraeth Leol a Thai 1989
< squarebracket elementid="20050757w-sb5">
[ 
< a type="href" elementid="20050757w-a8">
5
< /a>
]
< /squarebracket>
< /sentence>
< /p>
< /td>
< td elementid="20050757w-td6">
< p elementid="20050757w-p20">
< sentence type="normal" elementid="20050757w-s47">
Adran 70
< roundbracket elementid="20050757w-rb31">
(  1 )
< /roundbracket>
a
< roundbracket elementid="20050757w-rb32">
(  5 )
< /roundbracket>
< roundbracket elementid="20050757w-rb33">
(  fel y 'i diwygiwyd , yn achos is - adran
< roundbracket elementid="20050757w-rb34">
(  5 )
< /roundbracket>
, gan Ddeddf Archwilio Cyhoeddus 2004
< squarebracket elementid="20050757w-sb6">
[ 
< a type="href" elementid="20050757w-a9">
6
< /a>
]
< /squarebracket>
, adran 66 ac Atodlen 2 , paragraffau 10 a 12
< roundbracket elementid="20050757w-rb35">
(  1 )
< /roundbracket>
a
< roundbracket elementid="20050757w-rb36">
(  4 )
< /roundbracket>
)
< /roundbracket> .
< /sentence>
< /p>
< /td>
< td elementid="20050757w-td7">
< p elementid="20050757w-p21">
< sentence type="heading" elementid="20050757w-s48">
Deddf Grantiau Tai , Adeiladu ac Adfywio 1996
< squarebracket elementid="20050757w-sb7">
[ 
< a type="href" elementid="20050757w-a10">
7
< /a>
]
< /squarebracket>
< /sentence>
< /p>
< /td>
< td elementid="20050757w-td8">
< p elementid="20050757w-p22">
< sentence type="normal" elementid="20050757w-s49">
Adrannau 106
< roundbracket elementid="20050757w-rb37">
(  1 )
< /roundbracket>
< roundbracket elementid="20050757w-rb38">
(  b )
< /roundbracket>
a 146
< roundbracket elementid="20050757w-rb39">
(  1 )
< /roundbracket> .
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20050757w-p23">
< sentence type="heading" elementid="20050757w-s50">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20050757w-s51">
< em elementid="20050757w-em6">
< roundbracket elementid="20050757w-rb40">
(  Nid yw 'r nodyn hwn yn rhan o 'r Gorchymyn )
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20050757w-s52">
Mae 'r Gorchymyn hwn yn diwygio , o ran Cymru , ddau offeryn statudol er mwyn eu gwneud yn unol â darpariaethau Deddf Archwilio Cyhoeddus
< roundbracket elementid="20050757w-rb41">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20050757w-rb42">
(
< doublequotation elementid="20050757w-dq5">
"  y Ddeddf "
< /doublequotation>
)
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20050757w-s53">
Mae 'r Ddeddf yn rhoi nifer o swyddogaethau newydd i Archwilydd Cyffredinol Cymru .
< /sentence>
< sentence type="normal" elementid="20050757w-s54">
Effaith mwyaf arwyddocaol y swyddogaethau newydd yw mai 'r Archwilydd Cyffredinol , pan fydd y Ddeddf mewn grym yn llwyr , fydd yn arfer y rhan fwyaf o 'r swyddogaethau a arferir ar hyn o bryd yng Nghymru gan y Comisiwn Archwilio ar gyfer Awdurdodau Lleol a 'r Gwasanaeth Iechyd Gwladol yng Nghymru a Lloegr
< roundbracket elementid="20050757w-rb43">
(
< doublequotation elementid="20050757w-dq6">
"  y Comisiwn Archwilio "
< /doublequotation>
)
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20050757w-s55">
Mae 'r diwygiadau a wneir gan y Gorchymyn hwn o ganlyniad i 'r newid hwnnw .
< /sentence>
< sentence type="normal" elementid="20050757w-s56">
Mae erthygl 2 yn diwygio Gorchymyn Awdurdodau Lleol
< roundbracket elementid="20050757w-rb44">
(  Cwmnïau )
< /roundbracket>
1995 , er mwyn sicrhau bod awdurdodau lleol yng Nghymru yn cael cydsyniad Archwilydd Cyffredinol Cymru , yn hytrach na 'r Comisiwn Archwilio , i benodi person i archwilio cyfrifon cwmnïau o dan eu rheolaeth o fewn ystyr adran 68 o Ddeddf Llywodraeth Leol a Thai 1989 .
< /sentence>
< sentence type="normal" elementid="20050757w-s57">
Mae erthygl 3 yn diwygio Gorchymyn Eithrio Contractau Adeiladu
< roundbracket elementid="20050757w-rb45">
(  Cymru a Lloegr )
< /roundbracket>
1998 , o ran Cymru , er mwyn darparu bod cyrff y mae eu cyfrifon yn ddarostyngedig i archwiliad gan Archwilydd Cyffredinol Cymru yn cyflawni un o 'r amodau ar gyfer cymhwyster i ymrwymo i gontract o dan y fenter cyllid preifat .
< /sentence>
< sentence type="heading" elementid="20050757w-s58">
< em elementid="20050757w-em7">
Notes :
< /em>
< /sentence>
< sentence type="normal" elementid="20050757w-s59">
< squarebracket elementid="20050757w-sb8">
[  1 ]
< /squarebracket>
O.S. 1995/ 849 .
< /sentence>
< sentence type="heading" elementid="20050757w-s60">
< a type="href" elementid="20050757w-a11">
back
< /a>
< /sentence>
< /p>
< p elementid="20050757w-p24">
< sentence type="normal" elementid="20050757w-s61">
< squarebracket elementid="20050757w-sb9">
[  2 ]
< /squarebracket>
O.S. 1998/ 648 .
< /sentence>
< sentence type="heading" elementid="20050757w-s62">
< a type="href" elementid="20050757w-a12">
back
< /a>
< /sentence>
< /p>
< p elementid="20050757w-p25">
< sentence type="normal" elementid="20050757w-s63">
< squarebracket elementid="20050757w-sb10">
[  3 ]
< /squarebracket>
1998 p.38 .
< /sentence>
< sentence type="heading" elementid="20050757w-s64">
< a type="href" elementid="20050757w-a13">
back
< /a>
< /sentence>
< /p>
< p elementid="20050757w-p26">
< sentence type="normal" elementid="20050757w-s65">
< squarebracket elementid="20050757w-sb11">
[  4 ]
< /squarebracket>
O.S.1999/ 672 .
< /sentence>
< sentence type="heading" elementid="20050757w-s66">
< a type="href" elementid="20050757w-a14">
back
< /a>
< /sentence>
< /p>
< p elementid="20050757w-p27">
< sentence type="normal" elementid="20050757w-s67">
< squarebracket elementid="20050757w-sb12">
[  5 ]
< /squarebracket>
1989 p .
< /sentence>
< sentence type="normal" elementid="20050757w-s68">
42 .
< /sentence>
< sentence type="heading" elementid="20050757w-s69">
< a type="href" elementid="20050757w-a15">
back
< /a>
< /sentence>
< /p>
< p elementid="20050757w-p28">
< sentence type="normal" elementid="20050757w-s70">
< squarebracket elementid="20050757w-sb13">
[  6 ]
< /squarebracket>
2004 p .
< /sentence>
< sentence type="normal" elementid="20050757w-s71">
23 .
< /sentence>
< sentence type="heading" elementid="20050757w-s72">
< a type="href" elementid="20050757w-a16">
back
< /a>
< /sentence>
< /p>
< p elementid="20050757w-p29">
< sentence type="normal" elementid="20050757w-s73">
< squarebracket elementid="20050757w-sb14">
[  7 ]
< /squarebracket>
1996 p.53 .
< /sentence>
< sentence type="heading" elementid="20050757w-s74">
< a type="href" elementid="20050757w-a17">
back
< /a>
< /sentence>
< /p>
< p elementid="20050757w-p30">
< sentence type="heading" elementid="20050757w-s75">
< a type="href" elementid="20050757w-a18">
English version
< /a>
< /sentence>
< sentence type="heading" elementid="20050757w-s76">
ISBN 0 11 091097 4
< /sentence>
< /p>
< table elementid="20050757w-tb3">
< td elementid="20050757w-td9">
< p elementid="20050757w-p31">
< sentence type="heading" elementid="20050757w-s77">
< a type="href" elementid="20050757w-a19">
Other UK SIs
< /a>
< a type="href" elementid="20050757w-a20">
Home
< /a>
< a type="href" elementid="20050757w-a21">
National Assembly for Wales Statutory Instruments
< /a>
< a type="href" elementid="20050757w-a22">
Scottish Statutory Instruments
< /a>
< a type="href" elementid="20050757w-a23">
Statutory Rules of Northern Ireland
< /a>
< a type="href" elementid="20050757w-a24">
Her Majesty 's Stationery Office
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20050757w-td10">
< p elementid="20050757w-p32">
< sentence type="heading" elementid="20050757w-s78">
< strong elementid="20050757w-st9">
We welcome your 
< a type="href" elementid="20050757w-a25">
comments
< /a>
on this site
< /strong>
< /sentence>
< /p>
< /td>
< td elementid="20050757w-td11">
< p elementid="20050757w-p33">
< sentence type="heading" elementid="20050757w-s79">
© Crown copyright 2005
< /sentence>
< /p>
< /td>
< td elementid="20050757w-td12">
< p elementid="20050757w-p34">
< sentence type="heading" elementid="20050757w-s80">
Prepared 22 March 2005
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>944< /wordcount>
< /file>
< file elementid="20050761w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Rheoliadau Deddf Archwilio Cyhoeddus ( Cymru ) 2004 ( Diwygiadau Canlyniadol ) ( Cymru ) 2005< /pagetitle>
< doc_class>Statutory Instrument - Wales< /doc_class>
< year>2005< /year>
< number>0761< /number>
< legislation_code>30102< /legislation_code>
< legislation_type>Statutory Instrument - Wales< /legislation_type>
< subject>ARCHWILIO CYHOEDDUS, CYMRU< /subject>
< laid_date>_EMPTY< /laid_date>
< made_date>20050315< /made_date>
< coming_into_force>20050401< /coming_into_force>
< isbn>0110910982< /isbn>
< title>Rheoliadau Deddf Archwilio Cyhoeddus (Cymru) 2004 (Diwygiadau Canlyniadol) (Cymru) 2005< /title>
< signatory>D. Elis-Thomas< /signatory>
< signatory_title>Llywydd y Cynulliad Cenedlaethol< /signatory_title>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20050761w-p1">
< sentence type="heading" elementid="20050761w-s1">
Offerynnau Statudol 2005 Rhif 761
< roundbracket elementid="20050761w-rb1">
(  Cy.65 )
< /roundbracket>
< /sentence>
< /p>
< p elementid="20050761w-p2">
< sentence type="heading" elementid="20050761w-s2">
< strong elementid="20050761w-st1">
Rheoliadau Deddf Archwilio Cyhoeddus
< roundbracket elementid="20050761w-rb2">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20050761w-rb3">
(  Diwygiadau Canlyniadol )
< /roundbracket>
< roundbracket elementid="20050761w-rb4">
(  Cymru )
< /roundbracket>
2005
< /strong>
< /sentence>
< /p>
< p elementid="20050761w-p3">
< sentence type="heading" elementid="20050761w-s3">
© Hawlfraint y Goron 2005
< /sentence>
< /p>
< p elementid="20050761w-p4">
< sentence type="normal" elementid="20050761w-s4">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20050761w-p5">
< sentence type="normal" elementid="20050761w-s5">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20050761w-s6">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20050761w-p6">
< sentence type="normal" elementid="20050761w-s7">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20050761w-s8">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20050761w-s9">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20050761w-p7">
< sentence type="normal" elementid="20050761w-s10">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20050761w-s11">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan The Stationery Office Limited fel
< strong elementid="20050761w-st2">
Rheoliadau Deddf Archwilio Cyhoeddus
< roundbracket elementid="20050761w-rb5">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20050761w-rb6">
(  Diwygiadau Canlyniadol )
< /roundbracket>
< roundbracket elementid="20050761w-rb7">
(  Cymru )
< /roundbracket>
2005
< /strong>
, ISBN 0110910982 .
< /sentence>
< sentence type="normal" elementid="20050761w-s12">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20050761w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20050761w-s13">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20050761w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20050761w-p8">
< sentence type="normal" elementid="20050761w-s14">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20050761w-a3">
Frequently Asked Questions
< /a> .
< /sentence>
< /p>
< p elementid="20050761w-p9">
< sentence type="normal" elementid="20050761w-s15">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20050761w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20050761w-s16">
Pan welwch fotwm
< doublequotation elementid="20050761w-dq2">
"  continue "
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20050761w-p10">
< sentence type="heading" elementid="20050761w-s17">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="heading" elementid="20050761w-s18">
< strong elementid="20050761w-st3">
2005 Rhif 761
< roundbracket elementid="20050761w-rb8">
(  Cy.65 )
< /roundbracket>
< /strong>
< /sentence>
< sentence type="heading" elementid="20050761w-s19">
< strong elementid="20050761w-st4">
ARCHWILIO CYHOEDDUS , CYMRU
< /strong>
< /sentence>
< sentence type="heading" elementid="20050761w-s20">
Rheoliadau Deddf Archwilio Cyhoeddus
< roundbracket elementid="20050761w-rb9">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20050761w-rb10">
(  Diwygiadau Canlyniadol )
< /roundbracket>
< roundbracket elementid="20050761w-rb11">
(  Cymru )
< /roundbracket>
2005
< /sentence>
< /p>
< table elementid="20050761w-tb1">
< td elementid="20050761w-td1">
< p elementid="20050761w-p11">
< sentence type="heading" elementid="20050761w-s21">
< em elementid="20050761w-em1">
Wedi 'u gwneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20050761w-td2">
< p elementid="20050761w-p12">
< sentence type="heading" elementid="20050761w-s22">
< em elementid="20050761w-em2">
15 Mawrth 2005
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20050761w-td3">
< p elementid="20050761w-p13">
< sentence type="heading" elementid="20050761w-s23">
< em elementid="20050761w-em3">
Yn dod i rym
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20050761w-td4">
< p elementid="20050761w-p14">
< sentence type="heading" elementid="20050761w-s24">
< em elementid="20050761w-em4">
1 Ebrill 2005
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20050761w-p15">
< sentence type="heading" elementid="20050761w-s25">
Mae Cynulliad Cenedlaethol Cymru , drwy arfer y pwerau a roddwyd gan y deddfiadau a bennir yn yr Atodlen i 'r offeryn hwn , drwy hyn yn gwneud y Rheoliadau canlynol :
< /sentence>
< sentence type="heading" elementid="20050761w-s26">
< strong elementid="20050761w-st5">
Enwi , cychwyn a chymhwyso
< /strong>
< /sentence>
< sentence type="normal" elementid="20050761w-s27">
1 .
< /sentence>
< sentence type="normal" elementid="20050761w-s28">
 - 
< roundbracket elementid="20050761w-rb12">
(  1 )
< /roundbracket>
Enw 'r gorchymyn hwn yw Rheoliadau Deddf Archwilio Cyhoeddus
< roundbracket elementid="20050761w-rb13">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20050761w-rb14">
(  Diwygiadau Canlyniadol )
< /roundbracket>
< roundbracket elementid="20050761w-rb15">
(  Cymru )
< /roundbracket>
2005 a daw i rym ar 1 Ebrill 2005 .
< /sentence>
< sentence type="normal" elementid="20050761w-s29">
< roundbracket elementid="20050761w-rb16">
(  2 )
< /roundbracket>
Mae 'r Rheoliadau hyn yn gymwys o ran Cymru yn unig .
< /sentence>
< sentence type="heading" elementid="20050761w-s30">
< strong elementid="20050761w-st6">
Diwygio is-ddeddfwriaeth
< /strong>
< /sentence>
< sentence type="normal" elementid="20050761w-s31">
2 .
< /sentence>
< sentence type="normal" elementid="20050761w-s32">
 - 
< roundbracket elementid="20050761w-rb17">
(  1 )
< /roundbracket>
Diwygir Rheoliadau Ymddiriedolaethau 'r Gwasanaeth Iechyd Gwladol
< roundbracket elementid="20050761w-rb18">
(  Cyfarfodydd Cyhoeddus )
< /roundbracket>
1991
< squarebracket elementid="20050761w-sb1">
[ 
< a type="href" elementid="20050761w-a4">
1
< /a>
]
< /squarebracket>
fel a ganlyn .
< /sentence>
< sentence type="heading" elementid="20050761w-s33">
< roundbracket elementid="20050761w-rb19">
(  2 )
< /roundbracket>
Yn rheoliad 3  - 
< /sentence>
< sentence type="heading" elementid="20050761w-s34">
< roundbracket elementid="20050761w-rb20">
(  b )
< /roundbracket>
ar ôl y paragraff hwnnw , mewnosoder  - 
< /sentence>
< /p>
< p elementid="20050761w-p16">
< sentence type="normal" elementid="20050761w-s35">
3 .
< /sentence>
< sentence type="normal" elementid="20050761w-s36">
 - 
< roundbracket elementid="20050761w-rb21">
(  1 )
< /roundbracket>
Diwygir Rheoliadau Addysg
< roundbracket elementid="20050761w-rb22">
(  Grantiau )
< /roundbracket>
< roundbracket elementid="20050761w-rb23">
(  Teithwyr a Phobl Wedi 'u Dadleoli )
< /roundbracket>
1993
< squarebracket elementid="20050761w-sb2">
[ 
< a type="href" elementid="20050761w-a5">
2
< /a>
]
< /squarebracket>
fel a ganlyn .
< /sentence>
< sentence type="heading" elementid="20050761w-s37">
< roundbracket elementid="20050761w-rb24">
(  2 )
< /roundbracket>
Ym mharagraff
< roundbracket elementid="20050761w-rb25">
(  4 )
< /roundbracket>
< roundbracket elementid="20050761w-rb26">
(  b )
< /roundbracket>
o reoliad 7 - 
< /sentence>
< /p>
< p elementid="20050761w-p17">
< sentence type="normal" elementid="20050761w-s38">
< roundbracket elementid="20050761w-rb27">
(  b )
< /roundbracket>
yn lle  section 3  5 )-
< roundbracket elementid="20050761w-rb28">
(  7 )
< /roundbracket>
of the Audit Commission Act 1998
< doublequotation elementid="20050761w-dq3">
"  , rhodder "
< /doublequotation>
section 14
< roundbracket elementid="20050761w-rb29">
(  4 )
< /roundbracket>
and
< roundbracket elementid="20050761w-rb30">
(  9 )
< /roundbracket>
of the Public Audit
< roundbracket elementid="20050761w-rb31">
(  Wales )
< /roundbracket>
Act 2004  .
< /sentence>
< /p>
< p elementid="20050761w-p18">
< sentence type="normal" elementid="20050761w-s39">
4 .
< /sentence>
< sentence type="normal" elementid="20050761w-s40">
 - 
< roundbracket elementid="20050761w-rb32">
(  1 )
< /roundbracket>
Diwygir Rheoliadau Awdurdodau Lleol
< roundbracket elementid="20050761w-rb33">
(  Contractau )
< /roundbracket>
1997
< squarebracket elementid="20050761w-sb3">
[ 
< a type="href" elementid="20050761w-a6">
3
< /a>
]
< /squarebracket>
fel a ganlyn .
< /sentence>
< sentence type="heading" elementid="20050761w-s41">
< roundbracket elementid="20050761w-rb34">
(  2 )
< /roundbracket>
Yn rheoliad 2 , hepgorer y diffiniad o
< doublequotation elementid="20050761w-dq4">
"  the auditor "
< /doublequotation>
, ac yn ei le rhodder y canlynol  - 
< /sentence>
< /p>
< p elementid="20050761w-p19">
< sentence type="normal" elementid="20050761w-s42">
5 .
< /sentence>
< sentence type="normal" elementid="20050761w-s43">
 - 
< roundbracket elementid="20050761w-rb35">
(  1 )
< /roundbracket>
Diwygir Rheoliadau Addysg
< roundbracket elementid="20050761w-rb36">
(  Cyhoeddi Adroddiadau Arolygiadau Awdurdodau Addysg Lleol )
< /roundbracket>
1998
< squarebracket elementid="20050761w-sb4">
[ 
< a type="href" elementid="20050761w-a7">
4
< /a>
]
< /squarebracket>
fel a ganlyn .
< /sentence>
< sentence type="normal" elementid="20050761w-s44">
< roundbracket elementid="20050761w-rb37">
(  2 )
< /roundbracket>
Ym mharagraff
< roundbracket elementid="20050761w-rb38">
(  1 )
< /roundbracket>
< roundbracket elementid="20050761w-rb39">
(  f )
< /roundbracket>
o reoliad 4 , ar y dechrau , mewnosoder  in relation to a local education authority in England," .
< /sentence>
< sentence type="heading" elementid="20050761w-s45">
< roundbracket elementid="20050761w-rb40">
(  3 )
< /roundbracket>
Ar ôl paragraff
< roundbracket elementid="20050761w-rb41">
(  1 )
< /roundbracket>
< roundbracket elementid="20050761w-rb42">
(  f )
< /roundbracket>
o reoliad 4 , mewnosoder  - 
< /sentence>
< /p>
< p elementid="20050761w-p20">
< sentence type="normal" elementid="20050761w-s46">
6 .
< /sentence>
< sentence type="normal" elementid="20050761w-s47">
 - 
< roundbracket elementid="20050761w-rb43">
(  1 )
< /roundbracket>
Diwygir Rheoliadau Trefniadau Partneriaeth Cyrff Gwasanaeth Iechyd Gwladol ac Awdurdodau Lleol
< roundbracket elementid="20050761w-rb44">
(  Cymru )
< /roundbracket>
2000
< squarebracket elementid="20050761w-sb5">
[ 
< a type="href" elementid="20050761w-a8">
5
< /a>
]
< /squarebracket>
fel a ganlyn .
< /sentence>
< sentence type="heading" elementid="20050761w-s48">
< roundbracket elementid="20050761w-rb45">
(  2 )
< /roundbracket>
Ym mharagraff
< roundbracket elementid="20050761w-rb46">
(  6 )
< /roundbracket>
o reoliad 7 - 
< /sentence>
< /p>
< p elementid="20050761w-p21">
< sentence type="normal" elementid="20050761w-s49">
< roundbracket elementid="20050761w-rb47">
(  b )
< /roundbracket>
hepgorer y geiriau
< doublequotation elementid="20050761w-dq5">
"  adran 28
< roundbracket elementid="20050761w-rb48">
(  1 )
< /roundbracket>
< roundbracket elementid="20050761w-rb49">
(  d )
< /roundbracket>
o Ddeddf y Comisiwn Archwilio 1998 "
< /doublequotation>
ac yn eu lle rhodder
< doublequotation elementid="20050761w-dq6">
"  adran 96B o Ddeddf Llywodraeth Cymru 1998 "
< /doublequotation> .
< /sentence>
< /p>
< p elementid="20050761w-p22">
< sentence type="normal" elementid="20050761w-s50">
7 .
< /sentence>
< sentence type="normal" elementid="20050761w-s51">
 - 
< roundbracket elementid="20050761w-rb50">
(  1 )
< /roundbracket>
Diwygir Rheoliadau Ymchwiliadau Llywodraeth Leol
< roundbracket elementid="20050761w-rb51">
(  Swyddogaethau Swyddogion Monitro a Phwyllgorau Safonau )
< /roundbracket>
< roundbracket elementid="20050761w-rb52">
(  Cymru )
< /roundbracket>
2001
< squarebracket elementid="20050761w-sb6">
[ 
< a type="href" elementid="20050761w-a9">
6
< /a>
]
< /squarebracket>
fel a ganlyn .
< /sentence>
< sentence type="heading" elementid="20050761w-s52">
< roundbracket elementid="20050761w-rb53">
(  2 )
< /roundbracket>
Yn rheoliad 5
< roundbracket elementid="20050761w-rb54">
(  1 )
< /roundbracket>
 - 
< /sentence>
< /p>
< p elementid="20050761w-p23">
< sentence type="heading" elementid="20050761w-s53">
< roundbracket elementid="20050761w-rb55">
(  b )
< /roundbracket>
ar ddiwedd is-baragraff
< roundbracket elementid="20050761w-rb56">
(  dd )
< /roundbracket>
, mewnosoder - 
< /sentence>
< sentence type="normal" elementid="20050761w-s54">
< doublequotation elementid="20050761w-dq7">
"  ; neu - 
< roundbracket elementid="20050761w-rb57">
(  e )
< /roundbracket>
bod y datgelu yn cael ei wneud i Archwilydd Cyffredinol Cymru at ddibenion unrhyw swyddogaeth ganddo neu gan archwilydd o dan Ran 2 o Ddeddf Archwilio Cyhoeddus
< roundbracket elementid="20050761w-rb58">
(  Cymru )
< /roundbracket>
2004 "
< /doublequotation> .
< /sentence>
< /p>
< p elementid="20050761w-p24">
< sentence type="normal" elementid="20050761w-s55">
8 .
< /sentence>
< sentence type="normal" elementid="20050761w-s56">
 - 
< roundbracket elementid="20050761w-rb59">
(  1 )
< /roundbracket>
Diwygir  Rheoliadau Grantiau Safonau Addysg
< roundbracket elementid="20050761w-rb60">
(  Cymru )
< /roundbracket>
2002
< squarebracket elementid="20050761w-sb7">
[ 
< a type="href" elementid="20050761w-a10">
7
< /a>
]
< /squarebracket>
fel a ganlyn .
< /sentence>
< sentence type="heading" elementid="20050761w-s57">
< roundbracket elementid="20050761w-rb61">
(  2 )
< /roundbracket>
Ym mharagraff
< roundbracket elementid="20050761w-rb62">
(  5 )
< /roundbracket>
< roundbracket elementid="20050761w-rb63">
(  b )
< /roundbracket>
o reoliad 6 - 
< /sentence>
< /p>
< p elementid="20050761w-p25">
< sentence type="normal" elementid="20050761w-s58">
< roundbracket elementid="20050761w-rb64">
(  b )
< /roundbracket>
yn lle
< doublequotation elementid="20050761w-dq8">
"  adran 3
< roundbracket elementid="20050761w-rb65">
(  5 )
< /roundbracket>
,
< roundbracket elementid="20050761w-rb66">
(  6 )
< /roundbracket>
a
< roundbracket elementid="20050761w-rb67">
(  7 )
< /roundbracket>
o Ddeddf y Comisiwn Archwilio 1998 "
< /doublequotation>
, rhodder
< doublequotation elementid="20050761w-dq9">
"  adran 14
< roundbracket elementid="20050761w-rb68">
(  4 )
< /roundbracket>
a
< roundbracket elementid="20050761w-rb69">
(  9 )
< /roundbracket>
o Ddeddf Archwilio Cyhoeddus
< roundbracket elementid="20050761w-rb70">
(  Cymru )
< /roundbracket>
2004 "
< /doublequotation> .
< /sentence>
< /p>
< p elementid="20050761w-p26">
< sentence type="normal" elementid="20050761w-s59">
9 .
< /sentence>
< sentence type="normal" elementid="20050761w-s60">
 - 
< roundbracket elementid="20050761w-rb71">
(  1 )
< /roundbracket>
Diwygir Rheoliadau Addysg
< roundbracket elementid="20050761w-rb72">
(  Grantiau Cyfalaf )
< /roundbracket>
< roundbracket elementid="20050761w-rb73">
(  Cymru )
< /roundbracket>
2002
< squarebracket elementid="20050761w-sb8">
[ 
< a type="href" elementid="20050761w-a11">
8
< /a>
]
< /squarebracket>
fel a ganlyn .
< /sentence>
< sentence type="heading" elementid="20050761w-s61">
< roundbracket elementid="20050761w-rb74">
(  2 )
< /roundbracket>
Ym mharagraff
< roundbracket elementid="20050761w-rb75">
(  4 )
< /roundbracket>
< roundbracket elementid="20050761w-rb76">
(  b )
< /roundbracket>
o reoliad 5 - 
< /sentence>
< /p>
< p elementid="20050761w-p27">
< sentence type="normal" elementid="20050761w-s62">
< roundbracket elementid="20050761w-rb77">
(  b )
< /roundbracket>
yn lle
< doublequotation elementid="20050761w-dq10">
"  adran 3
< roundbracket elementid="20050761w-rb78">
(  5 )
< /roundbracket>
,
< roundbracket elementid="20050761w-rb79">
(  6 )
< /roundbracket>
a
< roundbracket elementid="20050761w-rb80">
(  7 )
< /roundbracket>
o Ddeddf y Comisiwn Archwilio 1998 "
< /doublequotation>
, rhodder
< doublequotation elementid="20050761w-dq11">
"  adran 14
< roundbracket elementid="20050761w-rb81">
(  4 )
< /roundbracket>
a
< roundbracket elementid="20050761w-rb82">
(  9 )
< /roundbracket>
o Ddeddf Archwilio Cyhoeddus
< roundbracket elementid="20050761w-rb83">
(  Cymru )
< /roundbracket>
2004 "
< /doublequotation> .
< /sentence>
< /p>
< p elementid="20050761w-p28">
< sentence type="normal" elementid="20050761w-s63">
10 .
< /sentence>
< sentence type="normal" elementid="20050761w-s64">
 - 
< roundbracket elementid="20050761w-rb84">
(  1 )
< /roundbracket>
Rheoliadau Addysg
< roundbracket elementid="20050761w-rb85">
(  Cynllun Grant Dysgu 'r Cynulliad )
< /roundbracket>
< roundbracket elementid="20050761w-rb86">
(  Cymru )
< /roundbracket>
2002
< squarebracket elementid="20050761w-sb9">
[ 
< a type="href" elementid="20050761w-a12">
9
< /a>
]
< /squarebracket>
fel a ganlyn .
< /sentence>
< sentence type="heading" elementid="20050761w-s65">
< roundbracket elementid="20050761w-rb87">
(  2 )
< /roundbracket>
Ym mharagraff
< roundbracket elementid="20050761w-rb88">
(  6 )
< /roundbracket>
< roundbracket elementid="20050761w-rb89">
(  b )
< /roundbracket>
o reoliad 6 - 
< /sentence>
< /p>
< p elementid="20050761w-p29">
< sentence type="normal" elementid="20050761w-s66">
< roundbracket elementid="20050761w-rb90">
(  b )
< /roundbracket>
yn lle
< doublequotation elementid="20050761w-dq12">
"  adran 3
< roundbracket elementid="20050761w-rb91">
(  5 )
< /roundbracket>
,
< roundbracket elementid="20050761w-rb92">
(  6 )
< /roundbracket>
a
< roundbracket elementid="20050761w-rb93">
(  7 )
< /roundbracket>
o Ddeddf y Comisiwn Archwilio 1998 "
< /doublequotation>
, rhodder
< doublequotation elementid="20050761w-dq13">
"  adran 14
< roundbracket elementid="20050761w-rb94">
(  4 )
< /roundbracket>
a
< roundbracket elementid="20050761w-rb95">
(  9 )
< /roundbracket>
o Ddeddf Archwilio Cyhoeddus
< roundbracket elementid="20050761w-rb96">
(  Cymru )
< /roundbracket>
2004 "
< /doublequotation> .
< /sentence>
< /p>
< p elementid="20050761w-p30">
< sentence type="normal" elementid="20050761w-s67">
Llofnodwyd ar ran y Cynulliad Cenedlaethol Cymru o dan adran 66
< roundbracket elementid="20050761w-rb97">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20050761w-sb10">
[ 
< a type="href" elementid="20050761w-a13">
10
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="heading" elementid="20050761w-s68">
< em elementid="20050761w-em5">
D. Elis-Thomas
< /em>
< /sentence>
< sentence type="heading" elementid="20050761w-s69">
Llywydd y Cynulliad Cenedlaethol
< /sentence>
< sentence type="heading" elementid="20050761w-s70">
15 Mawrth 2005
< /sentence>
< /p>
< p elementid="20050761w-p31">
< sentence type="heading" elementid="20050761w-s71">
YR ATODLEN Rhaglith
< /sentence>
< sentence type="heading" elementid="20050761w-s72">
< strong elementid="20050761w-st7">
RHAN 1  - 
< /strong>
< /sentence>
< sentence type="heading" elementid="20050761w-s73">
< strong elementid="20050761w-st8">
Deddfiadau sy 'n cynnwys pwerau a roddwyd i Gynulliad Cenedlaethol Cymru
< /strong>
< /sentence>
< /p>
< table elementid="20050761w-tb2">
< td elementid="20050761w-td5">
< p elementid="20050761w-p32">
< sentence type="heading" elementid="20050761w-s74">
Deddf Addysg 1996
< squarebracket elementid="20050761w-sb11">
[ 
< a type="href" elementid="20050761w-a14">
11
< /a>
]
< /squarebracket>
< /sentence>
< /p>
< /td>
< td elementid="20050761w-td6">
< p elementid="20050761w-p33">
< sentence type="normal" elementid="20050761w-s75">
Adran 484  fel y 'i diwygiwyd gan Ddeddf Safonau a Fframwaith Ysgolion 1998
< squarebracket elementid="20050761w-sb12">
[ 
< a type="href" elementid="20050761w-a15">
12
< /a>
]
< /squarebracket>
a Deddf Addysg 2002
< squarebracket elementid="20050761w-sb13">
[ 
< a type="href" elementid="20050761w-a16">
13
< /a>
]
< /squarebracket> .
< /sentence>
< /p>
< /td>
< td elementid="20050761w-td7">
< p elementid="20050761w-p34">
< sentence type="heading" elementid="20050761w-s76">
Deddf Llywodraeth Leol 2000
< squarebracket elementid="20050761w-sb14">
[ 
< a type="href" elementid="20050761w-a17">
14
< /a>
]
< /squarebracket>
< /sentence>
< /p>
< /td>
< td elementid="20050761w-td8">
< p elementid="20050761w-p35">
< sentence type="normal" elementid="20050761w-s77">
Adran 73
< roundbracket elementid="20050761w-rb98">
(  1 )
< /roundbracket> .
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20050761w-p36">
< sentence type="heading" elementid="20050761w-s78">
< strong elementid="20050761w-st9">
RHAN 2  - 
< /strong>
< /sentence>
< sentence type="heading" elementid="20050761w-s79">
< strong elementid="20050761w-st10">
Deddfiadau sy 'n rhoi swyddogaethau a drosglwyddwyd i Gynulliad Cenedlaethol Cymru gan Orchymyn Cynulliad Cenedlaethol Cymru
< roundbracket elementid="20050761w-rb99">
Trosglwyddo Swyddogaethau 1999
< squarebracket elementid="20050761w-sb15">
[ 
< a type="href" elementid="20050761w-a18">
15
< /a>
]
< /squarebracket>
< roundbracket elementid="20050761w-rb100">
(  Erthygl 2 ac Atodlen 1 )
< /roundbracket>
< /roundbracket>
< /strong>
< /sentence>
< /p>
< table elementid="20050761w-tb3">
< td elementid="20050761w-td9">
< p elementid="20050761w-p37">
< sentence type="heading" elementid="20050761w-s80">
Deddf y Gwasanaeth Iechyd Gwladol 1977
< squarebracket elementid="20050761w-sb16">
[ 
< a type="href" elementid="20050761w-a19">
16
< /a>
]
< /squarebracket>
< /sentence>
< /p>
< /td>
< td elementid="20050761w-td10">
< p elementid="20050761w-p38">
< sentence type="normal" elementid="20050761w-s81">
Adran 126
< roundbracket elementid="20050761w-rb101">
(  4 )
< /roundbracket>
< roundbracket elementid="20050761w-rb102">
(  fel y 'i diwygiwyd gan Ddeddf Diwygio 'r Gwasanaeth Iechyd Gwladol a Phroffesiynau Gofal Iechyd 2002
< squarebracket elementid="20050761w-sb17">
[ 
< a type="href" elementid="20050761w-a20">
17
< /a>
]
< /squarebracket>
)
< /roundbracket>
ac adran 128
< roundbracket elementid="20050761w-rb103">
(  1 )
< /roundbracket>
< roundbracket elementid="20050761w-rb104">
(  fel y 'i diwygiwyd gan Ddeddf y Gwasanaeth Iechyd Gwladol a Gofal Cymunedol 1990
< squarebracket elementid="20050761w-sb18">
[ 
< a type="href" elementid="20050761w-a21">
18
< /a>
]
< /squarebracket>
)
< /roundbracket> .
< /sentence>
< /p>
< /td>
< td elementid="20050761w-td11">
< p elementid="20050761w-p39">
< sentence type="heading" elementid="20050761w-s82">
Deddf Diwygio Addysg 1988
< squarebracket elementid="20050761w-sb19">
[ 
< a type="href" elementid="20050761w-a22">
19
< /a>
]
< /squarebracket>
)
< /sentence>
< /p>
< /td>
< td elementid="20050761w-td12">
< p elementid="20050761w-p40">
< sentence type="normal" elementid="20050761w-s83">
Adran 210
< roundbracket elementid="20050761w-rb105">
(  fel y 'i diwygiwyd gan Ddeddf Addysg 1996
< squarebracket elementid="20050761w-sb20">
[ 
< a type="href" elementid="20050761w-a23">
20
< /a>
]
< /squarebracket>
)
< /roundbracket>
a Deddf Addysg Bellach ac Uwch 1992
< squarebracket elementid="20050761w-sb21">
[ 
< a type="href" elementid="20050761w-a24">
21
< /a>
]
< /squarebracket>
) ) ac adran 232 .
< /sentence>
< /p>
< /td>
< td elementid="20050761w-td13">
< p elementid="20050761w-p41">
< sentence type="heading" elementid="20050761w-s84">
Deddf y Gwasanaeth Iechyd Gwladol a Gofal Cymunedol 1990
< /sentence>
< /p>
< /td>
< td elementid="20050761w-td14">
< p elementid="20050761w-p42">
< sentence type="normal" elementid="20050761w-s85">
Adran 5 ac Atodlen 2 , paragraff 7
< roundbracket elementid="20050761w-rb106">
(  2 )
< /roundbracket>
a
< roundbracket elementid="20050761w-rb107">
(  3 )
< /roundbracket>
< roundbracket elementid="20050761w-rb108">
(  fel y 'i diwygiwyd , yn achos paragraff
< roundbracket elementid="20050761w-rb109">
(  2 )
< /roundbracket>
, gan Ddeddf y Comisiwn Archwilio 1998
< squarebracket elementid="20050761w-sb22">
[ 
< a type="href" elementid="20050761w-a25">
22
< /a>
]
< /squarebracket>
, a Deddf Archwilio Cyhoeddus
< roundbracket elementid="20050761w-rb110">
(  Cymru )
< /roundbracket>
2004
< squarebracket elementid="20050761w-sb23">
[ 
< a type="href" elementid="20050761w-a26">
23
< /a>
]
< /squarebracket>
)
< /roundbracket>
) .
< /sentence>
< /p>
< /td>
< td elementid="20050761w-td15">
< p elementid="20050761w-p43">
< sentence type="heading" elementid="20050761w-s86">
Deddf Addysg 1996
< /sentence>
< /p>
< /td>
< td elementid="20050761w-td16">
< p elementid="20050761w-p44">
< sentence type="normal" elementid="20050761w-s87">
Adrannau 489
< roundbracket elementid="20050761w-rb111">
(  fel y 'i diwygiwyd gan Ddeddf Safonau a Fframwaith Ysgolion 1998 )
< /roundbracket>
a 569
< roundbracket elementid="20050761w-rb112">
(  1 )
< /roundbracket> .
< /sentence>
< /p>
< /td>
< td elementid="20050761w-td17">
< p elementid="20050761w-p45">
< sentence type="heading" elementid="20050761w-s88">
Deddf Addysg 1997
< squarebracket elementid="20050761w-sb24">
[ 
< a type="href" elementid="20050761w-a27">
24
< /a>
]
< /squarebracket>
< /sentence>
< /p>
< /td>
< td elementid="20050761w-td18">
< p elementid="20050761w-p46">
< sentence type="normal" elementid="20050761w-s89">
Adran 39
< roundbracket elementid="20050761w-rb113">
(  3 )
< /roundbracket> .
< /sentence>
< /p>
< /td>
< td elementid="20050761w-td19">
< p elementid="20050761w-p47">
< sentence type="heading" elementid="20050761w-s90">
Deddf Llywodraeth Leol
< roundbracket elementid="20050761w-rb114">
(  Contractau )
< /roundbracket>
1997
< squarebracket elementid="20050761w-sb25">
[ 
< a type="href" elementid="20050761w-a28">
25
< /a>
]
< /squarebracket>
< /sentence>
< /p>
< /td>
< td elementid="20050761w-td20">
< p elementid="20050761w-p48">
< sentence type="normal" elementid="20050761w-s91">
Adran 3
< roundbracket elementid="20050761w-rb115">
(  2 )
< /roundbracket>
< roundbracket elementid="20050761w-rb116">
(  e )
< /roundbracket>
ac
< roundbracket elementid="20050761w-rb117">
(  f )
< /roundbracket>
a
< roundbracket elementid="20050761w-rb118">
(  3 )
< /roundbracket>
a phob pŵer arall sy 'n ei alluogi yn y cyswllt hwnnw .
< /sentence>
< /p>
< /td>
< td elementid="20050761w-td21">
< p elementid="20050761w-p49">
< sentence type="heading" elementid="20050761w-s92">
Deddf Iechyd 1999
< squarebracket elementid="20050761w-sb26">
[ 
< a type="href" elementid="20050761w-a29">
26
< /a>
]
< /squarebracket>
< /sentence>
< /p>
< /td>
< td elementid="20050761w-td22">
< p elementid="20050761w-p50">
< sentence type="normal" elementid="20050761w-s93">
Adran 31
< roundbracket elementid="20050761w-rb119">
(  fel y 'i diwygiwyd gan Ddeddf Diwygio 'r Gwasanaeth Iechyd Gwladol a Phroffesiynau Gofal Iechyd 2002
< squarebracket elementid="20050761w-sb27">
[ 
< a type="href" elementid="20050761w-a30">
27
< /a>
]
< /squarebracket>
)
< /roundbracket> .
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20050761w-p51">
< sentence type="heading" elementid="20050761w-s94">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20050761w-s95">
< em elementid="20050761w-em6">
< roundbracket elementid="20050761w-rb120">
Nid yw 'r nodyn hwn yn rhan o 'r Rheoliadau.)
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20050761w-s96">
Effaith gyffredinol darpariaethau Deddf Archwilio Cyhoeddus
< roundbracket elementid="20050761w-rb121">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20050761w-rb122">
(
< doublequotation elementid="20050761w-dq14">
"  y Ddeddf "
< /doublequotation>
)
< /roundbracket>
yw rhoi nifer o swyddogaethau newydd i Archwilydd Cyffredinol Cymru .
< /sentence>
< sentence type="normal" elementid="20050761w-s97">
Effaith fwyaf arwyddocaol y swyddogaethau newydd yw mai 'r Archwilydd Cyffredinol , pan fydd y Ddeddf mewn grym yn llwyr , fydd yn arfer y rhan fwyaf o 'r swyddogaethau a arferir ar hyn o bryd yng Nghymru gan y Comisiwn Archwilio ar gyfer Awdurdodau Lleol a 'r Gwasanaeth Iechyd Gwladol yng Nghymru a Lloegr
< roundbracket elementid="20050761w-rb123">
(
< doublequotation elementid="20050761w-dq15">
"  y Comisiwn Archwilio "
< /doublequotation>
)
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20050761w-s98">
Mae 'r diwygiadau a wneir gan y Rheoliadau hyn yn ganlyniad i 'r newid hwnnw .
< /sentence>
< sentence type="normal" elementid="20050761w-s99">
Maent yn diwygio pum set o Reoliadau ym maes addysg , dwy set o ran y Gwasanaeth Iechyd Gwladol , a dwy set ym maes llywodraeth leol .
< /sentence>
< sentence type="normal" elementid="20050761w-s100">
Yn y maes addysg , mae rheoliadau 3 , 8 , 9 a 10 yn diwygio 'r gofynion , a osodir yn yr is-ddeddfwriaeth a ddiwygiwyd , ar gyfer ardystio grantiau .
< /sentence>
< sentence type="normal" elementid="20050761w-s101">
Ym mhob achos , mae 'r diwygiadau 'n darparu , o ran Cymru , y gwneir yr ardystio o hyn ymlaen gan archwilydd a benodwyd gan Archwilydd Cyffredinol Cymru
< roundbracket elementid="20050761w-rb124">
(  yn hytrach na chan y Comisiwn Archwilio )
< /roundbracket>
, neu gan archwilydd cymwys ar gyfer penodiad o 'r fath .
< /sentence>
< sentence type="normal" elementid="20050761w-s102">
Mae 'r ddarpariaeth sy 'n weddill o ran y maes addysg , rheoliad 5 , yn darparu bod adroddiadau o arolygiadau awdurdodau addysg lleol yng Nghymru a gyflawnir o dan adran 38 o Ddeddf Addysg 1997
< roundbracket elementid="20050761w-rb125">
(  fel y 'i diwygiwyd )
< /roundbracket>
i 'w hanfon at Archwilydd Cyffredinol Cymru , yn hytrach nag at y Comisiwn Archwilio .
< /sentence>
< sentence type="normal" elementid="20050761w-s103">
Mae rheoliadau 2 a 6 yn ymwneud â materion ynglŷn â 'r Gwasanaeth Iechyd Gwladol .
< /sentence>
< sentence type="normal" elementid="20050761w-s104">
Mae rheoliad 2 yn ymwneud â 'r amgylchiadau pan fo 'n rhaid i ymddiriedolaeth Gwasanaeth Iechyd Gwladol gynnal cyfarfod cyhoeddus .
< /sentence>
< sentence type="normal" elementid="20050761w-s105">
Mae 'n parhau 'r sefyllfa bod yn rhaid cynnal cyfarfod pan fo ymddiriedolaeth Gwasanaeth Iechyd Gwladol yn derbyn adroddiad buddiant cyhoeddus oddi wrth ei archwilydd .
< /sentence>
< sentence type="normal" elementid="20050761w-s106">
O ganlyniad i 'r Ddeddf , gwneir yr adroddiadau hynny , o 1 Ebrill 2005 ymlaen , gan Archwilydd Cyffredinol Cymru , yn hytrach na chan y Comisiwn Archwilio .
< /sentence>
< sentence type="normal" elementid="20050761w-s107">
Mae 'r diwygiadau a wneir gan reoliad 2 yn adlewyrchu hyn .
< /sentence>
< sentence type="normal" elementid="20050761w-s108">
Mae rheoliad 6 yn darparu , pan fydd cyrff y Gwasanaeth Iechyd Gwladol ac awdurdodau lleol yng Nghymru yn ymrwymo mewn trefniadau cronfa gyfun o dan Reoliadau Trefniadau Partneriaeth Cyrff Gwasanaeth Iechyd Gwladol ac Awdurdodau Lleol
< roundbracket elementid="20050761w-rb126">
(  Cymru )
< /roundbracket>
2000 , y dylai cyfrifon y gronfa gyfun gael ei harchwilio gan Archwilydd Cyffredinol Cymru , yn hytrach na chan y Comisiwn Archwilio .
< /sentence>
< sentence type="normal" elementid="20050761w-s109">
Diwygir yr is-ddeddfwriaeth ym maes llywodraeth leol gan reoliadau 4 a 7 .
< /sentence>
< sentence type="normal" elementid="20050761w-s110">
Mae rheoliad 4 yn diwygio 'r diffiniad o
< doublequotation elementid="20050761w-dq16">
"  the auditor "
< /doublequotation>
yn Rheoliadau Awdurdodau Lleol
< roundbracket elementid="20050761w-rb127">
(  Contractau )
< /roundbracket>
1997 .
< /sentence>
< sentence type="normal" elementid="20050761w-s111">
Effaith y diwygiad yw , ar gyfer cyrff sy 'n awdurdodau lleol at ddibenion y Rheoliadau hynny , ac sydd hefyd yn gyrff llywodraeth leol yng Nghymru at ddibenion adran 12 o 'r Ddeddf , ystyr
< doublequotation elementid="20050761w-dq17">
"  the auditor "
< /doublequotation>
yw 'r archwilydd a benodir gan Archwilydd Cyffredinol Cymru o dan adran 13 o 'r Ddeddf .
< /sentence>
< sentence type="normal" elementid="20050761w-s112">
Mae hyn yn cael effaith ar y gofynion y mae 'n rhaid i awdurdodau lleol penodol eu cyflawni er mwyn i gontract y maent yn ymrwymo iddo fod yn gontract ardystio o dan Ddeddf Llywodraeth Leol
< roundbracket elementid="20050761w-rb128">
(  Contractau )
< /roundbracket>
1997 .
< /sentence>
< sentence type="normal" elementid="20050761w-s113">
Mae rheoliad 7 yn ychwanegu Archwilydd Cyffredinol Cymru at y rhestr o bersonau y caniateir i swyddogion monitro awdurdod lleol ddatgelu gwybodaeth iddo at ddibenion penodol , o dan Reoliadau Ymchwiliadau Llywodraeth Leol
< roundbracket elementid="20050761w-rb129">
(  Swyddogaethau Swyddogion Monitro a Phwyllgorau Safonau )
< /roundbracket>
< roundbracket elementid="20050761w-rb130">
(  Cymru )
< /roundbracket>
2001 .
< /sentence>
< sentence type="heading" elementid="20050761w-s114">
< em elementid="20050761w-em7">
Notes :
< /em>
< /sentence>
< sentence type="normal" elementid="20050761w-s115">
< squarebracket elementid="20050761w-sb28">
[  1 ]
< /squarebracket>
O.S. 1991/ 482 , fel y 'i diwygiwyd gan adran 54
< roundbracket elementid="20050761w-rb131">
(  2 )
< /roundbracket>
o Ddeddf y Comisiwn Archwilio 1998 a pharagraff 4
< roundbracket elementid="20050761w-rb132">
(  1 )
< /roundbracket>
o Atodlen 4 iddi ,  p .
< /sentence>
< sentence type="normal" elementid="20050761w-s116">
18 ) .
< /sentence>
< sentence type="heading" elementid="20050761w-s117">
< a type="href" elementid="20050761w-a31">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p52">
< sentence type="normal" elementid="20050761w-s118">
< squarebracket elementid="20050761w-sb29">
[  2 ]
< /squarebracket>
O.S. 1993/ 569 , fel y 'i diwygiwyd gan adran 54
< roundbracket elementid="20050761w-rb133">
(  2 )
< /roundbracket>
o Ddeddf y Comisiwn Archwilio 1998 a pharagraff 4
< roundbracket elementid="20050761w-rb134">
(  1 )
< /roundbracket>
o Atodlen 4 iddi  p .
< /sentence>
< sentence type="normal" elementid="20050761w-s119">
18 ) .
< /sentence>
< sentence type="heading" elementid="20050761w-s120">
< a type="href" elementid="20050761w-a32">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p53">
< sentence type="normal" elementid="20050761w-s121">
< squarebracket elementid="20050761w-sb30">
[  3 ]
< /squarebracket>
O.S. 1997/ 2862 , fel y 'i diwygiwyd gan adran 54
< roundbracket elementid="20050761w-rb135">
(  2 )
< /roundbracket>
o Ddeddf y Comisiwn Archwilio 1998 a pharagraff 4
< roundbracket elementid="20050761w-rb136">
(  1 )
< /roundbracket>
o Atodlen 4 iddi  p .
< /sentence>
< sentence type="normal" elementid="20050761w-s122">
18 ) .
< /sentence>
< sentence type="heading" elementid="20050761w-s123">
< a type="href" elementid="20050761w-a33">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p54">
< sentence type="normal" elementid="20050761w-s124">
< squarebracket elementid="20050761w-sb31">
[  4 ]
< /squarebracket>
O.S. 1998/ 880 .
< /sentence>
< sentence type="heading" elementid="20050761w-s125">
< a type="href" elementid="20050761w-a34">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p55">
< sentence type="normal" elementid="20050761w-s126">
< squarebracket elementid="20050761w-sb32">
[  5 ]
< /squarebracket>
O.S. 2000/ 2993  Cy .
< /sentence>
< sentence type="normal" elementid="20050761w-s127">
193 ) , fel y 'i diwygiwyd gan O.S. 2004/ 1390  Cy .
< /sentence>
< sentence type="normal" elementid="20050761w-s128">
140 ) .
< /sentence>
< sentence type="heading" elementid="20050761w-s129">
< a type="href" elementid="20050761w-a35">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p56">
< sentence type="normal" elementid="20050761w-s130">
< squarebracket elementid="20050761w-sb33">
[  6 ]
< /squarebracket>
O.S. 2001/ 2281  Cy .
< /sentence>
< sentence type="normal" elementid="20050761w-s131">
171 ) .
< /sentence>
< sentence type="heading" elementid="20050761w-s132">
< a type="href" elementid="20050761w-a36">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p57">
< sentence type="normal" elementid="20050761w-s133">
< squarebracket elementid="20050761w-sb34">
[  7 ]
< /squarebracket>
O.S.2002/ 438  Cy .
< /sentence>
< sentence type="normal" elementid="20050761w-s134">
56 ) .
< /sentence>
< sentence type="heading" elementid="20050761w-s135">
< a type="href" elementid="20050761w-a37">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p58">
< sentence type="normal" elementid="20050761w-s136">
< squarebracket elementid="20050761w-sb35">
[  8 ]
< /squarebracket>
O.S.2002/ 679  Cy .
< /sentence>
< sentence type="normal" elementid="20050761w-s137">
76 ) .
< /sentence>
< sentence type="heading" elementid="20050761w-s138">
< a type="href" elementid="20050761w-a38">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p59">
< sentence type="normal" elementid="20050761w-s139">
< squarebracket elementid="20050761w-sb36">
[  9 ]
< /squarebracket>
O.S.2002/ 1857
< roundbracket elementid="20050761w-rb137">
(  Cy.181 )
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20050761w-s140">
< a type="href" elementid="20050761w-a39">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p60">
< sentence type="normal" elementid="20050761w-s141">
< squarebracket elementid="20050761w-sb37">
[  10 ]
< /squarebracket>
1998 p.38 .
< /sentence>
< sentence type="heading" elementid="20050761w-s142">
< a type="href" elementid="20050761w-a40">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p61">
< sentence type="normal" elementid="20050761w-s143">
< squarebracket elementid="20050761w-sb38">
[  11 ]
< /squarebracket>
1996 p.56 .
< /sentence>
< sentence type="heading" elementid="20050761w-s144">
< a type="href" elementid="20050761w-a41">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p62">
< sentence type="normal" elementid="20050761w-s145">
< squarebracket elementid="20050761w-sb39">
[  12 ]
< /squarebracket>
1998 p .
< /sentence>
< sentence type="normal" elementid="20050761w-s146">
31 .
< /sentence>
< sentence type="heading" elementid="20050761w-s147">
< a type="href" elementid="20050761w-a42">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p63">
< sentence type="normal" elementid="20050761w-s148">
< squarebracket elementid="20050761w-sb40">
[  13 ]
< /squarebracket>
2002 p .
< /sentence>
< sentence type="normal" elementid="20050761w-s149">
32 .
< /sentence>
< sentence type="heading" elementid="20050761w-s150">
< a type="href" elementid="20050761w-a43">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p64">
< sentence type="normal" elementid="20050761w-s151">
< squarebracket elementid="20050761w-sb41">
[  14 ]
< /squarebracket>
2000 p.22 .
< /sentence>
< sentence type="heading" elementid="20050761w-s152">
< a type="href" elementid="20050761w-a44">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p65">
< sentence type="normal" elementid="20050761w-s153">
< squarebracket elementid="20050761w-sb42">
[  15 ]
< /squarebracket>
O.S. 1999/ 672 .
< /sentence>
< sentence type="heading" elementid="20050761w-s154">
< a type="href" elementid="20050761w-a45">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p66">
< sentence type="normal" elementid="20050761w-s155">
< squarebracket elementid="20050761w-sb43">
[  16 ]
< /squarebracket>
1977 t.49 .
< /sentence>
< sentence type="heading" elementid="20050761w-s156">
< a type="href" elementid="20050761w-a46">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p67">
< sentence type="normal" elementid="20050761w-s157">
< squarebracket elementid="20050761w-sb44">
[  17 ]
< /squarebracket>
2002 p .
< /sentence>
< sentence type="normal" elementid="20050761w-s158">
17 .
< /sentence>
< sentence type="heading" elementid="20050761w-s159">
< a type="href" elementid="20050761w-a47">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p68">
< sentence type="normal" elementid="20050761w-s160">
< squarebracket elementid="20050761w-sb45">
[  18 ]
< /squarebracket>
1990 p .
< /sentence>
< sentence type="normal" elementid="20050761w-s161">
19 .
< /sentence>
< sentence type="heading" elementid="20050761w-s162">
< a type="href" elementid="20050761w-a48">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p69">
< sentence type="normal" elementid="20050761w-s163">
< squarebracket elementid="20050761w-sb46">
[  19 ]
< /squarebracket>
1998 p .
< /sentence>
< sentence type="normal" elementid="20050761w-s164">
18 .
< /sentence>
< sentence type="heading" elementid="20050761w-s165">
< a type="href" elementid="20050761w-a49">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p70">
< sentence type="normal" elementid="20050761w-s166">
< squarebracket elementid="20050761w-sb47">
[  20 ]
< /squarebracket>
2004 p .
< /sentence>
< sentence type="normal" elementid="20050761w-s167">
23 .
< /sentence>
< sentence type="heading" elementid="20050761w-s168">
< a type="href" elementid="20050761w-a50">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p71">
< sentence type="normal" elementid="20050761w-s169">
< squarebracket elementid="20050761w-sb48">
[  21 ]
< /squarebracket>
1988 p.40 .
< /sentence>
< sentence type="heading" elementid="20050761w-s170">
< a type="href" elementid="20050761w-a51">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p72">
< sentence type="normal" elementid="20050761w-s171">
< squarebracket elementid="20050761w-sb49">
[  22 ]
< /squarebracket>
1996 p .
< /sentence>
< sentence type="normal" elementid="20050761w-s172">
56 .
< /sentence>
< sentence type="heading" elementid="20050761w-s173">
< a type="href" elementid="20050761w-a52">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p73">
< sentence type="normal" elementid="20050761w-s174">
< squarebracket elementid="20050761w-sb50">
[  23 ]
< /squarebracket>
1992 p .
< /sentence>
< sentence type="normal" elementid="20050761w-s175">
13 .
< /sentence>
< sentence type="heading" elementid="20050761w-s176">
< a type="href" elementid="20050761w-a53">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p74">
< sentence type="normal" elementid="20050761w-s177">
< squarebracket elementid="20050761w-sb51">
[  24 ]
< /squarebracket>
1997 p.44 .
< /sentence>
< sentence type="heading" elementid="20050761w-s178">
< a type="href" elementid="20050761w-a54">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p75">
< sentence type="normal" elementid="20050761w-s179">
< squarebracket elementid="20050761w-sb52">
[  25 ]
< /squarebracket>
1997 p.65 .
< /sentence>
< sentence type="heading" elementid="20050761w-s180">
< a type="href" elementid="20050761w-a55">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p76">
< sentence type="normal" elementid="20050761w-s181">
< squarebracket elementid="20050761w-sb53">
[  26 ]
< /squarebracket>
1999 p.8 .
< /sentence>
< sentence type="heading" elementid="20050761w-s182">
< a type="href" elementid="20050761w-a56">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p77">
< sentence type="normal" elementid="20050761w-s183">
< squarebracket elementid="20050761w-sb54">
[  27 ]
< /squarebracket>
2002 p .
< /sentence>
< sentence type="normal" elementid="20050761w-s184">
17 .
< /sentence>
< sentence type="heading" elementid="20050761w-s185">
< a type="href" elementid="20050761w-a57">
back
< /a>
< /sentence>
< /p>
< p elementid="20050761w-p78">
< sentence type="heading" elementid="20050761w-s186">
< a type="href" elementid="20050761w-a58">
English version
< /a>
< /sentence>
< sentence type="heading" elementid="20050761w-s187">
ISBN 0 11 091098 2
< /sentence>
< /p>
< table elementid="20050761w-tb4">
< td elementid="20050761w-td23">
< p elementid="20050761w-p79">
< sentence type="heading" elementid="20050761w-s188">
< a type="href" elementid="20050761w-a59">
Other UK SIs
< /a>
< a type="href" elementid="20050761w-a60">
Home
< /a>
< a type="href" elementid="20050761w-a61">
National Assembly for Wales Statutory Instruments
< /a>
< a type="href" elementid="20050761w-a62">
Scottish Statutory Instruments
< /a>
< a type="href" elementid="20050761w-a63">
Statutory Rules of Northern Ireland
< /a>
< a type="href" elementid="20050761w-a64">
Her Majesty 's Stationery Office
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20050761w-td24">
< p elementid="20050761w-p80">
< sentence type="heading" elementid="20050761w-s189">
< strong elementid="20050761w-st11">
We welcome your 
< a type="href" elementid="20050761w-a65">
comments
< /a>
on this site
< /strong>
< /sentence>
< /p>
< /td>
< td elementid="20050761w-td25">
< p elementid="20050761w-p81">
< sentence type="heading" elementid="20050761w-s190">
© Crown copyright 2005
< /sentence>
< /p>
< /td>
< td elementid="20050761w-td26">
< p elementid="20050761w-p82">
< sentence type="heading" elementid="20050761w-s191">
Prepared 22 March 2005
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>1930< /wordcount>
< /file>
< file elementid="20051911w.xml">
< metadata>
< generator>HTML Tidy for Windows (vers 12 April 2005), see www.w3.org< /generator>
< pagetitle>Gorchymyn Deddf Archwilio Cyhoeddus ( Cymru ) 2004 ( Cychwyn Rhif 3 ) 2005< /pagetitle>
< doc_class>Statutory Instrument - Wales< /doc_class>
< year>2005< /year>
< number>1911< /number>
< legislation_code>30102< /legislation_code>
< legislation_type>Statutory Instrument - Wales< /legislation_type>
< subject>ARCHWILIO CYHOEDDUS, CYMRU A LLOEGR< /subject>
< laid_date>_EMPTY< /laid_date>
< made_date>20050712< /made_date>
< coming_into_force>_EMPTY< /coming_into_force>
< isbn>0110911806< /isbn>
< title>Gorchymyn Deddf Archwilio Cyhoeddus (Cymru) 2004 (Cychwyn Rhif 3) 2005< /title>
< signatory>D. Elis-Thomas< /signatory>
< signatory_title>Llywydd y Cynulliad Cenedlaethol< /signatory_title>
< GENERATOR>MSHTML 6.00.2900.2963< /GENERATOR>
< /metadata>
< text>
< p elementid="20051911w-p1">
< sentence type="heading" elementid="20051911w-s1">
Offerynnau Statudol 2005 Rhif 1911
< roundbracket elementid="20051911w-rb1">
(  Cy.154 )
< /roundbracket>
< roundbracket elementid="20051911w-rb2">
(  C.83 )
< /roundbracket>
< /sentence>
< /p>
< p elementid="20051911w-p2">
< sentence type="heading" elementid="20051911w-s2">
< strong elementid="20051911w-st1">
Gorchymyn Deddf Archwilio Cyhoeddus
< roundbracket elementid="20051911w-rb3">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20051911w-rb4">
(  Cychwyn Rhif 3 )
< /roundbracket>
2005
< /strong>
< /sentence>
< /p>
< p elementid="20051911w-p3">
< sentence type="heading" elementid="20051911w-s3">
© Hawlfraint y Goron 2005
< /sentence>
< /p>
< p elementid="20051911w-p4">
< sentence type="normal" elementid="20051911w-s4">
Caiff Offerynnau Statudol o 'r wefan hon eu hargraffu o dan oruchwyliaeth ac awdurdod Rheolwr Gwasg ei Mawrhydi sef Argraffydd Ddeddfau Senedd y Frenhines .
< /sentence>
< /p>
< p elementid="20051911w-p5">
< sentence type="normal" elementid="20051911w-s5">
Mae 'r ddeddfwriaeth a gynhwysir ar y wefan hon yn gaeth i ddiogelwch Hawlfraint y Goron .
< /sentence>
< sentence type="normal" elementid="20051911w-s6">
Gellir ei atgynhyrchu yn rhad ac am ddim cyn belled â 'i fod wedi ei atgynhyrchu 'n gywir a bod ffynhonnell a statws hawlfraint y deunydd wedi eu nodi 'n glir i 'r defnyddiwr .
< /sentence>
< /p>
< p elementid="20051911w-p6">
< sentence type="normal" elementid="20051911w-s7">
Dylid nodi nad yw 'r hawl i atgynhyrchu testun Offerynnau Statudol yn ymestyn i wasgnodau Argraffydd y Frenhines a dylid eu tynnu oddi ar unrhyw gopïau o 'r Offerynnau Statudol gaiff eu cyhoeddi neu fydd ar gael i’r cyhoedd .
< /sentence>
< sentence type="normal" elementid="20051911w-s8">
Mae hyn yn cynnwys atgynhyrchu yr Offeryn Statudol ar y Rhyngrwyd ac ar safleoedd mewnrwyd .
< /sentence>
< sentence type="normal" elementid="20051911w-s9">
Gellir ond atgynhyrchu Bathodyn Brenhinol Cymru pan ei fod yn rhan annatod o 'r ddogfen wreiddiol .
< /sentence>
< /p>
< p elementid="20051911w-p7">
< sentence type="normal" elementid="20051911w-s10">
Paratowyd testun y fersiwn Rhyngrwyd yma o Offeryn Statutdol i adlewyrchu 'r testun fel y cafodd ei Wneud .
< /sentence>
< sentence type="normal" elementid="20051911w-s11">
Mae fersiwn brint hefyd ar gael ac mae wedi ei chyhoeddi gan The Stationery Office Limited fel
< strong elementid="20051911w-st2">
Gorchymyn Deddf Archwilio Cyhoeddus
< roundbracket elementid="20051911w-rb5">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20051911w-rb6">
(  Cychwyn Rhif 3 )
< /roundbracket>
2005
< /strong>
, ISBN 0110911806 .
< /sentence>
< sentence type="normal" elementid="20051911w-s12">
Gellir prynu 'r fersiwn brint trwy glicio 
< a type="href" elementid="20051911w-a1">
yma
< /a> .
< /sentence>
< sentence type="normal" elementid="20051911w-s13">
Gellir hefyd prynu copïau Braille o 'r Offeryn Statudol yma am yr un pris â 'r argraffiad print drwy gysylltu â Gwasanaethau Cwsmeriaid TSO ar 0870 600 5522 neu ar e-bost : 
< a type="href" elementid="20051911w-a2">
customer.services@tso.co.uk
< /a> .
< /sentence>
< /p>
< p elementid="20051911w-p8">
< sentence type="normal" elementid="20051911w-s14">
Gellir cael rhagor o wybodaeth am gyhoeddiad deddfwriaeth ar y wefan hon drwy gyfeirio at 
< a type="href" elementid="20051911w-a3">
Frequently Asked Questions
< /a> .
< /sentence>
< /p>
< p elementid="20051911w-p9">
< sentence type="normal" elementid="20051911w-s15">
I sicrhau mynediad cyflym dros gysylltiadau araf , mae dogfennau mawr wedi eu rhannu i
< doublequotation elementid="20051911w-dq1">
"  ddarnau "
< /doublequotation> .
< /sentence>
< sentence type="normal" elementid="20051911w-s16">
Pan welwch fotwm
< doublequotation elementid="20051911w-dq2">
"  continue "
< /doublequotation>
ar ddiwedd tudalen o destun , mae hyn yn dynodi bod darn arall o destun ar gael .
< /sentence>
< /p>
< p elementid="20051911w-p10">
< sentence type="heading" elementid="20051911w-s17">
OFFERYNNAU STATUDOL
< /sentence>
< sentence type="heading" elementid="20051911w-s18">
< strong elementid="20051911w-st3">
2005 Rhif 1911
< roundbracket elementid="20051911w-rb7">
(  Cy.154 )
< /roundbracket>
< roundbracket elementid="20051911w-rb8">
(  C.83 )
< /roundbracket>
< /strong>
< /sentence>
< sentence type="heading" elementid="20051911w-s19">
< strong elementid="20051911w-st4">
ARCHWILIO CYHOEDDUS , CYMRU A LLOEGR
< /strong>
< /sentence>
< sentence type="heading" elementid="20051911w-s20">
Gorchymyn Deddf Archwilio Cyhoeddus
< roundbracket elementid="20051911w-rb9">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20051911w-rb10">
(  Cychwyn Rhif 3 )
< /roundbracket>
2005
< /sentence>
< /p>
< table elementid="20051911w-tb1">
< td elementid="20051911w-td1">
< p elementid="20051911w-p11">
< sentence type="heading" elementid="20051911w-s21">
< em elementid="20051911w-em1">
Wedi 'i wneud
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td2">
< p elementid="20051911w-p12">
< sentence type="heading" elementid="20051911w-s22">
< em elementid="20051911w-em2">
12 Gorffennaf 2005
< /em>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20051911w-p13">
< sentence type="heading" elementid="20051911w-s23">
Mae Cynulliad Cenedlaethol Cymru , drwy arfer y pwerau a roddwyd iddo gan adran 73 o Ddeddf Archwilio Cyhoeddus
< roundbracket elementid="20051911w-rb11">
(  Cymru )
< /roundbracket>
2004
< squarebracket elementid="20051911w-sb1">
[ 
< a type="href" elementid="20051911w-a4">
1
< /a>
]
< /squarebracket>
, drwy hyn yn gwneud y Gorchymyn a ganlyn :
< /sentence>
< sentence type="heading" elementid="20051911w-s24">
< strong elementid="20051911w-st5">
Enwi a dehongli
< /strong>
< /sentence>
< sentence type="normal" elementid="20051911w-s25">
1 .
< /sentence>
< sentence type="normal" elementid="20051911w-s26">
—
< roundbracket elementid="20051911w-rb12">
(  1 )
< /roundbracket>
Enw 'r Gorchymyn hwn yw Gorchymyn Deddf Archwilio Cyhoeddus
< roundbracket elementid="20051911w-rb13">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20051911w-rb14">
(  Cychwyn Rhif 3 )
< /roundbracket>
2005 .
< /sentence>
< sentence type="normal" elementid="20051911w-s27">
< roundbracket elementid="20051911w-rb15">
(  2 )
< /roundbracket>
Yn y Gorchymyn hwn ystyr
< doublequotation elementid="20051911w-dq3">
"  y Ddeddf "
< /doublequotation>
< roundbracket elementid="20051911w-rb16">
(
< doublequotation elementid="20051911w-dq4">
"
< em elementid="20051911w-em3">
the Act
< /em>
"
< /doublequotation>
)
< /roundbracket>
yw Deddf Archwilio Cyhoeddus
< roundbracket elementid="20051911w-rb17">
(  Cymru )
< /roundbracket>
2004 .
< /sentence>
< sentence type="normal" elementid="20051911w-s28">
< roundbracket elementid="20051911w-rb18">
(  3 )
< /roundbracket>
Mae cyfeiriadau at adrannau ac Atodlenni , oni nodir yn wahanol , yn gyfeiriadau at adrannau o 'r Ddeddf ac Atodlenni iddi .
< /sentence>
< sentence type="heading" elementid="20051911w-s29">
< strong elementid="20051911w-st6">
Darpariaethau sy 'n dod i rym ar 20 Gorffennaf 2005
< /strong>
< /sentence>
< sentence type="normal" elementid="20051911w-s30">
2 .
< /sentence>
< sentence type="normal" elementid="20051911w-s31">
Daw darpariaethau 'r Ddeddf a bennir yr Atodlen i 'r Gorchymyn hwn i rym at bob diben ar 20 Gorffennaf 2005 .
< /sentence>
< sentence type="normal" elementid="20051911w-s32">
Llofnodwyd ar ran y Cynulliad Cenedlaethol o dan adran 66
< roundbracket elementid="20051911w-rb19">
(  1 )
< /roundbracket>
o Ddeddf Llywodraeth Cymru 1998
< squarebracket elementid="20051911w-sb2">
[ 
< a type="href" elementid="20051911w-a5">
2
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="heading" elementid="20051911w-s33">
< em elementid="20051911w-em4">
D. Elis-Thomas
< /em>
< /sentence>
< sentence type="heading" elementid="20051911w-s34">
Llywydd y Cynulliad Cenedlaethol
< /sentence>
< sentence type="heading" elementid="20051911w-s35">
12 Gorffennaf 2005
< /sentence>
< sentence type="heading" elementid="20051911w-s36">
< strong elementid="20051911w-st7">
YR ATODLEN
< /strong>
Erthygl 2
< /sentence>
< sentence type="heading" elementid="20051911w-s37">
< strong elementid="20051911w-st8">
Darpariaethau sy 'n dod i rym ar 20 Gorffennaf 2005
< /strong>
< /sentence>
< sentence type="normal" elementid="20051911w-s38">
Is-adrannau
< roundbracket elementid="20051911w-rb20">
(  1 )
< /roundbracket>
i
< roundbracket elementid="20051911w-rb21">
(  5 )
< /roundbracket>
o adran 54
< squarebracket elementid="20051911w-sb3">
[ 
< a type="href" elementid="20051911w-a6">
3
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="normal" elementid="20051911w-s39">
Adran 54A
< squarebracket elementid="20051911w-sb4">
[ 
< a type="href" elementid="20051911w-a7">
4
< /a>
]
< /squarebracket> .
< /sentence>
< sentence type="heading" elementid="20051911w-s40">
NODYN ESBONIADOL
< /sentence>
< sentence type="heading" elementid="20051911w-s41">
< em elementid="20051911w-em5">
< roundbracket elementid="20051911w-rb22">
(  Nid yw 'r nodyn hwn yn rhan o 'r Gorchymyn )
< /roundbracket>
< /em>
< /sentence>
< sentence type="normal" elementid="20051911w-s42">
Mae 'r Gorchymyn hwn yn dod â 'r darpariaethau hynny yn Neddf Archwilio Cyhoeddus
< roundbracket elementid="20051911w-rb23">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20051911w-rb24">
(
< doublequotation elementid="20051911w-dq5">
"  y Ddeddf "
< /doublequotation>
)
< /roundbracket>
nad ydynt eisoes mewn grym i rym ar 20 Gorffennaf 2005 yng Nghymru a Lloegr .
< /sentence>
< sentence type="normal" elementid="20051911w-s43">
Y darpariaethau yw is-adrannau
< roundbracket elementid="20051911w-rb25">
(  1 )
< /roundbracket>
i
< roundbracket elementid="20051911w-rb26">
(  5 )
< /roundbracket>
o adran 54 , ac adran 54A o 'r Ddeddf .
< /sentence>
< sentence type="normal" elementid="20051911w-s44">
Mae adrannau 54 a 54A ill dwy yn rheoleiddio datgelu gwybodaeth .
< /sentence>
< sentence type="normal" elementid="20051911w-s45">
Mae adran 54 o 'r Ddeddf yn gymwys i bersonau nad ydynt yn awdurdodau cyhoeddus at ddibenion Deddf Rhyddid Gwybodaeth 2000
< roundbracket elementid="20051911w-rb27">
(
< doublequotation elementid="20051911w-dq6">
"  Deddf 2000 "
< /doublequotation>
)
< /roundbracket>
, ac nad ydynt yn gweithredu ar eu rhan .
< /sentence>
< sentence type="normal" elementid="20051911w-s46">
Mae 'n cyfyngu ar yr amgylchiadau pryd y caiff y personau hynny ddatgelu gwybodaeth benodol a geir gan Archwilydd Cyffredinol Cymru , neu archwilydd a benodir ganddo o dan adran 13 o 'r Ddeddf
< roundbracket elementid="20051911w-rb28">
(
< doublequotation elementid="20051911w-dq7">
"  archwilydd "
< /doublequotation>
)
< /roundbracket> .
< /sentence>
< sentence type="heading" elementid="20051911w-s47">
Yr wybodaeth dan sylw yw gwybodaeth a geir gan Archwilydd Cyffredinol Cymru , neu archwilydd , wrth arfer ei swyddogaethau o dan :
< /sentence>
< sentence type="normal" elementid="20051911w-s48">
— Rhan 2 o 'r Ddeddf ; — Rhan 1 o Ddeddf Llywodraeth Leol 1999
< roundbracket elementid="20051911w-rb29">
(  swyddogaethau o ran y drefn gwerth gorau ar gyfer llywodraeth leol )
< /roundbracket>
; neu — adran 145C o Ddeddf Llywodraeth Cymru 1988
< roundbracket elementid="20051911w-rb30">
(  a fewnosodwyd gan adran 5 o 'r Ddeddf )
< /roundbracket>
sy 'n ymdrin ag astudiaethau sy 'n ymwneud â landlordiaid cymdeithasol cofrestredig yng Nghymru .
< /sentence>
< /p>
< p elementid="20051911w-p14">
< sentence type="normal" elementid="20051911w-s49">
Mae 'r cyfyngiadau yn adran 54 yn gweithredu drwy waharddiad cyffredinol ar ddatgelu , yn ddarostyngedig i eithriadau eang iawn , a roddir yn is-adran
< roundbracket elementid="20051911w-rb31">
(  2 )
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20051911w-s50">
Datgelu at ddibenion unrhyw swyddogaeth Archwilydd Cyffredinol Cymru , neu archwilydd , o dan Ran 2 o 'r Ddeddf neu Ran 1 o Ddeddf Llywodraeth Leol 1999 yw un o 'r eithriadau .
< /sentence>
< sentence type="normal" elementid="20051911w-s51">
Os datgelir gwybodaeth heblaw o dan un o 'r eithriadau yn is-adran
< roundbracket elementid="20051911w-rb32">
(  2 )
< /roundbracket>
, mae 'n bosibl y bydd y person sy 'n datgelu yn agored i gael ei garcharu am ddwy flynedd ac/ neu ddirwy .
< /sentence>
< sentence type="normal" elementid="20051911w-s52">
Mae adran 54A yn gymwys i bersonau sy 'n awdurdodau cyhoeddus at ddibenion Deddf 2000 neu 'n gweithredu ar eu rhan .
< /sentence>
< sentence type="normal" elementid="20051911w-s53">
Caiff y personau hyn ddatgelu gwybodaeth y byddai adran 54 yn gwahardd personau eraill rhag ei datgelu .
< /sentence>
< sentence type="normal" elementid="20051911w-s54">
Yr unig amgylchiad pryd y mae 'n anghyfreithlon i awdurdod cyhoeddus o dan Ddeddf 2000 , neu berson sy 'n gweithredu ar ran awdurdod cyhoeddus o 'r fath , ddatgelu 'r wybodaeth honno yw pryd y byddai 'r datgelu yn debygol o ragfarnu perfformiad effeithiol swyddogaeth statudol gan berson o 'r fath .
< /sentence>
< sentence type="normal" elementid="20051911w-s55">
Os datgelir gwybodaeth gan dorri adran 54A bydd y person sy 'n datgleu yn agored i ddirwy ond nid i garchar .
< /sentence>
< sentence type="normal" elementid="20051911w-s56">
Diwygiwyd adran 54 , a mewnosodwyd adran 54A yn y Ddeddf gan Gorchymyn Deddf Archwilio Cyhoeddus
< roundbracket elementid="20051911w-rb33">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20051911w-rb34">
(  Llacio 'r Cyfyngiad ar Ddatgelu )
< /roundbracket>
2005
< roundbracket elementid="20051911w-rb35">
(  O.S. 2005/ 1018 )
< /roundbracket> .
< /sentence>
< sentence type="normal" elementid="20051911w-s57">
Daethpwyd ag is-adrannau
< roundbracket elementid="20051911w-rb36">
(  6 )
< /roundbracket>
i
< roundbracket elementid="20051911w-rb37">
(  8 )
< /roundbracket>
o adran 54 i rym ar 31 Ionawr 2005 i alluogi gwneud y Gorchymyn hwnnw .
< /sentence>
< /p>
< p elementid="20051911w-p15">
< sentence type="heading" elementid="20051911w-s58">
< strong elementid="20051911w-st9">
NOTE AS TO EARLIER COMMENCEMENT ORDERS
< /strong>
< /sentence>
< sentence type="heading" elementid="20051911w-s59">
< em elementid="20051911w-em6">
< roundbracket elementid="20051911w-rb38">
(  Nid yw 'r nodyn hwn yn rhan o 'r Gorchymyn )
< /roundbracket>
< /em>
< /sentence>
< sentence type="heading" elementid="20051911w-s60">
Mae darpariaethau canlynol y Ddeddf wedi 'u dwyn i rym drwy orchymyn cychwyn a wnaed cyn dyddiad y Gorchymyn hwn :
< /sentence>
< /p>
< table elementid="20051911w-tb2">
< td elementid="20051911w-td3">
< p elementid="20051911w-p16">
< sentence type="heading" elementid="20051911w-s61">
< em elementid="20051911w-em7">
Y Ddarpariaeth
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td4">
< p elementid="20051911w-p17">
< sentence type="heading" elementid="20051911w-s62">
< em elementid="20051911w-em8">
Y Dyddiad Cychwyn
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td5">
< p elementid="20051911w-p18">
< sentence type="heading" elementid="20051911w-s63">
< em elementid="20051911w-em9">
Rhif O.S.
< /em>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td6">
< p elementid="20051911w-p19">
< sentence type="heading" elementid="20051911w-s64">
Adrannau 1-6
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td7">
< p elementid="20051911w-p20">
< sentence type="heading" elementid="20051911w-s65">
1 Ebrill 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td8">
< p elementid="20051911w-p21">
< sentence type="heading" elementid="20051911w-s66">
2005 Rhif 558
< roundbracket elementid="20051911w-rb39">
(  Cy.48 )
< /roundbracket>
< roundbracket elementid="20051911w-rb40">
(  C.24 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td9">
< p elementid="20051911w-p22">
< sentence type="heading" elementid="20051911w-s67">
Adran 7
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td10">
< p elementid="20051911w-p23">
< sentence type="heading" elementid="20051911w-s68">
1 Ebrill 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td11">
< p elementid="20051911w-p24">
< sentence type="heading" elementid="20051911w-s69">
2005 Rhif 558
< roundbracket elementid="20051911w-rb41">
(  Cy.48 )
< /roundbracket>
< roundbracket elementid="20051911w-rb42">
(  C.24 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td12">
< p elementid="20051911w-p25">
< sentence type="heading" elementid="20051911w-s70">
Adrannau 8-11
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td13">
< p elementid="20051911w-p26">
< sentence type="heading" elementid="20051911w-s71">
1 Ebrill 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td14">
< p elementid="20051911w-p27">
< sentence type="heading" elementid="20051911w-s72">
2005 Rhif 558
< roundbracket elementid="20051911w-rb43">
(  Cy.48 )
< /roundbracket>
< roundbracket elementid="20051911w-rb44">
(  C.24 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td15">
< p elementid="20051911w-p28">
< sentence type="heading" elementid="20051911w-s73">
Adran 12
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td16">
< p elementid="20051911w-p29">
< sentence type="heading" elementid="20051911w-s74">
31 Ionawr 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td17">
< p elementid="20051911w-p30">
< sentence type="heading" elementid="20051911w-s75">
2005 Rhif 71
< roundbracket elementid="20051911w-rb45">
(  Cy.9 )
< /roundbracket>
< roundbracket elementid="20051911w-rb46">
(  C.3 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td18">
< p elementid="20051911w-p31">
< sentence type="heading" elementid="20051911w-s76">
Adrannau 13-15
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td19">
< p elementid="20051911w-p32">
< sentence type="heading" elementid="20051911w-s77">
1 Ebrill 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td20">
< p elementid="20051911w-p33">
< sentence type="heading" elementid="20051911w-s78">
2005 Rhif 558
< roundbracket elementid="20051911w-rb47">
(  Cy.48 )
< /roundbracket>
< roundbracket elementid="20051911w-rb48">
(  C.24 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td21">
< p elementid="20051911w-p34">
< sentence type="heading" elementid="20051911w-s79">
Adran 16
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td22">
< p elementid="20051911w-p35">
< sentence type="heading" elementid="20051911w-s80">
31 Ionawr 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td23">
< p elementid="20051911w-p36">
< sentence type="heading" elementid="20051911w-s81">
2005 Rhif 71
< roundbracket elementid="20051911w-rb49">
(  Cy.9 )
< /roundbracket>
< roundbracket elementid="20051911w-rb50">
(  C.3 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td24">
< p elementid="20051911w-p37">
< sentence type="heading" elementid="20051911w-s82">
Adrannau 17-19
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td25">
< p elementid="20051911w-p38">
< sentence type="heading" elementid="20051911w-s83">
1 Ebrill 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td26">
< p elementid="20051911w-p39">
< sentence type="heading" elementid="20051911w-s84">
2005 Rhif 558
< roundbracket elementid="20051911w-rb51">
(  Cy.48 )
< /roundbracket>
< roundbracket elementid="20051911w-rb52">
(  C.24 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td27">
< p elementid="20051911w-p40">
< sentence type="heading" elementid="20051911w-s85">
Is-adrannau
< roundbracket elementid="20051911w-rb53">
(  1 )
< /roundbracket>
i
< roundbracket elementid="20051911w-rb54">
(  3 )
< /roundbracket>
o adran 20
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td28">
< p elementid="20051911w-p41">
< sentence type="heading" elementid="20051911w-s86">
31 Ionawr 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td29">
< p elementid="20051911w-p42">
< sentence type="heading" elementid="20051911w-s87">
2005 Rhif 71
< roundbracket elementid="20051911w-rb55">
(  Cy.9 )
< /roundbracket>
< roundbracket elementid="20051911w-rb56">
(  C.3 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td30">
< p elementid="20051911w-p43">
< sentence type="heading" elementid="20051911w-s88">
Is-adrannau
< roundbracket elementid="20051911w-rb57">
(  4 )
< /roundbracket>
i
< roundbracket elementid="20051911w-rb58">
(  6 )
< /roundbracket>
o adran 20
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td31">
< p elementid="20051911w-p44">
< sentence type="heading" elementid="20051911w-s89">
1 Ebrill 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td32">
< p elementid="20051911w-p45">
< sentence type="heading" elementid="20051911w-s90">
2005 Rhif 558
< roundbracket elementid="20051911w-rb59">
(  Cy.48 )
< /roundbracket>
< roundbracket elementid="20051911w-rb60">
(  C.24 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td33">
< p elementid="20051911w-p46">
< sentence type="heading" elementid="20051911w-s91">
Is-adrannau
< roundbracket elementid="20051911w-rb61">
(  1 )
< /roundbracket>
,
< roundbracket elementid="20051911w-rb62">
(  2 )
< /roundbracket>
a
< roundbracket elementid="20051911w-rb63">
(  5 )
< /roundbracket>
o adran 21
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td34">
< p elementid="20051911w-p47">
< sentence type="heading" elementid="20051911w-s92">
1 Ionawr 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td35">
< p elementid="20051911w-p48">
< sentence type="heading" elementid="20051911w-s93">
2005 Rhif 71
< roundbracket elementid="20051911w-rb64">
(  Cy.9 )
< /roundbracket>
< roundbracket elementid="20051911w-rb65">
(  C.3 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td36">
< p elementid="20051911w-p49">
< sentence type="heading" elementid="20051911w-s94">
Is-adrannau
< roundbracket elementid="20051911w-rb66">
(  3 )
< /roundbracket>
a
< roundbracket elementid="20051911w-rb67">
(  4 )
< /roundbracket>
o adran 21
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td37">
< p elementid="20051911w-p50">
< sentence type="heading" elementid="20051911w-s95">
1 Ebrill 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td38">
< p elementid="20051911w-p51">
< sentence type="heading" elementid="20051911w-s96">
2005 Rhif 558
< roundbracket elementid="20051911w-rb68">
(  Cy.48 )
< /roundbracket>
< roundbracket elementid="20051911w-rb69">
(  C.24 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td39">
< p elementid="20051911w-p52">
< sentence type="heading" elementid="20051911w-s97">
Adrannau 22-38
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td40">
< p elementid="20051911w-p53">
< sentence type="heading" elementid="20051911w-s98">
1 Ebrill 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td41">
< p elementid="20051911w-p54">
< sentence type="heading" elementid="20051911w-s99">
2005 Rhif 558
< roundbracket elementid="20051911w-rb70">
(  Cy.48 )
< /roundbracket>
< roundbracket elementid="20051911w-rb71">
(  C.24 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td42">
< p elementid="20051911w-p55">
< sentence type="heading" elementid="20051911w-s100">
Adran 39
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td43">
< p elementid="20051911w-p56">
< sentence type="heading" elementid="20051911w-s101">
1 Ebrill 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td44">
< p elementid="20051911w-p57">
< sentence type="heading" elementid="20051911w-s102">
2005 Rhif 558
< roundbracket elementid="20051911w-rb72">
(  Cy.48 )
< /roundbracket>
< roundbracket elementid="20051911w-rb73">
(  C.24 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td45">
< p elementid="20051911w-p58">
< sentence type="heading" elementid="20051911w-s103">
Adrannau 40-49
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td46">
< p elementid="20051911w-p59">
< sentence type="heading" elementid="20051911w-s104">
1 Ebrill 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td47">
< p elementid="20051911w-p60">
< sentence type="heading" elementid="20051911w-s105">
2005 Rhif 558
< roundbracket elementid="20051911w-rb74">
(  Cy.48 )
< /roundbracket>
< roundbracket elementid="20051911w-rb75">
(  C.24 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td48">
< p elementid="20051911w-p61">
< sentence type="heading" elementid="20051911w-s106">
Adran 50 ac Atodlen 1
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td49">
< p elementid="20051911w-p62">
< sentence type="heading" elementid="20051911w-s107">
1 Ebrill 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td50">
< p elementid="20051911w-p63">
< sentence type="heading" elementid="20051911w-s108">
2005 Rhif 558
< roundbracket elementid="20051911w-rb76">
(  Cy.48 )
< /roundbracket>
< roundbracket elementid="20051911w-rb77">
(  C.24 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td51">
< p elementid="20051911w-p64">
< sentence type="heading" elementid="20051911w-s109">
Adrannau 51-53
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td52">
< p elementid="20051911w-p65">
< sentence type="heading" elementid="20051911w-s110">
1 Ebrill 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td53">
< p elementid="20051911w-p66">
< sentence type="heading" elementid="20051911w-s111">
2005 Rhif 558
< roundbracket elementid="20051911w-rb78">
(  Cy.48 )
< /roundbracket>
< roundbracket elementid="20051911w-rb79">
(  C.24 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td54">
< p elementid="20051911w-p67">
< sentence type="heading" elementid="20051911w-s112">
Is-adrannau
< roundbracket elementid="20051911w-rb80">
(  6 )
< /roundbracket>
i
< roundbracket elementid="20051911w-rb81">
(  8 )
< /roundbracket>
o adran 54
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td55">
< p elementid="20051911w-p68">
< sentence type="heading" elementid="20051911w-s113">
31 Ionawr 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td56">
< p elementid="20051911w-p69">
< sentence type="heading" elementid="20051911w-s114">
2005 Rhif 71
< roundbracket elementid="20051911w-rb82">
(  Cy.9 )
< /roundbracket>
< roundbracket elementid="20051911w-rb83">
(  C.3 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td57">
< p elementid="20051911w-p70">
< sentence type="heading" elementid="20051911w-s115">
Adrannau 55-65
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td58">
< p elementid="20051911w-p71">
< sentence type="heading" elementid="20051911w-s116">
1 Ebrill 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td59">
< p elementid="20051911w-p72">
< sentence type="heading" elementid="20051911w-s117">
2005 Rhif 558
< roundbracket elementid="20051911w-rb84">
(  Cy.48 )
< /roundbracket>
< roundbracket elementid="20051911w-rb85">
(  C.24 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td60">
< p elementid="20051911w-p73">
< sentence type="heading" elementid="20051911w-s118">
Adran 58
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td61">
< p elementid="20051911w-p74">
< sentence type="heading" elementid="20051911w-s119">
31 Ionawr 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td62">
< p elementid="20051911w-p75">
< sentence type="heading" elementid="20051911w-s120">
2005 Rhif 71
< roundbracket elementid="20051911w-rb86">
(  Cy.9 )
< /roundbracket>
< roundbracket elementid="20051911w-rb87">
(  C.3 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td63">
< p elementid="20051911w-p76">
< sentence type="heading" elementid="20051911w-s121">
Adran 59
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td64">
< p elementid="20051911w-p77">
< sentence type="heading" elementid="20051911w-s122">
31 Ionawr 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td65">
< p elementid="20051911w-p78">
< sentence type="heading" elementid="20051911w-s123">
2005 Rhif 71
< roundbracket elementid="20051911w-rb88">
(  Cy.9 )
< /roundbracket>
< roundbracket elementid="20051911w-rb89">
(  C.3 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td66">
< p elementid="20051911w-p79">
< sentence type="heading" elementid="20051911w-s124">
Adran 66 ac Atodlen 2
< roundbracket elementid="20051911w-rb90">
(  gydag arbedion )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td67">
< p elementid="20051911w-p80">
< sentence type="heading" elementid="20051911w-s125">
1 Ebrill 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td68">
< p elementid="20051911w-p81">
< sentence type="heading" elementid="20051911w-s126">
2005 Rhif 558
< roundbracket elementid="20051911w-rb91">
(  Cy.48 )
< /roundbracket>
< roundbracket elementid="20051911w-rb92">
(  C.24 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td69">
< p elementid="20051911w-p82">
< sentence type="heading" elementid="20051911w-s127">
Adran 67
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td70">
< p elementid="20051911w-p83">
< sentence type="heading" elementid="20051911w-s128">
1 Ebrill 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td71">
< p elementid="20051911w-p84">
< sentence type="heading" elementid="20051911w-s129">
2005 Rhif 558
< roundbracket elementid="20051911w-rb93">
(  Cy.48 )
< /roundbracket>
< roundbracket elementid="20051911w-rb94">
(  C.24 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td72">
< p elementid="20051911w-p85">
< sentence type="heading" elementid="20051911w-s130">
Adran 68 ac Atodlen 3
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td73">
< p elementid="20051911w-p86">
< sentence type="heading" elementid="20051911w-s131">
31 Ionawr 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td74">
< p elementid="20051911w-p87">
< sentence type="heading" elementid="20051911w-s132">
2005 Rhif 71
< roundbracket elementid="20051911w-rb95">
(  Cy.9 )
< /roundbracket>
< roundbracket elementid="20051911w-rb96">
(  C.3 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td75">
< p elementid="20051911w-p88">
< sentence type="heading" elementid="20051911w-s133">
Adrannau 69-70
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td76">
< p elementid="20051911w-p89">
< sentence type="heading" elementid="20051911w-s134">
1 Ebrill 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td77">
< p elementid="20051911w-p90">
< sentence type="heading" elementid="20051911w-s135">
2005 Rhif 558
< roundbracket elementid="20051911w-rb97">
(  Cy.48 )
< /roundbracket>
< roundbracket elementid="20051911w-rb98">
(  C.24 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td78">
< p elementid="20051911w-p91">
< sentence type="heading" elementid="20051911w-s136">
Adran 72 ac Atodlen 4
< roundbracket elementid="20051911w-rb99">
(  gydag arbedion )
< /roundbracket>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td79">
< p elementid="20051911w-p92">
< sentence type="heading" elementid="20051911w-s137">
1 Ebrill 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td80">
< p elementid="20051911w-p93">
< sentence type="heading" elementid="20051911w-s138">
2005 Rhif 558
< roundbracket elementid="20051911w-rb100">
(  Cy.48 )
< /roundbracket>
< roundbracket elementid="20051911w-rb101">
(  C.24 )
< /roundbracket>
< /sentence>
< /p>
< /td>
< /table>
< p elementid="20051911w-p94">
< sentence type="heading" elementid="20051911w-s139">
< em elementid="20051911w-em10">
Notes :
< /em>
< /sentence>
< sentence type="normal" elementid="20051911w-s140">
< squarebracket elementid="20051911w-sb5">
[  1 ]
< /squarebracket>
2004 p.23 .
< /sentence>
< sentence type="heading" elementid="20051911w-s141">
< a type="href" elementid="20051911w-a8">
back
< /a>
< /sentence>
< /p>
< p elementid="20051911w-p95">
< sentence type="normal" elementid="20051911w-s142">
< squarebracket elementid="20051911w-sb6">
[  2 ]
< /squarebracket>
1998 p.38 .
< /sentence>
< sentence type="heading" elementid="20051911w-s143">
< a type="href" elementid="20051911w-a9">
back
< /a>
< /sentence>
< /p>
< p elementid="20051911w-p96">
< sentence type="normal" elementid="20051911w-s144">
< squarebracket elementid="20051911w-sb7">
[  3 ]
< /squarebracket>
Diwygiwyd adran 54 gan Orchymyn Archwilio Cyhoeddus
< roundbracket elementid="20051911w-rb102">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20051911w-rb103">
(  Llacio 'r Cyfyngiad ar Ddatgelu )
< /roundbracket>
2005
< roundbracket elementid="20051911w-rb104">
(  O.S. 2005/ 1018 )
< /roundbracket>
, sydd , o dan erthygl 1
< roundbracket elementid="20051911w-rb105">
(  2 )
< /roundbracket>
ohono , yn dod i rym yn syth cyn i 'r Gorchymyn hwn ddod i rym .
< /sentence>
< sentence type="heading" elementid="20051911w-s145">
< a type="href" elementid="20051911w-a10">
back
< /a>
< /sentence>
< /p>
< p elementid="20051911w-p97">
< sentence type="normal" elementid="20051911w-s146">
< squarebracket elementid="20051911w-sb8">
[  4 ]
< /squarebracket>
Mewnosodwyd adran 54A yn y Ddeddf gan Orchymyn Archwilio Cyhoeddus
< roundbracket elementid="20051911w-rb106">
(  Cymru )
< /roundbracket>
2004
< roundbracket elementid="20051911w-rb107">
(  Llacio 'r Cyfyngiad ar Ddatgelu )
< /roundbracket>
2005
< roundbracket elementid="20051911w-rb108">
(  O.S. 2005/ 1018 )
< /roundbracket>
, sydd , o dan erthygl 1
< roundbracket elementid="20051911w-rb109">
(  2 )
< /roundbracket>
ohono , yn dod i rym yn syth cyn i 'r Gorchymyn hwn ddod i rym .
< /sentence>
< sentence type="heading" elementid="20051911w-s147">
< a type="href" elementid="20051911w-a11">
back
< /a>
< /sentence>
< /p>
< p elementid="20051911w-p98">
< sentence type="heading" elementid="20051911w-s148">
< a type="href" elementid="20051911w-a12">
English version
< /a>
< /sentence>
< sentence type="heading" elementid="20051911w-s149">
ISBN 0 11 091180 6
< /sentence>
< /p>
< table elementid="20051911w-tb3">
< td elementid="20051911w-td81">
< p elementid="20051911w-p99">
< sentence type="heading" elementid="20051911w-s150">
< a type="href" elementid="20051911w-a13">
Other UK SIs
< /a>
< a type="href" elementid="20051911w-a14">
Home
< /a>
< a type="href" elementid="20051911w-a15">
National Assembly for Wales Statutory Instruments
< /a>
< a type="href" elementid="20051911w-a16">
Scottish Statutory Instruments
< /a>
< a type="href" elementid="20051911w-a17">
Statutory Rules of Northern Ireland
< /a>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td82">
< p elementid="20051911w-p100">
< sentence type="heading" elementid="20051911w-s151">
< strong elementid="20051911w-st10">
We welcome your 
< a type="href" elementid="20051911w-a18">
comments
< /a>
on this site
< /strong>
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td83">
< p elementid="20051911w-p101">
< sentence type="heading" elementid="20051911w-s152">
© Crown copyright 2005
< /sentence>
< /p>
< /td>
< td elementid="20051911w-td84">
< p elementid="20051911w-p102">
< sentence type="heading" elementid="20051911w-s153">
Prepared 19 July 2005
< /sentence>
< /p>
< /td>
< /table>
< /text>
< wordcount>1425< /wordcount>
< /file>
< /corpus>

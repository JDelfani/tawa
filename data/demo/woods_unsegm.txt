WhosewoodstheseareIthinkIknow
Hishouseisinthevillagethough
Hewillnotseemestoppinghere
Towatchthewoodsfillupwithsnow

Mylittlehorsemustthinkitqueer
Tostopwithoutafarmhousenear
Betweenthewoodsandfrozenlake
Thedarkesteveningoftheyear

Hegiveshisharnessbellsashake
Toaskifthereissomemistake
Theonlyothersound'sthesweep
Ofeasywindanddownyflake

Thesewoodsarelovelydarkanddeep
ButIhavepromisestokeep
AndmilestogobeforeIsleep
AndmilestogobeforeIsleep

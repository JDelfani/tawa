include ../../CONFIG

BIN = bin
OBJDIR = obj

all: $(BIN)/encode \
	 $(BIN)/decode \
	 $(BIN)/encode1 \
	 $(BIN)/decode1 \
	 $(BIN)/encodeq \
	 $(BIN)/decodeq \
	 $(BIN)/codelength \
	 $(BIN)/codelength1 \
	 $(BIN)/codelength2 \
	 $(BIN)/codelength3 \
	 $(BIN)/codelength4 \
	 $(BIN)/codelength_server \
	 $(BIN)/probs \
	 $(BIN)/encode_word \
	 $(BIN)/decode_word

# For general executables
$(BIN)/%: $(OBJDIR)/%.o word_utils.o
	mkdir -p $(OBJDIR) $(BIN)
	$(CC) $(CFLAGS) -o $@ $^ $(LIBS)

# For C files
$(OBJDIR)/%.o: %.c
	@mkdir -p $(shell dirname $@)
	$(CC) $(CFLAGS) $(@:%.o) -o $@ -c $< $(LIBS)

word_utils:	word_utils.c
	$(CC) $(CFLAGS) -c word_utils.c

codelength_utils:	codelength_utils.c
	$(CC) $(CFLAGS) -c codelength_utils.c

word1_utils:	word1_utils.c
	$(CC) $(CFLAGS) -c word1_utils.c

encode_number:	encode_number.o
	$(CC) $(CFLAGS) -o encode_number encode_number.o $(LIBS)

decode_number:	decode_number.o
	$(CC) $(CFLAGS) -o decode_number decode_number.o $(LIBS)

encode_pt:	encode_pt.o
	$(CC) $(CFLAGS) -o encode_pt encode_pt.o $(LIBS)

decode_pt:	decode_pt.o
	$(CC) $(CFLAGS) -o decode_pt decode_pt.o $(LIBS)

encode_ppmo:	encode_ppmo.o
	$(CC) $(CFLAGS) -o encode_ppmo encode_ppmo.o $(LIBS)

decode_ppmo:	decode_ppmo.o
	$(CC) $(CFLAGS) -o decode_ppmo decode_ppmo.o $(LIBS)

encode_samm:	encode_samm.o
	$(CC) $(CFLAGS) -o encode_samm encode_samm.o $(LIBS)

decode_samm:	decode_samm.o
	$(CC) $(CFLAGS) -o decode_samm decode_samm.o $(LIBS)

encode_sss:	encode_sss.o
	$(CC) $(CFLAGS) -o encode_sss encode_sss.o $(LIBS)

decode_sss:	decode_sss.o
	$(CC) $(CFLAGS) -o decode_sss decode_sss.o $(LIBS)

encode_test:	encode_test.o
	$(CC) $(CFLAGS) -o encode_test encode_test.o $(LIBS)

encode_test1:	encode_test1.o
	$(CC) $(CFLAGS) -o encode_test1 encode_test1.o $(LIBS)

encode_EOL:	encode_EOL.o
	$(CC) $(CFLAGS) -o encode_EOL encode_EOL.o $(LIBS)

encode_EOL1:	encode_EOL1.o
	$(CC) $(CFLAGS) -o encode_EOL1 encode_EOL1.o $(LIBS)

wordd:	encode_word decode_word

wordc:
	rm encode_word decode_word

demo: $(BIN)/encode $(BIN)/decode $(BIN)/encode1 $(BIN)/decode1
	echo "Encoding the file encode.c using static English model..."
	$(BIN)/encode1 -m ../../models/english.model <encode.c >demo.test1.encoded
	$(BIN)/decode1 -m ../../models/english.model <demo.test1.encoded >demo.test1.decoded
	echo "...check for any differences (there shouldn't be any!):"
	diff encode.c demo.test1.decoded
	echo "\n\n\n"
	echo "Encoding the file encode.c using dynamic model..."
	$(BIN)/encode -a 256 -o 5 -i encode.c -o demo.test2.encoded
	$(BIN)/decode -a 256 -o 5 -i demo.test2.encoded -o demo.test2.decoded
	echo "...check for any differences (there shouldn't be any!):"
	diff encode.c demo.test2.decoded
	echo "\n\n\n"
	echo "Encoding the file junk.txt using static English model..."
	$(BIN)/encode1 -m ../../models/english.model <../../data/demo/junk.txt >demo.test3.encoded
	$(BIN)/decode1 -m ../../models/english.model <demo.test3.encoded >demo.test3.decoded
	echo "...check for any differences (there shouldn't be any!):"
	diff ../../data/demo/junk.txt demo.test3.decoded
	echo "\n\n\n"
	echo "Encoding the file junk.txt using dynamic model..."
	$(BIN)/encode -a 256 -o 5 -i ../../data/demo/junk.txt -o demo.test4.encoded
	$(BIN)/decode -a 256 -o 5 -i demo.test4.encoded -o demo.test4.decoded
	echo "...check for any differences (there shouldn't be any!):"
	diff ../../data/demo/junk.txt demo.test4.decoded

demo_bible: encode_word decode_word
	echo "\n\n\n"
	echo "Encoding the bible using dynamic order 5 character model..."
	./encode -a 256 -o 5 -i ../../data/bible/english.txt -o /dev/null -p 10000

demo_word: encode_word decode_word
	echo "\n\n\n"
	echo "Encoding the file jefferson.1 using dynamic word model..."
	./encode_word -1 1 -2 1 -3 4 -4 4 -p 1000 <../../data/misc/jefferson.1 >demo.test5.encoded
	./decode_word -1 1 -2 1 -3 4 -4 4 -p 1000 <demo.test5.encoded >demo.test5.decoded
	echo "...check for any differences (there shouldn't be any!):"
	diff ../../data/misc/jefferson.1 demo.test5.decoded
	echo "\n\n\n"

demo_word1: encode_word1 decode_word1
	echo "\n\n\n"
	echo "Encoding the file jefferson.1 using dynamic word1 model..."
	./encode_word1 -1 5 -2 2 -p 1000 <../../data/misc/jefferson.1 >demo.test5.encoded1
	./decode_word1 -1 5 -2 2 -p 1000 <demo.test5.encoded1 >demo.test5.decoded1
	echo "...check for any differences (there shouldn't be any!):"
	diff ../../data/misc/jefferson.1 demo.test5.decoded1
	echo "\n\n\n"

clean:
	rm -fr bin obj *.o encode decode encode1 decode1 encodeq decodeq encode_word decode_word encode_word1 decode_word1 encode_number decode_number encode_sss decode_sss probs codelength codelength1 codelength2 *~ demo.*


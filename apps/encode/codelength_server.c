/* Writes out the codelength for the input string specified by the -s argument
   for the model specified by the -m argument. */
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <unistd.h>

#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>

#if defined (SYSTEM_LINUX)
#include <getopt.h> /* for getopt on Linux systems */
#endif

#include "io.h"
#include "text.h"
#include "model.h"

#define MAX_ORDER 5       /* Maximum order of the model */

#define MAX_ALPHABET 256  /* Default max. alphabet size for all PPM models */

#define MAX_FILENAME 256  /* Maximum length of a filename string */
#define MAX_STRING 10000  /* Maximum length of a string */

int port = 15960;
int Debug_model = 0;      /* For dumping out the model */
int Debug_chars = 0;      /* For dumping out the codelength character by character */
int Display_entropy = 0;  /* For displaying cross-entropies and not codelengths */
unsigned int Model = 0;   /* The model being loaded */

boolean Encode_Numbers = FALSE;      /* TRUE if the input string is a list of numbers */

void
usage (void)
{
    fprintf (stderr,
	     "Usage: codelength [options] <input-text\n"
	     "\n"
	     "options:\n"
	     "  -N\tinput string is a sequence of unsigned numbers\n"
	     "  -d n\tdebug model=n\n"
	     "  -c\tprint out codelengths for each character=n\n"
	     "  -e\tcalculate cross-entropy and not codelength\n"
	     "  -m fn\tmodel filename=fn (required arugument)\n"
	     "  -r\tprint out arithmetic coding ranges\n"
	     "  -s str\tthe string being encoded (required argument)\n"
	);
    exit (2);
}

void
init_arguments (int argc, char *argv[])
{
    extern char *optarg;
    extern int optind;
    unsigned int model_found;
    int opt;

    /* get the argument options */

    model_found = 0;
    while ((opt = getopt (argc, argv, "Ncd:em:rp:")) != -1)
	switch (opt)
	{
	case 'N':
	    Encode_Numbers = TRUE;
	    break;
	case 'c':
	    Debug_chars = 1;
	    break;
	case 'd':
	    Debug_model = atoi (optarg);
	    break;
	case 'e':
	    Display_entropy = 1;
	    break;
    case 'p':
        port = atoi(optarg);
        assert(port > 1000);
        break;
	case 'm':
	    Model = TLM_read_model
	      (optarg, NULL, 
	       "Codelength1: can't open model file");
	    model_found = 1;
	    break;
	case 'r':
	    Debug.range = 1;
	    break;
	default:
	    usage ();
	    break;
	}
    if (!model_found)
    {
        fprintf (stderr, "\nFatal error: missing model\n\n");
        usage ();
    }
    for (; optind < argc; optind++)
	usage ();
}

int
getSymbol (char *text, unsigned int *pos, unsigned int *symbol)
/* Gets the next symbol from input stream TEXT and updates the position POS.
   Returns EOF if there are no more symbols or an error has occurred. */
{
    unsigned int sym;
	char temp_sym;
    unsigned int p;
    int result;

    sym = 0;
	temp_sym = 0;
    p = *pos;
    if (!Encode_Numbers)
      {
        sym = text [p];
	p++;
	if (sym == '\0')
	    result = EOF;
	else
	    result = 1;
      }
    else
      {
	/* Skip over leading spaces */
	while ((text [p] != '\0') && (text [p] == ' '))
	    p++;

	/* Scan the next number from the string */
	if (text [p] == '\0')
	    result = EOF;
	else
	    result = sscanf (text + p, "%c", &temp_sym);
	switch (result)
	  {
	  case 1: /* one number read successfully */
	    break;
	  case EOF: /* eof found */
	    break;
	  case 0:
	    fprintf (stderr, "Formatting error in file\n");
	    break;
	  default:
	    fprintf (stderr, "Unknown error (%i) reading file\n", result);
	    exit (1);
	  }
	/* Find where next space is */
	while ((text [p] != '\0') && (text [p] != ' '))
	    p++;
      }

	/*sym = sym & 255;*/
	sym = (char) temp_sym;
    *symbol = sym;
    *pos = p;

    if (result == EOF)
        *symbol = TXT_sentinel_symbol ();
    return (result);
}

float
codelengthText (unsigned int model, char *text)
/* Returns the codelength (in bits) for encoding the text using the PPM model. */
{
    unsigned int context, symbol, p;
    float codelen, codelength;

    context = TLM_create_context (model);
    TLM_set_context_operation (TLM_Get_Codelength);

    /* Insert the sentinel symbol at start of text to ensure first character
       is encoded using a sentinel symbol context rather than an order 0
       context */
    if (Debug.range)
        fprintf (stderr, "Coding ranges for the sentinel symbol (not included in overall total:\n");

    TLM_update_context (model, context, TXT_sentinel_symbol ());
    if (Debug.range)
        fprintf (stderr, "\n");

    codelength = 0.0;
    /* Now encode each symbol */
    p = 0;
    while (getSymbol (text, &p, &symbol) != EOF) /* encode each symbol */
    {
	TLM_update_context (model, context, symbol);
	codelen = TLM_Codelength;
	if (Debug_chars)
	  {
	    if (Encode_Numbers)
	      fprintf (stderr, "Codelength for number %d = %7.3f\n", symbol, codelen);
	    else
	      fprintf (stderr, "Codelength for character %d (%c) = %7.3f\n", symbol, symbol, codelen);
	  }
	codelength += codelen;
    }
    /* Now encode the sentinel symbol again to signify the end of the text */
    TLM_update_context (model, context, TXT_sentinel_symbol ());
    codelen = TLM_Codelength;
    if (Debug_chars)
        fprintf (stderr, "Codelength for sentinel symbol = %.3f\n", codelen);
    codelength += codelen;

    TLM_release_context (model, context);

    if (Display_entropy)
        return (codelength / p);
    else
        return (codelength);
}

int
main (int argc, char *argv[])
{
    float codelength;
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1, i;
    int addrlen = sizeof(address);
    char buffer[MAX_STRING] = {0};

    init_arguments (argc, argv);

    if (Debug_model > 4)
        TLM_dump_models (Stdout_File, NULL);

    if (TLM_numberof_models () < 1){
      usage();
    }


    printf("Opening server on port %d\n", port);

    /* Creating socket file descriptor */
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
       
    /* Forcefully attaching socket to the port 8080 */
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR,
                                                  &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( port );
       
    /* Forcefully attaching socket to the port 8080 */
    if (bind(server_fd, (struct sockaddr *)&address, 
                                 sizeof(address))<0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    if (listen(server_fd, 10) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    for(;;)
    {
        if ((new_socket = accept(server_fd, (struct sockaddr *)&address, 
                       (socklen_t*)&addrlen))<0)
        {
            perror("accept");
            exit(EXIT_FAILURE);
        }
        for(;;)
        {
            valread = read(new_socket, buffer, MAX_STRING-2);
            buffer[valread - 1] = 0;
            if(buffer[valread -2] == 13)
                buffer[valread - 2] = 0; 
            /* If we reached EOF or an error just continue */
            if(valread <= 0 || valread == EOF)
                break;

            codelength = codelengthText (Model, buffer);
            sprintf(buffer,"%f", codelength);
            send(new_socket, buffer, strlen(buffer), 0);
        }
        close(new_socket);
    }

    TLM_release_models ();

    exit (0);
}

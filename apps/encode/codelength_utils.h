/* Utility code for getting the codelength given a model. */

float
codelengthText (unsigned int model, char *text);
/* Returns the codelength (in bits) for encoding the text using the PPM model. */
